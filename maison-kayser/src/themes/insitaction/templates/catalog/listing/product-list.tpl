{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='content'}
  <section id="main">
  
      {if $listing.products|count}
        <div class="container">
          {block name='product_list_header'}
            <h2 class="h2">{$listing.label}</h2>
          {/block}

          <section id="products">

              <div id="">
                {block name='product_list_top'}
                  {include file='catalog/_partials/products-top.tpl' listing=$listing}
                {/block}
              </div>

              {block name='product_list_active_filters'}
                <div id="" class="hidden-sm-down">
                  {$listing.rendered_active_filters nofilter}
                </div>
              {/block}

              <div id="">
                {block name='product_list'}
                  {include file='catalog/_partials/products.tpl' listing=$listing}
                {/block}
              </div>

              <div id="js-product-list-bottom">
                {block name='product_list_bottom'}
                  {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
                {/block}
              </div>
          </section>


        {block name="products_bottom"}
          {if isset($cross_products) && $cross_products 
            && isset($cross_products.products) && count($cross_products.products)}
            <section id="a_decouvrir" class="featured-products clearfix container">
              <div class="row">
                <h2 class="h2 featured-products__title">
                  {if $cross_products.title}
                    {$cross_products.title}
                  {else}
                    {l s='A découvrir aussi' d='Shop.Theme.Catalog'}
                  {/if}
                </h2>
              </div>

              <div class="row">
                <div class="products">
                  <div class="swiper-container swiper--reassurance-hp">
                    <div class="swiper-wrapper">
                      {foreach from=$cross_products.products item="product"}
                        <div class="swiper-slide">
                          {include file="catalog/_partials/miniatures/product.tpl" product=$product}
                        </div>
                      {/foreach}
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                  </div>
                </div>
              </div>
              <a class="all-product-link float-xs-left float-md-right h4" href="{$category_link}">
                {l s='Les produits' d='Shop.Theme.Catalog'}<i class="material-icons">&#xE315;</i>
              </a> 
            </section>
            {/if}
          {/block}
        </div>
      {else}

        {include file='errors/not-found.tpl'}

      {/if}

  </section>
{/block}
