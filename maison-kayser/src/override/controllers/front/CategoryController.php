<?php

use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;

class CategoryController extends CategoryControllerCore
{

    /**
     * Initializes controller.
     *
     * @see FrontController::init()
     *
     * @throws PrestaShopException
     */
    public function init()
    {
        parent::init();

        $cross_products = false;
        $category_link = false;
        if($this->category->cross_category_id) {

            $category = new Category((int) $this->category->cross_category_id);

            $searchProvider = new CategoryProductSearchProvider(
                $this->context->getTranslator(),
                $category
            );

            $context = new ProductSearchContext($this->context);

            $query = new ProductSearchQuery();

            $nProducts = Configuration::get('HOME_FEATURED_NBR');
            if ($nProducts < 0) {
                $nProducts = 12;
            }

            $query
                ->setResultsPerPage($nProducts)
                ->setPage(1)
            ;

            if (Configuration::get('HOME_FEATURED_RANDOMIZE')) {
                $query->setSortOrder(SortOrder::random());
            } else {
                $query->setSortOrder(new SortOrder('product', 'position', 'asc'));
            }

            $result = $searchProvider->runQuery(
                $context,
                $query
            );

            $assembler = new ProductAssembler($this->context);

            $presenterFactory = new ProductPresenterFactory($this->context);
            $presentationSettings = $presenterFactory->getPresentationSettings();
            $presenter = new ProductListingPresenter(
                new ImageRetriever(
                    $this->context->link
                ),
                $this->context->link,
                new PriceFormatter(),
                new ProductColorsRetriever(),
                $this->context->getTranslator()
            );

            $products_for_template = [];

            foreach ($result->getProducts() as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
            }

            $title = ($this->category->cross_category_title[$this->context->language->id] ? $this->category->cross_category_title: false);

            $cross_products = array(
                'title' => $title,
                'products' => $products_for_template
            );

            $category_link = $this->context->link->getCategoryLink($this->category->cross_category_id);

        }

        $this->context->smarty->assign(array(
            'cross_products' => $cross_products,
            'category_link' => $category_link
        ));
    }

    public function getCanonicalURL()
    {
         // category exists in MKI ?
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'category_shop` cs
                LEFT JOIN `'._DB_PREFIX_.'category` c ON c.id_category = cs.id_category
                WHERE c.active = 1
                AND cs.id_shop = 1
                AND cs.id_category = '.$this->category->id;

        $exists = DB::getInstance()->getRow($sql);
        if($exists) {
            return  $this->context->link->getCategoryLink(
                        $this->category->id,
                        null,
                        $this->context->language->id,
                        null,
                        1
            );
        }
        return $this->context->link->getCategoryLink($this->category);
    }
}
