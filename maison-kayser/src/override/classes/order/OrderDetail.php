<?php

class OrderDetail extends OrderDetailCore {
    // on conserve les infos de la formule dans l'order_detail avent de l'eclater
    public $formule_info;
    public $formule_product_index;

     public static $definition = array(
        'table' => 'order_detail',
        'primary' => 'id_order_detail',
        'fields' => array(
            'id_order' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_order_invoice' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_warehouse' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_shop' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'product_id' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'product_attribute_id' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_customization' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'product_name' =>                array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'product_quantity' =>            array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'product_quantity_in_stock' =>    array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'product_quantity_return' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'product_quantity_refunded' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'product_quantity_reinjected' =>array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'product_price' =>                array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'reduction_percent' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'reduction_amount' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'reduction_amount_tax_incl' =>  array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'reduction_amount_tax_excl' =>  array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'group_reduction' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'product_quantity_discount' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'product_ean13' =>                array('type' => self::TYPE_STRING, 'validate' => 'isEan13'),
            'product_isbn' =>                array('type' => self::TYPE_STRING, 'validate' => 'isIsbn'),
            'product_upc' =>                array('type' => self::TYPE_STRING, 'validate' => 'isUpc'),
            'product_reference' =>            array('type' => self::TYPE_STRING, 'validate' => 'isReference'),
            'product_supplier_reference' => array('type' => self::TYPE_STRING, 'validate' => 'isReference'),
            'product_weight' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'tax_name' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'tax_rate' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'tax_computation_method' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_tax_rules_group' =>        array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'ecotax' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'ecotax_tax_rate' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'discount_quantity_applied' =>    array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'download_hash' =>                array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'download_nb' =>                array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'download_deadline' =>            array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'unit_price_tax_incl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'unit_price_tax_excl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_price_tax_incl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_price_tax_excl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_shipping_price_tax_excl' => array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_shipping_price_tax_incl' => array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'purchase_supplier_price' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'original_product_price' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'original_wholesale_price' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            // ADD BY ESTEBANW INSITACTION 14052018
            // Ajout des infos de la formule en json   
            'formule_info' => array('type' => self::TYPE_STRING),
            // index du produit formule : (id_product)_(id_product_attribute)
            'formule_product_index' =>  array('type' => self::TYPE_STRING)

        ),
    );
    
    public function createList(Order $order, Cart $cart, $id_order_state, $product_list, $id_order_invoice = 0, $use_taxes = true, $id_warehouse = 0)
    {
        $this->vat_address = new Address((int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
        $this->customer = new Customer((int)$order->id_customer);

        $this->id_order = $order->id;
        $this->outOfStock = false;
        $order_total_products = $order_total_products_wt = 0;
        foreach ($product_list as $product) {
            $retour = $this->create($order, $cart, $product, $id_order_state, $id_order_invoice, $use_taxes, $id_warehouse);
            //si le retour est un tableau alors c'est le cas particulier des formules
            //on cumule le total produit
            if(is_array($retour) && isset($retour['order_total'])) {
                $order_total_products += $retour['order_total'];
                $order_total_products_wt += $retour['order_total_wt'];
            }
        }

        if($order_total_products > 0) {   
            $order->total_paid_tax_excl = $order_total_products;
            $order->total_products = $order_total_products;
            $order->save();            
        }

        unset($this->vat_address);
        unset($products);
        unset($this->customer);
    }

    /**
     * [create description]
     * ESTEBANW INSITACTION 15052018
     * OVERRIDE QUI PERMET DE SCINDER LES PRODUITS D'UNE FORMULE AFIN D'EN RECUPERER LA TVA
     * principe : 
     * prix de la formule = X
     * total prix produits qui compose la formule = Y
     * X/Y = RATIO à appliquer aux calculs de TVA pour obtenir la part proportionnelle
     *
     * La génération de la facture utilise le total de l'order
     * Une table order_detail_tax recupére la taxe (uniquement) pour le calcul de la facture
     *
     * La ligne d'origine devant être générée par prestashop 
     * est stockée dans 'formule_info' en json pour l'affichage en front
     *
     * /!\ Aucune gestion des prix transporteur car les formules ne sont dispo qu'en clickandcollect
     */
    protected function create(Order $order, Cart $cart, $product, $id_order_state, $id_order_invoice, $use_taxes = true, $id_warehouse = 0)
    {      
        // recupération de la catégorie du produit
        $current_prod = new Product((int)$product['id_product']);
        // La catégorie 58 est la catégorie des formules
        if($current_prod->id_category_default == 58) {


            // TRAITEMENT DE LA FORMULE ON CONSERVE LES INFOS ORDER DETAILS
            $formule_details = new OrderDetail();

            // init de createList - ici on crée une liste d'order detail
            $formule_details->vat_address = new Address((int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
            $formule_details->customer = new Customer((int)$order->id_customer);

            $formule_details->id_order = $order->id;
            $formule_details->outOfStock = false;

            if ($use_taxes) {
                $formule_details->tax_calculator = new TaxCalculator();
            }
            $formule_details->id = null;
            $formule_details->product_id = (int)$product['id_product'];
            $formule_details->product_attribute_id = $product['id_product_attribute'] ? (int)$product['id_product_attribute'] : 0;
            $formule_details->id_customization = $product['id_customization'] ? (int)$product['id_customization'] : 0;
            $formule_details->product_name = $product['name'].
                ((isset($product['attributes']) && $product['attributes'] != null) ?
                    ' - '.$product['attributes'] : '');

            $formule_details->product_quantity = (int)$product['cart_quantity'];
            $formule_details->product_ean13 = empty($product['ean13']) ? null : pSQL($product['ean13']);
            $formule_details->product_isbn = empty($product['isbn']) ? null : pSQL($product['isbn']);
            $formule_details->product_upc = empty($product['upc']) ? null : pSQL($product['upc']);
            $formule_details->product_reference = empty($product['reference']) ? null : pSQL($product['reference']);
            $formule_details->product_supplier_reference = empty($product['supplier_reference']) ? null : pSQL($product['supplier_reference']);
            $formule_details->product_weight = $product['id_product_attribute'] ? (float)$product['weight_attribute'] : (float)$product['weight'];
            $formule_details->id_warehouse = $id_warehouse;

            $product_quantity = (int)Product::getQuantity($formule_details->product_id, $formule_details->product_attribute_id);
            $formule_details->product_quantity_in_stock = ($product_quantity - (int)$product['cart_quantity'] < 0) ?
                $product_quantity : (int)$product['cart_quantity'];

            $formule_details->setVirtualProductInformation($product);
            $formule_details->checkProductStock($product, $id_order_state);

            if ($use_taxes) {
                $formule_details->setProductTax($order, $product);
            } 

            $formule_details->setShippingCost($order, $product);
            $formule_details->setDetailProductPrice($order, $cart, $product);

            // Set order invoice id
            $formule_details->id_order_invoice = (int)$id_order_invoice;

            // Set shop id
            $formule_details->id_shop = (int)$product['id_shop'];

            // On conserve l'order detail initialisé au cas ou un des produits n'est pas trouvable
            // la seule raison et que l'ID PRODUIT n'ait pas été renseigné lors de la saisie d'attribut
            // on log donc si erreur
            $formule_details_json = json_encode($formule_details);

            //recuperation des differents attributs
            $attributes = Product::getAttributesParams($product['id_product'], $product['id_product_attribute']);
            // recupération des produits composant la formule
            $formule_products = array();
            $unit_price_formule_wt = $formule_details->unit_price_tax_incl;
            $no_way = false;
            $context = Context::getContext();

            // recupération du prix total de la formule avec taxes
            $total_produits_formules_wt = 0;
            $total_produits_formules_ht = 0;
            $index = 0;
            foreach ($attributes as $key => $attribute) {
                // si un produit n'a pas d'id produit, il est impossible d'effectuer le calcul      
                if($attribute['id_product'] && is_numeric($attribute['id_product'])) { 
                    
                    $formule_product = get_object_vars (new Product(
                            $attribute['id_product'], 
                            true, 
                            $context->language->id,
                            $product['id_shop']
                        )
                    );

                    // ajout des données indispensables a la création d'un order detail
                    $formule_product['cart_quantity'] = $product['cart_quantity'];
                    $formule_product['id_product'] = $formule_product['id'];
                    $formule_product['id_product_attribute'] = $formule_product['cache_default_attribute'];
                    $formule_product['price_wt'] = (1 + ($formule_product['tax_rate']/100)) * $formule_product['price'];
                    $formule_product['total_wt'] = $formule_product['cart_quantity'] * $formule_product['price_wt'];
                    $formule_product['total'] = $formule_product['cart_quantity'] * $formule_product['price'];
                
                    $total_produits_formules_wt += $formule_product['total_wt'];

                    $formule_products[] = $formule_product;

                } else {   
                    $message = 'Formule '.$product['name'].' Order#'.$order->id.' Impossible de trouver l\'id produit : '.print_r($attribute, true);

                    $logger = new FileLogger(); 
                    $logger->setFilename(_PS_ROOT_DIR_.'/log/formules.log');
                    $logger->log($message, 3);

                    $no_way = true;
                    break;
                }
            }            

            $ratio_tax = $total_produits_formules_wt / $product['total_wt'];

            // on a vérifié que tout est bon on peut inserer les produits qui composent la formule et appliquer les ratio de tva
            if(!$no_way) {

                $splited_vat_address = new Address((int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
                $splited_customer = new Customer((int)$order->id_customer);
                // INIT DES VARIABLES POUR METTRE AJOUR LES PRIX DE LA COMMANDE
                // QUI SERONT UTILISé POUR LA FACTURATION
                $order_total_products = $order_total_products_wt = 0;

                foreach ($formule_products as $index => $formule_product) {
                    // mise a jour des prix par rapport au ratio tax                    
                    $formule_product['price'] = Tools::ps_round($formule_product['price'] / $ratio_tax, 6);
                    $formule_product['price_wt'] = Tools::ps_round($formule_product['price'] * (1 + ($formule_product['tax_rate']/100)), 6);
                    $formule_product['total'] =  Tools::ps_round($formule_product['price'] * $formule_product['cart_quantity'], 6);
                    $formule_product['total_wt'] = Tools::ps_round($formule_product['total'] * (1 + ($formule_product['tax_rate']/100)), 6);

                    // UTILISE POUR METTRE A JOUR LE TOTAL DE LA COMMANDE HT
                    $order_total_products += $formule_product['total'];
                    $order_total_products_wt += $formule_product['total_wt'];

                    $splited_order_detail = new OrderDetail();

                    // init de createList - ici on crée une liste d'order detail
                    $splited_order_detail->vat_address = $splited_vat_address;
                    $splited_order_detail->customer = $splited_customer;

                    $splited_order_detail->id_order = $order->id;
                    $splited_order_detail->outOfStock = false;

                    if ($use_taxes) {
                        $splited_order_detail->tax_calculator = new TaxCalculator();
                    }

                    $splited_order_detail->id = null;

                    $splited_order_detail->product_id = $formule_product['id_product'];
                    $splited_order_detail->product_attribute_id = $formule_product['id_product_attribute'];
                    $splited_order_detail->id_customization = $product['id_customization'] ? (int)$product['id_customization'] : 0;
                    $splited_order_detail->product_name = $formule_product['name'];

                    $splited_order_detail->product_quantity = (int)$product['cart_quantity'];
                    $splited_order_detail->product_ean13 = empty($product['ean13']) ? null : pSQL($product['ean13']);
                    $splited_order_detail->product_isbn = empty($product['isbn']) ? null : pSQL($product['isbn']);
                    $splited_order_detail->product_upc = empty($product['upc']) ? null : pSQL($product['upc']);
                    $splited_order_detail->product_reference = empty($formule_product['reference']) ? null : pSQL($product['reference']);
                    $splited_order_detail->product_supplier_reference = empty($formule_product['supplier_reference']) ? null : pSQL($formule_product['supplier_reference']);
                    $splited_order_detail->product_weight = $product['id_product_attribute'] ? (float)$product['weight_attribute'] : (float)$product['weight'];
                    $splited_order_detail->id_warehouse = $id_warehouse;

                    $product_quantity = (int)Product::getQuantity($splited_order_detail->product_id, $splited_order_detail->product_attribute_id);
                    $splited_order_detail->product_quantity_in_stock = ($product_quantity - (int)$product['cart_quantity'] < 0) ?
                        $product_quantity : (int)$product['cart_quantity'];

                    $splited_order_detail->setVirtualProductInformation($formule_product);
                    $splited_order_detail->checkProductStock($product, $id_order_state);

                    if ($use_taxes) {
                        $splited_order_detail->setProductTax($order, $formule_product);
                    }
                    $splited_order_detail->setShippingCost($order, $formule_product);
                    $splited_order_detail->setDetailProductPrice($order, $cart, $formule_product);

                    // ecrase la valeur 
                    $splited_order_detail->product_price = Tools::ps_round($formule_product['price'], 6);

                    // Set order invoice id
                    $splited_order_detail->id_order_invoice = (int)$id_order_invoice;

                    // Set shop id
                    $splited_order_detail->id_shop = (int)$product['id_shop'];

                    // AJOUT DES CHAMPS SPECIFIQUES POUR RECUPERER LA LIGNE D'ORIGINE EN FRONT
                    $splited_order_detail->formule_info = $formule_details_json;
                    $splited_order_detail->formule_product_index = $product['id_product'].'_'.$product['id_product_attribute'];
                    // Add new entry to the table
                    $splited_order_detail->save();

                    if ($use_taxes) {
                        //SELECTION DE L'ID TAX PAR RAPPORT A L'ADRESSE ET AU PRODUIT
                        $sql = 'SELECT `id_tax` FROM `'._DB_PREFIX_.'tax_rule` WHERE `id_tax_rules_group` = '.$formule_product['id_tax_rules_group'].' AND `id_country` = '.$splited_order_detail->vat_address->id_country;
                        $id_tax = DB::getInstance()->getValue($sql);

                        if($id_tax && is_numeric($id_tax)) {
                            $values = '';
                            // insertion a la mano des order detail tax
                            $unit_amount = $formule_product['price_wt'] - $formule_product['price'];
                            $total_amount = $formule_product['total_wt'] - $formule_product['total'];

                            $values .= '('.(int)$splited_order_detail->id.','.(int)$id_tax.','.(float)$unit_amount.','.(float)$total_amount.'),';
                            $values = rtrim($values, ',');
                            $sql = 'INSERT INTO `'._DB_PREFIX_.'order_detail_tax` (id_order_detail, id_tax, unit_amount, total_amount) VALUES '.$values;

                            Db::getInstance()->execute($sql);                            
                        }
                    }
                    unset($splited_order_detail->tax_calculator);
                }
                // mise a jour des totaux de la commandea effectuer
                // sinon le calcul est faussé dans la facture
                // prestashop utilise l'order product pour les calculs
                return array(
                    'order_total' => $order_total_products, 
                    'order_total_wt' => $order_total_products_wt
                ); 

            } else {
                // insertion de l'order détail par défaut
                // car un produit de la formule n'a pas d'id produit
                // Add new entry to the table
                $formule_details->save();

                if ($use_taxes) {
                    $formule_details->saveTaxCalculator($order);
                }
                unset($formule_details->tax_calculator);
            }

        } else {
            parent::create($order, $cart, $product, $id_order_state, $id_order_invoice, $use_taxes, $id_warehouse);
        }
        
    }
}