<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

class BeVisibleGroup extends ObjectModel
{ 
    public $id_bevisible_group;     
 
    public $name; 

    public $france;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'bevisible_group',
        'primary' => 'id_bevisible_group',
        'multilang' => true, 
        'fields' => array( 
            'name' =>           array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 128),  
            'france' =>       array('type' => self::TYPE_INT),
        ), 
    );  

    public function addStores($stores_list = array()) {
        // delete les association existantes pour ce groupe
        $sql = 'DELETE FROM `'._DB_PREFIX_.'bevisible_group_store` 
                WHERE  `id_bevisible_group` = '.$this->id_bevisible_group;
        DB::getInstance()->execute($sql);
        if(is_array($stores_list) && $stores_list) {
            $values = '';
            foreach ($stores_list as $key => $store_id) {
                $values .= '('.$this->id_bevisible_group.', '.$store_id.'),'; 
            }

            $sql = 'INSERT INTO `'._DB_PREFIX_.'bevisible_group_store` (`id_bevisible_group`, `id_store`)
                    VALUES  '.substr($values, 0, -1); 
            return DB::getInstance()->execute($sql);
        }
        return true;
    }

    public static function getGroups($id_lang = null, $france = null, $select = false, $sql = false) { 
        if(is_null($id_lang))
            $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        if($sql)
            return DB::getInstance()->executeS(
                'SELECT * FROM `'._DB_PREFIX_.'bevisible_group` g
                LEFT JOIN `'._DB_PREFIX_.'bevisible_group_lang` gl ON gl.`id_bevisible_group` = g.`id_bevisible_group`
                WHERE gl.`id_lang` = '.$id_lang.'
                '.(!is_null($france) ? ' AND g.`france` = '.($france ? 1 : 0) : '')
            );
        $collection_group_zones = new PrestaShopCollection('BeVisibleGroup');  
        $group_zones = $collection_group_zones->where('l.id_lang', '=', $id_lang)->orderBy('l.name')->getResults();  
 
        if(!$select)
            return $group_zones;
        // parse pour affichage d'un select
        $result = array();
        if ($group_zones) {
            $context = Context::getContext();
            if(!is_null($context) && !is_null($context->language) && !is_null($context->language->id_lang)) {
                 $id_lang = $context->language->id_lang;
            } else {
                 $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
            }
            foreach ($group_zones as $group) {
                $tmp = array();
                $tmp['id_bevisible_group'] = $group->id_bevisible_group;
                $tmp['val'] = $group->id_bevisible_group;
                $tmp['name'] = $group->name[$id_lang]; 
                $result[$group->id_bevisible_group] = $tmp;
            } 
        } 
        return $result;    
    }
    public static function getGroupsInFrance($id_lang = null){
        $groups = self::getGroups($id_lang, true, false, true); 
        return $groups;

    }
    /**
     * [getGroupsWithCountries description]
     * @param  [type]  $id_lang      [description]
     * @param  boolean $id_countries [description]
     * @return [type]                [all the groups with all the countries includes]
     */
    public static function getGroupsWithCountries($id_lang = null, $id_countries = false){
        $groups = self::getGroups($id_lang, false, false, true);
        foreach ($groups as $key => &$group) {
             $countries = self::getCountriesByGroupID($group['id_bevisible_group'], $id_lang, $id_countries);
             $group['countries'] = $countries;
        }
        return $groups;

    }

    /**
     * [getCountriesByGroupID recupere les pays associés au groupe]
     * @param  [type]  $id_group     [description]
     * @param  [type]  $id_lang      [description]
     * @param  boolean $id_countries [liste d'id_country autorisé, si false ignoré]
     * @return [type]                [description]
     */
    public static function getCountriesByGroupID($id_group = null, $id_lang = null, $id_countries = false) {
        if(is_null($id_lang))
            $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $stores = self::getStoresByGroupID($id_group);
        $countries = array();
        if($stores && count($stores)) {
            foreach ($stores as $key => $store) {
                if(!$id_countries || (is_array($id_countries) && in_array($store['id_country'], $id_countries))) 
                    $countries[] = $store['id_country'];
            }
            //recupération des pays ordonnés par nom
            $collection_countries = new PrestaShopCollection('Country');  
            $countries = $collection_countries->orderBy('l.name')
                                            ->where('id_country', 'IN', $countries)
                                            ->where('l.id_lang', '=', $id_lang)
                                            ->getResults();   
        }
        return $countries;
    }

    public static function getStoresByGroupID($id_group = null, $id_country = null, $list = false, $object = false) { 

        if($id_group && is_numeric($id_group)) { 
            $stores = DB::getInstance()->executeS(
                    'SELECT * FROM `'._DB_PREFIX_.'bevisible_group_store` gs
                    LEFT JOIN  `'._DB_PREFIX_.'store` s  ON s.`id_store` = gs.`id_store`
                    LEFT JOIN  `'._DB_PREFIX_.'bevisible_group` g  ON gs.`id_bevisible_group` = g.`id_bevisible_group` 
                    WHERE gs.`id_bevisible_group` = '.$id_group.'
                    '.($id_country ? ' AND s.`id_country` = '.$id_country : '').'
                    ORDER BY s.`name` '
                ); 
           
            if(!$list) {
                if($object && !empty($stores)) {
                    $results = array(); 
                    foreach ($stores as $key => $value) { 
                        $store = new Store($value['id_store']);
                        if(Validate::isLoadedObject($store)) {
                            $results[$value['id_store']] = $store;
                        }
                    }
                    $stores = $results;
                }
                return $stores;
            }
            // parse pour affichage d'une checkbox
            $result = array();
            if ($stores) {  
                foreach ($stores as $store) { 
                    $result[] = $store['id_store'];
                } 
            } 
            return $result;  
        }
        return false;
    }
}
