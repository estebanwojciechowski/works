<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Class d'import produit initial // pas de maj détruit toute les données si le test du csv est bon
 */
class AddStockProducts {
  

    protected $default_id_lang;

    private $_logger;

    protected $errors = array();


    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {
      $sql = 'TRUNCATE `'._DB_PREFIX_.'stock_available` ';
      $results = DB::getInstance()->execute($sql);

      $sql = 'SELECT p.`id_product`, pa.`id_product_attribute` FROM `'._DB_PREFIX_.'product` p
      LEFT JOIN  `'._DB_PREFIX_.'product_attribute` pa ON pa.`id_product` = p.`id_product`
      ORDER BY p.`id_product`';
      $results = DB::getInstance()->executeS($sql); 

      $shops = Shop::getShops();
      if(is_array($results) && $results) {
        $this->log(count($results).' Produits trouvés');

        $attributes_not_empty = array();
        foreach ($results as $key => $result) {
          $id_product = (int) $result['id_product'];
          $id_product_attribute = (int) $result['id_product_attribute'];

          $sql = 'INSERT INTO `'._DB_PREFIX_.'stock_available`
                  (id_stock_available, id_product , id_product_attribute, id_shop, id_shop_group, quantity,
                    physical_quantity, reserved_quantity, depends_on_stock, out_of_stock)
                  VALUES ';
          foreach ($shops as $key => $shop) {
            $sql .= ' ( NULL, '.$id_product.', '.$id_product_attribute.', '.$shop['id_shop'].', 0, 0, 0, 0, 0, 2),';
            if(!isset($attributes_not_empty[$id_product]) && $id_product_attribute > 0) {
              $sql .= ' ( NULL, '.$id_product.', 0, '.$shop['id_shop'].', 0, 0, 0, 0, 0, 2),';
            }
          }
          //ajout de l'id produit pour ne pas ajouter une ligne id_product_attr a 0 pour la prochaine decli
          if(!isset($attributes_not_empty[$id_product]) && $id_product_attribute > 0) {
            $attributes_not_empty[$id_product] = $id_product;
          }
          
          $sql = substr($sql, 0, -1); 
          if(DB::getInstance()->execute($sql)) {
            $this->log('Stock ajouté '.$id_product.' '.$id_product_attribute);
          } else {
            $this->log('ERREUR Stock non ajouté '.$id_product.' '.$id_product_attribute);
          }
        }
          
        
      }
    }

}

$timestamp_debut = microtime(true);

$import = new AddStockProducts();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$import->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");
  