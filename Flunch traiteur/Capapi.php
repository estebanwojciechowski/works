<?php
/**
 * API avec CapAdresse pour l'implémentation des adresses postale
 * @author Insitaction
 */
class Insita_CapAdresse_Model_Capapi extends Mage_Core_Model_Abstract
{
	/* Configuration de test */
	protected $urlCapPhoneWSTest	= 'https://dev.capadresse.com:8015/';
	protected $urlApiAdressTest		= 'https://dev.capadresse.com:8014/';
	protected $urlApiMailTest		= 'https://dev.capadresse.com:8016/';

	/* Configuration de prod */
	protected $urlCapPhoneWS	= 'http://ws.capadresse.com:8015/';
	protected $urlApiAdress		= 'http://ws.capadresse.com:8017/';
	protected $urlApiMail		= 'http://ws.capadresse.com:8016/';

	// Gravitee Api Key - Différent pour chaque site Flunch
    private $gravitee_api_key_name;
	private $gravitee_api_key;

	private static $client;

	private $headers = array();
	private $auth_key = 'QUdBUEVTOlU1UU13NWVkYUZw';
	private static $type_client = 'curl';

	public $validPhone = false, $validMail = false, $validAdress = false;
	public $errors = array();
	public $message_code = false;
	public $propositions = array();

	/**
	 * Connexion à l'API
	 * @param type $type
	 */
	public function connect($type) {
		if(self::$type_client == 'curl') {
			$this->init(true);
		} else {

			$this->urlApiAdress = Mage::getStoreConfig('capadresse_options/general/url_webservice_address');
			$this->urlCapPhoneWS = Mage::getStoreConfig('capadresse_options/general/url_webservice_phone');
			$this->urlApiMail = Mage::getStoreConfig('capadresse_options/general/url_webservice_mail');
			$this->gravitee_api_key_name = Mage::getStoreConfig('capadresse_options/general/gravitee_key_name');
	        $this->gravitee_api_key = Mage::getStoreConfig('capadresse_options/general/gravitee_key');

			$headerGraviteeApi = $this->gravitee_api_key_name .':'. $this->gravitee_api_key;

			if (is_null(self::$client)) {
				try {
					self::$client = new SoapClient($this->$type .'?wsdl', array(
						'location' => $this->$type,
						'stream_context' => stream_context_create(array(
								'http' => array(
									'header' => $headerGraviteeApi
								),
							)),
						)
					);
				} catch (SoapFault $fault) {
					Mage::log('Construct Error : ' . 'Code: ' . $fault->getCode() . ' Message:' . $fault->getMessage() . "\n", null, 'capAdresse.log');
				}
			}
		}
	}

	/**
	 * Fonction de vérification du téléphone portable
	 * @param type $tel
	 * @param type $iso
	 */
	public function verifPhone($params) {

		$return = $this->isPhoneValid($params);

		$messages = '';
		if(!$this->validPhone) {
			if(isset($this->errors['phone']) && $this->errors['phone']) {
				$messages = $this->errors['phone'];
			} else {
				$messages = 'Numéro de téléphone invalide.';
			}
		}

		$propositions = array();
		if($this->propositions && count($this->propositions)) {
			foreach ($this->propositions as $key => $proposition) {
				$propositions[] = array('sTel' => $proposition['sTel']);
			}
		}

		return json_encode(array('isValid' => $this->validPhone, 'propositions' => $propositions, 'messages' => trim($messages)), true);
	}

	/**
	 *
	 * @param type $cp
	 */
	public function verifCP($params) {

		$return = $this->isAddressValid($params, true);
		$messages = '';
		if ($return === false) {
			if(isset($this->errors['address'])) {
				$messages = $this->errors['address'];
			} else {
				$messages = 'Code postal invalide.';
			}
		}
		$propositions = array();
		if($this->propositions && count($this->propositions)) {
			foreach ($this->propositions as $key => $prop) {
				if(strtolower($prop['step']) != 'forced')
					$propositions[] = array(
						'iType' 	=> $prop['localityFlags'],
						'sCp'		=> $prop['postalCode'],
						'sInsee'	=> $prop['localityId'],
						'sLoc'		=> $prop['locality'],
						'sLieuDit'	=> isset($prop['localitySynonym']) ? $prop['localitySynonym'] : '',
						'qualityCode' => isset($prop['qualityCode']) ? $prop['qualityCode'] : ''
						);
			}
		}
		return json_encode(array('isValid' => $this->validAdress, 'propositions' => $propositions, 'messages' => trim($messages)));

	}

	/**
	 * Fonction pour la vérification de la voie
	 * @param type $voie
	 * @param type $sInsee
	 * @param type $iType
	 */
	public function verifVoie($voie, $sInsee, $iType){

        $paramsCP = array(
        	'inputOutput'		=> $voie,
            'countryCode'       => 'FRA',
            'languageCode'      => 'fr',
            'localityId' 		=> $sInsee,
            'streetName'        => $voie,                   // Nom de la rue
            'step'              => 'SearchStreet'
        );

		$return = $this->isAddressValid($paramsCP, true);
		if ($return === false) {
			if(isset($this->errors['address'])) {
				$messages = $this->errors['address'];
			} else {
				$messages = 'Voie invalide.';
			}
		}
		$propositions = array();
		if($this->propositions && count($this->propositions)) {
			foreach ($this->propositions as $key => $prop) {
				if(strtolower($prop['step']) != 'forced')
					$propositions[] = array(
						'sMat' 				=> $prop['streetId'],
						'sL4' 				=> $prop['streetName'],
						'sOldL4' 			=>  isset($prop['streetSynonym ']) ? $prop['streetSynonym '] : '',
						'streetFlags' 		=> $prop['streetFlags'],
						'sNum' 				=> $prop['streetNumber'] .(isset($prop['streetNumberExt']) ? ' '.$prop['streetNumberExt'] : ''),
						'iType' 			=> $prop['localityFlags'],
						'sCp'				=> $prop['postalCode'],
						'sInsee'			=> $prop['localityId'],
						'sLoc'				=> $prop['locality'],
						'sLieuDit'			=> isset($prop['localitySynonym']) ? $prop['localitySynonym'] : '',
						'qualityCode' 		=> isset($prop['qualityCode']) ? $prop['qualityCode'] : ''
					);
			}
		}

		return json_encode(array('isValid' => $this->validAdress, 'propositions' => $propositions, 'messages' => trim($messages)));
	}

	/**
	 *
	 * @param type $value
	 * @param type $mat
	 * @param type $insee
	 * @param type $type
	 */
	public function verifNum($value, $mat, $insee, $type) {
		 $paramsCP = array(
        	'inputOutput'		=> $value,
            'countryCode'       => 'FRA',
            'languageCode'      => 'fr',
            'localityId' 		=> $insee,
            'streetId' 			=> $mat,
            'localityFlags' 	=> $type,
            'step'              => 'SearchStreetNumber'
        );

		$return = $this->isAddressValid($paramsCP, true);

		$propositions = array();
		if($this->propositions && count($this->propositions)) {
			foreach ($this->propositions as $key => $prop) {
				if(strtolower($prop['step']) != 'forced')
					$propositions[] = array(
						'sMat' 				=> $prop['streetId'],
						'sL4' 				=> $prop['streetName'],
						'sOldL4' 			=>  isset($prop['streetSynonym ']) ? $prop['streetSynonym '] : '',
						'streetFlags' 		=> $prop['streetFlags'],
						'sHcNum' 			=> $prop['streetNumberId'],
						'sNum' 				=> $prop['streetNumber'] .(isset($prop['streetNumberExt']) ? ' '.$prop['streetNumberExt'] : ''),
						'iType' 			=> $prop['localityFlags'],
						'sCp'				=> $prop['postalCode'],
						'sInsee'			=> $prop['localityId'],
						'sLoc'				=> $prop['locality'],
						'sLieuDit'			=> isset($prop['localitySynonym']) ? $prop['localitySynonym'] : '',
						'qualityCode' 		=> isset($prop['qualityCode']) ? $prop['qualityCode'] : ''
					);
			}
		}

		return json_encode(array('isValid' => $this->validAdress, 'propositions' => $propositions, 'messages' => trim($messages)));
	}

	/**
	 * Fonction pour la recherche de voie
	 * @param type $params
	 * @return type
	 */
	public function searchVoie($params){
		try {
			$result = self::$client->SearchVoie($params);
		} catch (SoapFault $fault) {
			Mage::log('Construct Error : ' . 'Code: ' . $fault->getCode() . ' Message:' . $fault->getMessage() . "\n", null, 'capAdresse.log');
		}

		return $result;
	}


	// MAJ CAP JSON


	/**
	 * Fonction pour la recherche de voie
	 * @param type $params
	 * @return type
	 */
	public function searchComplementVoie($params){
		try {
			$result = self::$client->SearchL3($params);
		} catch (SoapFault $fault) {
			Mage::log('Construct Error : ' . 'Code: ' . $fault->getCode() . ' Message:' . $fault->getMessage() . "\n", null, 'capAdresse.log');
		}

		return $result;
	}

	/**
	 * Fonction pour la vérification de la voie
	 * @param type $voie
	 * @param type $sInsee
	 * @param type $iType
	 */
	public function verifComplementVoie($complement, $insee, $mat, $hcnum, $num) {
		$paramsCP = array(
			'sInput'	=> $complement,
			'sInsee'	=> $mat,
			'sMat'	=> $insee,
			'sHexacle'	=> $hcnum,
			'sNum'	=> $num
		);

		$propositions = array();

		$return = $this->searchComplementVoie($paramsCP);

		if (is_object($return->ptrTabHl->Hl))
			$propositions[] = $return->ptrTabHl->Hl;
		elseif (is_array($return->ptrTabHl->Hl))
			foreach ($return->ptrTabHv->Hl as $hl)
				$propositions[] = $hl;

		$messages = self::$client->GetMessage(array('iCodeRet' => $return->iRet, 'sEtape' => 'LIGNE3'));
		$message = $messages->sMessage;

		$this->printResult('true', $propositions, $message);
	}

	public function isNumExist($num, $mat, $insee) {
		$params = array(
			'sNumIn' => $num,
			'sMat' => $mat,
			'sInsee' => $insee
		);

		try {
			$return = self::$client->IsNumExist($params);
		} catch (SoapFault $fault) {
			Mage::log('Construct Error : ' . 'Code: ' . $fault->getCode() . ' Message:' . $fault->getMessage() . "\n", null, 'capAdresse.log');
		}

		$propositions = array();

		if (is_object($return->ptrTabHc->Hc))
			$propositions[] = $return->ptrTabHc->Hc;
		elseif (is_array($return->ptrTabHc->Hc))
			foreach ($return->ptrTabHc->Hc as $hc)
				$propositions[] = $hc;

		$messages = self::$client->GetMessage(array('iCodeRet' => $return->iRet, 'sEtape' => 'NUM'));
		$message = $messages->sMessage;

		$this->printResult('true', $propositions, $message);
	}

	/**
	 * Appel à la fonction de recherche de téléphone chez capAdresse
	 * @param type $params
	 * @return type
	 */
	public function searchNum($params){
		try {
			$result = self::$client->SearchNum($params);
		} catch (SoapFault $fault) {
			Mage::log('Construct Error : ' . 'Code: ' . $fault->getCode() . ' Message:' . $fault->getMessage() . "\n", null, 'capAdresse.log');
		}

		return $result;
	}

	public function searchMailCivWS($params) {
		try {
			$result = self::$client->searchMailCivWS($params);
		} catch (SoapFault $fault) {
			Mage::log('Construct Error : ' . 'Code: ' . $fault->getCode() . ' Message:' . $fault->getMessage() . "\n", null, 'capAdresse.log');
		}

		return $result;
	}

	/**
	 * Fonction de vérification de mail
	 * @param type $mail
	 * @param type $pays
	 */
	public function verifMail($mail, $iso_lang = 'fr') {
		$paramsMail = array(
			'sMail'		=> $mail,
			'sLangue' 	=> $iso_lang,
		);
		$propositions = array();

		$return = $this->isMailValid($paramsMail, true);

		return json_encode($return);
	}

	/**
	 * Fonction d'affichage de résultat en json
	 * @param type $isValid
	 * @param type $propositions
	 * @param type $messages
	 */
	public function printResult($isValid, $propositions, $messages) {
		echo json_encode(array(
			'isValid'		=> $isValid,
			'propositions'	=> $propositions,
			'messages'		=> $messages
		));
	}

	public function parsePropositions($propositions, $needed_keys = array()) {

		$return = array();

		if($propositions && count($propositions)) {
			foreach ($propositions as $key => $value) {
				$ok = true;
				if($needed_keys) {
					// si on a mis des clés obligatoires
					// on vérifie qu'elles sont toutes présentes
					$keys = array_keys($value);
					$intersect = array_intersect($needed_keys, $keys);
					if(count($intersect) != count($needed_keys))
						$ok = false;
				}
				if($ok)
					$return[] = $value;
			}
		}
		return $return;
	}

	public function valideMail($email, $iso_lang = 'fr') {
		$paramsMail = array(
			'sMail'		=> $email,
			'sLangue' 	=> $iso_lang,
		);

		return $this->isMailValid($paramsMail);
	}

	public function valideAdresse($address) {
		// isAddressValid
		//self::$client->ValideFormulaireCSQ(array('sCQA' => $cqa));
	}


	public function init($test = true) {
		if($test) {
			$this->urlApiAdress = $this->urlApiAdressTest;
			$this->urlCapPhoneWS = $this->urlCapPhoneWSTest;
			$this->urlApiMail = $this->urlApiMailTest;
		} else {
			$this->urlApiAdress = Mage::getStoreConfig('capadresse_options/general/url_webservice_address');
			$this->urlCapPhoneWS = Mage::getStoreConfig('capadresse_options/general/url_webservice_phone');
			$this->urlApiMail = Mage::getStoreConfig('capadresse_options/general/url_webservice_mail');
		}
		$auth_key = Mage::getStoreConfig('capadresse_options/general/auth_key');
		if($auth_key) {
			$this->auth_key = $auth_key;
		}

		$this->headers = array(
			'Authorization: Basic '.$this->auth_key,
			'Content-Type: application/json'
		);
	}
	/**
	 * Verifie la validité de l'adresse email
	 * @param  array   $params
	 * @param  boolean $with_propositions retourne les propositions du webservice si true
	 * @return boolean
	 */
	public function isMailValid($params = array(), $with_propositions = false) {

		if(!$with_propositions) {

			return $this->checkMailSrvWS($params);

		} else {

			return $this->checkMailProp($params);
		}

	}

	public function isAddressValid(&$params = array(), $with_propositions = false, $force = false) {
		if($with_propositions) {
			return $this->searchAddress($params);
		} else {
			return $this->verifyAddress($params, $force);
		}
	}

	public function isPhoneValid($params = array()) {
		return $this->searchPhone($params);
	}

	/**
	 * Verifie la validité de l'adresse email
	 * et retourne des propositions si necessaire
	 * @param array $params [description]
	 */
	protected function checkMailProp($params = array()) {
		$method = ucfirst(__FUNCTION__);

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}

		$response = $this->call($this->urlApiMail.$method, $params);

		if($response && isset($response['iRet'])) {
			if($response['iRet'] == 0 || $response['iRet'] == 2) {

				if($response['iRet'] == 0)
					$this->validMail = true;

				$propositions = array();
				if(isset($response['Mail']) && $response['Mail']) {
					foreach ($response['Mail'] as $entry) {
						if(isset($entry['sMail'])) {
							if(is_array($entry['sMail'])) {
								foreach ($entry['sMail'] as $value) {
									$propositions[] = array('sMail' => $value);
								}
							} else {
								$propositions[] = $entry;
							}
						}
					}
				}

				$message = '';
				if(!$this->validMail && isset($response['sMessage'])) {
					$message = $response['sMessage'];
				}

				return array(
					'isValid'		=> $this->validMail,
					'propositions'	=> $propositions,
					'messages'		=> $message
				);

			} elseif(isset($response['sMessage'])) {

				$this->errors['email'] = $response['sMessage'];
			} else {
				Mage::log(__METHOD__.' ERREUR INCONNUE '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
				$this->errors['email'] = 'Erreur inconnue';
			}
		} else {
			$this->errors['email'] = 'Impossible de contacter le service';
			Mage::log(__METHOD__.' ERREUR '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}

		return array(
			'isValid'		=> $this->validMail,
			'propositions'	=> '',
			'messages'		=> isset($this->errors['email']) ? $this->errors['email'] : ''
		);
	}
	/**
	 * Verifie la validité de l'adresse email
			$params = array(
	            'sMail' => email,
	            'sLangue' => iso_langue (fr)
	        );
	 * @param  array  $params [description]
	 * @return [type]         [description]
	 */
	protected function checkMailSrvWS($params = array()) {
		$method = ucfirst(__FUNCTION__);

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}

		$response = $this->call($this->urlApiMail.$method, $params);

		if($response && isset($response['iRet'])) {
			if($response['iRet'] == 0) {

				$this->validMail = true;
				return true;

			} elseif(isset($response['sMessage'])) {

				$this->errors['email'] = $response['sMessage'];

			} else {

				$this->errors['email'] = 'Erreur inconnue';
				Mage::log(__METHOD__.' ERREUR INCONNUE '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
			}
		} else {
			$this->errors['email'] = 'Impossible de contacter le service';
			Mage::log(__METHOD__.' ERREUR '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}
		return false;
	}


	public function searchAddress(&$params = array(), $needed_keys = array()) {
		$method = ucfirst(__FUNCTION__);
		$this->validAdress = false;
		$this->errors['address'] = false;
		$this->propositions = array();

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}

		$response = $this->call($this->urlApiAdress.$method, $params);

		if($response && isset($response['returnValue'])) {
			if($response['returnValue'] >= 0) {
				// recuperation des propositions
				if(
					isset($response['addresses'])
					&& $response['addresses']
					&& count($response['addresses'])
				) {
					if(isset($response['addresses']['address'])) {
						$this->propositions = $response['addresses']['address'];
					}

				}

				if($response['returnValue'] == 0) {

					$this->validAdress = true;
					return true;

				} else {

					$message_params = array(
						'countryCode' => $params['countryCode'],
						'languageCode' => $params['languageCode'],
					);
					$this->message_code = $response['returnValue'];
					// recherche des messages de retour
					$response = $this->getMessage($message_params);

					if(isset($response['message'])) {
						$this->errors['address'] = $response['message'];
					}
				}

			} else {
				$this->errors['address'] = 'Erreur inconnue';
				Mage::log(__METHOD__.' ERREUR INCONNUE '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
			}
		} else {
			$this->errors['address'] = 'Impossible de contacter le service';
			Mage::log(__METHOD__.' ERREUR '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}
		return false;
	}
	/**
	 * Verifie la validité de l'adresse et la met à jour dans le tableau $params
			$params = array(
	            'sMail' => email,
	            'sLangue' => iso_langue (fr)
	        );
	 * @param  array  $params [description]
	 * @return [type]         [description]
	 */
	protected function verifyAddress(&$params = array(), $force = false) {
		$method = ucfirst(__FUNCTION__);

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}
		if(!isset($params['step'])) {
			$params['step'] = 'ValidateForm';
		}

		$response = $this->call($this->urlApiAdress.$method, $params);
		if($response && isset($response['returnValue'])) {
			if(
				isset($response['addresses'])
				&& isset($response['addresses']['address'])
			) {
				$this->propositions = $response['addresses']['address'];
			}

			if($response['returnValue'] == 0) {
				// recuperation de l'adresse
				if($response['addresses'] && count($response['addresses']) == 1) {
					if(isset($response['addresses']['address'])
						&& count($response['addresses']['address']) == 1) {
						// si il y en a qu'une elle a été retrouvée
						// si on force l'insertion de l'adresse suggérée
						// on écrase les données fournies
						if($force) {

							$validAdress = current($response['addresses']['address']);

					        $params['locality'] = $validAdress['locality'];
					        $params['streetName'] = $validAdress['streetName'];
					        $params['streetNumber'] = $validAdress['streetNumber'];

					        if(isset($validAdress['streetNumberExt']))
					        	$params['streetNumberExt'] = $validAdress['streetNumberExt'];
					    }
						$this->validAdress = true;
						return true;
					}

				}
			}

			$message_params = array(
				'countryCode' => $params['countryCode'],
				'languageCode' => $params['languageCode'],
			);
			$this->message_code = $response['returnValue'];
			// recherche des messages de retour
			$message_response = $this->getMessage($message_params);

			if(isset($message_response['message'])) {
				$this->errors['address'] = $message_response['message'];
			} else {
				$this->errors['address'] = 'Erreur inconnue';
				Mage::log(__METHOD__.' ERREUR INCONNUE '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
			}

		} else {
			$this->errors['address'] = 'Impossible de contacter le service';
			Mage::log(__METHOD__.' ERREUR '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}
		return false;
	}

	/**
	 * Verifie la validité d'un numéro de téléphone'
			$params = array(
	            'sTel' => tel,
	            'sIsoPays' => iso_pays 3 (FRA)
	            'sCaractereSep' => '' // caractère de separation des numéros
	            'iFormat' => 1 = international / 0 => national
	            'iTypeSearch' => 	0 pour une recherche par défaut
	            					1 pour une recherche plus large pour les cas litigieux (sur tous les pays)
	            					2 pour une recherche sur le pays précisé uniquement
	            					3 pour une recherche sur le pays précisé et les pays frontaliers.
	            					Si une autre valeur est passée, 0 sera pris par défaut.
	        );
	 * @param  array  $params [description]
	 * @return array si valide array(6) {
							      ["sCodeRet"]=>
							      string(6) "00000X"
							      ["sIndicatifOut"]=>
							      string(2) "33"
							      ["sIndiceInt"]=>
							      string(1) "0"
							      ["sIsoPays"]=>
							      string(3) "FRA"
							      ["sPays"]=>
							      string(6) "France"
							      ["sTel"]=>
							      string(14) "03 21 18 29 30"
							    }
	 */
	protected function searchPhone($params = array()) {
		$method = ucfirst(__FUNCTION__);

		$this->validPhone = false;
		$this->errors['phone'] = false;
		$this->propositions = array();

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}

		$response = $this->call($this->urlCapPhoneWS.$method, $params);

		if($response && isset($response['returnValue'])) {
			//propositions
			if(isset($response['phone']))
				$this->propositions = $response['phone'];

			if($response['returnValue'] == 0) {

				$this->validPhone = true;
				return true;

			} else {

				$message_params = array(
					'iReturnValue'	=> $response['returnValue'],
					'sCodePaysIso3' => $params['sIsoPays']
				);

				$return = $this->getMessageCP($message_params);

				if(isset($return['sMessage'])) {
					$this->errors['phone'] = $return['sMessage'];
				}

				if($response['returnValue'] != 1) {
					$this->propositions = array();
					Mage::log(__METHOD__.' ERREUR '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
				}
			}
		} else {
			$this->errors['phone'] = 'Impossible de contacter le service';
			Mage::log(__METHOD__.' ERREUR '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}
		return false;
	}

	/**
	 * Verifie la validité d'un numéro de téléphone mobile'
			$params = array(
	            'sTel' => tel,
	            'sIndiceInt' => Chaîne de caractères représentant l'indice téléphonique national.
	            'sIsoPays' => iso_pays 3 (FRA)
	            'sIndicatifOut' => Chaîne de caractères représentant l'indicatif international.
	            'iFormat' => 1 = international / 0 => national
	            'sCodeRetour' => 	Code retour résultant de la fonction SearchPhone
	        );
	 * @param  array  $params [description]
	 * @return [type]         [description]
	 */
	protected function validatePhoneMob($params = array()) {
		$method = ucfirst(__FUNCTION__);

		$this->validPhone = false;
		$this->errors['phone'] = false;
		$this->propositions = array();

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}

		$response = $this->call($this->urlCapPhoneWS.$method, $params);

		if($response && isset($response['iReturnValue'])) {
			if($response['iReturnValue'] == 0) {

				$this->validPhone = true;
				return true;

			} else {

				$message_params = array(
					'iReturnValue'	=> $response['iReturnValue'],
					'sCodePaysIso3' => $params['sIsoPays']
				);

				$return = $this->getMessageCP($message_params);
				if(isset($return['sMessage'])) {
					$this->errors['phone'] = $return['sMessage'];
				}

			}
		} else {
			$this->errors['phone'] = 'Impossible de contacter le service';
			Mage::log(__METHOD__.' ERREUR '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}
		return false;
	}

	private function call($url, $params = array()) {

		if(!empty($params)) {
			$url .= '?'.http_build_query($params);
		}

		$curl = curl_init($url);
		Mage::log(__METHOD__.' '.$url, null, date('Y-m-d').'-capAdresse.log');
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, false);
        curl_setopt( $curl, CURLOPT_HEADER, false);
        curl_setopt( $curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt( $curl, CURLOPT_TIMEOUT, 15);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$curl_response = curl_exec($curl);
        $curl_errno = curl_errno($curl);
        $curl_error = curl_error($curl);

		curl_close($curl);

		if(!$curl_errno && !$curl_error) {
			return json_decode($curl_response, true);
		}
		return false;

	}

	public function getError($type) {
		if(isset($this->errors[$type])) {
			return $this->errors[$type];
		}
		return false;
	}

	protected function getMessage($params = array()) {
		$method = ucfirst(__FUNCTION__);

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}

		if(!isset($params['messageCode'])) {
			$params['messageCode'] = $this->message_code;
		}

		$response = $this->call($this->urlApiAdress.$method, $params);

		if($response && isset($response['returnValue'])) {
			if($response['returnValue'] < 0) {
				Mage::log(__METHOD__.' ERREUR URL API : '.$urlApi.' REPONSE : '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
			} else {
				return $response;
			}
		} else {
			Mage::log(__METHOD__.' ERREUR URL API : '.$urlApi.' REPONSE : '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}
		return false;
	}

	protected function getMessageCP($params = array()) {
		$method = ucfirst(__FUNCTION__);

		if(!isset($params['request'])) {
			$params['request'] = $method;
		}

		$response = $this->call($this->urlCapPhoneWS.$method, $params);

		if($response && isset($response['iRet'])) {
			if($response['iRet'] == 0) {
				return $response;
			} else {
				Mage::log(__METHOD__.' ERREUR URL API : '.$urlApi.' REPONSE : '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
			}
		} else {
			Mage::log(__METHOD__.' ERREUR URL API : '.$urlApi.' REPONSE : '.json_encode($response), null, date('Y-m-d').'-capAdresse.log');
		}
		return false;
	}
}