
    {block name='stylesheets'}
	  {include file="_partials/stylesheets.tpl" stylesheets=$stylesheets}
	{/block}


	{block name='javascript_head'}
	  {include file="_partials/javascript.tpl" javascript=$javascript.head vars=$js_custom_vars}  
	{/block}

	{block name='hook_header'}
	  {$HOOK_HEADER nofilter}
	{/block}
  
	{block name='hook_extra'}{/block}
	
	{if $isWP}
	  {wp_head}
	{/if}
  </head>
  
  <body id="{$page_name}" class="{$body_class}">
	{block name='hook_after_body_opening_tag'}
      {hook h='displayAfterBodyOpeningTag'}
    {/block}

    <main>
      {block name='product_activation'}
        {include file='catalog/_partials/product-activation.tpl'}
      {/block}

     
      <span id="header-fixed-margin"></span>
      <header id="header">
        {block name='header'}
          {include file='_partials/header.tpl'}
        {/block}
      </header>
      <section id="wrapper">
        {* <div class="container"> *}

     