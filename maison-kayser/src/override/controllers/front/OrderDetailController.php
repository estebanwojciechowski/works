<?php

use PrestaShop\PrestaShop\Adapter\Order\OrderPresenter;

class OrderDetailController extends OrderDetailControllerCore
{
    /**
     * 
     * OVERRIDE : gestion de l'affichage des formules en front
     * les formules sont "éclatées" chacun des produits la composant disposent d'un order detail
     * il faut donc les rassembler afin d'afficher qu'une ligne
     */
    public function initContent()
    {
        if (Configuration::isCatalogMode()) {
            Tools::redirect('index.php');
        }

        $id_order = (int)Tools::getValue('id_order');
        $id_order = $id_order && Validate::isUnsignedId($id_order) ? $id_order : false;

        if (!$id_order) {
            $reference = Tools::getValue('reference');
            $reference = $reference && Validate::isReference($reference) ? $reference : false;
            $order = $reference ? Order::getByReference($reference)->getFirst() : false;
            $id_order = $order ? $order->id : false;
        }

        if (!$id_order) {
            $this->redirect_after = '404';
            $this->redirect();
        } else {
            if (Tools::getIsset('errorQuantity')) {
                $this->errors[] = $this->trans('You do not have enough products to request an additional merchandise return.', array(), 'Shop.Notifications.Error');
            } elseif (Tools::getIsset('errorMsg')) {
                $this->errors[] = $this->trans('Please provide an explanation for your RMA.', array(), 'Shop.Notifications.Error');
            } elseif (Tools::getIsset('errorDetail1')) {
                $this->errors[] = $this->trans('Please check at least one product you would like to return.', array(), 'Shop.Notifications.Error');
            } elseif (Tools::getIsset('errorDetail2')) {
                $this->errors[] = $this->trans('For each product you wish to add, please specify the desired quantity.', array(), 'Shop.Notifications.Error');
            } elseif (Tools::getIsset('errorNotReturnable')) {
                $this->errors[] = $this->trans('This order cannot be returned', array(), 'Shop.Notifications.Error');
            } elseif (Tools::getIsset('messagesent')) {
                $this->success[] = $this->trans('Message successfully sent', array(), 'Shop.Notifications.Success');
            }

            $order = new Order($id_order);
            if (Validate::isLoadedObject($order) && $order->id_customer == $this->context->customer->id) {
                $this->order_to_display = (new OrderPresenter())->present($order);
                //ADD BY ESTEBANW INSITACTION 16052018
                //REMPLACEMENT DES ORDER DETAILS PAR LES FORMULES SI EXISTANTES 
                $this->getInfosFormules();

                $this->context->smarty->assign([
                    'order' => $this->order_to_display,
                    'HOOK_DISPLAYORDERDETAIL' => Hook::exec('displayOrderDetail', ['order' => $order]),
                ]);
            } else {
                $this->redirect_after = '404';
                $this->redirect();
            }
            unset($order);
        }

        parent::initContent();
        $this->setTemplate('customer/order-detail');
    }

    private function getInfosFormules() {
        $formules = array();
        $has_formule = false;
        if(isset($this->order_to_display['products']) && count($this->order_to_display['products'])) {
            foreach ($this->order_to_display['products'] as $key => $order_detail) {

                if(isset($order_detail['formule_product_index']) 
                    && $order_detail['formule_product_index']) {

                    if(!isset($formule[$order_detail['formule_product_index']])) {
                        $id = $order_detail['formule_product_index'];
                        $id_product = explode('_', $id);
                        if(is_array($id_product) && count($id_product) == 2) {
                            $id_product_attribute = $id_product[1];
                            $id_product = $id_product[0];                            
                        } else {
                            //formule sans décli ? impossible en théorie
                            continue;
                        }   

                        $product = new Product($id_product);
                        
                        $formules[$id] = (array) json_decode($order_detail['formule_info']);

                        // Ajout des variables utilisées sur le template
                        $formules[$id]['reference'] = $product->reference;
                        $formules[$id]['name'] = $formules[$id]['product_name'];
                        $formules[$id]['price'] = Tools::displayPrice($formules[$id]['product_price'], $this->context->currency);
                        $formules[$id]['unit_price_tax_incl'] = $formules[$id]['price'];
                        $formules[$id]['quantity'] = $formules[$id]['product_quantity'];
                        $formules[$id]['total'] = Tools::displayPrice($formules[$id]['unit_price_tax_excl'], $this->context->currency);   
                        $formules[$id]['total_price_tax_incl'] = $formules[$id]['total'];   

                        $has_formule = true;
                    }
                } else {
                    //on conserve les produits qui ne sont pas issus de formules
                    $order_detail['price'] = Tools::displayPrice($order_detail['unit_price_tax_incl'], $this->context->currency);
                    $order_detail['total'] = Tools::displayPrice($order_detail['total_price_tax_incl'], $this->context->currency);
                    $formules[] = $order_detail;
                    
                }                
            }

            if(!empty($formules) && $has_formule) {    
                $this->context->smarty->assign([ 'formules' => $formules  ]);      
            }
        }
    }
    

}
