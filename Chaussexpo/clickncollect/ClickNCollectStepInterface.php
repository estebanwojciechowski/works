<?php
/**
 * @author    Esteban Wojciechowski <estebanw@insitaction.com>
 * @copyright 2017 INSITACTION
 */


use PrestaShop\PrestaShop\Core\Foundation\Templating\RenderableInterface;

interface ClickNCollectStepInterface extends RenderableInterface
{
    public function getTitle();
    public function handleRequest(array $requestParameters = array());
    public function setClickNCollectProcess(ClickNCollectProcess $clickncollectProcess);
    public function isReachable();
    public function isComplete();
    public function isCurrent();
    public function getIdentifier();
    public function getDataToPersist();
    public function restorePersistedData(array $data);
    public function getTemplate();
}
