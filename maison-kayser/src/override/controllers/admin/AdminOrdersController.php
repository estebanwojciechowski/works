<?php

/**
 * @property Order $object
 */
class AdminOrdersController extends AdminOrdersControllerCore
{
    public function ajaxProcessSendMailValidateOrder()
    {
        if ($this->access('edit')) {
            $cart = new Cart((int)Tools::getValue('id_cart'));
            if (Validate::isLoadedObject($cart)) {
                $customer = new Customer((int)$cart->id_customer);
                if (Validate::isLoadedObject($customer)) {

                    if(!defined('OVERRIDE_SHOP_LINK'))
                        define('OVERRIDE_SHOP_LINK', true);

                    $link = Context::getContext()->link->getPageLink('order', false, (int)$cart->id_lang, 'step=3&recover_cart='.(int)$cart->id.'&token_cart='.md5(_COOKIE_KEY_.'recover_cart_'.(int)$cart->id));
                    
                    $mailVars = array(
                        '{order_link}' => Context::getContext()->link->getPageLink('order', false, (int)$cart->id_lang, 'step=3&recover_cart='.(int)$cart->id.'&token_cart='.md5(_COOKIE_KEY_.'recover_cart_'.(int)$cart->id)),
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname
                    );
                    $cartLanguage = new Language((int) $cart->id_lang);
                    if (
                        Mail::Send(
                            (int)$cart->id_lang,
                            'backoffice_order',
                            $this->trans(
                                'Process the payment of your order',
                                array(),
                                'Emails.Subject',
                                $cartLanguage->locale
                            ),
                            $mailVars,
                            $customer->email,
                            $customer->firstname.' '.$customer->lastname,
                            null,
                            null,
                            null,
                            null,
                            _PS_MAIL_DIR_,
                            true,
                            $cart->id_shop)
                    ) {
                        die(json_encode(array('errors' => false, 'result' => $this->trans('The email was sent to your customer.', array(), 'Admin.Orderscustomers.Notification'))));
                    }
                }
            }
            $this->content = json_encode(array('errors' => true, 'result' => $this->trans('Error in sending the email to your customer.', array(), 'Admin.Orderscustomers.Notification')));
        }
    }
}
