{**
 *  2017 ModuleFactory.co
 *
 *  @author    ModuleFactory.co <info@modulefactory.co>
 *  @copyright 2017 ModuleFactory.co
 *  @license   ModuleFactory.co Commercial License
 *}

{extends file='page.tpl'}

{block name='page_title'}
    {* {$bevisible_title} *}
{/block}

{block name='page_content'}
    <div class="storeloc__container storeloc__container--map">
        <div class="storeloc__content">            
            {* title *}
            <b>{l s='Proche de vous' d='Shop.Theme.Global'}</b>
            <h1 class="storeloc__title">
                {l s="Trouvez votre boulangerie" d='Shop.Theme.Global'}
            </h1>

            {* form *}
            {if $bevisible_locator}
            <div class="storeloc__form">
                <input type="hidden" id="error_message_store_loc" name="error_message_store_loc" value="{l s='Veuillez saisir une adresse valide'}">
                <div class="storeloc__form__inner">
                    <div class="bevisible-cell bevisible-cell-input">
                        <input id="bevisible-locator-value" class="form-control" value="" placeholder="{l s='Rechercher' mod='bevisible'}" type="text">
                    </div>
                    <div class="bevisible-cell bevisible-cell-button">
                        <a href="javascript:;" class="btn btn-primary pull-right bevisible-search-button" data-back="{$back}">
                            {* <span class="bevisible-search-button-loader"><i class="bevisible-fa bevisible-fa-refresh bevisible-fa-spin" aria-hidden="true"></i></span> *}
                            <span class="bevisible-search-button-text">{l s='OK' d='Shop.Theme.Global'}</span>
                        </a>
                    </div>
                </div>

                {* Filtres *}
                <div class="filtres__container">
                    <label for="filtre-cafe" class="filtre__item">
                        <div class="custom-checkbox">
                            <input type="checkbox" id="filtre-cafe" name="filtre-cafe" class="input-checkbox" value="0"/>
                            <span><i class="material-icons checkbox-checked"></i></span>
                        </div>
                        <div class="filtre__content">
                            <span class="picto picto--cafe"></span>
                            <span class="filtre__content__text">{l s="Café" d='Shop.Theme.Global'}</span>
                        </div>
                    </label>
                    <label for="filtre-resto" class="filtre__item">
                        <div class="custom-checkbox">
                            <input type="checkbox" id="filtre-resto" name="filtre-resto" class="input-checkbox" value="0"/>
                            <span><i class="material-icons checkbox-checked"></i></span>
                        </div>
                        <div class="filtre__content">
                            <span class="picto picto--resto"></span>
                            <span class="filtre__content__text">{l s="Le restaurant du boulanger" d='Shop.Theme.Global'}</span>
                        </div>
                    </label>
                    <label for="filtre-sansgluten" class="filtre__item">
                        <div class="custom-checkbox">
                            <input type="checkbox" id="filtre-sansgluten" name="filtre-sansgluten" class="input-checkbox" value="0"/>
                            <span><i class="material-icons checkbox-checked"></i></span>
                        </div>
                        <div class="filtre__content">
                            <span class="picto picto--sansgluten"></span>
                            <span class="filtre__content__text">{l s="Corner sans gluten" d='Shop.Theme.Global'}</span>
                        </div>
                    </label>
                </div>
            </div>
            {/if}
        </div>
    </div>

    <div id="bevisible-wrapper" class="container">
        <div class="store-map__container">
            <div id="containerScrollbar" class="liste__store">
                {$content nofilter}
            </div>         
            
            <div id="bevisible-map-wrapper">
                <div id="bevisible-map"></div>
            </div>  
        </div>  
        {if isset($bevisible_content) && $bevisible_content}
            <div class="description__magasin">
                {$bevisible_content nofilter}
            </div>
        {/if}       
    </div>


{/block}