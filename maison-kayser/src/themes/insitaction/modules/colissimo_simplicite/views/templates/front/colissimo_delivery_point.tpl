

<input type="hidden" value="{$wsUrl|escape:'htmlall':'UTF-8'}" id="colissimo-version"/>
<input type="hidden" id="widget-conf-message" value="{l s='Your delivery point has been registered.' mod='colissimo_simplicite'}" />
<input type="hidden" id="widget-error-message" value="{l s='Error : the web service is inaccessible, please try again later and check your configurations.' mod='colissimo_simplicite'}" />
<input type="hidden" id="widget-conf-delete-message" value="{l s='Votre point retrait a été supprimé.' mod='colissimo_simplicite'}" />
<script type="text/javascript">
    var soInputs = new Object();
    var soCarrierId = "{$id_carrier|escape:'htmlall':'UTF-8'}";
    var initialCost_label = "{$initialCost_label|escape:'htmlall':'UTF-8'}";
    var initialCost = "{$initialCost|escape:'htmlall':'UTF-8'}";
    var taxMention = "{$taxMention|escape:'htmlall':'UTF-8'}";
    var moduleLink = "{$module_link|escape:'htmlall':'UTF-8'}";
    var colissimoModuleUrl = "{$baseUrl|escape:'htmlall':'UTF-8'}modules/colissimo_simplicite/"; 
    var colissimoModuleCss = "{$baseUrl|escape:'htmlall':'UTF-8'}modules/colissimo_simplicite/views/css/";
    var colissimoModuleJs =  "{$baseUrl|escape:'htmlall':'UTF-8'}modules/colissimo_simplicite/views/js/";
    var wsUrl = "{$wsUrl|escape:'htmlall':'UTF-8'}";
    var baseUrl = "{$baseUrl|escape:'htmlall':'UTF-8'}";
    {foreach from=$inputs item=input key=name name=myLoop}
    soInputs.{$name|escape:'htmlall':'UTF-8'} = "{$input|strip_tags|addslashes}";
    {/foreach}
</script>


<input type="hidden" id="pudoWidgetErrorCode">
<input type="hidden" id="pudoWidgetErrorCodeMessage">
<input type="hidden" id="pudoWidgetCompanyName">
<input type="hidden" id="pudoWidgetAddress1">
<input type="hidden" id="pudoWidgetAddress2">
<input type="hidden" id="pudoWidgetAddress3">
<input type="hidden" id ="pudoWidgetCity">
<input type="hidden" id="pudoWidgetZipCode">
<input type="hidden" id ="pudoWidgetCountry">
<input type="hidden" id="pudoWidgetType">
<div>
    <div id="widget-colissimo-choix-relais">        
        <p id="ColissimoRelais" class="h3 text-xs-center"{if !isset($current_point_relais) || !$current_point_relais} style="display: none"{/if}>
            {l s='Votre point relais' mod='colissimo_simplicite'}            
            <a id="removeColissimoRelais" href="#colissimo-remove" class="button button--primary" title="{l s='Supprimer la sélection' mod='colissimo_simplicite'}"><i class="material-icons float-xs-left"></i></a>
        </p>
        <p id="widget-colissimo-msg" class="h6 text-xs-center">    
            {if isset($current_point_relais) && $current_point_relais}
            <span class="point-relais">
                <span class="ligne">{$current_point_relais->nom}</span>
                <span class="ligne">{$current_point_relais->adresse1}</span>
                {if $current_point_relais->adresse2}
                    <span class="ligne">{$current_point_relais->adresse2}</span>
                {/if}      
                {if $current_point_relais->adresse3}
                    <span class="ligne">{$current_point_relais->adresse3}</span>
                {/if}                   
                <span class="ligne">{$current_point_relais->codePostal} {$current_point_relais->localite}, {$current_point_relais->libellePays}</span>
            </span>
            {else}
                {l s='Vous souhaitez être livré en point relais ?' mod='colissimo_simplicite'}
            {/if}
        </p>
        <div class="no-border shop__item">
            <a id="showColissimoModal" href="#colissimo-modal" data-toggle="modal" data-target="#colissimo-modal" class="button button--secondary">{l s='Choisir un point relais' mod='colissimo_simplicite'}</a>
        </div>
    </div>
    <div class="modal fade modal-xl" id="colissimo-modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-sm-center" id="myModalLabel">{l s='Choix du point relais colissimo' mod='colissimo_simplicite'}</h4>
            </div>
          <div id="widget-container" class="modal-body"></div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<br/><br/>
