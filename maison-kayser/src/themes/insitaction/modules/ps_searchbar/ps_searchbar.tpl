{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!-- Block search module TOP -->
{if $hookname == "displayNavFullWidth"}
	<span class="search__picto search__picto--open hidden-sm-down"></span>
	<div id="search_widget" class="search-widget" data-search-controller-url="{$search_controller_url}">
		<span class="search__picto search__picto--close"></span>
		<div class="container">
			<div class="row">
				<div class="col col-xs-12 col-md-10 offset-md-1">
					<form method="get" action="{$search_controller_url}">
						<input type="hidden" name="controller" value="search">
						<input id="input__search" type="text" name="s" value="{$search_string}" placeholder="{l s='Search our catalog' d='Shop.Theme.Catalog'}..." aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
						<button type="submit"></button>
					</form>
				</div>
			</div>
		</div>
	</div>
{else if $hookname == "displaySearch"}
	<div id="search_widget" class="search-widget search-widget--page" data-search-controller-url="{$search_controller_url}">
		<div class="container">
			<div class="row">
				<div class="col col-xs-12 col-md-6 offset-md-3">
					<form method="get" action="{$search_controller_url}">
						<input type="hidden" name="controller" value="search">
						<input id="input__search" type="text" name="s" value="{$search_string}" placeholder="{l s='Search our catalog' d='Shop.Theme.Catalog'}..." aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
						<button type="submit"></button>
					</form>
				</div>
			</div>
		</div>
	</div>
{else}
	<span class="search__picto search__picto--open hidden-sm-down"></span>
	<div id="search_widget" class="search-widget" data-search-controller-url="{$search_controller_url}">
		<span class="search__picto search__picto--close"></span>
		<div class="container">
			<div class="row">
				<div class="col col-xs-12 col-md-10 offset-md-1">
					<form method="get" action="{$search_controller_url}">
						<input type="hidden" name="controller" value="search">
						<input id="input__search" type="text" name="s" value="{$search_string}" placeholder="{l s='Search our catalog' d='Shop.Theme.Catalog'}..." aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
						<button type="submit"></button>
					</form>
				</div>
			</div>
		</div>
	</div>
{/if}
<!-- /Block search module TOP --> 