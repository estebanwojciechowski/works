<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class Colissimo_simpliciteOverride extends Colissimo_simplicite
{

    public function hookDisplayCarrierExtraContent($params)
    {
        
        $carrier_so = new Carrier((int)Configuration::get('COLISSIMO_CARRIER_ID'));

        if (!isset($carrier_so) || !$carrier_so->active) {
            return '';
        }
        $id_carrier = (int)$carrier_so->id;
        $address_delivery = new Address((int)$params['cart']->id_address_delivery);

        $country = new Country((int)$address_delivery->id_country);
        $carriers = Carrier::getCarriers(
            $this->context->language->id,
            true,
            false,
            false,
            null,
            (defined('ALL_CARRIERS') ? ALL_CARRIERS : Carrier::ALL_CARRIERS)
        );

        // bug fix for cart rule with restriction
        CartRule::autoAddToCart($this->context);

        // For now works only with single shipping !
        if (method_exists($params['cart'], 'carrierIsSelected')) {
            if ($params['cart']->carrierIsSelected((int)$carrier_so->id, $address_delivery->id)) {
                $id_carrier = (int)$carrier_so->id;
            }
        }

        $customer = new Customer($address_delivery->id_customer);

        $gender = array(
            '1' => 'MR',
            '2' => 'MME',
            '3' => 'MLE');

        if (in_array((int)$customer->id_gender, array(
                1,
                2))) {
            $cecivility = $gender[(int)$customer->id_gender];
        } else {
            $cecivility = 'MR';
        }

        $tax_rate = Tax::getCarrierTaxRate($id_carrier, isset($params['cart']->id_address_delivery) ? $params['cart']->id_address_delivery : null);
        $std_cost_with_taxes_float = (float)$this->initial_cost * (1 + ($tax_rate / 100));
        $std_cost_with_taxes_str = number_format($std_cost_with_taxes_float, 2, ',', ' ');

        $seller_cost_with_taxes_float = 0;
        $seller_cost_with_taxes_str = '';
        if (Configuration::get('COLISSIMO_COST_SELLER')) {
            if (Configuration::get('COLISSIMO_COST_IMPACT')) {
                $seller_cost_with_taxes_float = (float)Configuration::get('COLISSIMO_SELLER_AMOUNT') + $std_cost_with_taxes_float;
            } else {
                $seller_cost_with_taxes_float = $std_cost_with_taxes_float - (float)Configuration::get('COLISSIMO_SELLER_AMOUNT');
                if ($seller_cost_with_taxes_float < 0) {
                    $seller_cost_with_taxes_float = 0;
                }
            }
            $seller_cost_with_taxes_str = number_format($seller_cost_with_taxes_float, 2, ',', ' ');
        }

        $free_shipping = false;

        $rules = $params['cart']->getCartRules();
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                if ($rule['free_shipping'] && !$rule['carrier_restriction']) {
                    $free_shipping = true;
                    break;
                }
            }
            if (!$free_shipping) {
                $key_search = $id_carrier.',';
                $deliveries_list = $params['cart']->getDeliveryOptionList();
                foreach ($deliveries_list as $deliveries) {
                    foreach ($deliveries as $key => $elt) {
                        if ($key == $key_search) {
                            $free_shipping = $elt['is_free'];
                        }
                    }
                }
            }
        } else {
            // for cart rule with restriction
            $key_search = $id_carrier.',';
            $deliveries_list = $params['cart']->getDeliveryOptionList();
            foreach ($deliveries_list as $deliveries) {
                foreach ($deliveries as $key => $elt) {
                    if ($key == $key_search) {
                        $free_shipping = $elt['is_free'];
                    }
                }
            }
        }

        $current_point_relais = false;
        // town fix
        $town = str_replace('\'', ' ', Tools::substr($address_delivery->city, 0, 32));
        $wsUrl = Configuration::get('COLISSIMO_WS_URL');
        if (Configuration::get('COLISSIMO_USE_POINTDERETRAIT')) {
            $token = Configuration::get('COLISSIMO_TOKEN_POINTDERETRAIT');
            $token_update_date = Configuration::get('COLISSIMO_PDR_TOKEN_HOUR');
            $now = time();
            $diff = abs($now - (strtotime($token_update_date)));
            if ($token == null || $diff >= 899) { /* conrespond à 15 minutes */
                Configuration::updateGlobalValue('COLISSIMO_TOKEN_POINTDERETRAIT', $this->getTokenPointDeRetrait());
                Configuration::updateGlobalValue('COLISSIMO_PDR_TOKEN_HOUR', date('Y-m-d H:i:s'));
                $token = Configuration::get('COLISSIMO_TOKEN_POINTDERETRAIT');
            } else {
                $token = Configuration::get('COLISSIMO_TOKEN_POINTDERETRAIT');
            }
            
            // ADD BY ESTEBANW INSITACTION 18072018
            // Ajout des infos du point relais si 1 est sélectionné
            $socolissimo_pointderetrait_info_exist = ColissimoDeliveryPoint::alreadyExists($this->context->cart->id, $this->context->customer->id);

            if ((int)$socolissimo_pointderetrait_info_exist) {
                $current_point_relais = new ColissimoDeliveryPoint((int)$socolissimo_pointderetrait_info_exist);
            }
            // END
            $inputs = array(
                'ceCountry' => $this->replaceAccentedChars($town),
                'ceLang' => 'FR',
                'dyPreparationTime' => (int)Configuration::Get('COLISSIMO_PREPARATION_TIME'),
                'ceAddress' => $this->replaceAccentedChars(Tools::substr($address_delivery->address1, 0, 38)),
                'ceZipCode' => $this->replaceAccentedChars($address_delivery->postcode),
                'ceTown' => $this->replaceAccentedChars($town),
                'dyWeight' => (float)$params['cart']->getTotalWeight() * 1000,
                'cePays' => $country->iso_code,
                'token' => $token,
                'wsUrl' => $wsUrl,
            );
        } else {
            // Keep this fields order (see doc.)
            $inputs = array(
                'pudoFOId' => Configuration::get('COLISSIMO_ID'),
                'ceName' => $this->replaceAccentedChars(Tools::substr($address_delivery->lastname, 0, 34)),
                'dyPreparationTime' => (int)Configuration::Get('COLISSIMO_PREPARATION_TIME'),
                'dyForwardingCharges' => $std_cost_with_taxes_str,
                'dyForwardingChargesCMT' => $seller_cost_with_taxes_str,
                'trClientNumber' => (int)$address_delivery->id_customer,
                'orderId' => $this->formatOrderId((int)$address_delivery->id),
                'numVersion' => $this->api_num_version,
                'ceCivility' => $cecivility,
                'ceFirstName' => $this->replaceAccentedChars(Tools::substr($address_delivery->firstname, 0, 29)),
                'ceCompanyName' => $this->replaceAccentedChars(Tools::substr($address_delivery->company, 0, 38)),
                'ceAdress3' => $this->replaceAccentedChars(Tools::substr($address_delivery->address1, 0, 38)),
                'ceAdress4' => $this->replaceAccentedChars(Tools::substr($address_delivery->address2, 0, 38)),
                'ceZipCode' => $this->replaceAccentedChars($address_delivery->postcode),
                'ceTown' => $this->replaceAccentedChars($town),
                'ceEmail' => $this->replaceAccentedChars($params['cookie']->email),
                'cePhoneNumber' => $this->replaceAccentedChars(
                    str_replace(
                        array(
                        ' ',
                        '.',
                        '-',
                        ',',
                        ';',
                        '/',
                        '\\',
                        '(',
                        ')'
                        ),
                        '',
                        $address_delivery->phone_mobile
                    )
                ),
                'dyWeight' => (float)$params['cart']->getTotalWeight() * 1000,
                'trParamPlus' => $carrier_so->id,
                'trReturnUrlKo' => htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
                'trReturnUrlOk' => htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
                'CHARSET' => 'UTF-8',
                'cePays' => $country->iso_code,
                'trInter' => 1,
                'ceLang' => 'FR'
            );
        }
        if (isset($inputs['dyForwardingChargesCMT']) && !$inputs['dyForwardingChargesCMT'] && !Configuration::get('COLISSIMO_COST_SELLER')) {
            unset($inputs['dyForwardingChargesCMT']);
        }

        // set params for Api 3.0 if needed
        $inputs = $this->setInputParams($inputs);

        // generate key for API
        $inputs['signature'] = $this->generateKey($inputs);

        // calculate lowest cost
        $from_cost = $std_cost_with_taxes_str;
        if (($seller_cost_with_taxes_float < $std_cost_with_taxes_float ) && Configuration::get('COLISSIMO_COST_SELLER')) {
            $from_cost = $seller_cost_with_taxes_str;
        }
        $rewrite_active = true;
        if (!Configuration::get('PS_REWRITING_SETTINGS')) {
            $rewrite_active = false;
        }

        $link = new Link();
        if (Configuration::get('COLISSIMO_USE_POINTDERETRAIT') == '1') {
            $module_link = $link->getModuleLink('colissimo_simplicite', 'redirectdeliverypoint', array(), true);
        } else {
            $module_link = $link->getModuleLink('colissimo_simplicite', 'redirect', array(), true);
        }
        $module_link_mobile = $link->getModuleLink('colissimo_simplicite', 'redirectmobile', array(), true);

        // automatic settings api protocol for ssl
        $protocol = 'http://';
        if (Configuration::get('PS_SSL_ENABLED')) {
            $protocol = 'https://';
        }

        $from_mention = $this->l('From Cost');
        $initial_cost = $from_cost.$this->l(' €');
        $tax_mention = $this->l(' TTC');
        if ($free_shipping) {
            $from_mention = '';
            $initial_cost = $this->l('Free (Will be apply after address selection)');
            $tax_mention = '';
        }

        $on_mobile_device = false;

        if ($this->isMobileDevice()) {
            $on_mobile_device = true;
        }
        $base_url = $protocol.Tools::getShopDomainSsl().__PS_BASE_URI__;
        $this->context->smarty->assign(array(
            'select_label' => $this->l('Select delivery mode'),
            'edit_label' => $this->l('Edit delivery mode'),
            'token' => sha1('colissimo'._COOKIE_KEY_.Context::getContext()->cookie->id_cart),
            'urlSo' => $protocol.Configuration::get('COLISSIMO_URL').'?trReturnUrlKo='.htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
            'urlSoMobile' => $protocol.Configuration::get('COLISSIMO_URL_MOBILE').'?trReturnUrlKo='.htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
            'id_carrier' => $id_carrier,
            'inputs' => $inputs,
            'initialCost_label' => $from_mention,
            'initialCost' => $initial_cost, // to change label for price in tpl
            'taxMention' => $tax_mention, // to change label for price in tpl
            'finishProcess' => $this->l('To choose SoColissimo, click on a delivery method'),
            'rewrite_active' => $rewrite_active,
            'link_socolissimo' => $module_link,
            'link_socolissimo_mobile' => $module_link_mobile,
            'module_link' => $module_link,
            'on_mobile_device' => $on_mobile_device,
            'link_to_img' => $this->link_to_img,
            'wsUrl' => $protocol.$wsUrl,
            'baseUrl' => $base_url,
            'current_point_relais' => $current_point_relais
        ));
        if (Configuration::get('COLISSIMO_USE_POINTDERETRAIT')) {
            return $this->display(__FILE__, 'colissimo_delivery_point.tpl');
        }
        return $this->display(__FILE__, 'extra_content.tpl');
    }
}
