{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='content'}

  <section class="container" id="main">
    <div class="cart-grid row">

      <!-- Left Block: cart product informations & shpping -->
      <div class="cart-grid-body col-xs-12">
        <div class="card-block">
          <h1 class="h1">{l s='Shopping Cart' d='Shop.Theme.Checkout'}</h1>
        </div>
      </div>

      <div class="cart-grid-body col-xs-12 col-lg-9">

        <!-- cart products detailed -->
        <div class="card cart-container">
          {block name='cart_overview'}
            {include file='checkout/_partials/cart-detailed.tpl' cart=$cart}
          {/block}
        </div>

        {block name='continue_shopping'}
        <div class="row message-destinataire">
          {if $couverts}
            <div class="col-md-7">
              <div class="checkbox">
                <input id="couvert-panier" type="checkbox">
                <label class="couvert-panier" for="couvert-panier">{l s='Je souhaite des couverts' d='Shop.Theme.Checkout'}</label>
                <span class="couverts"></span>
              </div>
            </div>
          {/if}
          <div class="col-md-6">
            <span class="commentaire"><small class="todo">TO DO DEV</small> {l s='Laissez si vous le souhaitez un message pour le destinataire'  d='Shop.Theme.Checkout'} :</strong></span>
          </div>
          <div class="col-md-7 col-xs-12">
            <textarea class="msg-destinataire">{if isset($msg_destinataire)}{$msg_destinataire}{/if}</textarea>
          </div>
        </div>
          <a class="label continue-achat btn ins-button-bg btn-primary" href="{url entity='category' id=2}">
            {l s='Continue shopping' d='Shop.Theme.Actions'}
          </a>
        {/block}

        <!-- shipping informations -->
        {block name='hook_shopping_cart_footer'}
          {hook h='displayShoppingCartFooter'}
        {/block}
      </div>

      <!-- Right Block: cart subtotal & cart total -->
      <div class="cart-grid-right col-xs-12 col-lg-3">

        {block name='cart_summary'}
          <div class="box-shadow-container">
            <span class="box-shadow"></span>
            <div class="card cart-summary">

              {block name='hook_shopping_cart'}
                {hook h='displayShoppingCart'}
              {/block}

              {block name='cart_totals'}
                {include file='checkout/_partials/cart-detailed-totals.tpl' cart=$cart}
              {/block}

              {block name='cart_actions'}
                {include file='checkout/_partials/cart-detailed-actions.tpl' cart=$cart}
              {/block}

            </div>
            <div class="rea-panier">
              <span class="text-reassurance">{l s='Paiement sécurisé' d='Shop.Theme.Global'}</span>
              <img src="{$smarty.const._THEME_DIR_}/img/logo-cartes.jpg" alt="logo-cartes">
            </div>
          </div>
        {/block}

        {block name='hook_reassurance'}
          {hook h='displayReassurance'}
        {/block}

      </div>

    </div>
  {if !empty($accessories)}
    <section class="product-accessories clearfix container">
       <h3 class="other-product-cart label-cross-product">{l s='Avez-vous pensé à' d='Shop.Theme.Catalog'}</h3>
       <div class="featured-products">
         <div class="swiper-container swiper--featured-products">
             <div class="swiper-wrapper">
                {foreach from=$accessories item="product_accessory"}
                  {block name='product_miniature'}
                    <div class="swiper-slide">
                      {include file='catalog/_partials/miniatures/product.tpl' product=$product_accessory}
                    </div>
                  {/block}
                {/foreach}
             </div>
             <div class="swiper-button-prev"></div>
             <div class="swiper-button-next"></div>
         </div>
       </div>
     </section>
  {/if}
  </section>
{/block}
