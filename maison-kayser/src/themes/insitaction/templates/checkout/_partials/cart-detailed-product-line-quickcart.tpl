{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 
<div class="media-left">
	<a href="{$product.url}" title="{$product.name}">
	  {if $product.cover.small.url}
		<img class="media-object" src="{$product.cover.small.url}" alt="{$product.name}">
      {else}
	    <img class="media-object" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" alt="{$product.name}">
      {/if}
	</a>
</div>
<div class="media-body">
	<span class="product-name">
		{$product.name}
		{if isset($product.attributes) && count($product.attributes)}
		<ul class="attributes">
		 	{foreach from=$product.attributes key=attribute_name item=attribute}
			<li>{if $product.id_category_default != 58}{$attribute_name} : {/if}{$attribute}</li>
		 	{/foreach}
		</ul>
		{/if}
	</span>
	<span class="product-price">{$product.total}</span>
	<!-- <span class="product-quantity">x{$product.quantity}</span> -->
	<div class="qty">
	  <div class="input-group bootstrap-touchspin">
		<input
          class="js-cart-line-product-quantity input-group form-control"
          data-down-url="{$product.down_quantity_url}"
          data-up-url="{$product.up_quantity_url}"
          data-update-url="{$product.update_quantity_url}"
          data-product-id="{$product.id_product}"
          type="text"
          value="{$product.quantity}"
          name="product-quantity-spin"
          min="{$product.minimal_quantity}"
		  style="display: block;"
        />
		<span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
		<span class="input-group-btn-vertical">
		  <button class="btn btn-touchspin js-touchspin bootstrap-touchspin-up" type="button">
			<i class="material-icons touchspin-up"></i>
		  </button>
		  <button class="btn btn-touchspin js-touchspin bootstrap-touchspin-down" type="button">
			<i class="material-icons touchspin-down"></i>
		  </button>
		</span>
	  </div>
	</div>
	{hook h='displayProductPriceBlock' product=$product type="unit_price"}
</div>