<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Carrier extends CarrierCore
{

    const SHIPPING_PRICE_EXCEPTION = 0;
    const SHIPPING_WEIGHT_EXCEPTION = 1;
    const SHIPPING_SIZE_EXCEPTION = 2;

    public $attributes = array();

    public $datepicker = false;

    public $clickandcollect = false;

    public $velo;

    public function __construct($id = null, $id_lang = null)
    {
        parent::__construct($id, $id_lang);

        $this->attributes = $this->getAttributes();
        if(is_array($this->attributes) && count($this->attributes)) {
            foreach ($this->attributes as $key => $attribute) {
                if($attribute['datepicker']) {
                    //si un des attributs a besoin d'un datepicker, alors on affiche le datepicker
                    $this->datepicker = true;
                }
                if($attribute['field'] == 'delai_clickandcollect') {
                    //si un des attributs a besoin d'un datepicker, alors on affiche le datepicker
                    $this->clickandcollect = true;
                }
            }
        }
    }

    public function add($autoDate = true, $nullValues = false)
    {
        //associe les attributs au mode de transport
        if(parent::add($autoDate, $nullValues)) {
            if($this->setAttributes($this->attributes)) {
                return true;
            }
            parent::delete();
        }
        return false;
    }

    public function delete()
    {
        $this->setAttributes();
        return parent::delete();
    }

    public static function getCarriersByModuleName($id_lang, $active = false, $delete = false, $id_zone = false, $ids_group = null, $module_name = '')
    {
        // Filter by groups and no groups => return empty array
        if ($ids_group && (!is_array($ids_group) || !count($ids_group))) {
            return array();
        }

        $sql = '
        SELECT c.*, cl.delay
        FROM `'._DB_PREFIX_.'carrier` c
        LEFT JOIN `'._DB_PREFIX_.'carrier_lang` cl ON (c.`id_carrier` = cl.`id_carrier` AND cl.`id_lang` = '.(int) $id_lang.Shop::addSqlRestrictionOnLang('cl').')
        LEFT JOIN `'._DB_PREFIX_.'carrier_zone` cz ON (cz.`id_carrier` = c.`id_carrier`)'.
        ($id_zone ? 'LEFT JOIN `'._DB_PREFIX_.'zone` z ON (z.`id_zone` = '.(int) $id_zone.')' : '').'
        '.Shop::addSqlAssociation('carrier', 'c').'
        WHERE c.`deleted` = '.($delete ? '1' : '0');
        if ($active) {
            $sql .= ' AND c.`active` = 1 ';
        }
        if ($id_zone) {
            $sql .= ' AND cz.`id_zone` = '.(int) $id_zone.' AND z.`active` = 1 ';
        }
        if ($ids_group) {
            $sql .= ' AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'carrier_group
                                    WHERE '._DB_PREFIX_.'carrier_group.id_carrier = c.id_carrier
                                    AND id_group IN ('.implode(',', array_map('intval', $ids_group)).')) ';
        }
        $sql .= ' AND c.external_module_name = "'.$module_name.'"';

        $sql .= ' GROUP BY c.`id_carrier` ORDER BY c.`position` ASC';


        $carriers = Db::getInstance()->executeS($sql);

        foreach ($carriers as $key => $carrier) {
            if ($carrier['name'] == '0') {
                $carriers[$key]['name'] = Carrier::getCarrierNameFromShopName();
            }
        }

        return $carriers;
    }


     /**
     * Set Carrier Attributes.
     *
     * @param array $attributes Carrier Attributes
     * @param bool  $delete Delete all previously Carrier Attributes which
     *                      were linked to this Carrier
     *
     * @return bool Whether the Carrier Attributes have been successfully set
     */
    public function setAttributes($attributes, $delete = true)
    {
        if ($delete) {
            Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'carrier_attribute_carrier WHERE id_carrier = '.(int) $this->id);
        }
        if (!is_array($attributes) || !count($attributes)) {
            return true;
        }
        $sql = 'INSERT INTO '._DB_PREFIX_.'carrier_attribute_carrier (id_carrier, id_carrier_attribute) VALUES ';
        foreach ($attributes as $id_attribute) {
            $sql .= '('.(int) $this->id.', '.(int) $id_attribute.'),';
        }

        return Db::getInstance()->execute(rtrim($sql, ','));
    }

    /**
     * [getAttributes retourne la liste des attributs du transporteur sous forme de tableau]
     * @return [type] [description]
     */
    public function getAttributes()
    {
        if(is_null($this->id_reference))
            return array();

        $sql = 'SELECT ca.`id_carrier_attribute`, ca.`name`, ca.`field`, ca.`object`, ca.`datepicker`
                FROM `'._DB_PREFIX_.'carrier_attribute_carrier`  cac
                LEFT JOIN `'._DB_PREFIX_.'carrier_attribute` ca ON cac.`id_carrier_attribute` = ca.`id_carrier_attribute`
                LEFT JOIN `'._DB_PREFIX_.'carrier` c ON cac.`id_carrier` = c.`id_reference`
                WHERE cac.`id_carrier` = '.$this->id_reference.'
                GROUP BY  ca.`id_carrier_attribute`';
        return Db::getInstance()->executeS($sql);
    }

    public static function getCarrierByAttributeFields($attributes = array()) {
        if(is_array($attributes) && count($attributes)) {
             $sql = '
                SELECT cac.`id_carrier`
                FROM `'._DB_PREFIX_.'carrier_attribute_carrier`  cac
                LEFT JOIN `'._DB_PREFIX_.'carrier_attribute` ca ON cac.`id_carrier_attribute` = ca.`id_carrier_attribute`
                LEFT JOIN `'._DB_PREFIX_.'carrier` c ON cac.`id_carrier` = c.`id_reference`
                WHERE ca.`field` in ("'.implode('","', $attributes).'")
                GROUP BY  ca.`id_carrier_attribute`';

                $id_carrier = Db::getInstance()->getValue($sql);
                if($id_carrier) {
                    $carrier = new Carrier($id_carrier);
                    if(Validate::isLoadedObject($carrier))
                        return $carrier;
                }
        }
        return false;
    }

    /**
     * [getProductDelay retourne le délai en s minimum du temps de préparation du produit]
     * @param  [type] $id_product [description]
     * @return [type]             [temps en secondes ou false si impossible pour le context]
     */
    public function getProductDelay($id_product) {
        //si on ne set pas la timezone ici on est en UTC
        date_default_timezone_set(Configuration::get('PS_TIMEZONE'));

        if($id_product instanceof Product) {
            $product = $id_product;
        } else {
            $product = new Product($id_product);
        }

        if(!Validate::isLoadedObject($product)) {
            return false;
        }

        $id_shop = Shop::getContextShopID();
        // Si id_shop est null alors le contexte est en multiboutique, on est sur l'admin
        if(!$id_shop) {
            return false;
        }

        $attribute = $this->getDeliveryAttribute();

        // Si boutique principal on active les horaires du Louvre
        $store_default = Configuration::get('KAYSER_DEFAULT_STORE');
        if(!$store_default) {
            $store_default = 100001016;
        }

        if($id_shop == 1) {
            $store = new Store($store_default);
        } else {
            $store = new Store($id_shop);
        }

        // context Shop avec une boutique physique
        if(Validate::isLoadedObject($store)) {
            // comme on va chercher dans une boucle le prochain jour d'ouverture
            // on s'assure au moins qu'il y en ai bien un
            if(!$store->isClosedEveryDays()) {
                $clickncollect = false;
                if($attribute && isset($attribute['field'])
                        && isset($attribute['object']) && $attribute['object'] == 'product') {
                    // determine si on se basera sur les horaires du click and collect
                    // ou de l'ouverture magasin
                    $clickncollect = ($attribute['field'] == 'delai_clickandcollect');
                }

                // recupére le delai du produit + le temps de préparation
                // si un attribut est présent pour ce transporteur
                $product_delay = $product->getProductDelay($attribute);

                // recupération du délai de refabrication
                // si la boulangerie est fermée au moment de la date
                // on ajoute le délai de refabrication à la prochaine ouverture
                $product_refab_delay = $product->getRefabDelay();
                $product_morning_refab_delay = $product->getRefabDelay(true);


                $product_delay = $store->getDelayWithRefab($product_delay, $product_refab_delay, $product_morning_refab_delay, $clickncollect);
            } else {
                $product_delay = 0;
            }
        } else {
            // determine si on se basera sur les horaires du click and collect
            // ou de l'ouverture magasin
            $product_delay = $product->getProductDelay($attribute, true);
        }

        // on met une valeur d'au moins 10 minutes (600s)
        // pour que le client ait le temps de valider le panier
        $min_delay = Configuration::get('MIN_CART_DELAY');
        if(!$min_delay) {
            $min_delay = 600;
        }

        if($product_delay < $min_delay)
            $product_delay = $min_delay;

        return $product_delay;
    }
    /**
     * [getProductDate retourne la date à partir de laquelle le produit sera disponible au plus tôt]
     * @param  [type] $id_product [description]
     * @return [timestamp]        [date de dispo]
     */
    public function getProductDate($id_product) {
        $delay = self::getProductDelay($id_product);

        // verification si la date est dans la tranche horaire si on est en velo
        $delay = $this->addBikeDeliveryDelay($delay);

        if($delay !== false) {
            return time() + $delay;
        }
        return false;
    }

    /**
     * Recupération des horaires de vélo pour definir l'heure min et maxi
     * @param  [type] $delay delai initial
     * @return [type]        delai initial ou ouverture livrason velo
     */
    public function addBikeDeliveryDelay($delay) {

        $attribute = $this->getDeliveryAttribute();
        $velo = false;
        if($attribute && isset($attribute['field'])
            && isset($attribute['object']) && $attribute['object'] == 'product') {
            $velo = ($attribute['field'] == 'delai_livraison_velo');
        }
        if($velo) {
            $timestamp = time() + $delay;
            $horaires_velo = Configuration::get('KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES');
            if($horaires_velo) {
                try {
                    $horaires_velo = explode('-', $horaires_velo);
                    if($horaires_velo && count($horaires_velo) == 2) {

                        $today_min = strtotime(date('d-m-Y', $timestamp));
                        $start = explode(':', $horaires_velo[0]);

                        if($start && count($start) == 2) {

                            $today_min = mktime($start[0], $start[1], 0, date('m', $today_min), date('d', $today_min), date('Y', $today_min));

                            if($today_min > $timestamp) {
                                return $today_min - $timestamp + $delay;
                            }
                        }

                        // TODO heure de fin de livraison vélo
                        // $end = explode(':', $horaires_velo[0]);

                    }
                } catch (Exception $e) {

                }
            }
        }
        return $delay;
    }

    /**
     * [getCartDelay retroune le délai maximum du panier
     * en tenant compte des dates d'ouvertures du magasin]
     * @param  [type] $id_cart
     * @return [type]          [delai en secondes]
     */
    public function getCartDelay($id_cart) {
        //si on ne set pas la timezone ici on est en UTC
        date_default_timezone_set(Configuration::get('PS_TIMEZONE'));
        if(!$id_cart) {
            $cart = Context::getContext()->cart;
        } elseif($id_cart instanceof Cart) {
            $cart = $id_cart;
        } else {
            $cart = new Cart($id_cart);
        }

        if(Validate::isLoadedObject($cart)) {
            $delay = $cart->getCartDelay($this);
            return $this->addBikeDeliveryDelay($delay);
        }
        return false;
    }

    /**
     * [getCartDate retourne la date à laquelle la commande peut être retirée au minimum
     * en vérifiant les dates d'ouverture du magasin]
     * @param  [type] $id_cart [description]
     * @return [type]          [delai en secondes]
     */
    public function getCartDate($id_cart) {
        $delay = self::getCartDelay($id_cart);
        if($delay !== false) {
            return time() + $delay;
        }
        return false;
    }

    /**
     * [isOpenDay verifie si le jour passé en parametre est ]
     * @param  [type]  $timestamp_date_livraison [date de livraison en time]
     * @param  string  $attribute_delivery       [nom du champ de l'attribut]
     * @return boolean                           [description]
     */
    public static function isOpenDay($timestamp_date_livraison, $attribute_delivery = '') {
        if(is_numeric($timestamp_date_livraison)) {
            // la livraison en voiture n'est pas disponible le dimanche
            if($attribute_delivery == 'delai_livraison_voiture') {
                $config_jours_ouverts = json_decode(Configuration::get('KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS'), true);
                $jour = date('N', $timestamp_date_livraison);
                if(is_array($config_jours_ouverts) && isset($config_jours_ouverts[$jour]) && $config_jours_ouverts[$jour])
                    return true;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * [isBikeDeliveryOK  teste le poids panier, si il est inférieur à
     * ce que peut porter notre courageux cycliste, retourne true]
     * @param  [type]  $cart [objet panier]
     * @return boolean       [description]
     */
    public static function isBikeDeliveryOK($cart = null) {
        // si le panier n'est pas en paramètre par défaut on affiche le délai le plus court
        // Donc en vélo
        if(is_null($cart))
            return true;

        if(Validate::isLoadedObject($cart)) {
            $max_poids = Configuration::get('KAYSER_DELIVERY_LIVRAISON_VELO_POIDS');
            $cart_weight = $cart->getTotalWeight($cart->getProducts());
            if($cart_weight <= $max_poids) {
                return true;
            }
        }
        return false;
    }

    /**
     * [getCarDeliveryDelay Verifie si la livraison par voiture est disponible
     * le jour du délai pasé en paramètre, retourne le délai necessaire si jour fermé]
     * @param  integer $timestamp_cart_max_delay [description]
     * @return [int]                            [delai en s]
     */
    public static function getCarDeliveryDelay($timestamp_cart_max_delay = 0) {
        $car_delay = 0;
        $car_delivery_days = json_decode(Configuration::get('KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS'), true);
        // RECUPERATION DES JOURS D'OUVERTURE MAGASIN
        // POUR ÊTRE SUR QUE LES JOURS CORRESPONDENT
        // AUX OUVERTURES
        $context = Context::getContext();
        $store = $context->shop->getStore();
        if($store) {
            $open_days = $store->getDaysOpen();
            foreach ($car_delivery_days as $key => &$value) {
                if(!isset($open_days[$key]) || !$open_days[$key])
                    $value = 0;
            }
        }

        if($car_delivery_days) {
            // la date du jour + le delai produit
            $delivery_day = date('N' ,time() + $timestamp_cart_max_delay);
            if(!isset($car_delivery_days[$delivery_day]) || !$car_delivery_days[$delivery_day])
            {
                // on cherche le prochain jour livrable
                for ($i=1; $i <= 7 ; $i++) {
                    if($delivery_day % 7 == 0) {
                        $delivery_day = 1;
                    }
                    // si jour livrable on s'arrete
                    if(isset($car_delivery_days[$delivery_day]) && $car_delivery_days[$delivery_day]) {
                        // ajout du nombre de jours de délais
                        return $i * 3600 * 24;
                    }
                }
            }
        }
        return $car_delay;
    }

    /**
     * [getDeliveryAttribute retourne l'attribut de delai du transporteur
     * à ajouter au calcul de fabrication et d'ouverture magasin]
     * @param  [cart] $cart [objet panier] si il n'est pas fourni la livraison velo ne pourra jamais avoir lieu
     * @return [array]       [CarrierAttribute (array) ou false]
     */
    public function getDeliveryAttribute($cart = null) {
        $carrier_attributes = $this->attributes;
        if(is_array($carrier_attributes) && count($carrier_attributes)) {
            // Cas spécifique, 2 attributs délais CAS LIVRAISON RDV Velo ou Voiture
            // parcours des attributs si > 1 => cas velo ou voiture -> si velo dispo alors velo
            /*if(count($carrier_attributes) > 1) {
                $attributes = array('velo' => false, 'voiture' => false);
                // recherche des clés
                foreach ($carrier_attributes as $key => $carrier_attribute) {
                    if($carrier_attribute['field'] == 'delai_livraison_velo') {
                        $attributes['velo'] = $key;
                    } elseif($carrier_attribute['field'] == 'delai_livraison_voiture') {
                        $attributes['voiture'] = $key;
                    }
                }
                if($attributes['velo'] !== false && $attributes['voiture'] !== false) {
                    if(self::isBikeDeliveryOK($cart)) {
                        $this->velo = true;
                        return $carrier_attributes[$attributes['velo']];
                    }
                    $this->velo = false;
                    return $carrier_attributes[$attributes['voiture']];
                } elseif($attributes['velo'] !== false) {
                    if(self::isBikeDeliveryOK($cart)) {
                        $this->velo = true;
                        return $carrier_attributes[$attributes['velo']];
                    }
                } elseif($attributes['voiture'] !== false) {
                    $this->velo = false;
                    return $carrier_attributes[$attributes['voiture']];
                }
            } else {*/
            $carrier_attribute = current($carrier_attributes);
                if(isset($carrier_attribute['velo']) && $carrier_attribute['velo'] !== false) {
                    $this->velo = true;
                    if(!self::isBikeDeliveryOK($cart)) {
                        return false;
                    }
                }
                return current($carrier_attributes);
            //}
        }
        return false;
    }

    /**
     * [getAvailableCarrierList Override pour filtrer la liste des transporteurs dispo par
     * produit par attribut]
     * @param  Product $product             [description]
     * @param  [type]  $id_warehouse        [description]
     * @param  [type]  $id_address_delivery [description]
     * @param  [type]  $id_shop             [description]
     * @param  [type]  $cart                [description]
     * @param  array   &$error              [description]
     * @return [type]                       [description]
     */
     public static function getAvailableCarrierList(Product $product, $id_warehouse, $id_address_delivery = null, $id_shop = null, $cart = null, &$error = array())
    {
        $carrier_list = parent::getAvailableCarrierList($product, $id_warehouse, $id_address_delivery, $id_shop, $cart, $error);

        if(is_null($id_shop))
            $id_shop = Shop::getContextShopID();

        // ADD BY ESTEBANW INSITACTION
        // Remove carriers where attribute_carrier is null or 0
        $id_carrier_to_remove = $id_carrier_allowed = array();
        if($id_shop > 1) {
            foreach ($carrier_list as $id_carrier) {
                $carrierObj = new Carrier($id_carrier);
                if(Validate::isLoadedObject($carrierObj)) {
                    if(is_array($carrierObj->attributes) && !empty($carrierObj->attributes)) {
                        $attributes = $carrierObj->attributes;
                        foreach ($attributes as $key => $attribute) {
                            // Test attributes values
                            if(isset($attribute['field']) && $attribute['field']
                                && isset($attribute['object']) && $attribute['object'] == 'product'
                                    && property_exists($product, $attribute['field'])) {

                                $field_value = $product->{$attribute['field']};
                                // tout les produits sont clickandcollect
                                // sinon si pas de delai le produit n'est pas dispo pour ce mode de livraison
                                if($attribute['field'] != 'delai_clickandcollect'
                                    && (is_null($field_value) || !($field_value > 0))
                                    ) {
                                    // remove carrier
                                    $id_carrier_to_remove[$id_carrier] = $id_carrier;
                                }
                            }
                        }
                    }
                }
                if(!isset($id_carrier_to_remove[$id_carrier]))
                    $id_carrier_allowed[$id_carrier] = $id_carrier;
            }
        } else {
            //Only colissimo on main shop
            //get COLISSIMO carrier by ID reference (i.e 5)
            $id_ref_colissimo = Configuration::get('COLISSIMO_ID_REFERENCE');
            if(!$id_ref_colissimo)
                $id_ref_colissimo = 5;
            $carrierObj = Carrier::getCarrierByReference($id_ref_colissimo);
            $id_carrier_allowed[$carrierObj->id] = $carrierObj->id;
        }
        // return only carriers that match
        return array_intersect($id_carrier_allowed, $carrier_list);
    }


    // OVERRIDE FOR POSTAL CODE DELIVERY

    /**
     * Return the carrier name from the shop name (e.g. if the carrier name is '0').
     *
     * The returned carrier name is the shop name without '#' and ';' because this is not the same validation.
     *
     * @return string Carrier name
     */
    public static function getCarrierNameFromShopName()
    {
        return str_replace(
            array('#', ';'),
            '',
            Configuration::get('PS_SHOP_NAME')
        );
    }

    /**
     * Check if postcode starts with code
     *
     * Created for Postal Deliv
     */
    private static function __startsWith($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle != '' && strpos($haystack, $needle) === 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get all carriers in a given language
     *
     * @param integer $id_lang Language id
     * @param $modules_filters, possible values:
    PS_CARRIERS_ONLY
    CARRIERS_MODULE
    CARRIERS_MODULE_NEED_RANGE
    PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE
    ALL_CARRIERS
     * @param boolean $active Returns only active carriers when true
     * @return array Carriers
     * Created for Postal Deliv
     */
    public static function getCarriers(
        $id_lang,
        $active = false,
        $delete = false,
        $id_zone = false,
        $ids_group = null,
        $modules_filters = self::PS_CARRIERS_ONLY,
        $postcode = false,
        $id_country = false
    ) {
        // Filter by groups and no groups => return empty array
        if ($ids_group && (!is_array($ids_group) || !count($ids_group))) {
            return array();
        }

        $sql = '
        SELECT c.*, cl.delay
        FROM `'._DB_PREFIX_.'carrier` c
        LEFT JOIN `'._DB_PREFIX_.'carrier_lang` cl ON (c.`id_carrier` = cl.`id_carrier`
            AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
        LEFT JOIN `'._DB_PREFIX_.'carrier_zone` cz ON (cz.`id_carrier` = c.`id_carrier`)'.
            ($id_zone ? 'LEFT JOIN `'._DB_PREFIX_.'zone` z ON (z.`id_zone` = '.(int)$id_zone.')' : '').'
        '.Shop::addSqlAssociation('carrier', 'c').'
        WHERE c.`deleted` = '.($delete ? '1' : '0');
        if ($active) {
            $sql .= ' AND c.`active` = 1 ';
        }
        if ($id_zone) {
            $sql .= ' AND cz.`id_zone` = '.(int)$id_zone.' AND z.`active` = 1 ';
        }
        if ($ids_group) {
            $sql .= ' AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'carrier_group
                                    WHERE '._DB_PREFIX_.'carrier_group.id_carrier = c.id_carrier
                                    AND id_group IN ('.implode(',', array_map('intval', $ids_group)).')) ';
        }

        switch ($modules_filters) {
            case 1:
                $sql .= ' AND c.is_module = 0 ';
                break;
            case 2:
                $sql .= ' AND c.is_module = 1 ';
                break;
            case 3:
                $sql .= ' AND c.is_module = 1 AND c.need_range = 1 ';
                break;
            case 4:
                $sql .= ' AND (c.is_module = 0 OR c.need_range = 1) ';
                break;
        }
        $sql .= ' GROUP BY c.`id_carrier` ORDER BY c.`position` ASC';

        $cache_id = 'Carrier::getCarriers_'.md5($sql);
        if (!Cache::isStored($cache_id)) {
            $carriers = Db::getInstance()->executeS($sql);
            Cache::store($cache_id, $carriers);
        } else {
            $carriers = Cache::retrieve($cache_id);
        }

        // ADD BY ESTEBANW 30052018
        // SI plusieurs règles de codes postaux
        // ne s'arrete pas dès qu'un match est trouvé
        // chacune des règle doivent êtres validées
        $has_match = array();
        /* Postal Deliv START */
        foreach ($carriers as $key => $carrier) {
            if (Module::isEnabled('postaldeliv') && $postcode != false && $id_country != false) {
                $postcode = Tools::strtoupper($postcode);
                // Check if carrier has limitation
                $rules = Db::getInstance()->executeS('
                    SELECT * FROM `'._DB_PREFIX_.'postaldeliv` a
                    LEFT JOIN `'._DB_PREFIX_.'postaldeliv_shop` b ON (b.`id_postaldeliv` = a.`id_postaldeliv`)
                    WHERE a.`id_carrier`='.(int)$carrier['id_carrier'].'
                    AND b.`id_shop`='.(int)Context::getContext()->shop->id);

                if ($rules) {
                    foreach ($rules as $rule) {
                        $countries = explode(',', $rule['country']);
                        // Check if rule is applied to the country
                        if (in_array(0, $countries) || in_array($id_country, $countries)) {
                            $in_postcode = in_array($postcode, explode(',', Tools::strtoupper($rule['postcode'])));
                            $starts_with = Carrier::__startsWith(
                                $postcode,
                                explode(',', Tools::strtoupper($rule['county']))
                            );
                            // Check if postcode is in one of the ranges
                            $in_range = false;
                            if ($ranges = unserialize($rule['range'])) {
                                foreach ($ranges as $range) {
                                    if ($postcode >= $range[0] && $postcode <= $range[1]) {
                                        $in_range = true;
                                        if(!isset($has_match[$carrier['id_carrier']]))
                                            $has_match[$carrier['id_carrier']] = true;
                                    }
                                }
                            }

                            if (($rule['available'] == '0' && ($in_postcode || $starts_with || $in_range))
                                || ($rule['available'] == '1' && !$in_postcode && !$starts_with && !$in_range)) {
                                // ADD BY ESTEBANW - permet l'utilisation de plusieurs ranges de cp
                                if(!isset($has_match[$carrier['id_carrier']]))
                                    unset($carriers[$key]['id_carrier']);
                            } elseif ($carrier['name'] == '0') {
                                $carriers[$key]['name'] = Carrier::getCarrierNameFromShopName();
                            }
                        } else {
                            if ($rule['available'] == '1') {
                                unset($carriers[$key]['id_carrier']);
                            }
                        }
                    }
                }
            }
        }
        /* Postal Deliv END */
        return $carriers;
    }

    public static function getVeloCarrier() {
        return Db::getInstance()->getValue('
            SELECT id_carrier FROM '._DB_PREFIX_.'carrier_attribute_carrier cac
            LEFT JOIN '._DB_PREFIX_.'carrier_attribute ca ON ca.id_carrier_attribute = cac.id_carrier_attribute
            WHERE ca.field = "delai_livraison_velo"'
        );
    }

    /**
     * Get available Carriers for Order
     *
     * @param int       $id_zone Zone ID
     * @param array     $groups  Group of the Customer
     * @param Cart|null $cart    Optional Cart object
     * @param array     &$error  Contains an error message if an error occurs
     *
     * @return array Carriers for the order
     * Modified for Postal Deliv
     */
    public static function getCarriersForOrder($id_zone, $groups = null, $cart = null, &$error = array())
    {
        $context = Context::getContext();
        $id_lang = $context->language->id;
        if (is_null($cart)) {
            $cart = $context->cart;
        }
        if (isset($context->currency)) {
            $id_currency = $context->currency->id;
        }

        /* Postal Deliv START */
        $postcode = '';
        if (isset($context->cookie->postcode)) {
            $postcode = $context->cookie->postcode;
            $id_country = $context->cookie->id_country;
        } elseif (Tools::getIsset('id_address_delivery')) {
            $id_address = Tools::getValue('id_address_delivery');
            $address = new Address((int)$id_address);
            $postcode = $address->postcode;
            $id_country = $address->id_country;
        } else {
            $id_address = $cart->id_address_delivery;
            $address = new Address((int)$id_address);
            $postcode = $address->postcode;
            $id_country = $address->id_country;
        }

        if (is_array($groups) && !empty($groups)) {
            $result = Carrier::getCarriers(
                $id_lang,
                true,
                false,
                (int)$id_zone,
                $groups,
                self::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE,
                $postcode,
                $id_country
            );
        } else {
            $result = Carrier::getCarriers(
                $id_lang,
                true,
                false,
                (int)$id_zone,
                array(Configuration::get('PS_UNIDENTIFIED_GROUP')),
                self::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE,
                $postcode,
                $id_country
            );
        }
        $results_array = array();
        /* Postal Deliv END */

        foreach ($result as $k => $row) {
            /* Postal Deliv START */
            if (isset($row['id_carrier'])) {
                /* Postal Deliv END */
                $carrier = new Carrier((int)$row['id_carrier']);
                $shipping_method = $carrier->getShippingMethod();
                if ($shipping_method != Carrier::SHIPPING_METHOD_FREE) {
                    // Get only carriers that are compliant with shipping method
                    if (($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT
                        && $carrier->getMaxDeliveryPriceByWeight($id_zone) === false)) {
                        $error[$carrier->id] = Carrier::SHIPPING_WEIGHT_EXCEPTION;
                        unset($result[$k]);
                        continue;
                    }
                    if (($shipping_method == Carrier::SHIPPING_METHOD_PRICE
                        && $carrier->getMaxDeliveryPriceByPrice($id_zone) === false)) {
                        $error[$carrier->id] = Carrier::SHIPPING_PRICE_EXCEPTION;
                        unset($result[$k]);
                        continue;
                    }

                    // If out-of-range behavior carrier is set to "Deactivate carrier"
                    if ($row['range_behavior']) {
                        // Get id zone
                        if (!$id_zone) {
                            $id_zone = (int)Country::getIdZone(Country::getDefaultCountryId());
                        }

                        // Get only carriers that have a range compatible with cart
                        if ($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT
                            && (!Carrier::checkDeliveryPriceByWeight(
                                $row['id_carrier'],
                                $cart->getTotalWeight(),
                                $id_zone
                            ))) {
                            $error[$carrier->id] = Carrier::SHIPPING_WEIGHT_EXCEPTION;
                            unset($result[$k]);
                            continue;
                        }

                        if ($shipping_method == Carrier::SHIPPING_METHOD_PRICE
                            && (!Carrier::checkDeliveryPriceByPrice(
                                $row['id_carrier'],
                                $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING),
                                $id_zone,
                                $id_currency
                            ))) {
                            $error[$carrier->id] = Carrier::SHIPPING_PRICE_EXCEPTION;
                            unset($result[$k]);
                            continue;
                        }
                    }
                }

                $row['name'] = ((string)$row['name'] != '0' ? $row['name'] : Carrier::getCarrierNameFromShopName());
                $row['price'] = (($shipping_method == Carrier::SHIPPING_METHOD_FREE) ? 0 :
                    $cart->getPackageShippingCost((int)$row['id_carrier'], true, null, null, $id_zone));
                $row['price_tax_exc'] = (($shipping_method == Carrier::SHIPPING_METHOD_FREE) ? 0 :
                    $cart->getPackageShippingCost((int)$row['id_carrier'], false, null, null, $id_zone));
                $row['img'] = file_exists(_PS_SHIP_IMG_DIR_.(int)$row['id_carrier'].'.jpg') ?
                    _THEME_SHIP_DIR_.(int)$row['id_carrier'].'.jpg' : '';

                // If price is false, then the carrier is unavailable (carrier module)
                if ($row['price'] === false) {
                    unset($result[$k]);
                    continue;
                }
                $results_array[] = $row;
                /* Postal Deliv START */
            }
            /* Postal Deliv END */
        }

        // if we have to sort carriers by price
        $prices = array();
        if (Configuration::get('PS_CARRIER_DEFAULT_SORT') == Carrier::SORT_BY_PRICE) {
            foreach ($results_array as $r) {
                $prices[] = $r['price'];
            }
            if (Configuration::get('PS_CARRIER_DEFAULT_ORDER') == Carrier::SORT_BY_ASC) {
                array_multisort($prices, SORT_ASC, SORT_NUMERIC, $results_array);
            } else {
                array_multisort($prices, SORT_DESC, SORT_NUMERIC, $results_array);
            }
        }

        return $results_array;
    }


}