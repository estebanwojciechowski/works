<?php 
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

class BeVisibleZone extends ObjectModel
{ 
    public $id_bevisible_zone;

    public $id_bevisible_group;

    public $name;

    public $description;
 
    public $address;
 
    public $latitude;
 
    public $longitude;
 
    public $zoom;

    public $position;
 
    protected $options;


    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'bevisible_zone',
        'primary' => 'id_bevisible_zone',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            'id_bevisible_group' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'name' =>           array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => false, 'size' => 128),
            'description' =>    array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
            'address' =>        array('type' => self::TYPE_STRING, 'validate' => 'isAddress'),
            'latitude' =>       array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'longitude' =>      array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'zoom' =>           array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'position' =>       array('type' => self::TYPE_INT),
            'options' =>        array('type' => self::TYPE_STRING),
        )
    ); 

    public function save($null_values = false, $auto_date = true)
    {
        //permet de mettre un nom uniquement français et d'enregistrer tout de même la zone
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $base_name = isset($this->name[$id_lang_default]) ? $this->name[$id_lang_default] : '';
        
        if(strlen($base_name)) {
            foreach ($this->name as $id_lang => &$value) {
                if($id_lang == $id_lang_default)
                    continue;
                if(!strlen(trim($value)))
                    $value = $base_name;
            }
        }
        return parent::save();
    }

    public static function getZones($id_group = false, $sql = false) { 
        if($sql) {
            return DB::getInstance()->executeS(
                'SELECT * FROM `'._DB_PREFIX_.'bevisible_zone` z
                LEFT JOIN `'._DB_PREFIX_.'bevisible_zone_lang` zl ON zl.`id_bevisible_zone` = z.`id_bevisible_zone`
                '.Shop::addSqlAssociation('bevisible_zone', 'z').'
                '.($id_group ? ' LEFT JOIN `'._DB_PREFIX_.'bevisible_zone_group` zg ON zg.`id_bevisible_zone` = z.`id_bevisible_zone` ' : '').'
                WHERE zl.`id_lang` = '.(int)Configuration::get('PS_LANG_DEFAULT') 
                .($id_group ? ' AND `id_bevisible_group` = '.$id_group : '')
            );
        }

        $collection_zones = new PrestaShopCollection('BeVisibleZone');         
        $zones = $collection_group_zones->orderBy('position')->getResults();  
 
        return $zones;   
    }
}
