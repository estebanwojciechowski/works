<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

class BeVisibleMapValidate extends Validate
{
    public static function isOpenHour($open_hour)
    {
        $valid = true;
        $exploded = explode('-', $open_hour);
        if (count($exploded) > 1) {
            $valid = $valid && (bool)$exploded[0];
            $valid = $valid && (bool)$exploded[1];
        }

        return $valid;
    }

    public static function isMatchedFileType($is_matched)
    {
        return $is_matched;
    }
}
