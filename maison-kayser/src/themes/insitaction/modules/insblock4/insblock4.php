<?php
/*
*  @author Insitaction <contact@insitaction.com>
*  @copyright  2018 Insitaction
*
*  @version 1.0 (2018-02-16)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class insblock4 extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'insblock4';
        $this->tab = 'front_office_features';
        $this->author = 'Insitaction';
        $this->version = '1.0.';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Block contenu 4');
        $this->description = $this->l('Simple bloc de contenu 4.');

        $this->templateFile = 'module:insblock4/insblock4.tpl';

        $this->confirmUninstall = $this->l('Êtes-vous certain(e) de vouloir désinstaller ce module ?');

        if (!Configuration::get('INSBLOCK4'))
          $this->warning = $this->l('No name provided');
    }

    public function install()
    {
        return  parent::install() &&
            $this->registerHook('displayHome');
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('insblock4'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('insblock4'));
    }    

    /**
    * Return first category for specified single (post, page, custom post tpye)
    */
    public static function get_single_main_category($single_id = 0) {

        $single_categories = get_the_category($single_id);
        if(!empty($single_categories)){
        
            return $single_categories[0]; 
        }
        
        return false;
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {        

        $post_bloc = 'Placer votre contenu ici...';

        return array(
            'post_bloc' => $post_bloc,
        );
    }
}