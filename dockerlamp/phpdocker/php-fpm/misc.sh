
# install sendmail
apt-get install -y ssmtp && apt-get clean && echo "root=local@dev.lan" >> /etc/ssmtp/ssmtp.conf &&
echo "mailhub=${CONTAINER_PREFIX}-mailcatcher:1025" >> /etc/ssmtp/ssmtp.conf
echo "FromLineOverride=YES" >> /etc/ssmtp/ssmtp.conf && chmod 777 /etc/ssmtp /etc/ssmtp/*

# Install git
apt-get update && apt-get -y install git && apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# add nvm to choose node version
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
export NVM_DIR="${XDG_CONFIG_HOME/:-$HOME/.}nvm" [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
command -v nvm;
