{**
 * 2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 *
 *} 
 <form id="{if isset($fields.form.form.id_form)}{$fields.form.form.id_form|escape:'html':'UTF-8'}{else}{if $table == null}configuration_form{else}{$table}_form{/if}{if isset($smarty.capture.table_count) && $smarty.capture.table_count}_{$smarty.capture.table_count|intval}{/if}{/if}" class="defaultForm form-horizontal{if isset($name_controller) && $name_controller} {$name_controller}{/if}"{if isset($current) && $current} action="{$current|escape:'html':'UTF-8'}{if isset($token) && $token}&amp;token={$token|escape:'html':'UTF-8'}{/if}"{/if} method="post" enctype="multipart/form-data"{if isset($style)} style="{$style}"{/if} novalidate> 
<div id="bevisible_help" class="panel">
    <div class="panel-heading">
        <span>{l s='Zones' mod='bevisible'}</span>
    </div>
    <div class="form-wrapper clearfix"> 
    	{if $zones && is_array($zones)}
    		{assign var="group_index" value=0} 
	    	{foreach from=$zones key=group_name item=groups} 
	    	<div class="group_container">
				<div class="group form-group">
		    		<div class="row">
						<label class="control-label col-lg-3">{l s='Groupe de zones' mod='bevisible'}</label>
						<div class="col-lg-9">
							<input type="text" name="group_zones[]" value="{$group_name}" class="" size="64">
						</div>	
					</div>							
				</div>
				<div class="row">
					<div class="zone_container">
	    			{if $groups && is_array($groups)}
			    		{foreach from=$groups item=zone_value} 
			    		<div class="zone col-sm-offset-1 col-sm-11">
			    			<div class="form-group">
			    				<div class="row">
									<label class="control-label col-lg-3">{l s='Zone' mod='bevisible'}</label>
									<div class="col-lg-9">
										<input type="text" name="zones[{$group_name}][]" value="{$zone_value}" class="" size="64">
									</div>					
								</div>			
							</div>
						</div>
			        	{/foreach}
			        {else}
			        	<div class="zone col-sm-offset-1 col-sm-11">
			    			<div class="form-group">
			    				<div class="row">
									<label class="control-label col-lg-3">{l s='Zone' mod='bevisible'}</label>
									<div class="col-lg-9">
										<input type="text" name="zones[{$group_name}][]" value="" class="" size="64">
									</div>					
								</div>			
							</div>
						</div>
			        {/if}
			        </div>
			        <div class="col-sm-12 text-right">
				        <a href="#" class="add_zone btn btn-primary">+ Ajouter une zone</a>
				    </div>
		        </div>
		    </div>
		    <br/><hr/><br/>
	        {/foreach}
		{/if}
		<div class="group_container">
			<div class="group form-group">
	    		<div class="row">
					<label class="control-label col-lg-3">{l s='Groupe de zones' mod='bevisible'}</label>
					<div class="col-lg-9">
						<input type="text" name="group_zones[]" value="" class="" size="64">
					</div>	
				</div>							
			</div>
	    </div>
    </div>
    <div class="panel-footer">
		<button type="submit" value="1" id="submit_bevisible_zone_settings" name="submit_bevisible_zone_settings" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer</button>
	</div>
</div>
</form>
<script>
	jQuery(document).ready(function($) { 
		$('.add_zone').on('click', function() {
			var parent = $(this).parents('.group_container').find('.zone_container');
			var zone = parent.find('.zone:first-child');
			zone = zone.clone(true, true);
			//reinit du champ zone
			zone.find('input').val('');
			parent.append(zone);
			return false;
		});
	});
</script>