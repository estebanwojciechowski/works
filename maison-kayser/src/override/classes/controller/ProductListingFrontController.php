<?php

abstract class ProductListingFrontController extends ProductListingFrontControllerCore
{

	public function init()
    {
		
		$this->assignGeneralPurposeVariables();
		
        parent::init();
		
    }

}