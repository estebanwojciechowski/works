{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
    <div id="search_filters">
{*
    {block name='facets_title'}
      <h4 class="text-uppercase h6">{l s='Filter By' d='Shop.Theme.Actions'}</h4>
    {/block}

    {block name='facets_clearall_button'}
      <div id="_desktop_search_filters_clear_all" class=" clear-all-wrapper">
        <button data-search-url="{$clear_all_link}" class="btn btn-tertiary js-search-filters-clear-all">
          <i class="material-icons">&#xE14C;</i>
          {l s='Clear all' d='Shop.Theme.Actions'}
        </button>
      </div>
    {/block}
*}

    {foreach from=$facets item="facet"}
      {if $facet.displayed}
        <section class="facet clearfix">
          {* Recherche de l'éventuelle caractéristique *}
          {if $facet.properties}
            {foreach from=$facet.properties item="feature"}
              {if $feature}
                {assign var=id_feature value=$feature}
              {else}
                {assign var=id_feature value=NULL}
              {/if}
            {/foreach}
          {else}
            {assign var=id_feature value=NULL}
          {/if}

          {assign var="stopfilter" value=false}
          {if $facet.label == 'Végétarien' || $facet.label == 'Vegan'}
            {assign var="stopfilter" value=true}
           {*  {if $smarty.get.id_category == 8 }
              <h3 class="h6 facet-title hidden-sm-down">{$facet.label}</h3>
              {assign var=_expand_id value=10|mt_rand:100000}
              {assign var=_collapse value=false}
              {foreach from=$facet.filters item="filter"}
                {if $filter.active}{assign var=_collapse value=false}{/if}
              {/foreach}
              {assign var="stopfilter" value=false}
            {/if} *}
          {else}
            <h3 class="h6 facet-title hidden-sm-down">{$facet.label}</h3>
            {assign var=_expand_id value=10|mt_rand:100000}
            {assign var=_collapse value=true}
            {foreach from=$facet.filters item="filter"}
              {if $filter.active}{assign var=_collapse value=false}{/if}
            {/foreach}
          {/if}

          <div class="title hidden-md-up" data-target="#facet_{$_expand_id}" data-toggle="collapse"{if !$_collapse} aria-expanded="true"{/if}>
            <h3 class="h6 facet-title">{$facet.label}</h3>
            <span class="float-xs-right">
              <span class="navbar-toggler collapse-icons">
                <i class="material-icons add">&#xE313;</i>
                <i class="material-icons remove">&#xE316;</i>
              </span>
            </span>
          </div>

          {if $stopfilter == false}
          {if $facet.widgetType !== 'dropdown'}

            {block name='facet_item_other'}
              {if $id_feature === 11}
              <ul id="facet_{$_expand_id}" class="facet_livraison collapse{if !$_collapse} in{/if}">
              {else}
              <ul id="facet_{$_expand_id}" class="collapse{if !$_collapse} in{/if}">
              {/if}

                {foreach from=$facet.filters key=filter_key item="filter"}

                  {if $filter.displayed}
                    <li>
                      <label class="facet-label{if $filter.active} active {/if}" for="facet_input_{$_expand_id}_{$filter_key}">
                        {if $facet.multipleSelectionAllowed}
                          <span class="custom-checkbox">
                            <input
                              id="facet_input_{$_expand_id}_{$filter_key}"
                              data-search-url="{$filter.nextEncodedFacetsURL}"
                              type="checkbox"
                              {if $filter.active } checked {/if}
                            >
                            {if isset($filter.properties.color)}
                              <span class="color" style="background-color:{$filter.properties.color}"></span>
                              {elseif isset($filter.properties.texture)}
                                <span class="color texture" style="background-image:url({$filter.properties.texture})"></span>
                              {else}
                              <span {if !$js_enabled} class="ps-shown-by-js" {/if} style="float:left"><i class="material-icons checkbox-checked">&#xE5CA;</i></span>
                            {/if}
                          </span>
                        {else}
                          <span class="custom-radio">
                            <input
                              id="facet_input_{$_expand_id}_{$filter_key}"
                              data-search-url="{$filter.nextEncodedFacetsURL}"
                              type="radio"
                              name="filter {$facet.label}"
                              {if $filter.active } checked {/if}
                            >
                            <span {if !$js_enabled} class="ps-shown-by-js" {/if}></span>
                          </span>
                        {/if}
                        {assign var="picto" value=''}
                        {if $id_feature === 11}
                          {* Picto Livraison (dynamique) *}
                          {if $filter.value === 492}
                            {assign var="picto" value='picto__retrait'}
                          {elseif $filter.value === 491}
                            {assign var="picto" value='picto__rdv'}
                          {elseif $filter.value === 493}
                            {assign var="picto" value='picto__coli'}
                          {/if}
                        {/if}
                        {if $filter.type == 'category'}
                        <h2 style="display:inline-block">
                          <a
                            href="{$link->getCategoryLink($filter.value)}"
                            data-href="{$filter.nextEncodedFacetsURL}"
                            class="{$picto} _gray-darker search-link js-search-link category_filter"
                          >{$filter.label}
                            {if $filter.magnitude}
                              <span class="magnitude">({$filter.magnitude})</span>
                            {/if}
                          </a>
                        </h2>
                        {else}
                          <a
                            href="{$filter.nextEncodedFacetsURL}"
                            class="{$picto} _gray-darker search-link js-search-link"
                            rel="nofollow"
                          >{$filter.label}
                            {if $filter.magnitude}
                              <span class="magnitude">({$filter.magnitude})</span>
                            {/if}
                          </a>
                        {/if}
                      </label>
                    </li>
                  {/if}
                {/foreach}
              </ul>
            {/block}

          {else}

            {block name='facet_item_dropdown'}
              <ul id="facet_{$_expand_id}" class="collapse{if !$_collapse} in{/if}">
                <li>
                  <div class="col-sm-12 col-xs-12 col-md-12 facet-dropdown dropdown">
                    <a class="select-title" rel="nofollow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {$active_found = false}
                      <span>
                        {foreach from=$facet.filters item="filter"}
                          {if $filter.active}
                            {$filter.label}
                            {if $filter.magnitude}
                              ({$filter.magnitude})
                            {/if}
                            {$active_found = true}
                          {/if}
                        {/foreach}
                        {if !$active_found}
                          {l s='(no filter)' d='Shop.Theme.Global'}
                        {/if}
                      </span>
                      <i class="material-icons float-xs-right">&#xE5C5;</i>
                    </a>
                    <div class="dropdown-menu">
                      {foreach from=$facet.filters item="filter"}
                        {if !$filter.active}
                          <a
                            rel="nofollow"
                            href="{$filter.nextEncodedFacetsURL}"
                            class="select-list"
                          >
                            {$filter.label}
                            {if $filter.magnitude}
                              ({$filter.magnitude})
                            {/if}
                          </a>
                        {/if}
                      {/foreach}
                    </div>
                  </div>
                </li>
              </ul>
            {/block}

          {/if}
          {/if}{* stopfilter *}

        </section>
      {/if}
    {/foreach}

    {*<section class="facet clearfix">

      <ul id="facet_livraison" class="collapse">
        <li>
          <label class="facet-label" for="facet_input_71746_0">
            <span class="custom-checkbox">
                <input id="#" type="checkbox">
                <span class="ps-shown-by-js"></span>
            </span>
            <a href="#" class="picto__retrait _gray-darker search-link js-search-link" rel="nofollow">Retrait en boulangerie</a>
          </label>
        </li>

        <li>
          <label class="facet-label" for="facet_input_71746_0">
            <span class="custom-checkbox">
                <input id="#" type="checkbox">
                <span class="ps-shown-by-js"></span>
            </span>
            <a href="#" class="picto__rdv _gray-darker search-link js-search-link" rel="nofollow">Livraison sur rendez-vous</a>
          </label>
        </li>

        <li>
          <label class="facet-label" for="facet_input_71746_0">
            <span class="custom-checkbox">
                <input id="#" type="checkbox">
                <span class="ps-shown-by-js"></span>
            </span>
            <a href="#" class="picto__coli _gray-darker search-link js-search-link" rel="nofollow">Livraison Colissimo</a>
          </label>
        </li>

      </ul>
    </section>*}
  </div>