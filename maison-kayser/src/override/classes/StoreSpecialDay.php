<?php

class StoreSpecialDay extends ObjectModel {

	/** @var int id_special_day */
    public $id_special_day = null;

    /** @var int Store ID */
    public $id_store = null;

    /** @var  date */
    public $date = null;

    /** @var boolean fermeture */
    public $fermeture = null;

    /** @var  json hours */
    public $hours = null;

    public static $nb_months_max = 2;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'store_special_days',
        'primary' => 'id_special_day',
        'fields' => array(
            'id_store' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
            'date' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'fermeture' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'hours' => array('type' => self::TYPE_STRING, 'validate' => 'isKayserHoraires'),
        ),
    );

    public static function getByDate($date, $id_store, $current_id = false) {

    	$result = false;

    	if(Validate::isDate($date)) {
    		$query = new DbQuery();
	        $query->select('id_special_day, id_store, date');
	        $query->from('store_special_days', 'ssd');
	        $query->where('ssd.id_store = '.(int) $id_store);
	        $query->where('ssd.date LIKE "'.$date.'%"');
            if($current_id) {
                $query->where('ssd.id_special_day <> '.$current_id);
            }

       		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query);
       	}

       	return $result;
    }

    public static function getMaxDate() {
    	$date = new DateTime();
		$date->add(new DateInterval('P'.self::$nb_months_max.'M'));
		return $date->format('Y-m-d');
    }

    public static function getDates($id_store = false) {
    	$date = self::getMaxDate();
    	$results = self::getAll($id_store, $date);
    	if($results) {
    		$dates = array();
    		foreach ($results as $key => $result) {
    			$dates[strtotime($result['date'])] = array(
    				'date' => $result['date'],
    				'fermeture' => $result['fermeture'],
    				'hours' => $result['hours'],
    			);
    		}
    		return $dates;
    	}
    	return false;
    }

    /**
     * [getAll recupére toutes les dates d'un magasin ou de tous les magasins]
     * @param  int $id_store [description]
     * @param  date $date_limit [date maxi à retourner]
     * @return [array]            [tableau contenant les jours enregistrés]
     */
    public static function getAll($id_store = false, $date_limit = false) {
    	$query = new DbQuery();
        $query->select('*');
        $query->from('store_special_days', 'ssd');

        if($id_store)
        	$query->where('ssd.id_store = '.(int) $id_store);

        if($date_limit && Validate::isDate($date_limit)) {
        	$query->where('ssd.date >= "'.date('Y-m-d').'"');
        	$query->where('ssd.date <= "'.$date_limit.'"');
        }

        $query->orderBy('ssd.date');

        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        if($results) {
        	$results_by_id = array();
        	foreach ($results as $key => $result) {
				$results_by_id[$result['id_special_day']] = $result;
        	}
        	return $results_by_id;
        }
        return false;
    }
}