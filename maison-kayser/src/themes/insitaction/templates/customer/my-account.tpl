{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

  {block name='page_title'}
    <div class="container title-header">
      <h1>{l s='Your account' d='Shop.Theme.Customeraccount'}</h1>
    </div>
  {/block}

  {block name='page_content'}
  <div class="container">
    <div class="row">
      <div class="links">

        <a class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="identity-link" href="{$urls.pages.identity}">
          <div class="content-bloc">
            <span class="link-item identity-link">
              {l s='Information' d='Shop.Theme.Customeraccount'}
            </span>
          </div>
        </a>

        {if $customer.addresses|count}
          <a class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="addresses-link" href="{$urls.pages.addresses}">
           <div class="content-bloc">
              <span class="link-item addresses-link">
                {l s='Addresses' d='Shop.Theme.Customeraccount'}
              </span>
            </div>
          </a>
        {else}
          <a class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="address-link" href="{$urls.pages.address}">
            <div class="content-bloc">
              <span class="link-item addresses-link">
                {l s='Add first address' d='Shop.Theme.Customeraccount'}
              </span>
            </div>
          </a>
        {/if}

        {if !$configuration.is_catalog}
          <a class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="history-link" href="{$urls.pages.history}">
            <div class="content-bloc">
              <span class="link-item history-link">
                {l s='Order history and details' d='Shop.Theme.Customeraccount'}
              </span>
            </div>
          </a>
        {/if}

        {* {if !$configuration.is_catalog}
          <a class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="order-slips-link" href="{$urls.pages.order_slip}">
            <div class="content-bloc">
              <span class="link-item order-slips-link">
                {l s='Credit slips' d='Shop.Theme.Customeraccount'}
              </span>
            </div>
          </a>
        {/if} *}

        {if $configuration.voucher_enabled && !$configuration.is_catalog}
          <a class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="discounts-link" href="{$urls.pages.discount}">
            <div class="content-bloc">
              <span class="link-item discounts-link">
                {l s='Vouchers' d='Shop.Theme.Customeraccount'}
              </span>
            </div>
          </a>
        {/if}

        {if $configuration.return_enabled && !$configuration.is_catalog}
          <a class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="returns-link" href="{$urls.pages.order_follow}">
            <div class="content-bloc">
              <span class="link-item">
                <i class="material-icons">&#xE860;</i>
                {l s='Merchandise returns' d='Shop.Theme.Customeraccount'}
              </span>
            </div>
          </a>
        {/if}

        {block name='display_customer_account'}
          {hook h='displayCustomerAccount'}
        {/block}

      </div>
    </div>
  {/block}


  {block name='page_footer'}
    {block name='my_account_links'}
      <div class="text-sm-center">
        <a href="{$logout_url}" >
          {l s='Sign out' d='Shop.Theme.Actions'}
        </a>
      </div>
    {/block}
    </div>
  {/block}

