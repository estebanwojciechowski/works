<?php

class Cart extends CartCore
{

	public $delivery_option_date;

	 public static $definition = array(
        'table' => 'cart',
        'primary' => 'id_cart',
        'fields' => array(
            'id_shop_group' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_address_delivery' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_address_invoice' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_currency' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_guest' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_lang' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'recyclable' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'gift' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'gift_message' => array('type' => self::TYPE_STRING, 'validate' => 'isMessage'),
            'mobile_theme' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'delivery_option' => array('type' => self::TYPE_STRING),
            'delivery_option_date' => array('type' => self::TYPE_STRING),
            'secure_key' => array('type' => self::TYPE_STRING, 'size' => 32),
            'allow_seperated_package' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

	/**
	 * [setDeliveryDate Ajoute la valeur du datepicker dans le panier]
	 * @param [type] $timestamp [description]
	 */
	public function setDeliveryDate($timestamp = null)
    {
        $this->delivery_option_date = null;

        if(!is_null($this->id_carrier) && !is_null($timestamp)) {
        	// verifie si la date est valide
        	$carrier = new Carrier($this->id_carrier);
        	if(Validate::isLoadedObject($carrier)) {
        		if($carrier->datepicker) {
	        		$min_delay = time() + $this->getCartDelay($carrier);
	        		$diff = $timestamp - $min_delay;
	        		// si on est dans le cas < 0 on set le min_delay
	        		// = inactif sur la page de choix
        			if($diff > 0)
        				$this->delivery_option_date = $timestamp;
        			else
        				$this->delivery_option_date = $min_delay;
	        	}
        	}
        }
    }

	/**
	 * [getCartDelay retourne le temps nécessaire pour le produit qui a le plus grand délai
	 * avec le temps de preparation si le transporteur est passé en paramètre
	 * et si il y a un magasin, vérifie que le délai arrive sur un jour d'ouverture du magasin]
	 * @param  [Carrier] $carrier [obj carrier ou null]
	 * @return [type]          [temps de prepa necessaire ou false si impossible]
	 */
	public function getCartDelay($carrier = null) {
        //si on ne set pas la timezone ici on est en UTC
        date_default_timezone_set(Configuration::get('PS_TIMEZONE'));
		$attribute = false;
	    $context = Context::getContext();
		// si on a le transporteur on peut ajouter le delai de préparation
		if(Validate::isLoadedObject($carrier)
			&& $carrier->is_module
				&& $carrier->external_module_name == 'kayserdelivery'
		) {

	        if(Validate::isLoadedObject($context->shop)) {
	        	// Si boutique principal on active les horaires du Louvre
		        $store_default = Configuration::get('KAYSER_DEFAULT_STORE');
		        if(!$store_default) {
		            $store_default = 100001016;
		        }

		        if($context->shop->id == 1) {
		            $store = new Store($store_default);
		        } else {
		            $store = new Store($context->shop->id);
		        }

				//quel est l'attribut utilisé par le transporteur
				$attribute = $carrier->getDeliveryAttribute($this);

				$clickncollect = false;
				if($attribute && isset($attribute['field'])
		                && isset($attribute['object']) && $attribute['object'] == 'product') {
		            // determine si on se basera sur les horaires du click and collect
		            // ou de l'ouverture magasin
		            $clickncollect = ($attribute['field'] == 'delai_clickandcollect');
		        }

		        $with_refab = true;
	            if(Validate::isLoadedObject($store)) {
	            	$with_refab = false;
	            }

				$products = $this->getProducts();
				if(is_array($products) && count($products)) {
					$max_product_cart_delay = $max_refab_delay = $max_morning_refab_delay = 0;
					foreach ($products as $key => $product) {
						$obj = new Product($product['id_product']);
						if(Validate::isLoadedObject($obj)) {
							// recupére le delai du produit + le temps de préparation
							// si un attribut est présent pour ce transporteur
							$product_delay = $obj->getProductDelay($attribute, $with_refab);

							if($product_delay >=0) {
								if($product_delay > $max_product_cart_delay)
									$max_product_cart_delay = $product_delay;

								// recupération du délai de refabrication
								// si la boulangerie est fermée au moment de la date
								// on ajoute le délai de refabrication à la prochaine ouverture
								$product_refab_delay = $obj->getRefabDelay();
								$product_morning_refab_delay = $obj->getRefabDelay(true);

								if($product_refab_delay > $max_refab_delay) {
									$max_refab_delay = $product_refab_delay;
								}

								if($product_morning_refab_delay > $max_morning_refab_delay) {
									$max_morning_refab_delay = $product_morning_refab_delay;
								}

							} else {
								//si product_delay = false alors l'attribut n'est pas autorisé pour ce transporteur
								return false;
							}
						}
					}

		            // verification des horaires d'ouverture du magasin
		            // le jour de livraison ou collecte prévu
	            	if(Validate::isLoadedObject($store)) {
	            		$max_product_cart_delay = $store->getDelayWithRefab($max_product_cart_delay, $max_refab_delay, $max_morning_refab_delay, $clickncollect);
	            	}

					// Traitement particulier pour la livraison en voiture
					if(isset($attribute['field']) &&  $attribute['field'] == 'delai_livraison_voiture'
						&& isset($attribute['object']) && $attribute['object'] == 'product') {
			            // Est-ce que le délai de livraison prévu tombe un jour de livraison voiture disponible ?
			           	$max_product_cart_delay += Carrier::getCarDeliveryDelay($max_product_cart_delay);
			        }

		            // on met une valeur d'au moins 10 minutes (600s)
		            // pour que le client ait le temps de valider le panier

		            $min_delay = Configuration::get('MIN_CART_DELAY');
		            if(!$min_delay) {
			        	$min_delay = 600;
		            }
		            if($max_product_cart_delay < $min_delay)
		            	return $min_delay;
					return $max_product_cart_delay;
				}
			}
		}
		return false;
	}

	/**
	 * [changeCartShop change le panier de boutique
	 * remet en stock sur la boutique d'origine
	 * puis ajoute les produits dans le context de la nouvelle boutique]
	 * @param  [type] $id_shop         [description]
	 * @param  array  $remove_products [liste d'id produits a delete]
	 * @return [type]                  [description]
	 */
	public function changeShop($id_shop, $remove_products = array()) {
		$cart = new Cart();
		$shop = new Shop($id_shop);

		$products = $this->getProducts();

		if(!is_array($products) || empty($products))
			return true;

		// On filtre les produits du panier
		// les clés du tableau $remove_products sont de la forme (id_product)_(id_pa)
		$remove_id_pa = array_keys($remove_products);
		foreach ($products as $key => $product) {
			$identifier = $product['id_product'].'_'.$product['id_product_attribute'];
			if(in_array($identifier, $remove_id_pa)) {
				unset($products[$key]);
			}
		}


		if(Validate::isLoadedObject($shop)) {
			$context = Context::getContext();
			$cart->id_shop = $id_shop;
			$cart->id_shop_group = $shop->id_shop_group;
			$cart->id_currency  = $this->id_currency;
			$cart->id_customer  = $this->id_customer;
			$cart->secure_key  = $this->secure_key;
			//creation du nouveau panier
			if($cart->add()) {

				$cart->setCartToCookie($shop);

				if(is_array($products) && !empty($products)) {
					foreach ($products as $key => $product) {
						//ajout du produit au nouveau panier
						$cart->updateQty(
							        $product['quantity'],
							        $product['id_product'],
							        $product['id_product_attribute'],
							        $product['id_customization'],
							        $operator = 'up',
							        $this->id_address_delivery = 0,
							        $shop,
							        true
							    );
					}
				}
				//suppression de l'ancien panier
				return $this->delete();
			}
		}

		return false;
	}

    protected function setCartToCookie($shop) {
    	/* Instantiate cookie */
		$cookie_lifetime = defined('_PS_ADMIN_DIR_') ? (int)Configuration::get('PS_COOKIE_LIFETIME_BO') : (int)Configuration::get('PS_COOKIE_LIFETIME_FO');
		if ($cookie_lifetime > 0) {
		    $cookie_lifetime = time() + (max($cookie_lifetime, 1) * 3600);
		}
    	$force_ssl = Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE');
	    if ($shop->getGroup()->share_order) {
	        $cookie = new Cookie('ps-sg'.$shop->getGroup()->id, '', $cookie_lifetime, $shop->getUrlsSharedCart(), false, $force_ssl);
	    } else {
	        $domains = null;
	        if ($shop->domain != $shop->domain_ssl) {
	            $domains = array($shop->domain_ssl, $shop->domain);
	        }

	        $cookie = new Cookie('ps-s'.$shop->id, '', $cookie_lifetime, $domains, false, $force_ssl);
	    }

		$cookie->__set('id_cart', $this->id);

    }

    public function hasGlutenFreeProduct() {
		$products = $this->getProducts();
		//sans gluten feature group id = 10
    	foreach ($products as $key => $product) {
    		$features = Product::getFeaturesStatic($product['id_product']);
    		if(!empty($features)) {
    			foreach ($features as $key => $feature) {
    				if($feature['id_feature'] == 10)
    					return true;
    			}
    		}
    	}
    	return false;
    }
}
