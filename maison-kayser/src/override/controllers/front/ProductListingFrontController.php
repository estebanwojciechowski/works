<?php

abstract class ProductListingFrontController extends ProductListingFrontControllerCore
{

	protected function getAjaxProductSearchVariables()
    {
        $search = $this->getProductSearchVariables();
        $context = Context::getContext();
        $link = ($context ? $context->link : new Link);
        $rendered_products_top = $this->render('catalog/_partials/products-top', array('listing' => $search));
        $rendered_products = $this->render('catalog/_partials/products', array('listing' => $search, 'link' => $link));
        $rendered_products_bottom = $this->render('catalog/_partials/products-bottom', array('listing' => $search));

        $data = array(
            'rendered_products_top' => $rendered_products_top,
            'rendered_products' => $rendered_products,
            'rendered_products_bottom' => $rendered_products_bottom,
        );

        foreach ($search as $key => $value) {
            if ($key === 'products') {
                $value = $this->prepareProductArrayForAjaxReturn($value);
            }
            $data[$key] = $value;
        }

        return $data;
    }

}