import $ from 'jquery';

export default class Produit {
    
    constructor(el) {}

    init(){

        function hoverElement(picto, deli_info) {

            var picto = '.delivery__pictos ' + picto;

            $(picto).hover(function() {

                if(!$(this).hasClass('disabled')) {

                    $('.retrait_boulangerie_info').removeClass('is--open');   
                    $('.livraison_rdv_info').removeClass('is--open');   
                    $('.livraison_coli_info').removeClass('is--open');   

                    var $parent = $(this).closest('.product-miniature');
                    $($parent).find(deli_info).addClass('is--open');
                }
            });

            $('.product-miniature').hover(function() {
            }, function() {

                $(this).find(deli_info).removeClass('is--open');         
            });
        }

        function hoverAllElement() {

            hoverElement('.retrait__boulangerie', '.retrait_boulangerie_info');
            hoverElement('.livraison__rdv', '.livraison_rdv_info');
            hoverElement('.livraison__coli', '.livraison_coli_info');
        }
   
       // if($('body').is('#category')) {

            hoverAllElement();

            /* TO DO : infinity scroll */
            /* $( window ).load(function() {
                hoverAllElement();
            });*/
        //}

        /*
        $('.delivery__pictos span.retrait__boulangerie, .retrait_boulangerie_info').hover((e) => {
            this.hoverPictoProduct($(e.currentTarget));
        }, (e) => {
            this.noHoverPictoProduct($(e.currentTarget));
        });*
        */
    }

    /*
    hoverPictoProduct(element){
        let $parent = element.closest('.product-miniature');

        if(!$(element).hasClass('disabled'))    
        {
            $('.retrait_boulangerie_info', $parent).addClass('is--open');
        } else { 
            return false;
        }
    }

    noHoverPictoProduct(element){
        let $parent = element.closest('.product-miniature');
        $('.retrait_boulangerie_info', $parent).removeClass('is--open');
    }
    */
}