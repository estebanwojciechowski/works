{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="blockcart-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title h6 text-sm-center" id="myModalLabel">{l s='Vous devez choisir une boulangerie pour acheter ce produit' d='Shop.Theme.Checkout'}</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">
            <div class="cart-content-btn">
                <div class="">
                  <p class="h3">
                    {l s='Vous n\'avez pas choisi de boulangerie et ce produit ne peut pas être livré en colissimo.' d='Shop.Theme.Checkout'}
                  </p>
                  <p class="h4">
                    {l s='Pour ajouter ce produit choisissez une boulangerie !' d='Shop.Theme.Checkout'} 
                  </p>
                </div> 
                <br/>
                <div class="cart-content-btn">
                  <div class="row">

                    <!--div class="col-sm-6" style="text-align:center">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">{l s='Continue shopping' d='Shop.Theme.Actions'}</button>                      
                    </div-->

                    <div class="col-sm-12" style="text-align:center">
                      <a href="{$store_url}?cnc=1{if $back}&back={urlencode($back)}{/if}" class="btn btn-primary"><i class="material-icons">&#xE876;</i>{l s='Choisir une boulangerie' d='Shop.Theme.Actions'}</a>
                    </div>
                  </div>
                </div> 
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
