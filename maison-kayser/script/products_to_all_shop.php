<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Copie les produits existants sur les boutiques ou ils n'existent pas seulement
 * pour eviter d'ecraser les informations existantes 
 */

class CopyExistingProductsToAllShops {

    private $_logger;

    protected $errors = array();


    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        // ajout d'un compte admin pour permettre de charger les produits
        $context->employee = new Employee(1);
        define('_PRODUCT_SCRIPT_', true);
        // hack pour charger le shop même si désactivé lors de la sauvegarde
        // (OVERRIDE Shop::initialize())
        define('_PS_ADMIN_DIR_', true);

        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
        
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {
      $sql = 'SELECT * FROM `'._DB_PREFIX_.'product`';
      $results = DB::getInstance()->executeS($sql); 
      if(is_array($results) && $results) {
        $this->log(count($results).' Produits trouvés');

        $sql = 'SELECT id_shop FROM `'._DB_PREFIX_.'shop`';
        $shops = DB::getInstance()->executeS($sql); 
        $shops = array_column($shops, 'id_shop');
        //histoire d'avoir le shop 1 au premier index
        sort($shops);

        $context = Context::getContext();
        // on passe en context shop pour inserer les produits 
        // (cf classe ObjectModel methode update)
        
        foreach ($results as $key => $result) {
          $sql = 'SELECT id_shop FROM `'._DB_PREFIX_.'product_shop`
                  WHERE id_product = '.$result['id_product'];
          $product_shop_ids = DB::getInstance()->executeS($sql);
          $product_shop_ids = array_column($product_shop_ids, 'id_shop');
          // le produit est affilié à toutes les boutiques
          if(count($shops) == count($product_shop_ids)) {
            $this->log('Produit #'.$result['id_product'].' présent dans toutes les boutiques.');
            continue;
          }
          $add_to_shops = array_diff($shops, $product_shop_ids);
          $existing_shop_id = array_intersect($shops, $product_shop_ids);
          $existing_shop_id = current($existing_shop_id);
          $context->shop = new Shop($existing_shop_id);  
          if(Validate::isLoadedObject($context->shop)) {
            Shop::setContext(Shop::CONTEXT_SHOP, $existing_shop_id); 
            // chrgt product dans un context ou il existe 
            // pour obtenir toutes les infos nécessaires à sa création
            $product = new Product($result['id_product'], true);
            if(!Validate::isLoadedObject($product)) {
                $this->log('ERREUR : impossible de charger le Produit#'.$result['id_product']);
                continue;
            }
            $this->log('Produit#'.$result['id_product'].' chargé dans le Context Shop#'.$existing_shop_id);
            $carriers = $product->getCarriers();
            $carriers = array_column($carriers, 'id_reference');
            $combinations = false;
            if($product->hasCombinations()) {
              $combinations = $product->getWsCombinations();
              $combinationsObj = array();
              foreach ($combinations as $key => $combination) {
                $combinationsObj[] = new Combination($combination['id']);
              }
            }
          } else {
            $this->log('ERREUR : impossible d\'initialiser le produit#'.$result['id_product'].'dans le context Shop#'.$id_shop);
          }
          
          foreach ($add_to_shops as $key => $id_shop) {
            $context->shop = new Shop($id_shop);  
            Shop::setContext(Shop::CONTEXT_SHOP, $id_shop); 
            if(Validate::isLoadedObject($context->shop)) {
                $this->log('Context Shop#'.$id_shop);
                $reactivation = false;
                if($id_shop == 1 && $product->active) {
                  // si le produit n'était pas présent sur la boutique main
                  // on le crée en le désactivant
                  $product->active = 0;
                  $reactivation = true;
                }
                if($product->copyProductToOtherShop($id_shop)) {
                    $this->log('SUCCESS : MAJ Produit#'.$result['id_product']);
                    $product->setCarriers($carriers);
                    //ajout des stocks
                    StockAvailable::setProductOutOfStock($result['id_product'], $product->out_of_stock, $id_shop, 0);
                    if($combinations) {
                      foreach ($combinationsObj as $key => $combination) {
                        if($combination->save()){
                          $this->log('Combination #'.$combination->id.' ajoutée.');
                        } else {
                          $this->log('ERREUR Combination #'.$combination->id.' non ajoutée.');
                        } 
                        StockAvailable::setProductOutOfStock($result['id_product'], $product->out_of_stock, $id_shop, $combination->id);
                      }
                    }
                } else {
                    $this->log('ERREUR : MAJ Produit#'.$result['id_product']);
                }                
                if($id_shop == 1 && $reactivation) {
                  // reactivation
                  $product->active = 1;
                }
            } else {
                $this->log('ERREUR : impossible de charger le context Shop#'.$id_shop);
            }
          }
        }
      }
      // liaisons images shops
      // Set les cover a null car les 0 empeche les insertions (duplicate key)
      Db::getInstance()->execute('
          UPDATE `'._DB_PREFIX_.'image_shop`
          SET `cover` = NULL WHERE `cover` = 0
      ');
      //recupération des images sans faire attention aux shops
      $images = Db::getInstance()->executeS('
        SELECT i.`cover`, i.`id_image`, i.`id_product`, i.`position`
        FROM `'._DB_PREFIX_.'image` i'
      );
      foreach ($images as $key => $image) {
        // recupération des id_shop et cover par shop
        $existInShops =  Db::getInstance()->executeS('
          SELECT i.`id_shop`, i.`cover`
          FROM `'._DB_PREFIX_.'image_shop` i
          WHERE i.`id_image` = '.$image['id_image']
        );        
        if($existInShops) {
          $existantes = array_column($existInShops, 'id_shop'); 
          // ajouter pour les shop non existants dans la bdd
          $add_to_shops = array_diff($shops, $existantes);
          if(count($add_to_shops)) {
            $sql = 'INSERT INTO `'._DB_PREFIX_.'image_shop`
                    (`id_product`, `id_image`, `id_shop`, `cover`) 
                    VALUES ';
            foreach ($add_to_shops as $key => $id_shop) {
              if($id_shop > 0) {
                if($image['cover']) {
                  // PREVENT DUPLICATE COVER = 1
                  // met les covers des image du shop courant a null
                  Db::getInstance()->execute('
                      UPDATE `'._DB_PREFIX_.'image_shop`
                      SET `cover` = NULL 
                      WHERE `id_product` = "'.$image['id_product'].'"
                      AND `id_shop` = "'.$id_shop.'"'
                  );                           
                }
                // construction de la requete a executer
                $sql .= ' ("'.$image['id_product'].'", "'.$image['id_image'].'", "'.$id_shop.'", '.(!$image['cover'] ? 'NULL' : '"1"').'),';
              }
            }
            try {
              if(Db::getInstance()->execute(rtrim($sql, ',').';')) {
                $this->log('SUCCESS MAJ Images#'.$image['id_image']);
              } else {
                $this->log('ERREUR MAJ Images#'.$image['id_image']);
              }              
            } catch (Exception $e) {
              $this->log($e->getMessage());
            }            
          }
        }
      }
    }

}

$timestamp_debut = microtime(true);

$import = new CopyExistingProductsToAllShops();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
echo 'Exécution du script : ' . $difference_ms . ' secondes.'."\n";
  