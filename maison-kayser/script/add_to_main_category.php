<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Class d'import produit initial // pas de maj détruit toute les données si le test du csv est bon
 */
class AddProductsToMainCategory {

    protected $default_id_lang;

    private $_logger;

    protected $errors = array();


    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        // $context = Context::getContext();
        // $context->shop = new Shop();  
        // Shop::setContext(Shop::CONTEXT_ALL);  
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {
      $sql = 'SELECT id_category, id_product FROM `'._DB_PREFIX_.'category_product`';
      $results = DB::getInstance()->executeS($sql); 
      $products_categories = array();
      if(is_array($results) && $results) {
        $categories = array();
        foreach ($results as $key => $result) {          
          $id_product = $result['id_product'];
          //recherche des parents de la categorie en cours
          if(!isset($categories[$result['id_category']])) {
              $categories[$result['id_category']] = self::getParentsCategories($result['id_category']);
          }

          if(!isset($products_categories[$id_product])) {
            $products_categories[$id_product] = array($result['id_category']);
          } elseif(!in_array($result['id_category'], $products_categories)) {
            $products_categories[$id_product][] = $result['id_category'];            
          }

          if(count($categories[$result['id_category']])) {
            foreach ($categories[$result['id_category']] as $key => $value) {
              if(!in_array($value, $products_categories[$id_product])) {
                $products_categories[$id_product][] = $value;            
              }
            }
          }          
        }
        if(count($products_categories)) {
          foreach ($products_categories as $id_product => $categories) {
            $product = new Product($id_product);
            if(Validate::isLoadedObject($product)) {
              if($product->addToCategories($categories)) {
                $this->log('Produit #'.$id_product.' ajouté àux categories ID ('.implode(', ', $categories).')');
              } else {
                $this->log('ERREUR Produit #'.$id_product.' non ajouté aux catégories');
              }
            }
          }
        }
      }    
    }

    public static function getParentsCategories($id_category, &$categories = array(), $first = true) {
      if($first) {
        $categories = array();
      }
      $category = new Category($id_category);
      if(Validate::isLoadedObject($category) 
         && $category->id_parent >= 2) {
        $categories[] = $category->id_parent;
        return self::getParentsCategories($category->id_parent, $categories, false);
      } else { 
        return $categories;
      }

    }
}

$timestamp_debut = microtime(true);

$import = new AddProductsToMainCategory();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$import->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");
  