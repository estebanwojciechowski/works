<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04.09.15
 * Time: 20:33
 */
require_once(dirname(__FILE__) . '/../../classes/faqsPost.php');

class faqsAjaxFormModuleFrontController extends FrontController
{
  public function initContent()
  {
    if (!$this->ajax) {
      parent::initContent();
    }
  }

  public function displayAjax()
  {
     $json = array();

     try {
        if (Tools::getValue('action') == 'send'){
          $captcha_session = Tools::strtolower(Context::getContext()->cookie->_CAPTCHA);
          $name = Tools::getValue('name');
          $email = Tools::getValue('email');
          $category = Tools::getValue('category');
          $question = Tools::getValue('question');
          $id_lang = Tools::getValue('id_lang');

          if(!$name){
            throw new Exception ('name_customer:' . Module::getInstanceByName('faqs')->l('Name is required', 'AjaxForm'));
          }

          if(!$email){
            throw new Exception ('email_customer:' . Module::getInstanceByName('faqs')->l('E-mail is required', 'AjaxForm'));
          } elseif ($email && !Validate::isEmail($email)) {
            throw new Exception ('email_customer:' . Module::getInstanceByName('faqs')->l('E-mail is not valid', 'AjaxForm'));
          }

          if(!$question){
            throw new Exception ('question_customer:' . Module::getInstanceByName('faqs')->l('Question is required', 'AjaxForm'));
          }

          if (Configuration::get('FAQS_ENABLE_CAPTCHA') == 1) {
            $captcha = Tools::strtolower(Tools::getValue('captcha'));

            if($captcha_session !== $captcha || !$captcha){
              throw new Exception ('captcha_res:' . Module::getInstanceByName('faqs')->l('Captcha is not valid', 'AjaxForm'));
            }
          }
          $json['form'] = 'response';
          $set = $this->setQuestion($name, $email, $category, $question);
          $message = $this->addQuestion($name, $email, $category, $question, $id_lang);

          if ($set && $message){
            $json['form'] = $this->notificationForm(Module::getInstanceByName('faqs')->l('Please, check your mailbox from time to time. We will respond you as soon as possible.'), 'success');
          } else {
            $json['form'] = $this->notificationForm(Module::getInstanceByName('faqs')->l('Something went wrong... Can you try again?'), 'error');
          }
        }

        if (Tools::getValue('action') == 'showForm'){
           $json['form'] = $this->getFreeCallForm(Tools::getValue('id_lang'), Tools::getValue('id_shop')) ;
        }

        die( json_encode($json) );
      } catch(Exception $e){
        $error_info = explode(':', $e->getMessage());
        $json['error_field'] = $error_info[0];
        $json['error_message'] = $error_info[1];

        if( $e->getCode() == 10 ){
          $json['error_message'] = $e->getMessage();
        }
      }

      die(json_encode($json));
  }

  public function notificationForm($msg, $status){
    $tpl_path = Module::getInstanceByName('faqs')->absolutePathToFrontTemplates . 'notification.tpl';
    $data = Context::getContext()->smarty->createTemplate($tpl_path);
    $data->assign(
      array(
        'status' => $status,
        'msg' => $msg,
      )
    );

    return $data->fetch();
  }

  public function addQuestion($name, $email, $category, $question, $id_lang) {
    $email_admin = Configuration::get('FAQS_EMAILS');
    $emails = explode(',', $email_admin);

    foreach($emails as $send_to) {
      $template_vars = $this->templateMail($name, $email, $category, $question, $id_lang);
      $template_vars = array('{content}' => $template_vars);
      $this->sendMessage($template_vars, trim($send_to), $email);
    }

    return true;
  }

  public function sendMessage($template_vars, $send_to, $email) {
    $mail = Mail::Send(
      Configuration::get('PS_LANG_DEFAULT'),
      'faqs',
      Module::getInstanceByName('faqs')->l('New question'),
      $template_vars,
      "$send_to",
      NULL,
      $email ? $email : NULL,
      NULL,
      NULL,
      NULL,
      dirname(__FILE__).'/../../mails/');
    return $mail;
  }

  public function templateMail($name, $email, $category, $question, $id_lang) {
    $tpl_path = Module::getInstanceByName('faqs')->absolutePathToFrontTemplates . 'templateMail.tpl';
    $data = Context::getContext()->smarty->createTemplate($tpl_path);
    $baseUrl = _PS_BASE_URL_SSL_.__PS_BASE_URI__;
    $logo = self::$link->getMediaLink(_PS_IMG_.Configuration::get('PS_LOGO'));
    $obj = new faqsCategory($category, $id_lang);

    $data->assign(
      array(
        'logo_url'     =>  $logo,
        'baseUrl'      => $baseUrl,
        'name'      => $name,
        'email'      => $email,
        'category'      => $obj->name,
        'question'      => $question,
        'id_lang'      => $id_lang,
      )
    );
    return $data->fetch();
  }

  public function setQuestion($name, $email, $category, $question) {
    $faqsPost = new faqsPost();
    $faqsPost->active = 0;
    $faqsPost->most = 0;
    $faqsPost->as_url = 0;
    $faqsPost->association = 0;
    $faqsPost->id_gomakoil_faq_category = $category;

    $this->position = (int)faqsPost::getLastPostPosition($category) + 1;

    $faqsPost->by_customer = 1;
    $faqsPost->name = $name;
    $faqsPost->email = $email;
    $languages = Language::getLanguages(false);
    foreach ($languages as $lang){
      $faqsPost->question[$lang['id_lang']] = $question;
      $faqsPost->link_rewrite[$lang['id_lang']] = Tools::truncate(Tools::str2url($question), 125, '');
    }
    
    return $faqsPost->save();
  }

  public function getFreeCallForm($id_lang, $id_shop) {
    $tpl_path = Module::getInstanceByName('faqs')->absolutePathToFrontTemplates . 'form.tpl';
    $data = Context::getContext()->smarty->createTemplate($tpl_path);
    $faqCategories = faqsCategory::getCategoriesFaq(Context::getContext()->shop->id, Context::getContext()->language->id);

    $captcha = false;

    if (Configuration::get('FAQS_ENABLE_CAPTCHA') == 1) {
      $captcha = _PS_BASE_URL_SSL_.__PS_BASE_URI__.'modules/faqs/secpic.php';
    }

    $data->assign(
      array(
        'id_shop'           => $id_shop,
        'id_lang'           => $id_lang,
        'faqCategories'     => $faqCategories,
        'captcha_url'       => $captcha,
        'base_url'          => _PS_BASE_URL_SSL_.__PS_BASE_URI__,
      )
    );

    return $data->fetch();
  }

}