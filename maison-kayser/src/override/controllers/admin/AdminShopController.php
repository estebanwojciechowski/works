<?php
class AdminShopController extends AdminShopControllerCore
{

	public function postProcess()
    {
    	$id_shop = Tools::getValue('identifiant');
        if($id_shop) {
        	if(Validate::isUnsignedInt($id_shop)
                && Validate::isUnsignedInt($id_shop)        
	        ) {
	        	$new_shop = new Shop($id_shop);
	            if(Validate::isLoadedObject($new_shop)) {
	                $link = Context::getContext()->link->getAdminLink('AdminShop', true, array(), array('id_shop' => $id_shop, 'updatestore' => 1));
	                $this->errors[] = 'L\'identifiant '.$id_shop.' est déjà utilisé. <a href="'.$link.'" target="_blank">Voir</a>';
	            }
	        } else {
	            $this->errors[] = 'L\'identifiant '.$id_shop.' est invalide (nombre entier seulement).';
	        }
    	}
    	return parent::postProcess();

    }
    
    public function renderForm()
    {
    	$renderForm = parent::renderForm();

    	if(Tools::getValue('shop_id')) {
    		return $renderForm;
    	}

    	$id_field = array(
            'type' => 'text',
            'label' => $this->trans('ID', array(), 'Admin.Shopparameters.Feature'),
            'desc' => array($this->trans('Laisser vide si vous ne souhaiter pas définir l\'ID.', array(), 'Admin.Shopparameters.Help')),
            'name' => 'identifiant',
            'required' => false,
        );

    	array_unshift($this->fields_form[0]['form']['input'], $id_field);

    	$this->fields_value['identifiant'] = Tools::getValue('identifiant') ? Tools::getValue('identifiant') : '';
        $this->tpl_form_vars['identifiant'] = '';

    	$helper = new HelperForm($this);
        $this->setHelperDisplay($helper);
        $helper->fields_value = $this->fields_value;
        $helper->submit_action = $this->submit_action;
        $helper->tpl_vars = $this->getTemplateFormVars();
        $helper->show_cancel_button = (isset($this->show_form_cancel_button)) ? $this->show_form_cancel_button : ($this->display == 'add' || $this->display == 'edit');

        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) {
            $back = self::$currentIndex.'&token='.$this->token;
        }
        if (!Validate::isCleanHtml($back)) {
            die(Tools::displayError());
        }

        $helper->back_url = $back;
        !is_null($this->base_tpl_form) ? $helper->base_tpl = $this->base_tpl_form : '';
        if ($this->access('view')) {
            if (Tools::getValue('back')) {
                $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue('back'));
            } else {
                $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue(self::$currentIndex.'&token='.$this->token));
            }
        }
        $form = $helper->generateForm($this->fields_form);

        return $form;
    }

	protected function beforeAdd($object)
    {
        $id_shop = Tools::getValue('identifiant');
        if($id_shop) {
            $object->id =$id_shop;
            $object->force_id = true;
    	}
    }
}

?>