<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Foundation\Database\EntityManager;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Ps_EmailsubscriptionOverride extends Ps_Emailsubscription
{

    public function __construct(EntityManager $entity_manager)
    {
        parent::__construct($entity_manager);
    }

    public function install()
    {
        if (
            !parent::install()
            || !Configuration::updateValue('PS_NEWSLETTER_RAND', rand().rand())
            || !$this->registerHook(
                array(
                    'displayFooterBefore',
                    'actionCustomerAccountAdd',
                    // ADD BY ESTEBANW INSITACTION 15052018
                    'actionCustomerAccountUpdate', 
                    // FIN ADD
                    'additionalCustomerFormFields',
                )
            )
        ) {
            return false;
        }

        Configuration::updateValue('NW_SALT', Tools::passwdGen(16));

        $conditions = array();
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $conditions[(int) $lang['id_lang']] = $this->getConditionFixtures($lang);
        }
        Configuration::updateValue('NW_CONDITIONS', $conditions, true);

        return Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'emailsubscription` (
            `id` int(6) NOT NULL AUTO_INCREMENT,
            `id_shop` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
            `id_shop_group` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',
            `email` varchar(255) NOT NULL,
            `newsletter_date_add` DATETIME NULL,
            `ip_registration_newsletter` varchar(15) NOT NULL,
            `http_referer` VARCHAR(255) NULL,
            `active` TINYINT(1) NOT NULL DEFAULT \'0\',
            PRIMARY KEY(`id`)
        ) ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8');
    }

    /**
     * [hookActionCustomerAccountUpdate Lors de la modification d'un client si un changement
     * est appliqué sur la newsletter effectue le traitement necessaire]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function hookActionCustomerAccountUpdate($params) {
        $customer = $params['customer'];
        if($customer->newsletter == false) {
            $customer->newsletter_date_add = null;
            $customer->ip_registration_newsletter = null;
            $customer->optin = false;
            $customer->update();
            
            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'emailsubscription` WHERE email=\''.pSQL($customer->email)."'");
        } else {
            if(is_null($customer->ip_registration_newsletter)) {
                // ADD BY ESTEBANW INSITACTION 14052018            
                // SI NEWSLETTER ENREGISTREE CA BLOQUE L'INSCRIPTION PAR LE MODULE  
                Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer SET newsletter = 0 WHERE email=\''.pSQL($customer->email)."'");
                // END
                //si pas d'ip envoie la confirmation
                $action = false;
                if(isset($_POST['action'])) {
                    $action = $_POST['action'];
                }
                $_POST['action'] = 0;
                $result = $this->newsletterRegistration();
                if($action) {
                    $_POST['action'] = $action;
                } else {
                    unset($_POST['action']);
                }
            }
        }
    }

    public function hookAdditionalCustomerFormFields($params)
    {
        $label = $this->trans(
            'Sign up for our newsletter[1][2]%conditions%[/2]',
            array(
                '[1]' => '<br>',
                '[2]' => '<em>',
                '%conditions%' => Configuration::get('NW_CONDITIONS', $this->context->language->id),
                '[/2]' => '</em>',
            ),
            'Modules.Emailsubscription.Shop'
        );

        return array(
            (new FormField())
                ->setName('newsletter')
                ->setType('checkbox')
                ->setLabel($label)
                ->addAvailableValue('nolabel', true)
        );
    }


    /**
     * Deletes duplicates email in newsletter table.
     *
     * @param $params
     *
     * @return bool
     */
    public function hookActionCustomerAccountAdd($params)
    {
        //if e-mail of the created user address has already been added to the newsletter through the ps_emailsubscription module,
        //we delete it from ps_emailsubscription table to prevent duplicates
        $id_shop = $params['newCustomer']->id_shop;
        $email = $params['newCustomer']->email;
        // ADD BY ESTEBANW INSITACTION 14052018
        $newsletter = $params['newCustomer']->newsletter;
        // END
        if (Validate::isEmail($email)) {
            Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'emailsubscription WHERE email=\''.pSQL($email)."'");
            // ADD BY ESTEBANW INSITACTION 14052018            
            // SI NEWSLETTER ENREGISTREE CA BLOQUE L'INSCRIPTION PAR LE MODULE  
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer SET newsletter = 0 WHERE email=\''.pSQL($email)."'");
            // END
        }
        // ADD BY ESTEBANW INSITACTION 14052018
        $newsletter = $params['newCustomer']->newsletter;
        if($newsletter) {
            $action = false;
            if(isset($_POST['action'])) {
                $action = $_POST['action'];
            }
            $_POST['action'] = 0;
            $result = $this->newsletterRegistration();
            if($action) {
                $_POST['action'] = $action;
            } else {
                unset($_POST['action']);
            }
        }
        // END
        return true;
    }

    public function newsletterInscription() {
        return $this->newsletterRegistration();
    }

    protected function registerUser($email)
    {
        $sql = 'UPDATE '._DB_PREFIX_.'customer
                SET `newsletter` = 1, `optin` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.pSQL(Tools::getRemoteAddr()).'\'
                WHERE `email` = \''.pSQL($email).'\'';

        return Db::getInstance()->execute($sql);
    }

    public function confirmEmail($token)
    {
        $activated = false;
        
        if ($email = $this->getGuestEmailByToken($token)) {
            $activated = $this->activateGuest($email);
            if($activated) {
                $activated = $this->registerUser($email);    
            }            
        } elseif ($email = $this->getUserEmailByToken($token)) {
            $activated = $this->registerUser($email);
        }

        if (!$activated) {
            return $this->trans('This email is already registered and/or invalid.', array(), 'Modules.Emailsubscription.Shop');
        }

        if ($discount = Configuration::get('NW_VOUCHER_CODE')) {
            $this->sendVoucher($email, $discount);
        }

        if (Configuration::get('NW_CONFIRMATION_EMAIL')) {
            $this->sendConfirmationEmail($email);
        }

        return $this->trans('Thank you for subscribing to our newsletter.', array(), 'Modules.Emailsubscription.Shop');
    }

    // fonctions privées conservées non modifiées
    
    private function getCMSRoles()
    {
        $cms_repository = $this->entity_manager->getRepository('CMS');
        $id_lang = Context::getContext()->employee->id_lang;
        $id_shop = Context::getContext()->shop->id;
        $cms_pages = array();

        $fake_object = new stdClass();
        $fake_object->id = 0;
        $fake_object->name = $this->trans('-- Select associated page --', array(), 'Modules.Emailsubscription.Admin');
        $cms_pages[-1] = $fake_object;
        unset($fake_object);

        foreach ($cms_repository->i10nFindAll($id_lang, $id_shop) as $cms_page) {
            $object = new stdClass();
            $object->id = $cms_page->id;
            $object->name = $cms_page->meta_title;
            $cms_pages[] = $object;
        }

        return $cms_pages;
    }

    private function getCustomers()
    {
        $id_shop = false;

        // Get the value to know with subscrib I need to take 1 with account 2 without 0 both 3 not subscrib
        $who = (int) Tools::getValue('SUSCRIBERS');

        // get optin 0 for all 1 no optin 2 with optin
        $optin = (int) Tools::getValue('OPTIN');

        $country = (int) Tools::getValue('COUNTRY');

        if (Context::getContext()->cookie->shopContext) {
            $id_shop = (int) Context::getContext()->shop->id;
        }

        $customers = array();
        if ($who == 1 || $who == 0 || $who == 3) {
            $dbquery = new DbQuery();
            $dbquery->select('c.`id_customer` AS `id`, s.`name` AS `shop_name`, gl.`name` AS `gender`, c.`lastname`, c.`firstname`, c.`email`, c.`newsletter` AS `subscribed`, c.`newsletter_date_add`');
            $dbquery->from('customer', 'c');
            $dbquery->leftJoin('shop', 's', 's.id_shop = c.id_shop');
            $dbquery->leftJoin('gender', 'g', 'g.id_gender = c.id_gender');
            $dbquery->leftJoin('gender_lang', 'gl', 'g.id_gender = gl.id_gender AND gl.id_lang = '.$this->context->employee->id_lang);
            $dbquery->where('c.`newsletter` = '.($who == 3 ? 0 : 1));
            if ($optin == 2 || $optin == 1) {
                $dbquery->where('c.`optin` = '.($optin == 1 ? 0 : 1));
            }
            if ($country) {
                $dbquery->where('(SELECT COUNT(a.`id_address`) as nb_country
                                                    FROM `'._DB_PREFIX_.'address` a
                                                    WHERE a.deleted = 0
                                                    AND a.`id_customer` = c.`id_customer`
                                                    AND a.`id_country` = '.$country.') >= 1');
            }
            if ($id_shop) {
                $dbquery->where('c.`id_shop` = '.$id_shop);
            }

            $customers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery->build());
        }

        $non_customers = array();
        if (($who == 0 || $who == 2) && (!$optin || $optin == 2) && !$country) {
            $dbquery = new DbQuery();
            $dbquery->select('CONCAT(\'N\', e.`id`) AS `id`, s.`name` AS `shop_name`, NULL AS `gender`, NULL AS `lastname`, NULL AS `firstname`, e.`email`, e.`active` AS `subscribed`, e.`newsletter_date_add`');
            $dbquery->from('emailsubscription', 'e');
            $dbquery->leftJoin('shop', 's', 's.id_shop = e.id_shop');
            $dbquery->where('e.`active` = 1');
            if ($id_shop) {
                $dbquery->where('e.`id_shop` = '.$id_shop);
            }
            $non_customers = Db::getInstance()->executeS($dbquery->build());
        }

        $subscribers = array_merge($customers, $non_customers);

        return $subscribers;
    }

    private function myFputCsv($fd, $array)
    {
        $line = implode(';', $array);
        $line .= "\n";
        if (!fwrite($fd, $line, 4096)) {
            $this->post_errors[] = $this->trans('Error: Write access limited', array(), 'Modules.Emailsubscription.Admin').' '.dirname(__FILE__).'/'.$this->file.' !';
        }
    }

    private function getConditionFixtures($lang)
    {
        $locale = $lang['locale'];

        return
            $this->trans('You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.', array(), 'Modules.Emailsubscription.Shop', $locale)
        ;
    }
}
