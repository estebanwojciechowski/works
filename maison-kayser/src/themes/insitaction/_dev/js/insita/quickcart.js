import $ from 'jquery';

export default class Quickcart {
    
    constructor(el) {}

    init(){
        $('#_desktop_cart, #_mobile_cart').on('click', '.quickcart__picto', (e) => {
            if( $(e.currentTarget).hasClass('quickcart__picto--open') ){
                this.openQuickCart();
            } else if( $(e.currentTarget).hasClass('quickcart__picto--close') ){
                this.closeQuickCart();
            }
        });

        prestashop.on( 'updateShoppingCart', (e) => {
            if(!$("body#cart").length)
                this.openQuickCart();
        });
    }

    openQuickCart() { //On Rajoute la classe is--open et on cache le blockcart avec le z-index
        $('#quick-cart').addClass('is--open'); 
        $('.blockcart').css('z-index', 5);

        $("body").off("click.quickcart").on("click.quickcart", (e) => {
            if(!$(e.target).closest("#_desktop_cart, #_mobile_cart").length){  
                this.closeQuickCart();
                $("body").off("click.quickcart");
            }
        });
    } 

    closeQuickCart(){
        $('#quick-cart').removeClass('is--open'); //On enlève la classe is--open et on rend visible le blockcart avec le z-index
        $('.blockcart').css('z-index', 9999);
    }
}