<?php
/**
 * @author    Esteban Wojciechowski <estebanw@insitaction.com>
 * @copyright 2017 INSITACTION
 */


use Symfony\Component\Translation\TranslatorInterface;

class ClickNCollectConfirmationStepCore extends AbstractClickNCollectStep
{
    protected $template = 'clickncollect/_partials/steps/confirmation.tpl';
    public $errors = array();

    public function __construct(
        Context $context,
        TranslatorInterface $translator
    ) {
        parent::__construct($context, $translator);
    }

    public function handleRequest(array $requestParameters = array())
    { 
        $translator = $this->getTranslator(); 
        if($this->getClickNCollectSession()->getIDStore() &&
            $this->getClickNCollectSession()->customerHasLoggedIn()) {
            $this->step_is_current = true;
            $this->step_is_reachable = true;
        }
       if($this->step_is_current) { 
            $session = $this->getClickNCollectProcess()
                        ->getClickNCollectSession(); 

            if($mail_send = (int) Tools::getValue('mail_send')) {
                if($mail_send === 1)
                    //reinit le mail pour réenvoi
                    $session->setMailSend(0);
            } 
            if(!$session->getMailSend()) { 
                $customer = $session->getCustomer();  
                $id_store = $session->getIDStore();
                $cart = $session->getCart(); 

                if( $cart instanceof Cart && !is_null($cart->id) 
                    && $id_store && Store::storeExists($id_store)
                     && $customer instanceof Customer && !is_null($customer)
                 ) {
					 
					// SAM, modification système CnC, on souhaite créer une commande avec un statut CnC_ATTENTE
					// Réinitialisation du panier pour pouvoir faire au cas où une autre commande CnC.
					// On crée un email qui permet de changer le statut de la commande en CnC_OK.
					// Ce lien déclenchera l'appel au WS
					
					$order_states = OrderState::getOrderStates((int)$this->context->language->id);
					
					do {
						$reference = Order::generateReference();
					} while (Order::getByReference($reference)->count());
					
					$clickncollectOrderState = false;
					if(is_array($order_states) && count($order_states)) {
						foreach ($order_states as $key => $order_state) {
							if($order_state['module_name'] == 'clickncollect') {
								$clickncollectOrderState = $order_state;
								break;
							}
						}
					}
					
					if($clickncollectOrderState) {
						$order_status = new OrderState($clickncollectOrderState['id_order_state'], (int)$this->context->language->id);
						if (!Validate::isLoadedObject($order_status)) {
							PrestaShopLogger::addLog('PaymentModule::validateOrder - Order Status cannot be loaded', 3, null, 'Cart', (int)$cart->id, true);
							throw new PrestaShopException('Can\'t load Order status');
						}
						
						$id_carrier = 0;
						//clickncollect order state
						$id_order_state = $clickncollectOrderState['id_order_state'];
						$shop = new Shop($cart->id_shop);
						
						$order = new Order();
						$order->current_state = $id_order_state;
						$order->product_list = $cart->getProducts();
						$order->id_customer = (int)$cart->id_customer;
						$order->id_carrier = $id_carrier;
						$order->id_address_invoice = 0;
						$order->id_address_delivery = 0;
						$order->id_currency = $this->context->currency->id;
						$order->id_lang = (int)$cart->id_lang;
						$order->id_cart = (int)$cart->id;
						$order->reference = $reference;
						$order->id_shop = (int)$cart->id_shop;
						$order->id_shop_group = (int)$shop->getGroup()->id;
						$order->secure_key = pSQL($customer->secure_key);
						$order->payment = 0;
						$order->module = 'clickncollect';
						$order->recyclable = $cart->recyclable;
						$order->gift = (int)$cart->gift;
						$order->gift_message = $cart->gift_message;
						$order->mobile_theme = $cart->mobile_theme;
						$order->conversion_rate = $this->context->currency->conversion_rate;
						$order->total_paid_real = 0;
						$order->total_products = (float)$cart->getOrderTotal(false, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
						$order->total_products_wt = (float)$cart->getOrderTotal(true, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
						$order->total_discounts_tax_excl = (float)abs($cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
						$order->total_discounts_tax_incl = (float)abs($cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
						$order->total_discounts = $order->total_discounts_tax_incl;
						$order->total_shipping_tax_excl = 0;
						$order->total_shipping_tax_incl = 0;
						$order->total_shipping = 0;
						$order->carrier_tax_rate = 0;
						$order->total_wrapping_tax_excl = 0;
						$order->total_wrapping_tax_incl = 0;
						$order->total_wrapping = 0;

						$order->total_paid_tax_excl = $order->total_products_wt; // optional
						//(float)Tools::ps_round((float)$cart->getOrderTotal(false, Cart::BOTH, $order->product_list, $id_carrier), _PS_PRICE_COMPUTE_PRECISION_);
						$order->total_paid_tax_incl = $order->total_products_wt;
						//(float)Tools::ps_round((float)$cart->getOrderTotal(true, Cart::BOTH, $order->product_list, $id_carrier), _PS_PRICE_COMPUTE_PRECISION_);
						$order->total_paid = $order->total_products_wt;
						//$order->total_paid_tax_incl;

						$order->round_mode = Configuration::get('PS_PRICE_ROUND_MODE');
						$order->round_type = Configuration::get('PS_ROUND_TYPE');
						$order->invoice_date = '0000-00-00 00:00:00';
						$order->delivery_date = '0000-00-00 00:00:00';

						// Creating order
						$result = $order->add();
						if($result) {
							// Insert new Order detail list using cart for the current order
							$order_detail = new OrderDetail(null, null, $this->context);
							// indique que nous sommes dans le cas du clickncollect, 
							// empeche la mise a jour des stock webs
							$order_detail->clickncollect = true;

							$order_detail->createList($order, $cart, $id_order_state, $order->product_list);
							$order_created = true;
							//TODO APPEL WEBSERVICE COMMANDE CLICNCOLLECT
							//mise a jour du stock magasin
							
							//SAM, on ne souhaite pas appellé le WS tant que le client n'a pas validé sa commande
							/*if(class_exists('Ins_Stock_Magasin')) {
								$stock = new Ins_Stock_Magasin();
								$stock->updateStocks($store, $order->product_list);
							}*/
							
							$this->confirmation = true;
							$this->order = $order;
							
							$data = serialize(array(
								'id_customer' => $customer->id,
								'id_order' =>  $order->id,
								'id_store' =>  $id_store,
								'time' =>  time(), // pour avoir des parametres différents entre le lien de l'email et le lien de redirection
							));
							
							$url = $this->context->link->getPageLink('clickncollect').'?n='.base64_encode($data);
							
							$store = new Store($id_store);
							$store_html = '';
							if($store) {
								$store_html = '<p>'.$store->name.'</p><address>'.$store->address1.'</address><p>Tel : '.$store->phone.'</p> ';    
							} 
							
							// envoi mail 
							/*if(Mail::Send(
								$this->context->language->id,
								'clickncollect',
								$translator->trans(
									'Votre réservation',
									array(),
									'Emails.Subject'
								),
								array(
									'{firstname}' => $customer->firstname,
									'{lastname}' => $customer->lastname,
									'{email}' => $customer->email,
									'{url}' => $url,
									'{store}' => $store_html,
								),
								$customer->email,
								$customer->firstname.' '.$customer->lastname
							)) {
								//ajout du mail_send dans la session et le panier
								$session->setMailSend(1);                        
								// TODO creation d'un nouveau panier si le mail a été envoyé correctement et l'assigné au client
								//$session->resetCart();
							} else {
								$this->errors[] = $translator->trans('Une erreur s\'est produite lors de l\'envoi du mail. Veuillez réactualiser votre page.'); 
							}*/

							// ajoute la demande click and collect au magasin
							// mise a jour du stock magasin dans la base
							Hook::exec(
								'actionValidateClickNCollect',
								array(
									'order' => $order,
									'store' => $store
								)
							);
							
							$data = serialize(array(
								'id_customer' =>  $customer->id,
								'id_order' =>  $order->id,
								'id_store' =>  $id_store
							));
							
							$redirect = $this->context->link->getPageLink('clickncollect').'?redirect='.base64_encode($data);
							Tools::redirect($redirect);
							exit;

						}
							
					}
					
					//ANCIEN SYSTEME, la commande n'est pas créer de suite.
                    /*$data = serialize(array(
                        'id_customer' => $customer->id,
                        'id_cart' =>  $cart->id,
                        'id_store' =>  $id_store,
                    ));
                    
                    $url = $this->context->link->getPageLink('clickncollect').'?n='.base64_encode($data); 

                    $store = new Store($id_store);
                    $store_html = '';
                    if($store) {
                        $store_html = '<p>'.$store->name.'</p><address>'.$store->address1.'</address> ';    
                    } 

                    // envoi mail 
                    if(Mail::Send(
                        $this->context->language->id,
                        'clickncollect',
                        $translator->trans(
                            'Votre réservation',
                            array(),
                            'Emails.Subject'
                        ),
                        array(
                            '{firstname}' => $customer->firstname,
                            '{lastname}' => $customer->lastname,
                            '{email}' => $customer->email,
                            '{url}' => $url,
                            '{store}' => $store_html,
                        ),
                        $customer->email,
                        $customer->firstname.' '.$customer->lastname
                    )) {
                        //ajout du mail_send dans la session et le panier
                        $session->setMailSend(1);                        
                        // TODO creation d'un nouveau panier si le mail a été envoyé correctement et l'assigné au client
                        //$session->resetCart();
                    } else {
                        $this->errors[] = $translator->trans('Une erreur s\'est produite lors de l\'envoi du mail. Veuillez réactualiser votre page.'); 
                    } */
                } else {
                    $this->errors[] = $translator->trans('Un paramètre est manquant ou erronnée.'); 
                }

            } else {
                //mail déjà envoyé
                $this->context->smarty->assign([
                    'mail_send' =>1,
                    'mail_resend' => $this->context->link->getPageLink('clickncollect').'?mail_send=1'
                ]);
            }
        }

        $this->setTitle(
            $translator->trans(
                'Confirmation',
                array(),
                'Shop.Theme.ClickNCollect'
            )
        );
    } 	

    public function render(array $extraParams = array())
    {
        return $this->renderTemplate(
            $this->getTemplate(), $extraParams, array(
                'content_only' => $this->renderType(),
                'guest_allowed' => $this->getClickNCollectSession()->isGuestAllowed(),
                'empty_cart_on_logout' => !Configuration::get('PS_CART_FOLLOWING'),
                'errors' => $this->errors
            )
        );
    }
}