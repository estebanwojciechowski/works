/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

var BV = BV || { };
BV.select2 = $({ });

BV.select2.escapeMarkup = function(json_string) {
    var entity_map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
    };

    return String(json_string).replace(/[&<>"'`=\/]/g, function (s) {
        return entity_map[s];
    });
};

BV.select2.markMatch = function(text, term) {
    var markup=[];
    var match = -1;

    do {
        match = text.toUpperCase().indexOf(term.toUpperCase());
        if (match > -1) {
            markup.push(BV.select2.escapeMarkup(text.substring(0, match)));
            markup.push("<span class='select2-match'>");
            markup.push(BV.select2.escapeMarkup(text.substring(match, match+term.length)));
            markup.push("</span>");
            text = text.substring(match+term.length, text.length);
        }
    } while (match > -1 && term.length > 0);

    markup.push(BV.select2.escapeMarkup(text));
    return markup.join('');
};