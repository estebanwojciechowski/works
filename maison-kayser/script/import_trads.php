<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require_once 'vendor/autoload.php'; // logger
require_once(dirname(__FILE__) . '/../src/config/config.inc.php');

class ImportTrads {
	private $_logger;
    private $base_dir = '';
    private $dirImportFile = '';

    private static $test = false;

    protected $errors = array();

	public static $current_iso = 'en';
    public static $base_filename = 'export_tr_';
    public static $id_lang;



	public function __construct() {
		//enleve la boutique du context pour gérer nativement les produits sur toute les boutiques
        $context = Context::getContext();
        $context->shop = new Shop();
        Shop::setContext(Shop::CONTEXT_ALL);
        self::$id_lang = $this->getIdLangByISO(self::$current_iso);
		$this->base_dir = dirname(__FILE__).'/';
        $this->dirImportFile = dirname(__FILE__) . '/../import/traductions/';
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO);
    }

    private function getIdLangByISO($iso_code) {
    	return DB::getInstance()->getValue('
    		SELECT id_lang FROM `'._DB_PREFIX_.'lang` WHERE iso_code = "en"
    	');
    }

    /**
     * Parcourt les fichiers du repertoire et effectue les traitement selon le nom du fichier
     * @return [type] [description]
     */
    public function start() {
    	if(self::$id_lang) {
    		$files = false;
    		if(is_dir($this->dirImportFile))
	    		$files = scandir($this->dirImportFile);
	    	if($files) {
	    		foreach ($files as $key => $file) {
                    if(preg_match('/^'.self::$base_filename.'([^0-9]*)[0-9]+.csv$/', $file, $matches)) {
	    			    $type_file = substr(strtolower($matches[1]),0,-1);

                        $method_name = 'import'.ucfirst($type_file);
                        if(method_exists($this, $method_name)) {
                            $this->{$method_name}($file);
                        } else {
                            $this->log('ERREUR : La méthode '.$method_name.' n\'est pas définie');
                        }
                    }
	    		}
	    	} else {
	    		$this->log('ERREUR : Aucun fichier trouvés dans le repertoire : "'.$this->dirImportFile.'"');
	    	}
	    } else {
	    	$this->log('ERREUR : Aucune langue trouvé pour le code iso '.self::$current_iso);
	    }
    }

    // TRAITEMENTS DES FICHIERS PRESTASHOP QUI METTENT A JOUR LES LANGUES

    private function importAttributs($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'attribute_lang';
        $keys = array(
            'id_attribute', 'name'
        );
        $exclude_fields = array();
        $extra_fields = array();
        $definition = Attribute::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function importAttributs_groupes($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'attribute_group_lang';
        $keys = array(
            'id_attribute_group', 'public_name'
        );
        $exclude_fields = array();
        $extra_fields = array();
        $definition = AttributeGroup::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function importCaracteristiques($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'feature_lang';
        $keys = array(
            'id_feature', 'name'
        );
        $exclude_fields = array();
        $extra_fields = array();
        $definition = Feature::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function importCaracteristiques_valeurs($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'feature_value_lang';
        $keys = array(
            'id_feature_value', 'value'
        );
        $exclude_fields = array();
        $extra_fields = array();
        $definition = FeatureValue::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function importCategories($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'category_lang';
        $keys = array(
            'id_category', 'name', 'description'
        );
        $exclude_fields = array();
        $extra_fields = array(
            'name' => array('link_rewrite', 'meta_title'),
            'description' => array('meta_description')
        );
        $definition = Category::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function importCms($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'cms_lang';
        $keys = array(
            'id_cms', 'meta_title', 'meta_description', 'meta_keywords', 'content'
        );
        $exclude_fields = array();
        $extra_fields = array(
            'meta_title' => array('link_rewrite')
        );
        $definition = CMS::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function importConfigurations($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'configuration_lang';
        $keys = array(
            'id_configuration', 'value'
        );
        $extra_fields = array();
        $exclude_fields = array('name');
        $definition = Store::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function importMagasins($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'store_lang';
        $keys = array(
            'id_store', 'name', 'description', 'commercial'
        );
        $extra_fields = array();
        $exclude_fields = array('name');
        $definition = Store::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

     private function importProducts($file) {
        $this->log(__FUNCTION__.' début de l\'import');
        $table = 'product_lang';
        $keys = array(
            'id_product', 'name', 'description'
        );
        $extra_fields = array(
            'name' => array('link_rewrite', 'meta_title'),
            'description' => array('meta_description')
        );
        $exclude_fields = array();
        $definition = Product::$definition['fields'];

        $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    }

    private function letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition, $limit = 50)
    {
        $errors = array();
        $full_path = $this->dirImportFile.$file;
        if(file_exists($full_path)) {
            if (($handle = fopen($full_path, "r")) !== FALSE) {
                $sql = '';
                $first = true;
                $rows = 1;
                $current_line = 1;
                while (($data = fgetcsv($handle, 0, "|")) !== FALSE) {
                    if($first) {
                        $first = false;
                        continue;
                    }
                    // on vérifie la présence de toute les données
                    $row_to_update = array();
                    $error = false;
                    foreach ($data as $index => &$value) {
                        if(!isset($keys[$index]) || in_array($keys[$index], $exclude_fields)) {
                             continue 1;
                        }
                        if(strlen(trim($value))) {
                            $validate = true;
                            if(isset($definition[$keys[$index]]['validate'])) {
                                if(method_exists('Validate', $definition[$keys[$index]]['validate'])) {
                                    // le champs est il valide ?
                                    $validate = Validate::{$definition[$keys[$index]]['validate']}($value);
                                    // la taille est elle valide ?
                                    // si supérieur on la tronque
                                    if($validate && isset($definition[$keys[$index]]['size'])) {
                                        if(strlen(trim($value)) > $definition[$keys[$index]]['size']) {
                                            $value = Tools::truncate(trim($value), $definition[$keys[$index]]['size']);
                                        }
                                    }
                                }
                            }
                            if($validate) {
                                $row_to_update[$keys[$index]] = $value;
                                if(!empty($extra_fields) && isset($extra_fields[$keys[$index]])) {

                                    foreach ($extra_fields[$keys[$index]] as $field_from => $field_dest) {
                                        $row_to_update[$field_dest] = $this->addExtraField($field_dest, $value, $definition);
                                    }
                                }
                            } else {
                                $error = true;
                                $this->log('ERREUR : LIGNE '.$current_line.' - La valeur : '.$value.' est invalide pour le champ '.$index);
                            }
                        }
                    }

                    if(!$error) {
                        if(count($row_to_update) > 1) {
                            $sql .= self::getSqlUpdate($row_to_update, $table);

                            if($rows % $limit == 0) {
                                // Execute la requete
                                if(!self::$test) {
                                    if(!DB::getInstance()->execute($sql)) {
                                        $this->log($sql);
                                        $this->log('Une erreur s\'est produite lors de la maj de la table '.$table);
                                    }
                                } else {
                                    $this->log($sql);
                                }
                                $rows = 1;
                            }
                            $rows++;
                        }
                    } else {
                        $errors[] = ' Erreur à la ligne '.$current_line.' du fichier '.$file;
                    }

                    $current_line++;
                }

                if($rows % $limit != 0) {
                    // Execute la dernière requete
                    if(!self::$test) {
                        if(!DB::getInstance()->execute($sql)) {
                            $this->log($sql);
                            $this->log('Une erreur s\'est produite lors de la maj de la table '.$table);
                        }
                    } else {
                        $this->log($sql);
                    }
                }

                fclose($handle);

                if(!empty($errors)) {
                    $this->log('Les données du fichier '.$file.' comportent des erreurs.');
                }
            }
        }
    }

    private function getSqlUpdate($datas = array(), $table = false) {

        $sql = false;

        if(count($datas) && $table) {

            $sql = '
                UPDATE `'._DB_PREFIX_.$table.'`
                SET
            ';

            $first = true;
            $identifier = false;
            foreach ($datas as $key => $data) {
                if($first) {
                    $identifier = $key;
                    $first = false;
                    continue;
                }
                $sql .= ' '.$key.' = \''.pSQL($data).'\',';
            }
            $sql = substr($sql, 0, -1). '
                WHERE '.$identifier.' = '.$datas[$identifier].'
                AND id_lang = '.self::$id_lang.'; ';

        } else {
            $this->log(__FUNCTION__.' ERREUR : impossible de créer la requete d\'UPDATE.');
        }
        return $sql;

    }

    /**
     * Generation à la volée des metas
     * @param [type] $field_name [description]
     * @param [type] $value      [description]
     * @param [type] $definition [description]
     */
    private function addExtraField($field_name, $value, $definition) {
        $new_field_value = false;
        switch ($field_name) {
            case 'link_rewrite':
                $new_field_value = Tools::link_rewrite($value);
                break;
            case 'meta_title':
            case 'meta_description':
                $new_field_value = self::cleanForMeta($value);
                if(isset($definition[$field_name]['size']) && strlen($new_field_value) > $definition[$field_name]['size'])
                    $new_field_value = Tools::truncate($new_field_value, $definition[$field_name]['size']);
                if(isset($definition[$field_name]['validate']) && !Validate::{$definition[$field_name]['validate']}($new_field_value)) {
                    $this->log('ERREUR lors de la génération de la '.$meta_title.' '.$new_field_value);
                    $new_field_value = false;
                }
                break;
        }
        return $new_field_value;
    }
    // FIN PRESTASHOP BDD

    // WORDPRESS

    // private function importWpcategories($file) {

    //     $table = 'products_lang';
    //     $keys = array(
    //         'id_product', 'name', 'description'
    //     );
    //     $extra_fields = array(
    //         'name' => array('link_rewrite', 'meta_title'),
    //         'description' => array('meta_description')
    //     );
    //     $exclude_fields = array();
    //     $definition = Product::$definition['fields'];

    //     $this->letsGo($file, $table, $keys, $extra_fields, $exclude_fields, $definition);
    // }


    // FIN WORDPRESS


    // TOOLS

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    public static function cleanForMeta($value) {
        return trim(strip_tags($value));
    }
}

$timestamp_debut = microtime(true);

$instance = new ImportTrads();
$instance->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$instance->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");