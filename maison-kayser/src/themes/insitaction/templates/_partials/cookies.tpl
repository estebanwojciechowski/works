{literal}
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#ffffff",
      "text": "#000000"
    },
    "button": {
      "background": "#ff6955",
      "text": "#fff",
      "border": "#ff6955"
    }
  },
  "content": {
    "message": "{/literal}{l s='Afin d\'améliorer votre expérience, nous utilisons des cookies techniques permettant le fonctionnement et la navigation sur le site ainsi que des cookies d\'analyse mesurant l\'audience du site. ' d='Shop.Theme.Cookie'}{literal}",
    "dismiss": "{/literal}{l s='J\'accepte' d='Shop.Theme.Cookie'}{literal}",
    "link": "{/literal}{l s='En savoir plus' d='Shop.Theme.Cookie'}{literal}",
    "href": "/index.php?id_cms=8&controller=cms"
  }
})});
</script>
{/literal}