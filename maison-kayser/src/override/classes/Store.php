<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * Class Store
 */
class Store extends StoreCore
{
    /** @var string Quartier */
    public $quartier;

    /** @var string Arrondissement */
    public $arrondissement;

    /** @var string Store hours click and collect (PHP serialized) */
    public $hours_clickandcollect;

    /** @var string Type de paiement */
    public $type_payment;

    /** @var string Identifiant de caisse */
    public $identifiant_caisse;

    /** @var bool Service Sans glutene */
    public $sans_gluten = false;

    /** @var bool Service Restaurant */
    public $restaurant = false;

    /** @var bool Service Cafe */
    public $cafe = false;

    /** @var bool Service Terrasse */
    public $terrasse = false;

    /** @var bool Livraison Colissimo */
    public $colissimo = false;

    /** @var bool Livraison Click and Collect */
    public $clickandcollect = false;

    public $description;
    public $commercial;

    public static $store_closed_value = array('x', 'null', 'à venir');

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'store',
        'primary' => 'id_store',
        'multilang' => true,
        'fields' => array(
            'id_country' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_state' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
            'identifiant_caisse' => array('type' => self::TYPE_STRING, 'size' => 128, 'validate' => 'isGenericName'),
            'type_payment' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true, 'size' => 128),
            'address1' => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'required' => true, 'size' => 128),
            'address2' => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            'postcode' => array('type' => self::TYPE_STRING, 'size' => 12),
            'city' => array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'required' => true, 'size' => 64),
            'quartier' => array('type' => self::TYPE_STRING, 'size' => 128),
            'arrondissement' => array('type' => self::TYPE_STRING, 'size' => 32),
            'latitude' => array('type' => self::TYPE_FLOAT, 'validate' => 'isCoordinate', 'size' => 13),
            'longitude' => array('type' => self::TYPE_FLOAT, 'validate' => 'isCoordinate', 'size' => 13),
            'hours' => array('type' => self::TYPE_STRING, 'validate' => 'isJson', 'size' => 65000),
            'hours_clickandcollect' => array('type' => self::TYPE_STRING, 'validate' => 'isJson', 'size' => 65000),
            'phone' => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 30),
            'fax' => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 16),
            'email' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            'note' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 65000),
            'sans_gluten' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'restaurant' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'cafe' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'terrasse' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'colissimo' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'clickandcollect' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),

            'description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'commercial' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
        )
    );

    public static function parseHours($hours, $getStartEnd = false) {
        $hours = json_decode($hours, true);
        if(is_array($hours)) {
            foreach ($hours as $key => &$day) {
                if(is_array($day) && count($day) == 1) {
                    $day = current($day);
                }
                if(is_string($day)
                    &&
                    (
                        in_array(strtolower($day), self::$store_closed_value)
                        || !preg_match('/([0-9]+)/', $day)
                    )
                ) {
                    $day = '';
                } else if($getStartEnd) {
                    $start = explode('-', $day);
                    if(is_array($start) && count($start)==2)  {
                        $start[0] = explode(':', $start[0]);
                        $start[1] = explode(':', $start[1]);
                        if(is_array($start[0]) && count($start[0]) == 2
                                && is_array($start[1]) && count($start[1]) == 2)  {
                            $day = array(
                                'start' => array(
                                    'hours' => $start[0][0],
                                    'minutes' => $start[0][1],
                                ),
                                'end' => array(
                                    'hours' => $start[1][0],
                                    'minutes' => $start[1][1],
                                ),
                            );
                        }
                    }
                }
            }
        }
        return $hours;
    }



    /**
     * [isClosedEveryDays teste si le magasin dispose ou non d'horaires d'ouverture et fermeture]
     * @return boolean
     */
    public function isClosedEveryDays($clickncollect = false) {
        $next_day = 24*60*60;
        for ($i=0; $i < 7; $i++) {
            if($this->getOpenCloseTimestamps(time()+($i*$next_day), $clickncollect)) {
                //dés qu'on a une valeur differente de array() on a un jour d'ouverture
                return false;
            }
        }
        return true;
    }

    /**
     * [getStoreDelay si le magasin est ouvert pas de délai
     * si par contre il est fermé on cherche la prochaine date d'ouverture]
     * @param  boolean $attribute [non utilisé pour le moment mais si un transporteur ultérieurement
     *                             applique un délai au traitement en magasin ce sera utile]
     * @return [timestamp] [timestamp delai avant prochaine ouverture du magasin ou 0 si ouvert]
     */
     public function getStoreDelay($attribute = false, $product_delay = false) {

        //on filtre si l'attribut est clickncollect
        $clickncollect = false;
        if($attribute && isset($attribute['field'])
                && isset($attribute['object']) && $attribute['object'] == 'product') {
            // determine si on se basera sur les horaires du click and collect
            // ou de l'ouverture magasin
            $clickncollect = ($attribute['field'] == 'delai_clickandcollect');
        }

        $store_delay = 0;
        $open_when_product_ready = true;
        $timestamp_start = time();

        if($product_delay) {
            //on verifie si avec le temps de refabrication du produit
            //l'horaire de fermeture de la boulangerie n'est pas dépassé
            $hours = $this->getOpenCloseTimestamps(null, $clickncollect);
            if(isset($hours['end']))
                if($timestamp_start + $product_delay > $hours['end']) {
                    $open_when_product_ready = false;
                }
        }

        if(!($this->isOpen($timestamp_start, $clickncollect) && $open_when_product_ready)) {
            // on cherche la prochaine heure d'ouverture
            if($product_delay) {
                // recupération du jour ou le produit sera prêt
                $min_time = $timestamp_start + $product_delay;
                if($this->isOpen($min_time, $clickncollect)) {
                    //cas le plus simple quand le produit est dispo (refabriqué) la boulangerie est ouverte
                    return $product_delay;
                } else {
                    //récupération du prochain jour d'ouverture
                    $store_delay = $this->getNextOpening($clickncollect, $min_time);
                }
            } else
                $store_delay = $this->getNextOpening($clickncollect);
            //si un attribut de transporteur est passé en parametre
            //on ajoute le delai de préparation
            if($attribute && isset($attribute['field'])
                && isset($attribute['object']) && $attribute['object'] == 'store') {
                //on verifie que le champ existe, si il existe le délai est exprimé en heures
                if(property_exists($this, $attribute['field'])) {
                    //on ajoute le delai de péparation
                    $store_delay += $this->{$attribute['field']} * 3600;
                }
            }
        }
        return $store_delay;
    }

    /**
     * [getNextOpening recherche le delai en s ou le magasin sera ouvert]
     * @param  [type]  $timestamp
     * @param  boolean $diff      [retourn la difference entre le timestamp actuel et l'ouverture ou non]
     * @return [type]             [description]
     */
    public function getNextOpening($clickncollect = false, $timestamp = null, $diff = true, $diff_with_timestamp = false) {

        if($this->isClosedEveryDays($clickncollect))
            return false;

        $now = time();

        if(is_null($timestamp))
            $timestamp = time();

        // on regarde les heures d'ouverture du lendemain
        $nb_days_after = 0;
        $next_day =  24*60*60;
        // tant qu'on ne trouve pas un jour d'ouverture on continue de le chercher
        while(true) {
            $store_delay = $this->getOpenCloseTimestamps($timestamp + ($nb_days_after * $next_day), $clickncollect);
            if(!empty($store_delay)) {
                if($store_delay['start'] > $timestamp)
                    break;
            }
            $nb_days_after++;
        }
        // $store_delay['start'] - $now  -> diff pour obtenir le temps total a ajouter
        return ($diff ? ($diff_with_timestamp ? $store_delay['start'] - $timestamp : $store_delay['start'] - $now) : $store_delay['start']);
    }

    /**
     * [isOpen retourne si le magasin est ouvert]
     * @param  int $timestamp           [timestamp representant le moment choisi pour le test]
     * @param  boolean $clickncollect   [horaires clic and collect ou non]
     * @return boolean                  [open or not]
     */
    public function isOpen($timestamp = null, $clickncollect = false) {
        if(is_null($timestamp)) {
            $timestamp = time();
        }
        $hours = $this->getOpenCloseTimestamps($timestamp, $clickncollect);
        if(!empty($hours)) {
            if($timestamp >= $hours['start'] && $timestamp < $hours['end'])
                return true;
        }
        return false;
    }

    /**
     * [getOpenCloseTimestamps retourne les timestamp d'ouverture et fermeture à la date choisie]
     * @param  [type] $timestamp [timestamp (date) or null]
     * @return [array]            [tableau vide si fermé aujourd'hui sinon timestamp start et end]
     */
    public function getOpenCloseTimestamps($timestamp = null, $clickandcollect = true) {
        if($clickandcollect) {
            $hours = json_decode($this->hours_clickandcollect, true);
        } else {
            $hours = json_decode($this->hours, true);
        }
        if(is_array($hours) && count($hours)) {
            if(is_null($timestamp)) {
                $timestamp = time();
            }
            // recupération date via timestamp
            $jour = date('N', $timestamp) - 1; // lundi = 1, dimanche = 7
            if(isset($hours[$jour])) {
                $day = $hours[$jour];
                if(is_array($day) && count($day) == 1) {
                    $day = current($day);
                }
                if(is_string($day) && !in_array(strtolower($day), self::$store_closed_value)) {
                    // format horaire : xx:xx-xx:xx
                    $day = explode('-', $day);
                    if(is_array($day) && count($day) == 2) {
                        $start = explode(':', trim($day[0]));
                        $end = explode(':', trim($day[1]));
                        if(is_array($start) && count($start) == 2
                            && is_array($end) && count($end) == 2 ) {
                            $start = mktime(
                                        $start[0],
                                        $start[1],
                                        0,
                                        date('m', $timestamp),
                                        date('d', $timestamp),
                                        date('Y', $timestamp)
                                    );
                            $end = mktime(
                                        $end[0],
                                        $end[1],
                                        0,
                                        date('m', $timestamp),
                                        date('d', $timestamp),
                                        date('Y', $timestamp)
                                    );
                            return array(
                                'start' => $start,
                                'end' => $end,
                            );
                        }
                    }
                }

            }
        }
        return array();
    }

    public static function getName() {
        $context = Context::getContext();
        $shop = $context->shop;
        if(Validate::isLoadedObject($shop)) {
            $store = $context->shop->getStore();
            if(Validate::isLoadedObject($store)) {
                return $store->name;
            }
        }
        return false;
    }

    public static function getAddress() {
        $context = Context::getContext();
        $shop = $context->shop;
        if(Validate::isLoadedObject($shop)) {
            $store = $context->shop->getStore();
            if(Validate::isLoadedObject($store)) {
                return $store->address1.($store->address2 ? ' '.$store->address2 : '').' '.$store->postcode.' '.$store->city;
            }
        }
        return false;
    }

    public static function hasServices($id_store) {
        $store = new Store($id_store);
        if(Validate::isLoadedObject($store)) {
            if($store->sans_gluten || $store->restaurant || $store->colissimo || $store->clickandcollect
                    || $store->cafe || $store->terrasse)
                return true;
        }
        return false;
    }

    public static function changeStoreID($originID, $destID) {
        $errors = array();
        if($destID && Validate::isUnsignedInt($destID)) {
            if($originID != $destID) {
                $store_exist = DB::getInstance()->getValue('
                    SELECT id_store FROM `'._DB_PREFIX_.'store`
                    WHERE `id_store` = '.$destID.' ;
                ');
                if(!$store_exist) {
                    // MAJ DE L'ID PAR SQL
                    if(!DB::getInstance()->execute('

                            UPDATE `'._DB_PREFIX_.'store`
                            SET `id_store` = '.$destID.'
                            WHERE `id_store` = '.$originID.' ;

                            UPDATE `'._DB_PREFIX_.'store_lang`
                            SET `id_store` = '.$destID.'
                            WHERE `id_store` = '.$originID.' ;

                            UPDATE `'._DB_PREFIX_.'store_shop`
                            SET `id_store` = '.$destID.'
                            WHERE `id_store` = '.$originID.' ;

                            UPDATE `'._DB_PREFIX_.'store_special_days`
                            SET `id_store` = '.$destID.'
                            WHERE `id_store` = '.$originID.' ;
                        ')
                    ) {

                        $errors[]  = 'Erreur lors de la modification de la base de données';
                    }
                } else {
                    $errors[] = 'L\'identifiant '.$destID.' est déjà utilisé.';
                }
            }
        } else {
            $errors[] = 'L\'identifiant '.$destID.' est invalide.';
        }
        return $errors;
    }

    /**
     * [getDaysOpen construit un tabeau avec les jours d'ouvertures uniquement]
     * @param  boolean $clickandcollect [description]
     * @return [array]                   [tableau de boolean, l'index correspond au jour]
     */
    public function getDaysOpen($clickandcollect = false) {
        if($clickandcollect) {
            $hours = Store::parseHours($this->hours_clickandcollect);
        } else {
            $hours = Store::parseHours($this->hours);
        }

        if(is_array($hours) && count($hours)) {
            $open_days = array();
            $day = 1;
            foreach ($hours as $key => &$value) {
                if(!empty($value))
                    $open_days[$day] = 1;
                $day++;
            }
            return $open_days;
        }
        return array();
    }

    /**
     * [getDelayWithRefab
     * Ajoute le délai de refabrication au temps avant mise à dispo au client
     * a savoir :
     * 2 délais de refab dispo :
     * avant 11h
     * après 11h     *
     * ]
     * @param  [type] $max_product_cart_delay
     * [delai actuel avant mise à disposition (delai max des produits du panier) ]
     * @param  [type] $max_refab_delay
     * [delai de refabrication maxi des produits du panier
     * (avant ou apres 11h selon l'heure de commande)]
     * @param  [type] $max_morning_refab_delay
     * [delai de refabrication maxi des produits du panier avant 11h]
     * @param  [type] $clickncollect           [true or false]
     * @return [type]
     * [timestamp a jouter à time pour le délai de mise à dispo]
     */
    public function getDelayWithRefab($max_product_cart_delay, $max_refab_delay, $max_morning_refab_delay, $clickncollect) {
        $next = 0;
        $timestamp_start = time();
        $day_in_second = 60*60*24;

        if(!$this->isOpen($timestamp_start + $max_product_cart_delay, $clickncollect)) {
            // le magasin est fermé on récupère la prochaine date d'ouverture
            // avec le delai de refabrication avant 11h
            $next = $this->getNextOpening($clickncollect, $timestamp_start + $max_product_cart_delay, true) + $max_morning_refab_delay;
        } else {
            // magasin ouvert on ajoute le delai de refab
            $max_product_cart_delay += $max_refab_delay;
            if(!$this->isOpen($timestamp_start + $max_product_cart_delay, $clickncollect)) {
                // le magasin est fermé on écupère la prochaine date d'ouverture
                // avec le delai de refabrication avant 11h
                $next = $this->getNextOpening($clickncollect, $timestamp_start + $max_product_cart_delay, true) + $product_morning_refab_delay;
            }
        }
        // si next est > 0 alors le magasin est fermé lorsque le delai de refab est ajouté
        // Mais est ce que le delai de fabrication du matin + l'horaire d'ouverture
        // tombe sur un jour d'ouverture ??
        if($next) {
            // On contrôle s'il y a des jours fermés dans le calcul
            // Si oui, on retire ces jours du calcul
            // Le calcul est inutil si le délai est inférieur à 24h
            if($next > $day_in_second){
                $delta = $day_in_second;
                while($delta < $next){
                    if(!$this->isOpen($timestamp_start + $next, $clickncollect)) {
                        $next += $day_in_second;
                    }
                    $delta += $day_in_second;
                }
            }
            // derniere verification
            if(!$this->isOpen($timestamp_start + $next, $clickncollect)) {
                // si fermé lors de la fin de fabrication
                // on prend la prochaine ouverture
                return $this->getNextOpening($clickncollect, $timestamp_start + $next, true);
            }
            return $next;
        }elseif($max_product_cart_delay > $day_in_second){
            // On contrôle s'il y a des jours fermés dans le calcul
            // Si oui, on retire ces jours du calcul
            // Le calcul est inutil si le délai est inférieur à 24h
            $delta = $day_in_second;
            while($delta < $max_product_cart_delay){
                if(!$this->isOpen($timestamp_start + $max_product_cart_delay, $clickncollect)) {
                    $max_product_cart_delay += $day_in_second;
                }
                $delta += $day_in_second;
            }
        }
        return $max_product_cart_delay;
    }
}
