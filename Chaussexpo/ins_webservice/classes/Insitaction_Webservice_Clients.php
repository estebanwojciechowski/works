<?php
 
include_once(dirname(__FILE__).'/Insitaction_Webservice.php'); 

class Insitaction_Webservice_Clients extends Insitaction_Webservice {

	protected $update_time = 3600;
	/**
	 * [$data definition des champs de chaque méhode
	 * si il n'existent pas dans une méthode appelée elle ne pourra pas fonctionner]
	 * @var array
	 */
	protected static $data = array(
		'CreerClient' => array( 
            "Civilite" => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            "Nom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "Prenom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "Adresse1" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "Adresse2" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "Code_postal" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "Ville" => array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'size' => 64),
            "Tel_fixe" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "Tel_portable" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "Email" => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            "Date_naissance" => array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
            "Nb_enfants" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "Code_pays" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "Pays" => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 64),
            "Code_langue" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "Mot_de_passe" => array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'size' => 60)
        ),
        'CreerClient_mode_degrade' => array( 
            "Civilite" => '',
            "Nom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "Prenom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "Adresse1" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "Adresse2" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "Code_postal" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "Ville" => array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'size' => 64),
            "Tel_fixe" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "Tel_portable" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "Email" => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            "Date_naissance" => array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
            "Nb_enfants" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "Code_pays" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "Pays" => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 64),
            "Code_langue" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "Mot_de_passe" => array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'size' => 60)
        ),
        'ModifierClient' => array( 
            "Code_client" => '',
            "Civilite" => '',
            "Nom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "Prenom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "Adresse1" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "Adresse2" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "Code_postal" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "Ville" => array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'size' => 64),
            "Tel_fixe" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "Tel_portable" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "Email" => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            "Date_naissance" => array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
            "Nb_enfants" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "Code_pays" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "Pays" => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 64),
            "Code_langue" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "Mot_de_passe" => array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'size' => 60)
        ),
        'identification_client_email' => array(
        	'email_client' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
        ),
        'identification_client' => array(
        	'code_client' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
        	'nomFamille' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
        ),
	);

	/**
	 * [$response_data définition des champs attendu dans la réponse webservice]
	 * @var array
	 */
	protected static $response_data = array( 
		'CreerClient' => array(
			'CreerClientResult'
		),
		'ModifierClient' => array(
			'ModifierClientResult'
		),
		'CreerClient_mode_degrade' => array(
			'CreerClient_mode_degradeResult'
		),
		'identification_client_email' => array(
			'code_client',
            "civilite",
            "nom",
            "prenom",
            "adresse1",
            "adresse2",
            "code_postal",
            "ville",
            "tel_fixe",
            "tel_portable",
            "email",
            "date_naissance",
            "nb_enfants",
            "code_pays",
            "pays",
            "code_langue",
            "mot_de_passe"
        ),  
	);

	public function __construct($verbose = false)
    { 
    	$this->setUrl(Configuration::get('INS_WEBSERVICE_URL_CLIENT'));   
    	parent::__construct($verbose);
    } 

    public function getCodeClientByEmail($email) {
		$response = $this->identification('email', array('email_client' => $email)); 
		if($response) {
			if(isset($response['WSVC_CLIENTS']) && isset($response['WSVC_CLIENTS']['code_client']))
				return $response['WSVC_CLIENTS']['code_client'];
		}
		return false;
    }

    /**
     * [crud appel webservice ajout et modification client]
     * @param  string  $mode            [description]
     * @param  array   $params          [description]
     * @param  boolean $get_params_keys [description]
     * @return [type]                   [description]
     */
    public function crud($mode = 'create', $params = array(), $get_params_keys = false) {  
      	if ($this->client) {
      		$customer = false;	
      		if(is_array($params) && isset($params['id_customer'])) {
      			$customer = new Customer($params['id_customer']);
      		}
	    	if(Validate::isLoadedObject($params)) {
	    		$customer = $params;
	    	 	$params	= self::parseCustomer($customer);	 
	    	} 
    		if($mode == 'degrade')
    			$this->method = 'CreerClient_mode_degrade';
    		elseif($mode == 'update') { 
    			if(isset($params['Email'])) {
    				//recupération du codeclient
    				if(!trim($params['Code_client']))
    					$params['Code_client'] = $this->getCodeClientByEmail($params['Email']); 
    				$this->method = 'ModifierClient'; 
    			} else
    				$this->errors[] = __FUNCTION__.'() le champ EMAIL n\'a pas été transmis.';
    		}
    		elseif($mode = 'add') {
    			$this->method = 'CreerClient';
    			if(isset($params['Email'])) {
    				$params['Code_client'] = $this->getCodeClientByEmail($params['Email']);
    				if($params['Code_client']) {
    					$this->method = 'ModifierClient';
    				}
    			} 
    		}
    		
    		if($this->method) {  
	    		if($get_params_keys) { 
	    			if(isset(self::$data[$this->method]))			
	    				return array_keys(self::$data[$this->method]);
	    			return $this->errors[] = 'self::$data['.$this->method.'] n\'est pas defini!';
	    		}
 
 				$data = array();
 				if(isset(self::$data[$this->method])) {
		    		self::$definition['fields'] = self::$data[$this->method];
		    		foreach (self::$data[$this->method] as $key => $value) {
		    			if(isset($params[$key])) {
		    				$data[$key] = $params[$key];
		    				$validate = $this->validateField($key, $value);
		    				if(!$validate)
		    					$this->errors[] = __FUNCTION__.'() la validation champs '.$key.' : '.$value.' a échoué';
		    			} else {
		    				$this->errors[] = __FUNCTION__.'() '.$key.' n\'est pas défini pour la méthode '.$this->method;
		    			}
		    		} 
		    		if(empty($this->errors)) {
		    			//TODO catch erreur birthday - datedenaissance required
		    			if(strlen(trim($data['Date_naissance'])) == 0
		    				|| $data['Date_naissance'] == '0000-00-00')
		    				$data['Date_naissance'] = '1980-01-01';
 
			       		try {
			       			$this->response = $this->client->__soapCall(
			       						$this->method,
			       						array(
			       							$this->method => $data   	
			       						)
				           	);	
			           		$this->log("REQUEST:\n" . $this->client->__getLastRequest() . "\n");
				           	$response = $this->response;
				           	$champ = self::$response_data[$this->method];  	
				           	$champ = array_shift($champ);
				            $response = $this->parseResults($champ, $response); 
							if($response) { 
								$this->log('Insitaction_Webservice_Clients Customer : '.$params['Email'].' '.$this->method.' reponse : '.json_encode($this->response).' params: '.json_encode($data));

								if($response != 1) {
									if(!$customer) {
										$customer = new Customer();
										$customer = $customer->getByEmail($params['Email']);
									}
									$this->addCron($customer);      
								}
								return array(
												'code_retour' => $response,
											);
							} else {
								$this->errors[] = "La réponse ne contient aucune donnée.\n";
							} 
				        } catch (Exception $e) {
				            $this->errors[] = __FUNCTION__.' Exception reçue : '.$e->getMessage().' '.json_encode($this->response)."\n";
				        }
				    }
			    } else {			    	
		    		$this->errors[] = __FUNCTION__.' self::$data['.$this->method.'];';
			    }
		    } else {
		    	$this->errors[] = __FUNCTION__.' La methode \'est pas definie.';
		    }
	    } else {
		    $this->errors[] = 'Insitaction_Webservice_Clients Customer '.__FUNCTION__.' : '.(is_array($params) && isset($params['Email']) ? $params['Email'] : ($params instanceof Customer ? $params->email : 'Email non transmis'));
		}
		$this->logErrors();
	    return false;
    }

    public function nouveauMotDePasse($email) {
    	if ($this->client) {
    		$this->method = 'NouveauMotDePasse';    		 
       		try {
	            $this->response = $this->client->__soapCall(
	            	$this->method, 
	            	array(
		                $this->method => array(
		                    "eMailClient" => $email
		                )
	            	)
				); 
				return $this->response;
	        } catch (Exception $e) {
	            $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
	        }
	    }
	    return $this->errors;
    }

    /**
     * [pointFidelite retourne le nb de points fidélité du client]
     * @param  [type] $code_client [description]
     * @return [type]              [description]
     */
    public function pointFidelite($code_client) {
    	if ($this->client) {
    		$this->method = 'PointFidelite';    		 
       		try {
	            $this->response = $this->client->__soapCall(
	            	$this->method, 
	            	array(
		                $this->method => array(
		                    "Code_Client" => $code_client
		                )
	            	)
				);
				
				$this->log('pointFidelite: '.json_encode($this->response));
				if(is_object($this->response) && property_exists($this->response, 'PointFideliteResult')) {
					return $this->response->PointFideliteResult;
				}
				//par défaut retourne 0
				return 0;
	        } catch (Exception $e) {
	            $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
	        }
	    }
        $this->logErrors(); 
	    return false;
    }

    /**
     * [authByMail recupere les infos client par envoi de l'email client ou du code_client + nom de famille]
     * @param  [type] $email [email]
     * @return [type]        [array() contenant les infos clients]
     */
    public function identification($mode = 'email', $params = array(), $get_params_keys = false) {
    	if ($this->client) {
    		if($mode == 'email')
    			$method = 'identification_client_email';
    		elseif($mode = 'code')
    			$method = 'identification_client';
    		
    		if($method) { 
	    		if($get_params_keys) { 
	    			if(isset(self::$data[$method]))			
	    				return array_keys(self::$data[$method]);
	    			return $this->errors[] = 'self::$data['.$method.'] n\'est pas defini!';
	    		}
	       		try {
		            $response = $this->client->__soapCall(
		            	$method, 
		            	array(
			                $method => $params
		            	)
					); 
					$xml_data = $this->parseResults('any', $response); 
					if($xml_data) { 
						$WSVC_STATE = $this->getValueFromXML($xml_data, 'code_retour', '//NewDataSet/WSVC_STATE'); 
					 	if($this->verbose) {
						 	print("\nWSVC_STATE : ");
						 	print_r($WSVC_STATE);
						 	print("\n");
						}
 
				 		$WSVC_CLIENTS_STATE = $this->getValueFromXML($xml_data, 'code_retour', '//NewDataSet/WSVC_CLIENTS_STATE'); 
					 	if($this->verbose) {
						 	print("\nWSVC_CLIENTS_STATE : ");
						 	print_r($WSVC_CLIENTS_STATE);
						 	print("\n");
						 }

						$response = array(
									'WSVC_STATE' => $WSVC_STATE,
									'WSVC_CLIENTS_STATE' => $WSVC_CLIENTS_STATE,
									);

						if(isset(self::$response_data[$method])) {
						 	$WSVC_CLIENTS = array(); 
						 	foreach (self::$response_data[$method] as $key) {
						 		$WSVC_CLIENTS[$key] = $this->getValueFromXML($xml_data, $key, '//NewDataSet/WSVC_CLIENTS'); 
						 	}  
						 	if($this->verbose) {
							 	print("\nWSVC_CLIENTS : ");
							 	print_r($WSVC_CLIENTS);
							 	print("\n");
							}
							$response['WSVC_CLIENTS'] = $WSVC_CLIENTS;
						}
						$this->log('Insitaction_Webservice_Clients : ' . $WSVC_CLIENTS);
						$this->log('Insitaction_Webservice_Clients '.$method.' reponse : '.$WSVC_STATE.' '.json_encode($response));

						return $response;  
					} else {
						$this->errors[] = "La réponse ne contient aucune donnée.\n";
					}
		        } catch (Exception $e) {
		            $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
		        }
		    } else {
		    	$this->errors[] = __FUNCTION__.' La methode \'est pas definie.';
		    }
	    }
        $this->logErrors(); 
	    return false;
    }

    /**
     * [parseCustomer parse un objet customer 
     *  pour un appel webservice ajout ou modification client
     *  fonction crud() ]
     * @param  [type] $customer [description]
     * @return [type]           [description]
     */
    public static function parseCustomer($customer) {
    	if($customer && $customer instanceof Customer) {
    		//récupération de l'adresse client la plus récente
    		$addresses = $customer->getAddresses($customer->id_lang);
    		$adresse = false;
    		$country_name = false;
    		$country_iso = false;
    		$telephone = trim($customer->tel);
    		if($addresses && !empty($addresses))
	    		foreach ($addresses as $key => $address) {
	    			if(!$adresse)
	    				$adresse = $address;

	    			$timestamp_current = strtotime($address['date_upd']);
	    			$timestamp = strtotime($adresse['date_upd']);

	    			if($timestamp_current > $timestamp) 
	    				$adresse = $address;
	    		}

	    	if($adresse) {
	    		$country_name = Country::getNameById($customer->id_lang, $adresse['id_country']);
	    		$country_iso = Country::getIsoById($adresse['id_country']);
	    	} else {
	    		// default country
	    		$country_name = Country::getNameById($customer->id_lang, Configuration::get('PS_SHOP_COUNTRY_ID'));
	    		$country_iso = Country::getIsoById(Configuration::get('PS_SHOP_COUNTRY_ID'));
	    	}
    		//récupération de la langue du client
    		$langue = Language::getIsoById($customer->id_lang);
    		
    		return array( 
    								"Code_client" => '',
                                    "Civilite" => $customer->id_gender ? (int) $customer->id_gender : 1,
                                    "Nom" => $customer->lastname,
                                    "Prenom" => $customer->firstname,
                                    "Adresse1" => ($adresse ? stripslashes ($adresse['address1']) : ''),
                                    "Adresse2" => ($adresse ? stripslashes($adresse['address2']) : ''),
                                    "Code_postal" => ($adresse ? $adresse['postcode'] : ''),
                                    "Ville" => ($adresse ? $adresse['city'] : ''),
                                    "Tel_fixe" => ($telephone ? $telephone : ($adresse ? $adresse['phone'] : '')),
                                    "Tel_portable" => ($telephone ? $telephone : ($adresse ? $adresse['phone_mobile'] : '')),
                                    "Email" => $customer->email,
                                    "Date_naissance" => $customer->birthday,
                                    "Nb_enfants" => 0,
                                    "Code_pays" => strtoupper($country_iso),
                                    "Pays" => $country_name,
                                    "Code_langue" => strtoupper($langue),
                                    "Mot_de_passe" => $customer->passwd
                                );
    	}
    	return false;
    }

    /**
     * [updateCustomerFields met ajour les infos clients prestashop 
     * depuis les infos du webservice pour en preparer la maj]
     * @param  [type] &$customer [objet Customer]
     * @param  [type] $response  [Reponse du webservice client]
     * @return [type]            [description]
     */
    public static function updateCustomerFields(&$customer, $response, $new_customer = false) {
    	if(Validate::isLoadedObject($customer) || $new_customer) {
    		if(isset($response['WSVC_CLIENTS']) 
                && isset($response['WSVC_CLIENTS']['code_client'])
                 && $response['WSVC_CLIENTS']['code_client']) {

	    		// $response['WSVC_CLIENTS']['code_client'];
	            $customer->id_gender = trim($response['WSVC_CLIENTS']["civilite"]);
	            $customer->lastname = preg_replace('/[0-9!<>,;?=+()@#"°{}_$%:]/', '', trim($response['WSVC_CLIENTS']["nom"]));
	            $customer->firstname = preg_replace('/[0-9!<>,;?=+()@#"°{}_$%:]/', '', trim($response['WSVC_CLIENTS']["prenom"]));
	            // $response['WSVC_CLIENTS']["adresse1"];
	            // $response['WSVC_CLIENTS']["adresse2"];
	            // $response['WSVC_CLIENTS']["code_postal"];
	            // $response['WSVC_CLIENTS']["ville"];
	            // 
	            if(strlen(trim($response['WSVC_CLIENTS']["tel_portable"]))) {
	            	$telephone = trim($response['WSVC_CLIENTS']["tel_portable"]);
	            } else {
	            	$telephone = trim($response['WSVC_CLIENTS']["tel_fixe"]);
	            }
	            if($telephone && strlen($telephone) > 0) {
	            	$customer->tel = $telephone;
	            } elseif(is_null($customer->tel) || !($customer->tel && strlen($customer->tel) > 0)) {
	            	//protection contre les télephones vides 
	            	$customer->tel = '0600000000';
	            }
	            
	            $customer->email = trim($response['WSVC_CLIENTS']["email"]);

	            $format = "d/m/Y H:i:s";
	            $dateobj = DateTime::createFromFormat($format, $response['WSVC_CLIENTS']['date_naissance']);
		            
	            if($dateobj) {
	            	$response['WSVC_CLIENTS']['date_naissance'] = $dateobj->format('d/m/Y');
		            if(!Validate::isBirthDate($response['WSVC_CLIENTS']['date_naissance'])) {
		                $response['WSVC_CLIENTS']['date_naissance'] = null;
		            }
	            	$customer->birthday = $response['WSVC_CLIENTS']["date_naissance"];
		        }
	            return true;
	        }

    	}
    	return false;
    }

    
    /**
     * TODO reduire le nombre d'appels identification client
     * [updateCookieCustomer met à jour le cookie pour
     * conserver depuis quand il n'y a pas eu de maj]
     * @param  [type] $id_customer [description]
     * @param  [type] $cookie      [description]
     * @return [type]              [description]
     */
    public static function updateCookieCustomer($id_customer, $cookie) {
    	if(is_numeric($id_customer) && $id_customer > 0
    		&& Validate::isLoadedObject($cookie)) {
	       
	        $last_ws_client_update = $cookie->__get('last_ws_client_update');
	        if(is_array($last_ws_client_update)) {
	            $last_ws_client_update[$context->customer->id] = time();
	        } else {
	            $last_ws_client_update = [$context->customer->id => time()];
	        }
	        $cookie->__set('last_ws_client_update', $last_ws_client_update);
	        $cookie->write();
	    }
    }
    /**
     * [updateNeeded doit on mettre a jour le client sur la bdd locale ?]
     * @param  [type] $id_customer [description]
     * @param  [type] $cookie      [description]
     * @return [type]              [description]
     */
    public static function updateNeeded($id_customer, $cookie) {
    	if(is_numeric($id_customer) && $id_customer > 0
    		&& Validate::isLoadedObject($cookie)) {
	       
	        $last_ws_client_update = $cookie->__get('last_ws_client_update');
	        if(is_array($last_ws_client_update) 
	        	&& isset($last_ws_client_update[$context->customer->id])) {

	            if(time() - $last_ws_client_update[$context->customer->id] < self::$update_time) {
	            	return false;
	            } 
	        }
	        return true;
	    }
    }
}