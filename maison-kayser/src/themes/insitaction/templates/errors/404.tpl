{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_title' hide}
  {$page.title}
{/block}

{block name='page_content_container'}
  {* {include file='errors/not-found.tpl'} *}

  <section id="content" class="">
    {block name='page_content'}

      <div class="container-404">
        <div class="flex-50">
          <div class="container-404__inner container-404__left">
            <h4>{l s='Oups ! Erreur' d='Shop.Theme.Global'}</h4>
            <h4 class="big">{l s="404"}</h4>
            <p>{l s="Cette page n'existe plus." d='Shop.Theme.Global'}</p>

            <div class="btn__container">
              <a href="{$urls.base_url}" class="btn">{l s="Retour à la page d'accueil"}</a>
            </div>
          </div>
        </div>

        <div class="flex-50">
          <div class="container-404__inner">
            <img src="{$urls.base_url}{$smarty.const._THEME_DIR_}img/404.jpg" alt="Erreur 404">
          </div>
        </div>
      </div>

    {/block}
  </section>
{/block}
