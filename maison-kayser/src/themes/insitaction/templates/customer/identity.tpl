{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends 'customer/page.tpl'}

{block name='page_title'}
  {l s='Your personal information' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
	{if $my_shop}
	<form action="{$urls.current_url}" id="myshop-form" class="js-customer-form formulaire-inscription" method="post">
		<section>
			<div class="form-group">
				<label class=" col-sm-4 col-xs-12 label-my_shop form-control-label">
					{l s='Votre Boulangerie préférée' d='Shop.Theme.Customeraccount'}
	          	</label>
	          	<div class="input-form input-my_shop form-control-valign">
	          		{$my_shop->name}
	          		<br/>
	          		<a href="{$urls.current_url}?deleteMyshop=1">
						{l s='Supprimer' d='Shop.Theme.Customeraccount'}
					</a>
	          	</div>
			</div>	
	  	</section>	  
	</form>
	<hr/>
	{/if}
  	{render file='customer/_partials/customer-form.tpl' ui=$customer_form}
{/block}
