<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

class BeVisibleMapHelperConfig extends BeVisibleMapHelperFormAbstract
{
    public function __construct()
    {
        parent::__construct();

        if (Shop::isFeatureActive()) {
            if (Shop::getContext() != Shop::CONTEXT_ALL) {
                $this->enable_multishop = true;
            }
        }
    }

    public function getAutoSaveFieldsName()
    {
        $names = array();
        foreach (array_keys($this->fields_form) as $id_fieldset) {
            foreach ($this->fields_form[$id_fieldset]['form']['input'] as $field) {
                if (!(isset($field['auto_save']) && $field['auto_save'])) {
                    continue;
                }

                if ($this->enable_multishop) {
                    if (!(isset($field['disable_multistore']) && $field['disable_multistore'])) {
                        $names[] = $field['name'];
                    }
                } else {
                    $names[] = $field['name'];
                }
            }
        }

        return array_unique($names);
    }

    public function generateForm($fields_form)
    {
        foreach (array_keys($fields_form) as $id_fieldset) {
            //Add a save button to every panel
            $fields_form[$id_fieldset]['form']['submit'] = array('title' => $this->module->l('Save'));

            if ($this->enable_multishop) {
                $fields_form[$id_fieldset]['form']['legend']['show_multishop_header'] = true;
            }

            foreach ($fields_form[$id_fieldset]['form']['input'] as $key => $input) {
                $field_name = $input['name'];

                //Loadup width default values
                if (isset($input['default_value'])) {
                    $field_value = $this->fields_value[$field_name];
                    if (is_array($field_value)) {
                        foreach ($field_value as $id_lang => $value) {
                            if (!$value) {
                                $field_value[$id_lang] = $input['default_value'];
                            }
                        }
                    } else {
                        if (!$field_value) {
                            $field_value = $input['default_value'];
                        }
                    }

                    $this->fields_value[$field_name] = $field_value;
                }

                //Check checkboxes
                if ($input['type'] == 'checkbox') {
                    $checked = json_decode($this->fields_value[$field_name], true);
                    if (!$checked) {
                        $checked = array();
                    }

                    foreach ($input['values']['query'] as $item) {
                        if (in_array($item[$input['values']['id']], $checked)) {
                            $this->fields_value[$field_name.'_'.$item[$input['values']['id']]] = true;
                        } else {
                            $this->fields_value[$field_name.'_'.$item[$input['values']['id']]] = false;
                        }
                    }
                }

                //Content editor
                if ($input['type'] == 'textarea' && isset($input['editors'])) {
                    $this->fields_value[$field_name.'_editor'] = Configuration::get($field_name.'_editor');

                    $desc = '';
                    if (isset($input['desc'])) {
                        $desc = $input['desc'];
                    }

                    if ($desc) {
                        $desc .= '<br /><br />';
                    }

                    $desc .= '<span class="bevisible-bold bevisible-red">';
                    $desc .= $this->module->l('If you not see the editor content, please click on the editor selector button');
                    $desc .= '</span>';

                    $fields_form[$id_fieldset]['form']['input'][$key]['desc'] = $desc;

                    if (!$this->fields_value[$field_name.'_editor'] && isset($input['default_editor'])) {
                        $this->fields_value[$field_name.'_editor'] = $input['default_editor'];
                    }
                }
            }
        }

        return parent::generateForm($fields_form);
    }

    public function generate()
    {
        foreach ($this->fields_form as &$fieldset) {
            if (isset($fieldset['form']['input'])) {
                foreach ($fieldset['form']['input'] as &$params) {
                    $label = '';
                    if (isset($params['label'])) {
                        $label = $params['label'];
                    }

                    if ($params['type'] == 'file') {
                        if ($params['files']) {
                            foreach ($params['files'] as &$file) {
                                $ml_file_name = $this->getMultiShopFileNameAdmin($file['file_name']);

                                $module_img_uri = _MODULE_DIR_.$this->module->name.'/views/img/';
                                $image_url = $this->context->link->getMediaLink(
                                    $module_img_uri.$ml_file_name
                                );

                                if ($image_url) {
                                    $file['image'] = '<img src="'.$image_url.'">';
                                }

                                $desc = $this->getMultiShopFileMessageAdmin($file['file_name']);
                                if ($desc && $params['desc']) {
                                    $params['desc'] .= '<br /><br />';
                                }

                                if ($desc) {
                                    $params['desc'] .= $desc;
                                }
                            }
                        }
                    }

                    if ($this->enable_multishop) {
                        $disable_multistore = false;
                        if (isset($params['disable_multistore']) && $params['disable_multistore']) {
                            $disable_multistore = true;
                        }

                        if (!$disable_multistore) {
                            $is_disabled = false;
                            if (!Configuration::isOverridenByCurrentContext($params['name'])) {
                                $is_disabled = true;
                            }

                            if ($params['type'] == 'file') {
                                $module_img_dir = _PS_MODULE_DIR_.$this->module->name.'/views/img/';
                                $file_name = $this->getContextFileNameAdmin($params['files'][0]['file_name']);

                                if (file_exists($module_img_dir.$file_name)) {
                                    $is_disabled = false;
                                }
                            }

                            $params['disabled'] = $is_disabled;
                            $params['multishop_group_prefix'] = $this->identifier;
                            $this->module->bvhelper->smartyAssign(array('params' => $params));
                            $params['label'] = $this->module->bvhelper->smartyFetch(
                                'admin/multishop_form_extension.tpl'
                            ).' '.$label;

                            $params['form_group_class'] = ' conf_id_'.$params['name'];
                        } else {
                            //unset($fieldset['form']['input'][$key]);
                        }
                    }
                }
            }
        }

        return parent::generate();
    }

    public function postProcess()
    {
        $multishop_override_enabled = Tools::getValue($this->identifier.'_multishop_override_enabled', array());
        $this->fields_value = array();

        $rendered_fields = $this->getAutoSaveFieldsName();
        if ($rendered_fields) {
            foreach ($rendered_fields as $field_name) {
                if ($this->isFieldMultilang($field_name)) {
                    $this->fields_value[$field_name] = $this->bvhelper->getValueMultilang($field_name);
                } else {
                    $this->fields_value[$field_name] =  $this->bvhelper->getValue($field_name);
                }

                $field_info = $this->getFieldInfo($field_name);
                if (($field_info['type'] == 'textarea') && isset($field_info['editors'])) {
                    $this->fields_value[$field_name.'_editor'] =  $this->bvhelper->getValue($field_name.'_editor');
                    if ($this->enable_multishop && in_array($field_name, $multishop_override_enabled)) {
                        $multishop_override_enabled[] = $field_name.'_editor';
                    }
                }

                if ($field_info['type'] == 'checkbox') {
                    $this->fields_value[$field_name] = array();
                    foreach ($field_info['values']['query'] as $item) {
                        $value = $this->bvhelper->getValue($field_name.'_'.$item[$field_info['values']['id']]);
                        if ($value) {
                            $this->fields_value[$field_name][] = $value;
                        }
                    }
                    $this->fields_value[$field_name] = json_encode($this->fields_value[$field_name]);
                }

                if ($field_info['type'] == 'file') {
                    if (isset($_FILES[$field_info['name']])
                        && isset($_FILES[$field_info['name']]['tmp_name'])
                        && !empty($_FILES[$field_info['name']]['tmp_name'])
                        && !$_FILES[$field_info['name']]['error']) {
                        $this->fields_value[$field_name] = true;
                        if (isset($field_info['files'][0]['file_type']) && $field_info['files'][0]['file_type'] &&
                            isset($_FILES[$field_info['name']]['type']) && $_FILES[$field_info['name']]['type']) {
                            if ($field_info['files'][0]['file_type'] != $_FILES[$field_info['name']]['type']) {
                                $this->fields_value[$field_name] = false;
                            }
                        }
                    } else {
                        $this->fields_value[$field_name] = true;
                    }
                }

                if ($this->enable_multishop) {
                    if (!in_array($field_name, $multishop_override_enabled)) {
                        unset($this->fields_value[$field_name]);
                        Configuration::deleteFromContext($field_name);

                        if ($field_info['type'] == 'file') {
                            $module_img_dir = _PS_MODULE_DIR_.$this->module->name.'/views/img/';
                            $file_name = $this->getContextFileNameAdmin($field_info['files'][0]['file_name']);
                            if (file_exists($module_img_dir.$file_name)) {
                                unlink($module_img_dir.$file_name);
                            }
                        }
                    }
                }
            }
        }

        $valid = $this->validate();
        if ($valid) {
            $field_values = $this->fields_value;
            foreach ($field_values as $field_name => $field_value) {
                $field_info = $this->getFieldInfo($field_name);
                if ($field_info['type'] == 'file') {
                    if ($field_value) {
                        unset($this->fields_value[$field_name]);

                        $module_img_dir = _PS_MODULE_DIR_.$this->module->name.'/views/img/';
                        $file_name = $this->getContextFileNameAdmin($field_info['files'][0]['file_name']);
                        move_uploaded_file($_FILES[$field_info['name']]['tmp_name'], $module_img_dir.$file_name);
                    }
                } else {
                    Configuration::updateValue($field_name, $field_value, true);
                }
            }
        }

        return $valid;
    }

    public function getModuleDir()
    {
        return dirname(__FILE__).'/../';
    }

    public function getContextFileNameAdmin($file_name)
    {
        if (Shop::isFeatureActive() && Shop::CONTEXT_GROUP == Shop::getContext()) {
            $id_shop_group = Shop::getContextShopGroupID();
            return 'g-'.$id_shop_group.'-'.$file_name;
        }

        if (Shop::isFeatureActive() && Shop::CONTEXT_SHOP == Shop::getContext()) {
            $id_shop = Shop::getContextShopID();
            return 's-'.$id_shop.'-'.$file_name;
        }

        return $file_name;
    }

    public function getMultiShopFileNameAdmin($file_name)
    {
        $path = $this->getModuleDir().'views/img/';

        if (Shop::isFeatureActive() && Shop::CONTEXT_GROUP == Shop::getContext()) {
            $id_shop_group = Shop::getContextShopGroupID();
            $g_file_name = 'g-'.$id_shop_group.'-'.$file_name;

            if (file_exists($path.$g_file_name)) {
                return $g_file_name;
            }
        }

        if (Shop::isFeatureActive() && Shop::CONTEXT_SHOP == Shop::getContext()) {
            $id_shop = Shop::getContextShopID();
            $id_shop_group = Shop::getContextShopGroupID();
            $s_file_name = 's-'.$id_shop.'-'.$file_name;
            $g_file_name = 'g-'.$id_shop_group.'-'.$file_name;

            if (file_exists($path.$s_file_name)) {
                return $s_file_name;
            }

            if (file_exists($path.$g_file_name)) {
                return $g_file_name;
            }
        }

        if (file_exists($path.$file_name)) {
            return $file_name;
        }

        if (file_exists($path.'default-'.$file_name)) {
            return 'default-'.$file_name;
        }

        return '';
    }

    public function getMultiShopFileMessageAdmin($file_name)
    {
        $path = $this->getModuleDir().'views/img/';

        if (Shop::isFeatureActive() && Shop::CONTEXT_GROUP == Shop::getContext()) {
            $id_shop_group = Shop::getContextShopGroupID();
            $g_file_name = 'g-'.$id_shop_group.'-'.$file_name;

            if (!file_exists($path.$g_file_name) && file_exists($path.$file_name)) {
                return $this->module->l('This shop group is using the all shop image.');
            }

            if (!file_exists($path.$g_file_name) && file_exists($path.'default-'.$file_name)) {
                return $this->module->l('This shop group is using the default image.');
            }
        }

        if (Shop::isFeatureActive() && Shop::CONTEXT_SHOP == Shop::getContext()) {
            $id_shop = Shop::getContextShopID();
            $id_shop_group = Shop::getContextShopGroupID();
            $s_file_name = 's-'.$id_shop.'-'.$file_name;
            $g_file_name = 'g-'.$id_shop_group.'-'.$file_name;

            if (!file_exists($path.$s_file_name)) {
                if (file_exists($path.$g_file_name)) {
                    return $this->module->l('This shop is using shop group image.');
                }

                if (file_exists($path.$file_name)) {
                    return $this->module->l('This shop is using the all shop image.');
                }

                if (file_exists($path.'default-'.$file_name)) {
                    return $this->module->l('This shop is using the default image.');
                }
            }
        }

        return '';
    }
}
