<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

class BeVisibleGeocodeModuleFrontController extends ModuleFrontController
{
    /** @var FsStoresMap */
    public $module;

    private static $distance_max = 150;

    public function initContent()
    {
        $response = array('status' => 'ok');
        $mode = Tools::getValue('mode', 'context');

        switch ($mode) {
            case 'context':
                $response = array_merge($response, $this->geocodeContext());
                break;
            case 'address':
                $response = array_merge($response, $this->geocodeAddress());
                break;
        }

        die(json_encode($response));
    }

    protected function geocodeAddress()
    {
        $address = Tools::getValue('address');
        $centerOnStore = Tools::getValue('centerOnStore');
        $services = json_decode(Tools::getValue('services'));
        if($services) {
            $services = get_object_vars($services);
        } 
        if ($address) {
            $coords = $this->geocode($address);
            if ($coords) {
                $result = $this->getNearestStore($coords['lat'], $coords['lng'], $services);

                $zone = false;                
                $country = false; 
                $stores = isset($result['closest_stores']) ? $result['closest_stores'] : false;
                if(isset($services['cafe']) && !$services['cafe']
                    && isset($services['resto']) && !$services['resto']
                        && isset($services['sansgluten']) && !$services['sansgluten'])
                    //si aucun services dans le tri on les passe a false
                    $services = false;
                
               

                $back = Tools::getValue('back');

                $this->module->bvhelper->smartyAssign(array(
                        'zone' => $zone,
                        'stores' => $stores,
                        'country' => $country,
                        'services' => $services ,
                        'link' => $this->context->link,
                        'back' => $back
                    )
                );
                $content = $this->module->bvhelper->smartyFetch('front/zone.tpl', true); 

                // Si des services sélectionnéses on réduit l'affichage sur la carte aux boutiques répondant aux criteres
                if($services) {
                    $stores_by_ID = array();
                    foreach ($stores as $key => $store) {
                        $stores_by_ID[$store['id_store']] = $store;
                    }
                    $stores = $stores_by_ID;
                }

                return array(
                    'id_store' => isset($result['closest_id_store']) ? $result['closest_id_store'] : false,
                    'stores' => $stores,
                    'address' => $address,
                    'coords' => $coords,
                    'centerOnStore' => (int) $centerOnStore,
                    'refresh_content' => $content,
                    'reload' => ($services ? true : false)
                );
            } else {
                return array('error' => 'no-result');
            }
        } elseif($services) {
            $result = $this->getNearestStore(-999999, -999999, $services);
            $zone = false;                
            $country = false; 
            $stores = isset($result['closest_stores']) ? $result['closest_stores'] : false;
            if(isset($services['cafe']) && !$services['cafe']
                && isset($services['resto']) && !$services['resto']
                    && isset($services['sansgluten']) && !$services['sansgluten'])
                //si aucun services dans le tri on les passe a false
                $services = false;        
           
            $back = Tools::getValue('back');

            $stores_content = $stores;
            // reorder stores by postcode and names   
            usort($stores_content, function($a, $b) {
                if(isset($a['postcode']) && isset($b['postcode']))
                    if($a['postcode'] == $b['postcode']) {
                        //si un chiffre est présent on teste le chiffre
                        if(preg_match('/^([0-9]+).*$/', $a['name'], $a_nb)) {
                            if(preg_match('/^([0-9]+).*$/', $b['name'], $b_nb)) {
                                if(isset($a_nb[1]) && isset($b_nb[1]))
                                    return $a_nb[1] > $b_nb[1];
                            }
                        }
                        return strcmp($a['name'], $b['name']);
                    }
                    return $a['postcode'] > $b['postcode'];
                return false;
            });


            $this->module->bvhelper->smartyAssign(array(
                    'zone' => $zone,
                    'stores' => $stores_content,
                    'country' => $country,
                    'services' => $services ,
                    'link' => $this->context->link,
                    'back' => $back,
                )
            );
            $content = $this->module->bvhelper->smartyFetch('front/zone.tpl', true); 

            return array(
                'id_store' => isset($result['closest_id_store']) ? $result['closest_id_store'] : false,
                'stores' => $stores,
                'address' => $address,
                'coords' => $coords,
                'centerOnStore' => (int) $centerOnStore,
                'refresh_content' => $content,
                'reload' => true,
            );
        } else {
            return array('error' => 'no-address');
        }
    }

    protected function geocodeContext()
    {
        if (Validate::isLoadedObject($this->context->customer)) {
            $addresses = $this->context->customer->getAddresses($this->context->language->id);
            if ($addresses) {
                $address = array_pop($addresses);
                $address_text = $address['address1'].', '.$address['city'];
                if ($address['state']) {
                    $address_text .= ', '.$address['state'];
                }
                $address_text .= ', '.$address['country'].', '.$address['postcode'].'';

                $coords = $this->geocode($address_text);
                if ($coords) {
                    $result = $this->getNearestStore($coords['lat'], $coords['lng']);
                    return array(
                        'id_store' => isset($result['closest_id_store']) ? $result['closest_id_store'] : false,
                        'stores' => isset($result['closest_stores']) ? $result['closest_stores'] : false,
                        'address' => $address_text
                    );
                } else {
                    return array('error' => 'no-result');
                }
            } else {
                return array('error' => 'no-address');
            }
        } else {
            return array('error' => 'no-customer');
        }
    }

    protected function geocode($address)
    {
        //ADD BY ESTEBANW 30/05/2018 - correction gestion code postal
        if(is_numeric($address)) {
            if(preg_match('/^[0-9]{5}$/', $address)){
                $address .= ',fr';
            } elseif(preg_match('/^[0-9]{2}$/', $address)) {
                $address .= '000,fr';
            }
        }

        $query = array(
            'key' => Configuration::get('BEVISIBLE_MAP_API_KEY'),
            'address' => $address
        );

        $http_query = http_build_query($query);
        $result = Tools::file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?'.$http_query);
        $result = json_decode($result, true); 
        
        if ($result['status'] == 'OK') {
            return array(
                'lat' => $result['results'][0]['geometry']['location']['lat'],
                'lng' => $result['results'][0]['geometry']['location']['lng']
            );
        } else {
            return false;
        }
    }

    /**
     * [getNearestStore retourne l'id_store le plus proches et les magasins les plus proches trouvés]
     * @param  [type] $lat      [description]
     * @param  [type] $lng      [description]
     * @param  array  $services [description]
     * @return [type]           [description]
     */
    protected function getNearestStore($lat, $lng, $services = array())
    {
        $closest_id_store = null;
        $closest_distance = null;
        //ADD BY ESTEBANW INSITACTION 01062018
        //Permet l'affichage de tout les magasin si aucune adresse n'est saisie
        $show_all = ($lat == -999999 && $lng == -999999 ? true : false);
        // end
        $stores = $this->module->getStoresFront(true, $services);        

        $stores_by_distance = array();
        $stores_by_ID = array();

        foreach ($stores as $key => &$store) {
            $dist = $this->module->getDistanceBetweenPointsNew(
                $lat,
                $lng,
                $store['latitude'],
                $store['longitude']
            );

            if (!is_null($closest_distance)) {
                if ($dist < $closest_distance) {
                    $closest_id_store = $store['id_store'];
                    $closest_distance = $dist;
                }
            } else {
                $closest_id_store = $store['id_store'];
                $closest_distance = $dist;
            }
            if($dist < self::$distance_max || $show_all) {
                $dist = floatval($dist) * 1000;
                $store['distance'] = $dist;
                if($show_all) {
                    $stores_by_ID[$store['id_store']] = $store;
                } elseif(!isset($stores_by_distance[$dist])) {
                    $stores_by_distance[$dist] = $store;
                } else {
                    while(isset($stores_by_distance[$dist])) {
                        $dist += 1;
                    }
                    $stores_by_distance[$dist] = $store;
                } 
            }
        } 
        if($show_all) {
            // les magasins sont indexés par ID afin de reactualiser la map
            $stores_by_distance = $stores_by_ID;
        }

        ksort($stores_by_distance); 


        // si il y a une distance > a 100 km à l'addresse sélectionnée
        if($closest_distance > self::$distance_max && !$show_all)
            return array(
                    'closest_id_store' => false,
                    'closest_stores' => $stores_by_distance
                );

        return array(
                    'closest_id_store' => $closest_id_store,
                    'closest_stores' => $stores_by_distance
                );
    }
}
