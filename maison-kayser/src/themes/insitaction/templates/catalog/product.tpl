{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='head_seo' prepend}
  <link rel="canonical" href="{Product::getMainShopCanonical($product.id, $product.canonical_url)}">
{/block}

{block name='head' append}
  <meta property="og:type" content="product">
  <meta property="og:url" content="{$urls.current_url}">
  <meta property="og:title" content="{$page.meta.title}">
  <meta property="og:site_name" content="{$shop.name}">
  <meta property="og:description" content="{$page.meta.description}">
  <meta property="og:image" content="{$product.cover.large.url}">
  <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
  <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
  <meta property="product:price:amount" content="{$product.price_amount}">
  <meta property="product:price:currency" content="{$currency.iso_code}">
  {if isset($product.weight) && ($product.weight != 0)}
  <meta property="product:weight:value" content="{$product.weight}">
  <meta property="product:weight:units" content="{$product.weight_unit}">
  {/if}
{/block}

{block name='content'}

  <section id="main" class="container" itemscope itemtype="https://schema.org/Product">
    <meta itemprop="url" content="{$product.url}">

    <div class="row">
      <div class="col-sm-6">
        {block name='page_content_container'}
          <section class="page-content" id="content">
            {block name='page_content'}
              {block name='product_cover_thumbnails'}
                {include file='catalog/_partials/product-cover-thumbnails.tpl'}
              {/block}
              <div class="scroll-box-arrows">
                <i class="material-icons left">&#xE314;</i>
                <i class="material-icons right">&#xE315;</i>
              </div>

            {/block}
          </section>
        {/block}
        </div>
        <div class="col-sm-6">
          <div class="box-shadow-container">
            <div class="box-shadow"></div>
            <div class="product--info">
              {block name='page_header_container'}
                {block name='page_header'}
                  {block name='product_flags'}
                    {if isset($product.flags) && count($product.flags)}
                      <ul class="product-flags">
                        {foreach from=$product.flags item=flag}
                          <li class="product-flag {$flag.type}"><span>{$flag.label}</span></li>
                        {/foreach}
                      </ul>
                    {elseif isset($product.features) && count($product.features)}
                      <ul class="product-flags product-page-flags">
                        {foreach from=$product.features item=feature}
                          {if $feature.id_feature == 14}
                          <li class="product-flag-kayser new"><span>{$feature.value}</span></li>
                          {/if}
                        {/foreach}
                      </ul>
                    {/if}
                  {/block}
                  <h1 class="h1 product--title" itemprop="name">
                    {block name='page_title'}{$product.name}{/block}
                    {foreach from=$product.features item=feature}
                      {if $feature.id_feature == 5 && is_numeric($feature.value)}
                        <div class="product--piece">{$feature.value} {if $feature.value > 1}{l s='personnes' d='Shop.Theme.Global'}{else}{l s='personne' d='Shop.Theme.Global'}{/if}</div>
                      {/if}
                    {/foreach}
                  </h1>
                {/block}
              {/block}

              {block name='product_caracteristiques'}
                {foreach from=$product.features item=feature}
                  {if $feature.id_feature == 6 && is_numeric($feature.value)}
                    <div class="product--poids">{$feature.value} {l s='g' d='Shop.Theme.Global'}</div>
                  {/if}
                  {if $feature.id_feature == 4}
                    <div class="product--piece">{$feature.value}{if is_numeric($feature.value)} {if $feature.value > 1}{l s='pièces' d='Shop.Theme.Global'}{else}{l s='pièce' d='Shop.Theme.Global'}{/if}{/if}</div>
                  {/if}
                {/foreach}
              {/block}


              {block name='product_picto'}
                 <ul class="product--pictos">
                  {foreach from=$product.features item=feature}
                    {if $feature.id_feature == 10}
                      <li class="sansgluten{if strtolower($feature.value) == 'non'} hidden-xs-up{/if}" title="Sans gluten"></li>
                    {elseif $feature.id_feature == 8}
                      <li class="vegetarien{if strtolower($feature.value) == 'non'} hidden-xs-up{/if}" title="Végétarien"></li>
                    {elseif $feature.id_feature == 9}
                      <li class="vegetalien{if strtolower($feature.value) == 'non'} hidden-xs-up{/if}" title="Végétalien"></li>
                    {/if}
                  {/foreach}
                </ul>
              {/block}


              <div class="product-information">
                {block name='product_description_short'}
                  <div id="product-description-short-{$product.id}" class="product--description" itemprop="description">
                    {if $product.description_short}
                      {$product.description_short|truncate:100:"..." nofilter}
                      {if $product.description}
                        <a href="#product-description" id="see-more" class="scroll-to">{l s='Lire la suite' d='Shop.Theme.Catalog'}</a>
                      {/if}
                    {else if $product.description}
                      {$product.description|truncate:100:"..." nofilter}
                      <a href="#product-description" id="see-more" class="scroll-to">{l s='Lire la suite' d='Shop.Theme.Catalog'}</a>
                    {/if}
                  </div>
                {/block}

                {if $product.is_customizable && count($product.customizations.fields)}
                  {block name='product_customization'}
                    {include file="catalog/_partials/product-customization.tpl" customizations=$product.customizations}
                  {/block}
                {/if}

                <div class="product-actions">
                  {block name='product_buy'}
                    <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                      <input type="hidden" name="token" value="{$static_token}">
                      <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                      <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">

                      {block name='product_variants'}
                        {include file='catalog/_partials/product-variants.tpl'}
                      {/block}

                      {block name='product_pack'}
                        {if $packItems}
                          <section class="product-pack">
                            <h3 class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</h3>
                            {foreach from=$packItems item="product_pack"}
                              {block name='product_miniature'}
                                {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}
                              {/block}
                            {/foreach}
                        </section>
                        {/if}
                      {/block}

                      {block name='product_discounts'}
                        {include file='catalog/_partials/product-discounts.tpl'}
                      {/block}

                      {block name='product_add_to_cart'}
                        {include file='catalog/_partials/product-add-to-cart.tpl'}
                      {/block}

                      {block name='product_additional_info'}
                        <div style="display: none;">
                          {include file='catalog/_partials/product-additional-info.tpl'}
                        </div>
                      {/block}

                      {block name='product_refresh'}
                        <input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="{l s='Refresh' d='Shop.Theme.Actions'}">
                      {/block}
                    </form>
                  {/block}

                </div>

                {block name='hook_display_reassurance'}
                  <div style="display: none">
                    {hook h='displayReassurance'}
                  </div>
                {/block}
            </div>
          </div>
        </div>
        <div class="product__disponibilite">
          <div class="product__disponibilite--title">{l s='disponible en' d='Shop.Theme.Global'}</div>
          <ul class="product__disponibilite--list">

              <li class="delai_clickandcollect" {if !isset($delais.clickandcollect) || $delais.clickandcollect === false} style="color:#e5e5e5;"{/if}>
                <span>{l s='Retrait'}<br />{l s='dans votre boulangerie' d='Shop.Theme.Global'}&nbsp;<strong>
                  {if isset($delais.clickandcollect) &&  $delais.clickandcollect !== false}
                    {if $delais.clickandcollect.type == 'minutes'}
                      {if $delais.clickandcollect.value == 0}
                        {l s='Immédiatement' d='Shop.Theme.Catalog'}
                      {else}
                        {l s='dans' d='Shop.Theme.Catalog'}&nbsp;{$delais.clickandcollect.value} {l s='minutes' d='Shop.Theme.Catalog'}
                      {/if}
                    {elseif $delais.clickandcollect.type == 'day'}
                      {l s='à partir de' d='Shop.Theme.Catalog'}&nbsp;{$delais.clickandcollect.value}
                    {else}
                      {l s='à partir du' d='Shop.Theme.Catalog'}&nbsp;{$delais.clickandcollect.value}
                    {/if}
                  {else}
                    {l s='en 30 minutes' d='Shop.Theme.Catalog'}
                  {/if}
                </strong></span>
              </li>

               <li class="delai_livraison" {if !isset($delais.velo) || $delais.velo === false} style="color:#e5e5e5;{if (isset($delais.voiture) && $delais.voiture)};display:none{/if}"{/if}>
                <span>{l s='Livraison' d='Shop.Theme.Catalog'}<br />{l s='sur rendez-vous' d='Shop.Theme.Catalog'}&nbsp;<strong>
                  {if isset($delais.velo) &&  $delais.velo !== false}
                    {if $delais.velo.type == 'minutes'}
                      {l s='dans' d='Shop.Theme.Catalog'}&nbsp;{$delais.velo.value} {l s='minutes' d='Shop.Theme.Catalog'}
                    {elseif $delais.velo.type == 'day'}
                      {l s='à partir de' d='Shop.Theme.Catalog'}&nbsp;{$delais.velo.value}
                    {else}
                      {l s='à partir du' d='Shop.Theme.Catalog'}&nbsp;{$delais.velo.value}
                    {/if}
                  {else}
                    {l s='en 6H' d='Shop.Theme.Catalog'}
                  {/if}
                </strong></span>
              </li>

              <li class="delai_livraison voiture" {if (isset($delais.velo) && $delais.velo) || !isset($delais.voiture) || $delais.voiture === false} style="display:none"{/if}>
                <span>{l s='Livraison' d='Shop.Theme.Global'}<br />{l s='sur rendez-vous' d='Shop.Theme.Global'}&nbsp;<strong>
                  {if isset($delais.voiture) &&  $delais.voiture !== false}
                    {if $delais.voiture.type == 'minutes'}
                      {l s='dans' d='Shop.Theme.Catalog'}&nbsp;{$delais.voiture.value} {l s='minutes' d='Shop.Theme.Catalog'}
                    {elseif $delais.voiture.type == 'day'}
                      {l s='à partir de' d='Shop.Theme.Catalog'}&nbsp;{$delais.voiture.value}
                    {else}
                      {l s='à partir du' d='Shop.Theme.Catalog'}&nbsp;{$delais.voiture.value}
                    {/if}
                  {else}
                    {l s='en 48H' d='Shop.Theme.Catalog'}
                  {/if}
                </strong></span>
              </li>

              <li class="delai_colissimo" {if $product.delai_colissimo == "0" } style="color:#e5e5e5;" {/if}>
                <span>{l s='Expédition' d='Shop.Theme.Catalog'}<br />{l s='à domicile' d='Shop.Theme.Catalog'}<br />{l s='en France' d='Shop.Theme.Catalog'}{*&nbsp;<strong>{l s='en' d='Shop.Theme.Global'}&nbsp;{if !is_null($product.delai_colissimo) && $product.delai_colissimo}{$product.delai_colissimo}{else}{l s='48' d='Shop.Theme.Catalog'}{/if}h</strong>*}</span>
              </li>
          </ul>
        </div>

      </div><!-- .col.md-6 -->
    </div>
    {block name='product_footer'}
      {hook h='displayFooterProduct' product=$product category=$category}
    {/block}

    {block name='product_images_modal'}
      {if isset($formules) && $formules
        && isset($attributes_images) && count($attributes_images)}
        {include file='catalog/_partials/product-formule-images-modal.tpl'}
      {else}
        {include file='catalog/_partials/product-images-modal.tpl'}
      {/if}
    {/block}

    {block name='page_footer_container'}
      <footer class="page-footer">
        {block name='page_footer'}
          <!-- Footer content -->
        {/block}
      </footer>
    {/block}
  </section>

  {if $product.description}
    <div id="product-description" class="product--description">
      {$product.description nofilter}
    </div>
  {/if}
  {block name='product_tabs'}
    <div class="tabs-insita">
      <ul id="product_nav_tabs" class="nav nav-tabs" role="tablist">
        {assign var=first_tab value=true}
        {if isset($attributes_features) && count($attributes_features)}
          {include file='catalog/_partials/formule-features.tpl' part='top'}
        {else}
          {foreach from=$product.features item=feature}
            {if in_array($feature.id_feature, $display_features) && strtolower($feature.value) != 'non'}
            <li class="nav-item">
                <a
                  class="nav-link{if $first_tab} active{/if}"
                  data-toggle="tab"
                  href="#{str_replace(' ', '', $feature.name)}"
                  role="tab"
                  aria-controls="description"
                  {if $first_tab} aria-selected="true"{/if}>{$feature.name}</a>
                  {assign var=first_tab value=false}
            </li>
            {/if}
          {/foreach}
        {/if}
        {if $product.attachments}
          <li class="nav-item">
            <a
              class="nav-link"
              data-toggle="tab"
              href="#attachments"
              role="tab"
              aria-controls="attachments"
              {if $first_tab} aria-selected="true"{/if}>{l s='Attachments' d='Shop.Theme.Catalog'}</a>
                {assign var=first_tab value=false}
          </li>
        {/if}
        {*{foreach from=$product.extraContent item=extra key=extraKey}
          <li class="nav-item">
            <a
              class="nav-link"
              data-toggle="tab"
              href="#extra-{$extraKey}"
              role="tab"
              aria-controls="extra-{$extraKey}">{$extra.title}</a>
          </li>
        {/foreach} *}
      </ul>
      {if isset($attributes_features) && count($attributes_features)}
        {include file='catalog/_partials/formule-features.tpl' part='content'}
      {else}
        <div class="tab-content" id="tab-content">


          {*block name='product_details'}
            {include file='catalog/_partials/product-details.tpl'}
          {/block*}

          {assign var=first_tab value=true}
          {foreach from=$product.features item=feature}
            {if in_array($feature.id_feature, $display_features) && strtolower($feature.value) != 'non'}
              {if $feature.id_feature == 1}
                {assign var=classnav value='composition'}

              {elseif $feature.id_feature == 2}
                {assign var=classnav value='allergenes'}

              {elseif $feature.id_feature == 3}
                {assign var=classnav value='valeurnutritionnelles'}

              {elseif $feature.id_feature == 7}
                {assign var=classnav value='description'}

              {/if}

              <div class="tab-pane in{if $first_tab} active{/if}" id="{str_replace(' ', '', $feature.name)}" role="tabpanel">
                <div class="product-{$classnav} product-info">
                  <div class="product-{$classnav}__content product-info_content">
                    <div class="product-{$classnav}__title product-info_title">
                      {$feature.name}
                    </div>
                    <div class="{$classnav}">
                      {$feature.value}
                    </div>
                  </div>
                </div>
              </div>
              {assign var=first_tab value=false}
            {/if}
          {/foreach}

          {block name='product_attachments'}
            {if $product.attachments}
            <div class="tab-pane in{if $first_tab} active{/if}" id="attachments" role="tabpanel">
                <section class="product-attachments">
                  <h3 class="h5 text-uppercase">{l s='Download' d='Shop.Theme.Actions'}</h3>
                  {foreach from=$product.attachments item=attachment}
                    <div class="attachment">
                      <h4><a href="{url entity='attachment' params=['id_attachment' => $attachment.id_attachment]}">{$attachment.name}</a></h4>
                      <p>{$attachment.description}</p
                      <a href="{url entity='attachment' params=['id_attachment' => $attachment.id_attachment]}">
                        {l s='Download' d='Shop.Theme.Actions'} ({$attachment.file_size_formatted})
                      </a>
                    </div>
                  {/foreach}
                </section>
              </div>
              {assign var=first_tab value=false}
            {/if}
          {/block}

          {foreach from=$product.extraContent item=extra key=extraKey}
          <div class="tab-pane in{if $first_tab} active{/if} {$extra.attr.class}" id="extra-{$extraKey}" role="tabpanel" {foreach $extra.attr as $key => $val} {$key}="{$val}"{/foreach}>
            {$extra.content nofilter}
          </div>
          {assign var=first_tab value=false}
          {/foreach}
        </div>
      {/if}
    </div>
  {/block}
  {block name='product_accessories'}
    {if $accessories}
      <section class="product-accessories clearfix container">
        <h3 class="label-cross-product">{l s='à découvrir' d='Shop.Theme.Catalog'}</h3>
        <div class="featured-products">
          <div class="swiper-container swiper--featured-products">
              <div class="swiper-wrapper">
                {foreach from=$accessories item="product_accessory"}
                  {block name='product_miniature'}
                    <div class="swiper-slide">
                      {include file='catalog/_partials/miniatures/product.tpl' product=$product_accessory}
                    </div>
                  {/block}
                {/foreach}
              </div>
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>
          </div>
        </div>
      </section>
    {/if}
  {/block}
{/block}
