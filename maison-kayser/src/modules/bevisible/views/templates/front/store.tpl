{**
 *  2017 ModuleFactory.co
 *
 *  @author    ModuleFactory.co <info@modulefactory.co>
 *  @copyright 2017 ModuleFactory.co
 *  @license   ModuleFactory.co Commercial License
 *}

{extends file='page.tpl'}

{block name='page_title'}
    {* {$bevisible_title} *}
{/block}

{block name='page_content'}

    {* Image *}
    <div class="storeloc__container storeloc__container--shop"{if isset($bg_image) && $bg_image}style="background-image:url('{$bg_image}');"{/if}></div>

    <div id="bevisible-wrapper" class="container">
        <div class="shop__container shop__item">
            {* Information du magasin *}
            <div class="shop__information">
                <div class="picto picto--location"></div>
                <h1 class="shop__title">{$store.name}</h1>
                <h2 class="shop__adresse">
                    {$store.address1}
                    {if strlen(trim($store.address2))}
                        <br/>{$store.address2}
                    {/if} 
                    {*if strlen(trim($store.arrondissement))}
                        <br/>{$store.arrondissement} {l s='arrondissement'}
                    {/if*} 
                    {if strlen(trim($store.quartier))}
                        <br/>{$store.quartier}
                    {/if}  
                    <br>{$store.postcode} {$store.city}
                </h2>
                <a href="tel:{$store.phone}" class="shop__tel">{$store.phone}</a>
                <div class="shop__horaire">
                    {assign var=today value=($smarty.now|date_format:'n')-1}
                    {if $today < 0}
                        {if $today == -1}
                            {assign var=today value=5}
                        {else}
                            {assign var=today value=6}
                        {/if}
                    {/if} 
                    
                    {assign var=closed value=true}
                    {if isset($store['hours'][$today])}
                        {if !$store['hours'][$today]}
                            {l s='Fermé le'} {$bevisible_days_translations[$today]}
                        {else}
                            {assign var=closed value=false}
                            {l s='Ouvert de'} {$store['hours'][$today]}
                        {/if}
                    {/if}
                    {if !$closed}
                    {foreach $store.hours as $key => $day}
                        {if !$day}
                            {l s=', fermé le'} {$bevisible_days_translations.$key}
                        {/if}
                    {/foreach}
                    {/if}               
                </div>
                {if $store.hours}
                <div class="horaire__tab">
                    {foreach $store.hours as $key => $day}
                    <div class="horaire">
                        <div class="horaire-jour">{$bevisible_days_translations.$key}</div>
                        <div class="horaire-heure">{if !$day}{l s='Fermé'}{else}{$day}{/if}</div>
                    </div> 
                    {/foreach}
                </div>
                {/if}
                {* Ouverture google map *}
                <a href="https://www.google.com/maps/place/Eric+Kayser/@{$store.latitude},{$store.longitude},17z/data=!3m1!4b1!4m5!3m4!1s0x47e66589e9c24ad3:0x8969bebd76a36c1!8m2!3d48.858337!4d2.328237" target="_blank" class="geoloc-map">{l s="S'y rendre"}</a> 
                <br>
                <a href="mailto:{$store.email}" class="contact">{l s="Nous contacter"}</a>
                <br>

                {if $store_choose_action}
                    <form action="{$bevisible_url}" method="post" id="choose-store">
                        <input type="hidden" name="token" value="{$static_token}"/>
                        <input type="hidden" name="id_store" value="{$store.id_store}" id="choose_store_id_store"/>
                        <input type="hidden" name="method" value="choose-store-submit"/>
                        <input type="hidden" name="back" value="{$back}"/>
                       
                        <br/>
                        <button
                            class="btn btn-primary choose-store"
                            name="choose-store-submit"
                            data-button-action="choose-store" 
                            data-id-store="{$store.id_store}"
                            data-token="{$static_token}"
                            data-action-url="{$bevisible_url}"
                            type="submit" 
                          >
                            {l s="Choisir cette boulangerie" d='Shop.Theme.Actions'} 
                          </button>
                    </form>
                {else}
                    {if $has_shop}
                        {* Decouvrir produit : Selection magasin + redirect HP *}
                        <a href="{$urls.base_url}" class="button button--secondary button__select-store">{l s="Découvrez nos produits"}</a>
                    {else}
                        <p class="button button--secondary button__select-store">{l s='Cette boulangerie n\'a pas encore de boutique en ligne.'}</p>
                    {/if}

                {/if}

                {if strlen(trim($store.description))}
                    <div class="description__magasin">
                        {$store.description nofilter} 
                    </div>
                {elseif isset($bevisible_content_before) && $bevisible_content_before}
                    <div class="description__magasin">
                        {$bevisible_content_before nofilter} 
                    </div>
                {/if}
            </div>

            {* container map  *}
            <div class="store-map__container"> 
                <div id="bevisible-map-wrapper">
                    <div id="bevisible-map"></div>
                </div>  

                {* Services *}
                {if $has_services}
                <div class="services__container">
                    <div class="service__title title">{l s="Nos services"}</div>
                    <div class="services__inner">
                        {if $store.sans_gluten}
                        <div class="service__item"><div class="box"><span class="service__content"><span class="picto picto--sansgluten"></span><span class="title">{l s="Corner sans gluten"}</span></span></div></div>
                        {/if}

                        {if $store.cafe}
                        <div class="service__item"><div class="box"><span class="service__content"><span class="picto picto--cafe"></span><span class="title">{l s="Café"}</span></span></div></div>
                        {/if}

                        {if $store.restaurant}
                        <div class="service__item"><div class="box"><span class="service__content"><span class="picto picto--resto"></span><span class="title">{l s="Le restaurant du boulanger"}</span></span></div></div>
                        {/if}

                        {if $store.clickandcollect }
                        <div class="service__item"><div class="box"><span class="service__content"><span class="picto picto--clickncollect"></span><span class="title">{l s="Click & collect"}</span></span></div></div>
                        {/if}

                        {if isset($store.wifi) && $store.wifi}
                        <div class="service__item"><div class="box"><span class="service__content"><span class="picto picto--wifi"></span><span class="title">{l s="Wifi"}</span></span></div></div>
                        {/if}

                        {if $store.terrasse}
                        <div class="service__item"><div class="box"><span class="service__content"><span class="picto picto--terasse"></span><span class="title">{l s="Terrasse"}</span></span></div></div>
                        {/if}
                    </div>
                </div>
                {/if}
                {* Moments *}
                {if strlen(trim($store.commercial))} 
                <div class="moment__container">
                    <h2 class="service__title title">{l s='En ce moment dans votre boulangerie'}</h2>
                    <div class="moment__content">
                        {$store.commercial nofilter}                        
                    </div>
                </div>
                {elseif isset($bevisible_content_after) && $bevisible_content_after} 
                <div class="moment__container">
                    <h2 class="service__title title">{l s='En ce moment dans votre boulangerie'}</h2>
                    <div class="moment__content">
                        {$bevisible_content_after nofilter}                        
                    </div>
                </div>
                {/if}
            </div>
        </div>
    </div>

{/block}