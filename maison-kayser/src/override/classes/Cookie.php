<?php

class Cookie extends CookieCore
{     
    /*
     * Delete cookie
     * As of version 1.5 don't call this function, use Customer::logout() or Employee::logout() instead;.
     */
    public function logout()
    {
        parent::logout();

        $main_shop_cookie = self::getMainShopCookie();
        if($main_shop_cookie) {
            $main_shop_cookie->_content = array();
            $main_shop_cookie->encryptAndSetCookie();
            unset($_COOKIE[$main_shop_cookie->_name]);
            $main_shop_cookie->_modified = true;
        }
    }

    /**
     * Soft logout, delete everything links to the customer
     * but leave there affiliate's informations.
     * As of version 1.5 don't call this function, use Customer::mylogout() instead;.
     */
    public function mylogout()
    {
        parent::mylogout();

        $main_shop_cookie = self::getMainShopCookie();
        if($main_shop_cookie) {
            // eleve les infos de connexion sur la boutique principale
            // + redirection 
            unset($main_shop_cookie->_content['id_customer']);
            unset($main_shop_cookie->_content['id_guest']);
            unset($main_shop_cookie->_content['is_guest']);
            unset($main_shop_cookie->_content['id_connections']);
            unset($main_shop_cookie->_content['customer_lastname']);
            unset($main_shop_cookie->_content['customer_firstname']);
            unset($main_shop_cookie->_content['passwd']);
            unset($main_shop_cookie->_content['logged']);
            unset($main_shop_cookie->_content['email']);
            unset($main_shop_cookie->_content['id_cart']);
            unset($main_shop_cookie->_content['id_address_invoice']);
            unset($main_shop_cookie->_content['id_address_delivery']);
            $main_shop_cookie->_modified = true;
            $main_shop_cookie->write();
            
            $context = Context::getContext();
            Tools::redirect($context->link->getBaseLink(1));
        }
    }

    public static function getMainShopCookie() {        
        $force_ssl = Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE');
        $cookie_lifetime = (int)Configuration::get('PS_COOKIE_LIFETIME_FO');
        if ($cookie_lifetime > 0) {
            $cookie_lifetime = time() + (max($cookie_lifetime, 1) * 3600);
        }
        $main_shop = new Shop(1);

        if(Validate::isLoadedObject($main_shop)) {
            // Le choix des boulangeries concernent uniquement les id_shop > 1
            $domains = null;
            // tant que les domaines ne sont pas différents 
            // prestashop le garde à null pour instancier les cookie
            if ($main_shop->domain != $main_shop->domain_ssl) {
                $domains = array($main_shop->domain_ssl, $shop->domain);
            }
            $cookie = new Cookie('ps-s'.$main_shop->id, '', $cookie_lifetime, $domains, false, $force_ssl);
            return $cookie;
        }
        return false;
    }
}