{**
 * 2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 *
 *} 

<strong>{$bevisible_store.name|escape:'html':'UTF-8'}</strong><br />
{$bevisible_store.address1|escape:'html':'UTF-8'} {$bevisible_store.address2|escape:'html':'UTF-8'}<br />
{$bevisible_store.city|escape:'html':'UTF-8'}{if $bevisible_store.state_iso_code}, {$bevisible_store.state_iso_code|escape:'html':'UTF-8'}{/if}<br />
{$bevisible_store.postcode|escape:'html':'UTF-8'}<br />
{foreach $bevisible_store.hours as $key => $day}
    <br /><strong>{$bevisible_days_translations.$key|escape:'html':'UTF-8'}:</strong> {$day|escape:'html':'UTF-8'}
{/foreach}
<br />
<br /> 
{*<a href="{$bevisible_get_direction_url|escape:'html':'UTF-8'|bevisibleCorrectTheMess}" data-href="{$bevisible_get_direction_url|escape:'html':'UTF-8'|bevisibleCorrectTheMess}" onclick="bevisible.extendDirectionLink(this);" target="_blank">
    {l s='Get directions' mod='fsstoresmap'}
</a>*}
<a href="{$bevisible_url nofilter}?id_store={$bevisible_store.id_store}{if $back}&back={$back}{/if}" class="button">{l s="Voir la fiche"}</a> 