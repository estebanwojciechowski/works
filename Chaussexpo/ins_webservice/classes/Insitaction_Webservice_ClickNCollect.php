<?php
 
include_once(dirname(__FILE__).'/Insitaction_Webservice.php');   
if(file_exists(dirname(__FILE__).'/../../ins_stock/classes/Ins_Stock_Magasin.php')) {
    include_once(dirname(__FILE__).'/../../ins_stock/classes/Ins_Stock_Magasin.php');   
}

class Insitaction_Webservice_ClickNCollect extends Insitaction_Webservice {

    protected static $data = array(
        //reservation en 48h
        'CreeDemandeReservationEco2' => array(
            'Nom' => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
            "Email" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isEmail', 'size' => 255),
            "Code_Magasin" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isInt', 'size' => 255),
            "Prenom" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPostCode', 'size' => 12),
            "Telephone" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isLanguageIsoCode', 'size' => 2),
        ),
        //reservation en 2h
        'CreeDemandeReservationCC' => array(
            'Nom' => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
            "Email" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isEmail', 'size' => 255),
            "Code_Magasin" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isInt', 'size' => 255),
            "Prenom" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPostCode', 'size' => 12),
            "Telephone" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isLanguageIsoCode', 'size' => 2),
        ),
        'AjouteArticleReservationPrix' => array(
            'ID' => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
            "Code_Article" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
            "Quantite" => array('type' => self::TYPE_INT, 'typeXML' => 'int', 'validate' => 'isPostCode', 'size' => 12),
            "Prix" => array('type' => self::TYPE_STRING, 'typeXML' => 'decimal', 'validate' => 'isLanguageIsoCode', 'size' => 2),
        ),
        'TermineReservationCC' => array(
            'ID' => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
        ),
    );

    protected static $response_data = array( 
        'CreeDemandeReservationCC' => 'CreeDemandeReservationCCResult', 
        'CreeDemandeReservationEco2' =>'CreeDemandeReservationEco2Result',

        'AjouteArticleReservationPrix' => array( 
            'AjouteArticleReservationPrixResult'
        ), 
        'TermineReservationCC' => 'TermineReservationCCResult',
        'TermineReservationEcommerce' => 'TermineReservationEcommerceResult',
    );

    protected $curl_header = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">   <soap:Body>';

    protected $curl_footer = '</soap:Body></soap:Envelope>';
    protected $service_url = 'http://tempuri.org/WebServiceCommandes/Service1';

    protected $schema = '';
    protected $content;
    private $products = array(
        2 => array(),
        48 => array()
    );

    public function __construct($verbose = false)
    {
        $this->setUrl(Configuration::get('INS_WEBSERVICE_URL_RESERVATION_MAGASIN'));
        parent::__construct($verbose);
    }

    public function parseDemande($method, $order, $products = false, $store = false) {
        $params = array();
        if(Validate::isLoadedObject($order)) { 
            if(isset(self::$data[$method])) {
                if($method == 'CreeDemandeReservationCC' || $method == 'CreeDemandeReservationEco2') {
                    $customer = new Customer($order->id_customer);

                    if(!Validate::isLoadedObject($store))
                        $store = $order->getSelectedStore();

                    if(Validate::isLoadedObject($customer)) {
                        $params['Nom'] = $customer->lastname;
                        $params['Email'] = $customer->email;
                        $params['Code_Magasin'] = $store->code_magasin;
                        $params['Prenom'] = $customer->firstname;
                        $params['Telephone'] = $customer->tel;
                    } else {
                        $this->log('Le client n\a pas été retrouvé pour l\'id');                        
                    }

                } elseif ($method == 'AjouteArticleReservationPrix') {
                    if(is_array($products) && count($products)) {
                        foreach ($products as $key => $product) {   
                            $product['pointure'] = self::getPointure4WS($product['pointure']); 

                            $code_article = $product['reference'].($product['pointure'] ? '.'.$product['pointure'] : '');       
                            $params[$code_article] = array(
                                'Code_Article' => $code_article,
                                'Quantite' => (int) $product['product_quantity'],
                                'Prix' => number_format($product['product_price_wt'], 2)
                            );
                        }
                    }

                }
                // TODO VALIDATE FIELDS 
                /*$keys = array_keys(self::$data[$method]);
                foreach ($keys as $key) {
                    if(isset($params[$key])) {
                        if(!Validate::{self::$data[$method][$key]['validate']}($params[$key])) {
                            $this->log('Insitaction_Webservice_ClickNCollect '.$method.' '.$key.' : '.$params[$key].' invalide');
                            return false;
                        }
                    } else {
                        $this->log('Insitaction_Webservice_ClickNCollect '.$method.' '.$key.' : invalide');
                            return false;
                    }
                }*/
                return $params;
            }

        }
        return false;
    }

    /**
     * [reservation soumet la reservation de la commande au ws borne tactile
     * avec la commande en paramètre]
     * @param  [type] $params [commande Objet Order]
     * @return [type]         [description]
     */
    public function reservation($params) {
        $reservation = array();
        if(isset($params['order']) && isset($params['store'])) {
            $order = $params['order'];
            $store = $params['store'];

            if(Validate::isLoadedObject($order) && Validate::isLoadedObject($store)) {

                $products = $this->getProductsWithDisponibilite($store->id, $order);
                if(count($products[2])) {
                    $method = 'clickandcollect';
                    // crée la demande de réservation clickandcollect      
                    $id_demande = $this->creeDemande($method, $order, $store);
                    // si OK on ajoute les articles
                    if($id_demande) {
                       $result = $this->ajoutArticlesDemande($id_demande, $order, $products[2]);
                        // Tous les articles sont ajoutés sans erreurs on valide
                        if($result) {
                            $reservation['clickandcollect'] = $this->termineReservation($id_demande, $method);
                           if(!$reservation['clickandcollect']) {
                               $this->log('ERREUR FINALISTATION RESERVATION CNC.');
                            }
                        } else {
                            $this->log('ERREUR CONTACT AJOUT ARTICLE RESERVATION CNC.');
                        }
                    } else {
                        $this->log('ERREUR CONTACT RESERVATION CNC.');
                    }
                }
                if(count($products[48])) {
                    $method = 'ecommerce';
                    // crée la demande de réservation ecommerce      
                    $id_demande = $this->creeDemande($method, $order, $store);
                    // si OK on ajoute les articles
                    if($id_demande) {
                       $result = $this->ajoutArticlesDemande($id_demande, $order, $products[48]);
                        // Tous les articles sont ajoutés sans erreurs on valide
                        if($result) {
                            $reservation['ecommerce'] = $this->termineReservation($id_demande, $method);
                            if(!$reservation['ecommerce']) {
                                $this->log('ERREUR FINALISTATION RESERVATION CNC ECOMMERCE.');
                            }
                        } else {
                            $this->log('ERREUR CONTACT AJOUT ARTICLE RESERVATION CNC ECOMMERCE.');
                        }
                    } else {
                        $this->log('ERREUR CONTACT RESERVATION CNC ECOMMERCE.');
                    }
                }                
            } else {
                $this->log('La commande '.$order->id.' n\'a pas été retrouvée.');
            }
        }
        return $reservation;
    }

    /**
     * [creeDemande crée la demande de prise en charge en clickncollect]
     * @param  [type] $method [sting reservation clickandcollect ou 48h]
     * @param  [type] $customer [objet Customer]
     * @param  [type] $store    [objet Store]
     * @return [type]           [id prise en charge ou 0]
     */
    

    public function creeDemande($method, $order, $store) {
        if ($this->client) {  
            if($method == 'clickandcollect') {
                $method = 'CreeDemandeReservationCC'; 
            } else {
                $method = 'CreeDemandeReservationEco2';
            }
            $params = $this->parseDemande($method, $order, null, $store);
            if($params && isset(self::$response_data[$method])) { 
                try {
                    $response = $this->client->__soapCall(
                        $method, 
                        array(
                            $method => $params
                        )
                    );  
                    $this->log($method.' '.print_r($response, true));
                    return $this->parseResults(self::$response_data[$method], $response); 
                } catch (Exception $e) {
                    $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
                }
            } else {
                $this->errors[] = __FUNCTION__.' Les paramètres sont incorrects.';
            }
        }
        $this->logErrors(); 
        return false;
    }

    public function ajoutArticlesDemande($id_demande, $order, $products) {
        if ($this->client) {  
            $method = 'AjouteArticleReservationPrix'; 
            $params = $this->parseDemande($method, $order, $products);

            if($params) { 
                // un appel par article 
                if(is_array($params) && count($params)) {
                    $result = array();
                    foreach ($params as $key => &$product) {
                        $product['ID'] = $id_demande;
                        $tmp['ID'] = $id_demande;
                        $tmp['Code_Article'] = $product['Code_Article'];
                        $tmp['Quantite'] = $product['Quantite'];
                        $tmp['Prix'] = $product['Prix'];

                        try {
                            $response = $this->client->__soapCall(
                                $method, 
                                array(
                                    $method => $tmp
                                )
                            ); 
                            $result[$product['Code_Article']] = $this->parseResults('AjouteArticleReservationPrixResult', $response);
                            //pour debug si erreur lors du traitement
                            $this->log('AjouteArticleReservationPrixResult '.print_r($response, true));
                        } catch (Exception $e) {
                            $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
                        }
                    }
                    if(empty($this->errors))
                        return $result;
                } else {
                    $this->errors[] = __FUNCTION__.' Aucun produit à ajouter à la demande clickandcollect #'.$id_demande.' Order ID #'.$order->id;
                }
            } else {
                $this->errors[] = __FUNCTION__.' Les paramètres sont incorrects.';
            }
        }
        $this->logErrors(); 
        return false;
    }

    public function termineReservation($id_demande, $method) {        
        if ($this->client) {  
            if($method == 'clickandcollect') {
                $method = 'TermineReservationCC';
            } else {
                $method = 'TermineReservationEcommerce';     
            }
            
            if(isset(self::$response_data[$method])) { 
                try {
                    $response = $this->client->__soapCall(
                        $method, 
                        array(
                            $method => array('ID' => $id_demande),
                        )
                    );  
                    $this->log($method.' '.print_r($response, true));
                    return $this->parseResults(self::$response_data[$method], $response); 
                } catch (Exception $e) {
                    $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
                }  
            } else {
                $this->errors[] = 'ERREUR termineReservation '.$method.' paramètres incorrects.';
            }
        }
        $this->logErrors(); 
        return false;
    }

    public function getProductsWithDisponibilite($id_store = null, $order) {
        if(is_null($id_store)) {
            $this->log('WSCNC getProductsWithDisponibilite ID STORE non fourni.');
            return false;
        }
         
        if(class_exists('Ins_Stock_Magasin')) {
            $stock = new Ins_Stock_Magasin();
            //verification des dispo pour chaque produits
            $products = $order->getProducts();
            $hors_stock = false; 
            foreach ($products as $key => &$product) { 
                $productObj = new Product($product['product_id']);
                //recupération des attributs produit
                $attributes = $productObj->getAttributeCombinationsById($product['product_attribute_id'], 1);
                $product['pointure'] = 0;
                foreach ($attributes as $key => $attribut) {
                    if($attribut['id_attribute_group'] == Product::ID_ATTRIBUTE_GROUP_POINTURE)
                        $product['pointure'] = $attribut['attribute_name'];
                }
                
                $stock_produit = $stock->getStock($id_store, $product['reference'], $product['pointure']); 
            
                if(!$stock_produit || ($stock_produit - $product['quantity'] < 0)) {
                    $this->products[48][] = $product;
                    $hors_stock = true;
                } else {                    
                    $this->products[2][] = $product;
                }
            }
        }
        return $this->products;        
    }   
}