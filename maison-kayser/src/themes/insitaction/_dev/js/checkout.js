/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import $ from 'jquery';
import prestashop from 'prestashop';

function setUpCheckout() {
  if ($('.js-cancel-address').length !== 0) {
    $('.checkout-step:not(.js-current-step) .step-title').addClass('not-allowed');
  }

  $('.js-terms a').on('click', (event) => {
    event.preventDefault();
    var url = $(event.target).attr('href');
    if (url) {
      // TODO: Handle request if no pretty URL
      url += `?content_only=1`;
      $.get(url, (content) => {
        $('#modal').find('.js-modal-content').html($(content).find('.page-cms').contents());
      }).fail((resp) => {
        prestashop.emit('handleError', {eventType: 'clickTerms', resp: resp});
      });
    }

    $('#modal').modal('show');
  });

  $('.js-gift-checkbox').on('click', (event) => {
    $('#gift').collapse('toggle');
  });
}

$(document).ready(() => {
  if ($('body#checkout').length === 1) {
    setUpCheckout();
  }

  prestashop.on('updatedDeliveryForm', (params) => {
    if (typeof params.deliveryOption === 'undefined' || 0 === params.deliveryOption.length) {
        return;
    }
    // Hide all carrier extra content ...
    $(".carrier-extra-content").hide();
    // and show the one related to the selected carrier
    params.deliveryOption.next(".carrier-extra-content").slideDown();
  });
});

$(document).ready(() => {

    if ($('body#cart').length === 1){

      $('.card .checkout a.btn').on('click',function(e){        // Au clic sur le bouton commander
          e.preventDefault();                                   // Désactivation de l'action du lien
          updateCommentAndCouverts($(e.target).attr('href'));   // Lancement fonction ajax avec lien du bouton en paramètre
      });

    }

});

/**
 * Requête ajax pour mettre à jour le commentaire destinataire et ajout ou non du couvert
 * @param link
 */
function updateCommentAndCouverts(link){

    let couverts    = $('#couvert-panier').is(':checked') ? 1 : 0;  // Récupération case à cocher couverts
    let msg         = $('.msg-destinataire').val();                 // Récupération message destinataire

    $.ajax({
        url : prestashop.urls.pages.cart + '?updateMessageCouverts=1',  // Url ajax dans le controller CartController
        type : 'POST',                                                  // Type POST et pas dans l'url
        data : { comment  : msg, couverts : couverts },                 // Paramètres à envoyer, message du client et ajout couverts
        success : function(result, statut){                             // Au succès de la requête
            window.location.href = link;                                // Simulation du clic qui a été désactivé
        }
    });

}
