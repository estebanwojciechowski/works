<?php
/**
 *  2018 Insitaction
 *
 *  @author    Florent Vatin
 *  @copyright 2018 Insitaction 
 */

if (!defined('_PS_VERSION_')) {
    exit;
}  

class CustomerForm extends CustomerFormCore
{    
    public function validate()
    {
        $emailFieldValue = $this->getField('email')->getValue();
        $emailConfField = $this->getField('emailConf');

        $passwordConfField = $this->getField('passwordconf');
        $password = $this->getField('password')->getValue();
        $passwordConf = $this->getField('passwordconf')->getValue();

        if($emailConfField != null){
        $emailConfFieldValue = $this->getField('emailConf')->getValue();
            // TODO REVOIR PROCESSUS DE TRADUCTION
            if ($emailFieldValue !== $emailConfFieldValue) {
                $emailConfField->addError(sprintf(
                    $this->translator->trans(
                        'L\'adresse email et sa confirmation ne sont pas identiques', array(), 'Shop.Notifications.Error'
                    ),
                    $emailConfField->getValue()
                ));
            }
        }

        if ($password !== $passwordConf) {
            $passwordConfField->addError(sprintf(
                $this->translator->trans(
                    'Le mot de passe et sa confirmation ne sont pas identiques.', array(), 'Shop.Notifications.Error'
                ),
                $passwordConfField->getValue()
            ));
        }

        return parent::validate();
    }

    /**
     * @return \Customer
     */
    public function getCustomer()
    {
        $customer = new Customer($this->getValue('id_customer'));
        foreach ($this->formFields as $field) {
            $customerField = $field->getName();
            if ($customerField === 'id_customer') {
                $customerField = 'id';
            }

            if($customerField !== 'emailConf'){
                if (property_exists($customer, $customerField)) {
                    $customer->$customerField = $field->getValue();
                }
            }
        }
        return $customer;
    }

    public function fillFromCustomer(Customer $customer)
    {        
        $params = get_object_vars($customer);
        $params['id_customer'] = $customer->id;
        $params['birthday'] = $customer->birthday === '0000-00-00' || !isset($customer->birthday) ? null : date('d/m/Y', strtotime(Tools::displayDate($customer->birthday)));
;

        return $this->fillWith($params);
    }
}