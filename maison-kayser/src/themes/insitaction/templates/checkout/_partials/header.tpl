{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block}

{block name='header_nav'}
  <nav class="header-nav hidden-md-up ">
    <div class="mobile">
      <div id="menu-icon">
        <i class="material-icons d-inline">&#xE5D2;</i>
      </div>
      <div class="top-logo" id="_mobile_logo"></div>

      <span class="search__picto search__picto--open"></span>

      <div class="store-loc-header">
        <a href="{if $my_boulangerie_link}{$my_boulangerie_link}{else}{if $store_locator_url}{$store_locator_url}{else}#{/if}{/if}">
          <span class="store-loc__picto"></span>
          <span class="hidden-sm-down">{if $active_store }{$active_store->name}{else}{l s='Choisir une boulangerie' d='Shop.Theme.Global'}{/if}</span>
        </a>
      </div>
      <div id="_mobile_user_info"></div>
      <div id="_mobile_cart"></div>
    </div>
  </nav>
{/block}

{block name='header_top'}
  <div class="header-top">

    {* logo *}
    <div class="hidden-sm-down" id="_desktop_logo">
      <a href="{$urls.base_url}">
        <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
      </a>
    </div>
    {* right *}
    <div class="navigation__wrapper position-static">
      {* HOOK - Nav 1 & 2 *}
      <div class="navigation__container navigation__log-cart hidden-sm-down">
          {* TODO Dev : Demande de devis *}
          <div class="devis__container">
            <a href="#"><small class="todo">@todo</small>{l s="Demande de devis"}</a>
          </div>
          <div class="store-loc-header">
              <a href="{if $my_boulangerie_link}{$my_boulangerie_link}{else}{if $store_locator_url}{$store_locator_url}{else}#{/if}{/if}">
                <span>
                  <span class="store-loc__picto"></span>
                  <span class="hidden-sm-down">
                      {if $active_store }{$active_store->name}{else}{l s='Choisir une boulangerie'}{/if}
                  </span>
              </a>
          </div>
          {* HOOK *}
          {hook h='displayNav1'}
          {hook h='displayNav2'}
      </div>

      {* HOOK - Navigation *}
      <div class="navigation__container navigation__menu-search">
        {hook h='displayNavFullWidth'}
      </div>

      {* HOOK - top *}
      {* <div class="navigation__container">
        {hook h='displayTop'}
      </div> *}
    </div>

  </div>
{/block}
