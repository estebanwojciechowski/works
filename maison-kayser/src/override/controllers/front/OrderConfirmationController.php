<?php

class OrderConfirmationController extends OrderConfirmationControllerCore
{
    public function initContent()
    {
        if (Configuration::isCatalogMode()) {
            Tools::redirect('index.php');
        }
        // force la locale sinon affichage date en gmt
        date_default_timezone_set(Configuration::get('PS_TIMEZONE'));

        $order = new Order(Order::getIdByCartId((int) ($this->id_cart)));
        $presentedOrder = $this->order_presenter->present($order);
        $register_form = $this
            ->makeCustomerForm()
            ->setGuestAllowed(false)
            ->fillWith(Tools::getAllValues());

        parent::initContent();

        $date_livraison = false; 
        $id_order_carrier = $order->getIdOrderCarrier();
        if($id_order_carrier) {
            $order_carrier = new OrderCarrier($id_order_carrier);
            $date_livraison = $order_carrier->date_livraison;
            $carrier = new Carrier($order->id_carrier);
            if(Validate::isLoadedObject($carrier)) {
                if(isset($presentedOrder['carrier'])) {
                    $presentedOrder['carrier']['clickandcollect'] = $carrier->clickandcollect;
                }        
            }
        }

        $this->getInfosFormules($presentedOrder);

        $this->context->smarty->assign(array(
            'HOOK_ORDER_CONFIRMATION' => $this->displayOrderConfirmation($order),
            'HOOK_PAYMENT_RETURN' => $this->displayPaymentReturn($order),
            'order' => $presentedOrder,
            'register_form' => $register_form,
            'date_livraison' => $date_livraison,
        ));

        if ($this->context->customer->is_guest) {
            /* If guest we clear the cookie for security reason */
            $this->context->customer->mylogout();
        }
        $this->setTemplate('checkout/order-confirmation');
    } 

    private function getInfosFormules($presentedOrder) {
        $formules = array();
        $has_formule = false;
        
        if(isset($presentedOrder['products']) && count($presentedOrder['products'])) {
            foreach ($presentedOrder['products'] as $key => $order_detail) {
                if(isset($order_detail['formule_product_index']) 
                    && $order_detail['formule_product_index']) {

                    if(!isset($formule[$order_detail['formule_product_index']])) {
                        $id = $order_detail['formule_product_index'];
                        $id_product = explode('_', $id);
                        if(is_array($id_product) && count($id_product) == 2) {
                            $id_product_attribute = $id_product[1];
                            $id_product = $id_product[0];                            
                        } else {
                            //formule sans décli ? impossible en théorie
                            continue;
                        }   

                        $product = new Product($id_product);
                        
                        $formules[$id] = (array) json_decode($order_detail['formule_info']);

                        // Ajout des variables utilisées sur le template
                        $formules[$id]['reference'] = $product->reference;
                        $formules[$id]['name'] = $formules[$id]['product_name'];
                        $formules[$id]['price'] = Tools::displayPrice($formules[$id]['product_price'], $this->context->currency);
                        $formules[$id]['quantity'] = $formules[$id]['product_quantity'];
                        $formules[$id]['total'] = Tools::displayPrice($formules[$id]['unit_price_tax_excl'], $this->context->currency);  


                        $price_wt = $product->getPrice(true, $id_product_attribute, 6);

                        $formules[$id]['product_price_wt'] = Tools::displayPrice($price_wt, $this->context->currency); 

                        $formules[$id]['total_wt'] = Tools::displayPrice($price_wt * $formules[$id]['quantity'], $this->context->currency); 

                        $has_formule = true;
                        $formules[$order_detail['formule_product_index']]['products'] = array();
                    }

                    $formules[$order_detail['formule_product_index']]['products'][] = $order_detail;
                } else {
                    //on conserve les produits qui ne sont pas issus de formules
                    $formules[] = $order_detail;
                }                
            }

            if(!empty($formules) && $has_formule) {   
                $this->context->smarty->assign([ 'formules' => $formules  ]);     
            }
        }
    }
}