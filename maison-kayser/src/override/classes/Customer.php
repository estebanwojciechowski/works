<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
use PrestaShop\PrestaShop\Adapter\ServiceLocator;
use PrestaShop\PrestaShop\Adapter\CoreException;

/***
 * Class CustomerCore
 */
class Customer extends CustomerCore
{
    public $my_shop;
    public $phone;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'customer',
        'primary' => 'id_customer',
        'fields' => array(
            'secure_key' => array('type' => self::TYPE_STRING, 'validate' => 'isMd5', 'copy_post' => false),
            'my_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'lastname' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 255),
            'firstname' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 255),
            'email' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 128),
            'passwd' => array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => true, 'size' => 60),
            'last_passwd_gen' => array('type' => self::TYPE_STRING, 'copy_post' => false),
            'id_gender' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'birthday' => array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
            'newsletter' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'newsletter_date_add' => array('type' => self::TYPE_DATE, 'copy_post' => false),
            'ip_registration_newsletter' => array('type' => self::TYPE_STRING, 'copy_post' => false),
            'optin' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'website' => array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
            'company' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'siret' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'phone' => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'required' => true),
            'ape' => array('type' => self::TYPE_STRING, 'validate' => 'isApe'),
            'outstanding_allow_amount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'copy_post' => false),
            'show_public_prices' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'id_risk' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'max_payment_days' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'deleted' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'note' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'size' => 65000, 'copy_post' => false),
            'is_guest' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'id_shop_group' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'id_default_group' => array('type' => self::TYPE_INT, 'copy_post' => false),
            'id_lang' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'reset_password_token' => array('type' => self::TYPE_STRING, 'validate' => 'isSha1', 'size' => 40, 'copy_post' => false),
            'reset_password_validity' => array('type' => self::TYPE_DATE, 'validate' => 'isDateOrNull', 'copy_post' => false),
        ),
    );

    /**
     * [setMyShop enregistre la boulangerie préférée en BDD
     * Et connecte le client à la boutique cible (met les infos dans le cookie de la cible)]
     * @param [type] $id_shop [description]
     */
    public function setMyShop($id_shop) {
        // ADD BY ESTEBANW INSITACTION 31052018
        // Ajout de la boutique choisie au cookie wp_shop
        setcookie("wp_shop", $id_shop, time()+3600, '/');
        // END

        if($this->isLogged()) {
            $context = Context::getContext();
            if($context->shop->id != $id_shop)
                $context->logToOtherShop($this, $id_shop);

           $this->my_shop = $id_shop;

           return  DB::getInstance()->execute('
                        UPDATE mk_customer SET my_shop = '.$id_shop.'
                        WHERE id_customer = '.$this->id
                    );

        }
        return true;
    }

    /**
     * [setMyShop recupére la boulangerie préférée en BDD]
     */
    public function getMyShop() {
        $my_shop = false;
        // utile pour eviter des redirection lors de soumissions formulaire
        // par module en front
        $ignore_redirect_shop = Tools::getValue('ignore_redirect_shop');
        if(isset($_COOKIE['wp_shop']) && $_COOKIE['wp_shop'] > 1 && !$ignore_redirect_shop) {
            // boutique toujours active ?
            $shop = new Shop($_COOKIE['wp_shop']);
            if(Validate::isLoadedObject($shop) && $shop->active) {
                // Récupération de la boutique préférée en cookie
                $my_shop = $_COOKIE['wp_shop'];
            }
        }

        if($this->isLogged() && !$ignore_redirect_shop) {
            // boutique toujours active ?
            $shop = new Shop( $this->my_shop);
            if(Validate::isLoadedObject($shop) && $shop->active) {
                if($my_shop && $this->my_shop != $my_shop) {
                    $this->setMyShop($my_shop);
                }
            }
        }

        return is_numeric($my_shop) ? (int) $my_shop : false;
    }

}
