# DOCKERLAMP

Environnement de développement contenant :

APACHE 2

PHP (version au choix 5.6 / 7.0 / 7.1 / 7.2 / 7.3, images générées depuis phpdockerio)

MYSQL 5.6.36


Mailcatcher : http://localhost:1080

PhpMyAdmin : http://localhost:8080

## Prerequis

Docker ou Docker Toolbox

Au moins 2Go de mémoire RAM partagée dans Docker

Testé sur Windows 10 PRO uniquement

Quelques connaissances serveur unix


## Quelques infos

Pour l'initialisation, CONTAINER_PREFIX du fichier .env permet de différencier le nom des images d'un projet à l'autre.

Mysql dispose d'un volume persistant en local, quelques variables d'environnements sont aussi personnalisables.
Il est donc possible d'automatiser la création et l'insertion des données de la BDD au build pour fonctionner en local uniquement.

## Utilisation en Serveur LAMP

- Les projets doivent être dans /public

- Définir la version PHP à utiliser dans le fichier .env

- Definir les VHOSTS dans /phpdocker/vhosts à l'image du fichier /phpdocker/vhosts/000-default.conf

### Construire l'image docker :
docker-compose build

### Pour démarrer le serveur
docker-compose up

### Pour arrêter
docker-compose stop

### Pour enlever les données non persistantes créées par up
docker-compose down

Par défaut les mails envoyés par PHP sont redirigés sur le MailCatcher.

docker-compose up sur Windows 10 peut retourner une erreur si docker n'a pas été éteint avant l'arrêt du pc. Un restart de Docker et la commande devrait fonctionner.

## Commandes utiles

Par défaut le container s'appelle insitaction-(service)

### Accès au bash php :

docker exec -it insitaction-php-fpm bash


### Accès au bash sql :

docker exec -it insitaction-mysql bash


### Supprimer tout les containers
docker rm $(docker ps -a -q)

### Supprimer toutes les images
docker rmi $(docker images -q)
docker rmi --force $(docker images -q)

### Supprimer le volume assigné à la bdd persistante
docker volume remove [nom du repertoire]_database_data2
