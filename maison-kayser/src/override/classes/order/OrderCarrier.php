<?php
 

class OrderCarrier extends OrderCarrierCore
{

	public $date_livraison;
    public $message;

	public static $definition = array(
        'table' => 'order_carrier',
        'primary' => 'id_order_carrier',
        'fields' => array(
            'id_order' =>                	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_carrier' =>            		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_order_invoice' =>        	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'weight' =>                		array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'shipping_cost_tax_excl' => 	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'shipping_cost_tax_incl' => 	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'tracking_number' =>        	array('type' => self::TYPE_STRING, 'validate' => 'isTrackingNumber'),
            'date_livraison' =>				array('type' => self::TYPE_STRING),
            'message'  =>                   array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 1600),
            'date_add' =>                	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );
}