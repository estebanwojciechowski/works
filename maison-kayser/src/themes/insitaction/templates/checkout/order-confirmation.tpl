{extends file='page.tpl'}

{block name='page_content_container' prepend}
  <div class="container">
    <section id="content-hook_order_confirmation" class="card">
      <div class="card-block">
        <div class="row">
          <div class="col-md-12">

            {block name='order_confirmation_header'}
              <h3 class="h1 card-title">
                <i class="material-icons done">&#xE876;</i>{l s='Your order is confirmed' d='Shop.Theme.Checkout'}
              </h3>
            {/block}

            <p>
              {l s='An email has been sent to your mail address %email%.' d='Shop.Theme.Checkout' sprintf=['%email%' => $customer.email]} 
            </p>

            {block name='hook_order_confirmation'}
              {$HOOK_ORDER_CONFIRMATION nofilter}
            {/block}

          </div>
        </div>
      </div>
    </section>
  {/block}

  {block name='page_content_container'}
    <section id="content" class="page-content page-order-confirmation card">
      <div class="card-block">
        <div class="row">

          {block name='order_confirmation_table'}
            {if isset($formules) && count($formules)}              
              {include
                file='checkout/_partials/order-confirmation-table-formules.tpl'
                products=$formules
                subtotals=$order.subtotals
                totals=$order.totals
                labels=$order.labels
                add_product_link=false
              }
            {else}
              {include
                file='checkout/_partials/order-confirmation-table.tpl'
                products=$order.products
                subtotals=$order.subtotals
                totals=$order.totals
                labels=$order.labels
                add_product_link=false
              }
            {/if}
          {/block}
 
          {block name='order_details'}
            <div id="order-details" class="col-md-4">
              <h3 class="h3 card-title">{l s='Order details' d='Shop.Theme.Checkout'}:</h3>
              <ul>
                <li>{l s='Order reference: %reference%' d='Shop.Theme.Checkout' sprintf=['%reference%' => $order.details.reference]}</li>
                <li>{l s='Payment method: %method%' d='Shop.Theme.Checkout' sprintf=['%method%' => $order.details.payment]}</li>
              </ul>
                {if !$order.details.is_virtual}
                <h3 class="h3 card-title">{l s='Shipping Method' d='Shop.Theme.Checkout'}:</h3>
                <ul>
                  <li>
                    {l s='%method%' d='Shop.Theme.Checkout' sprintf=['%method%' => $order.carrier.name]}
                    {if isset($order.carrier.clickandcollect) && $order.carrier.clickandcollect}
                      <br>{$active_store->name}
                    {/if}
                    {if isset($date_livraison) && $date_livraison}
                      <br>{l s='le ' d='Shop.Theme.Checkout'} {$date_livraison|date_format:'d/m/Y H:i'} 
                    {else}<br>
                      <em>{$order.carrier.delay}</em>
                    {/if} 
                  </li>
                </ul>
                {/if}
            </div>
          {/block}

        </div>
      </div>
    </section>

    {block name='hook_payment_return'}
      {if ! empty($HOOK_PAYMENT_RETURN)}
      <section id="content-hook_payment_return" class="card definition-list">
        <div class="card-block">
          <div class="row">
            <div class="col-md-12">
              {$HOOK_PAYMENT_RETURN nofilter}
            </div>
          </div>
        </div>
      </section>
      {/if}
    {/block}

    {block name='customer_registration_form'}
      {if $customer.is_guest}
        <div id="registration-form" class="card">
          <div class="card-block">
            <h4 class="h4">{l s='Save time on your next order, sign up now' d='Shop.Theme.Checkout'}</h4>
            {render file='customer/_partials/customer-form.tpl' ui=$register_form}
          </div>
        </div>
      {/if}
    {/block}

    {block name='hook_order_confirmation_1'}
      {hook h='displayOrderConfirmation1'}
    {/block}

    {block name='hook_order_confirmation_2'}
      <section id="content-hook-order-confirmation-footer">
        {hook h='displayOrderConfirmation2'}
      </section>
    {/block}
  </div>
{/block}
