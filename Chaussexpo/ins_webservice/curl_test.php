<?php 

	$xml = '<?xml version="1.0" encoding="UTF-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  					<soap:Body>
						<ajout_client xmlns="http://desmazieres.tm.fr/"> 
							<dataSetClientEntree>
								<xs:schema id="dataSetClientEntree" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
									<xs:element name="dataSetClientEntree" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
										<xs:complexType>
											<xs:choice minOccurs="0" maxOccurs="unbounded">

												<xs:sequence>
													<xs:element name="civilite" type="xs:int" minOccurs="0"/>
													<xs:element name="nom" type="xs:string" minOccurs="0"/>
													<xs:element name="prenom" type="xs:string" minOccurs="0"/>
													<xs:element name="adresse1" type="xs:string" minOccurs="0"/>
													<xs:element name="adresse2" type="xs:string" minOccurs="0"/>
													<xs:element name="code_postal" type="xs:string" minOccurs="0"/>
													<xs:element name="ville" type="xs:string" minOccurs="0"/>
													<xs:element name="tel_fixe" type="xs:string" minOccurs="0"/>
													<xs:element name="tel_portable" type="xs:string" minOccurs="0"/>
													<xs:element name="email" type="xs:string" minOccurs="0"/>
													<xs:element name="date_naissance" type="xs:string" minOccurs="0"/>
													<xs:element name="nb_enfants" type="xs:int" minOccurs="0"/>
													<xs:element name="pays" type="xs:string" minOccurs="0"/>
													<xs:element name="code_langue" type="xs:string" minOccurs="0"/>
													<xs:element name="mot_de_passe" type="xs:string" minOccurs="0"/>
												</xs:sequence>

											</xs:choice>
										</xs:complexType>
									</xs:element> 

								</xs:schema>

								<diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">
									<dataSetClientEntree xmlns="">
										<civilite>2</civilite>
										<nom>wojciechowski</nom>
										<prenom>esteban</prenom>
										<adresse1>22 rue de bergues</adresse1>
										<adresse2> </adresse2>
										<code_postal>59000</code_postal>
										<ville>LILLE</ville>
										<tel_fixe>0303030303</tel_fixe>
										<tel_portable>0303030303</tel_portable>
										<email>estebanw@insitaction.com</email>
										<date_naissance>28/05/1985</date_naissance>
										<nb_enfants>2</nb_enfants>
										<code_pays>FR</code_pays>
										<pays>FRANCE</pays>
										<code_langue>FR</code_langue>
										<mot_de_passe>insitaction</mot_de_passe>
									</dataSetClientEntree>
								</diffgr:diffgram> 
							</dataSetClientEntree>
							<site> </site>
						</ajout_client>
					</soap:Body>
				</soap:Envelope>';

/*$xml = '<?xml version="1.0" encoding="UTF-8"?>
				<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns1="http://desmazieres.tm.fr/">
					<env:Body>
						<ns1:ajout_client>
							<ns1:dataSetClientEntree>
								<ns1:civilite>2</ns1:civilite>
								<ns1:nom>Test</ns1:nom>
								<ns1:prenom>Test</ns1:prenom>
								<ns1:adresse1>Test</ns1:adresse1>
								<ns1:adresse2>Test</ns1:adresse2>
								<ns1:code_postal>59000</ns1:code_postal>
								<ns1:ville>Lille</ns1:ville>
								<ns1:tel_fixe>0366666666</ns1:tel_fixe>
								<ns1:tel_portable></ns1:tel_portable>
								<ns1:email>estebanw@insitaction.com</ns1:email>
								<ns1:date_naissance></ns1:date_naissance>
								<ns1:nb_enfants>0</ns1:nb_enfants>
								<ns1:code_pays>FR</ns1:code_pays>
								<ns1:pays>France</ns1:pays>
								<ns1:code_langue>FR</ns1:code_langue>
								<ns1:mot_de_passe>insitaction</ns1:mot_de_passe>
							</ns1:dataSetClientEntree>
							<ns1:site> </ns1:site>
						</ns1:ajout_client>
					</env:Body>
				</env:Envelope>';*/

				$headers = array(
					'POST /WSClient_TEST/ServiceClient.asmx HTTP/1.1',
					'Host: ws.chaussexpo.fr',
				 	"Content-Type: text/xml; charset=utf-8",
				 	"SOAPAction: \"http://desmazieres.tm.fr/ajout_client\"",
				    "Content-length: ".strlen($xml),
				); 


		$xml_commande = '<?xml version="1.0" encoding="utf-8"?>
							<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
								<soap:Body>
									<ajout_commande xmlns="http://tempuri.org/WebServiceCommandes/Service1">
										<dataSetCommandeEntree>
											<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
												<xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
													<xs:complexType>
														<xs:choice minOccurs="0" maxOccurs="unbounded">
															<xs:element name="AJOUT_COMMANDE">
																<xs:complexType>
																	<xs:sequence>
																		<xs:element name="code_avantage" type="xs:string" minOccurs="0" />
																		<xs:element name="code_concept" type="xs:string" minOccurs="0" />
																		<xs:element name="code_client" type="xs:string" minOccurs="0" />
																		<xs:element name="REMISE_AVANTAGE" type="xs:double" minOccurs="0" />
																		<xs:element name="type_reglement" type="xs:string" minOccurs="0" />
																		<xs:element name="type_livraison" type="xs:short" minOccurs="0" />
																		<xs:element name="frais_reglement" type="xs:double" minOccurs="0" />
																		<xs:element name="frais_livraison" type="xs:double" minOccurs="0" />
																		<xs:element name="numero_carte" type="xs:string" minOccurs="0" />
																		<xs:element name="date_valid_carte" type="xs:string" minOccurs="0" />
																		<xs:element name="NUM_CVV_CARTE" type="xs:string" minOccurs="0" />
																		<xs:element name="civilite_livraison" type="xs:short" minOccurs="0" />
																		<xs:element name="NOM_LIVRAISON" type="xs:string" minOccurs="0" />
																		<xs:element name="PRENOM_LIVRAISON" type="xs:string" minOccurs="0" />
																		<xs:element name="ADRESSE1_LIVRAISON" type="xs:string" minOccurs="0" />
																		<xs:element name="ADRESSE2_LIVRAISON" type="xs:string" minOccurs="0" />
																		<xs:element name="CP_LIVRAISON" type="xs:string" minOccurs="0" />
																		<xs:element name="VILLE_LIVRAISON" type="xs:string" minOccurs="0" />
																		<xs:element name="CODE_PAYS" type="xs:string" minOccurs="0" />
																		<xs:element name="PAYS" type="xs:string" minOccurs="0" />
																		<xs:element name="CODE_RELAIS_LIVRAISON" type="xs:string" minOccurs="0" />
																		<xs:element name="MONTANT" type="xs:double" minOccurs="0" />
																		<xs:element name="QUANTITE" type="xs:short" minOccurs="0" />
																		<xs:element name="NOUVEAU_CLIENT" type="xs:string" minOccurs="0" />
																		<xs:element name="NUM_PANIER_ALTIMA" type="xs:int" minOccurs="0" />
																		<xs:element name="CODE_LANGUE" type="xs:string" minOccurs="0" />
																		<xs:element name="CODE_DEVISE" type="xs:string" minOccurs="0" />
																	</xs:sequence>
																</xs:complexType>
															</xs:element>
															<xs:element name="AJOUT_ARTICLE">
																<xs:complexType>
																	<xs:sequence>
																		<xs:element name="ARTICLE_CODE_ARTICLE" type="xs:long" minOccurs="0" />
																		<xs:element name="POINTURE" type="xs:string" minOccurs="0" />
																		<xs:element name="QUANTITE" type="xs:short" minOccurs="0" />
																		<xs:element name="PRIX_NORMAL" type="xs:double" minOccurs="0" />
																		<xs:element name="PRIX_BARRE" type="xs:double" minOccurs="0" />
																		<xs:element name="POS_AVANTAGE" type="xs:short" minOccurs="0" />
																		<xs:element name="REMISE_AVANTAGE" type="xs:double" minOccurs="0" />
																		<xs:element name="CODE_CATALOGUE" type="xs:string" minOccurs="0" />
																		<xs:element name="CADEAU" type="xs:string" minOccurs="0" />
																		<xs:element name="CODE_DEVISE" type="xs:string" minOccurs="0" />
																	</xs:sequence>
																</xs:complexType>
															</xs:element>
														</xs:choice>
													</xs:complexType>
												</xs:element>
											</xs:schema>
											<diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">
												<NewDataSet xmlns="">
													<AJOUT_COMMANDE diffgr:id="AJOUT_COMMANDE1" msdata:rowOrder="0" diffgr:hasChanges="inserted">
														<code_avantage />
														<code_concept />
														<code_client>F7249415</code_client>
														<REMISE_AVANTAGE>0</REMISE_AVANTAGE>
														<type_reglement>CR</type_reglement>
														<type_livraison>1</type_livraison>
														<frais_reglement>8</frais_reglement>
														<frais_livraison>7</frais_livraison>
														<numero_carte />
														<date_valid_carte />
														<NUM_CVV_CARTE />
														<civilite_livraison>1</civilite_livraison>
														<NOM_LIVRAISON>GUILLON</NOM_LIVRAISON>
														<PRENOM_LIVRAISON>THIBAULT</PRENOM_LIVRAISON>
														<ADRESSE1_LIVRAISON>495 RUE DU MAL FOCH</ADRESSE1_LIVRAISON>
														<ADRESSE2_LIVRAISON />
														<CP_LIVRAISON>59830</CP_LIVRAISON>
														<VILLE_LIVRAISON>BOUVINES</VILLE_LIVRAISON>
														<CODE_PAYS>FR</CODE_PAYS>
														<PAYS>FRANCE</PAYS>
														<CODE_RELAIS_LIVRAISON />
														<MONTANT>21.5</MONTANT>
														<QUANTITE>2</QUANTITE>
														<NOUVEAU_CLIENT>N</NOUVEAU_CLIENT>
														<NUM_PANIER_ALTIMA>1693407</NUM_PANIER_ALTIMA>
														<CODE_LANGUE>FR</CODE_LANGUE>
														<CODE_DEVISE>EUR</CODE_DEVISE>
													</AJOUT_COMMANDE>
													<AJOUT_ARTICLE diffgr:id="AJOUT_ARTICLE1" msdata:rowOrder="0" diffgr:hasChanges="inserted">
														<ARTICLE_CODE_ARTICLE>811568</ARTICLE_CODE_ARTICLE>
														<POINTURE>33</POINTURE>
														<QUANTITE>1</QUANTITE>
														<PRIX_NORMAL>9</PRIX_NORMAL>
														<PRIX_BARRE>18</PRIX_BARRE>
														<POS_AVANTAGE>0</POS_AVANTAGE>
														<REMISE_AVANTAGE>0</REMISE_AVANTAGE>
														<CODE_CATALOGUE>ECOVAD</CODE_CATALOGUE>
														<CADEAU>N</CADEAU>
														<CODE_DEVISE>EUR</CODE_DEVISE>
													</AJOUT_ARTICLE>
													<AJOUT_ARTICLE diffgr:id="AJOUT_ARTICLE2" msdata:rowOrder="1" diffgr:hasChanges="inserted">
														<ARTICLE_CODE_ARTICLE>810889</ARTICLE_CODE_ARTICLE>
														<POINTURE>28</POINTURE>
														<QUANTITE>1</QUANTITE>
														<PRIX_NORMAL>12.5</PRIX_NORMAL>
														<PRIX_BARRE>25</PRIX_BARRE>
														<POS_AVANTAGE>0</POS_AVANTAGE>
														<REMISE_AVANTAGE>0</REMISE_AVANTAGE>
														<CODE_CATALOGUE>ECOVAD</CODE_CATALOGUE>
														<CADEAU>N</CADEAU>
														<CODE_DEVISE>EUR</CODE_DEVISE>
													</AJOUT_ARTICLE>
												</NewDataSet>
											</diffgr:diffgram>
										</dataSetCommandeEntree>
									</ajout_commande>
								</soap:Body>
							</soap:Envelope>';

							$headers = array(
								'POST /WSCommande_TEST/ServiceCommande.asmx HTTP/1.1',
								'Host: ws.chaussexpo.fr',
							 	"Content-Type: text/xml; charset=utf-8",
							 	"SOAPAction: \"http://tempuri.org/WebServiceCommandes/Service1/ajout_commande\"",
							    "Content-length: ".strlen($xml_commande),
							); 

		//$return = $this->curl_post($this->url.'?WSDL', array($curl_content), array(CURLOPT_HTTPHEADER => $headers));
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://ws.chaussexpo.fr/WSCommande_TEST/ServiceCommande.asmx?WSDL');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml_commande); // the SOAP request
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		 
        print("\n");
        print_r($curl);
        print("\n");
        $return = curl_exec($curl); 
		if($return)
       	 	print_r($return);
       	else
       		print_r('no_return');
        curl_close($curl); 
        exit;
   ?>