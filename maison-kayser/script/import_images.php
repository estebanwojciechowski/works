<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Class d'import produit initial // pas de maj détruit toute les données si le test du csv est bon
 */
class ImportImages {   

    private $_logger;

    protected $errors = array();
    protected $images = array();
    protected $dirImportFile;
    protected $directories = array(
      'products',
      'stores'
    );

    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  

        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        
        $this->dirImportFile = dirname(__FILE__) . '/images';
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() { 
      if($this->cleanBDD()) {
        //recup des images
        foreach ($this->directories as $key => $dir) {
          $this->readDir($dir); 
        }    

        $this->ImportImagesProduits();
        $this->ImportImagesBoutiques();
      } 
    } 

    public function desactiveProductWithoutImages() {

        $this->log("Desactivation des produits sans images");

        // desactive tout produit sans images de la table ps_product
        $sql = 'UPDATE '._DB_PREFIX_.'product p
                SET p.`active` = 0
                WHERE p.`id_product` IN (
                  SELECT tmp.`id_product` FROM (
                    SELECT p2.`id_product` FROM '._DB_PREFIX_.'product p2 
                    WHERE p2.id_product NOT IN (
                      SELECT DISTINCT i.`id_product` FROM '._DB_PREFIX_.'image i
                    ) AND p2.id_product < 379
                  ) AS tmp
                )';
        $this->log("Desactivation des produits sans images sur la table product");
        
        if(DB::getInstance()->execute($sql)) {
          $this->log("OK");
        } else {
          $this->log("KO");          
        }

        $sql = 'UPDATE '._DB_PREFIX_.'product_shop ps
                SET ps.`active` = 0
                WHERE ps.`id_product` IN (
                  SELECT tmp.`id_product` FROM (
                    SELECT p2.`id_product` FROM '._DB_PREFIX_.'product p2 
                    WHERE p2.id_product NOT IN (
                      SELECT DISTINCT i.`id_product` FROM '._DB_PREFIX_.'image i
                    ) AND p2.id_product < 379
                  ) AS tmp
                )'; 
        $this->log("Desactivation des produits sans images sur la table product_shop");

        if(DB::getInstance()->execute($sql)) {
          $this->log("OK");
        } else {
          $this->log("KO");          
        }
    }

    public function cleanBDD() {
        
      return true;

       $clean = DB::getInstance()->execute(
                  // reinit des categories
                ' TRUNCATE `'._DB_PREFIX_.'image`; 
                  TRUNCATE `'._DB_PREFIX_.'image_lang`; 
                  TRUNCATE `'._DB_PREFIX_.'image_shop`;
                  '
            );
        if(!$clean) {
          $this->log("ERREUR Clean BDD KO");
          return false;
        }
        $this->log("Clean BDD OK");
        return true;
    }

    public function ImportImagesProduits() {
      if(isset($this->images['products']) && count($this->images['products'])) {
        foreach ($this->images['products'] as $id_product => $positions) {
          $product = new Product($id_product);
          if(!Validate::isLoadedObject($product)) {
            $this->log("[PRODUIT IMAGE] non ajoutée id_product : " . $id_product.'" inexistant en bdd.');
            continue;
          }
          if(is_array($positions)) {
            ksort($this->images['products'][$id_product]);
            foreach ($positions as $position => $file) {  
              $id_image = DB::getInstance()->getValue(
                'SELECT `id_image` FROM `'._DB_PREFIX_.'image`
                WHERE `id_product` = '.$id_product.' 
                AND `position` = '.$position);
              if($id_image)
                $image = new Image($id_image);
              else
                $image = new Image();
              $image->id_product = $id_product;
              $image->position = $position;
              $image->cover = ($position == 1); 
              $extension =  $file['regex'][4];

              if($id_image) {
                $this->log("[PRODUIT IMAGE] UPDATE#".$id_image);
                 if ($image->update()) {  
                  $image->deleteImage();
                  if (!self::copyImg($id_product, $image->id, $file['file']->getRealPath(), 'products', true, $extension)) {
                      $image->delete();
                  } else { 
                      $this->log("[PRODUIT IMAGE] Image mise a jour (id: " . $image->id . ") id_product : " . $id_product);
                  }
                } else {
                    $this->log("[PRODUIT IMAGE] Impossible de mettre a jour l'image#".$id_image." " . $url);
                }  
              } else {
                $this->log("[PRODUIT IMAGE] ADD");
                if ($image->add()) {  
                  if (!self::copyImg($id_product, $image->id, $file['file']->getRealPath(), 'products', true, $extension)) {
                      $image->delete();
                  } else { 
                      $this->log("[PRODUIT IMAGE] Image ajoutée (id: " . $image->id . ") id_product : " . $id_product);
                  }
                } else {
                    $this->log("[PRODUIT IMAGE] Impossible d'ajouter l'image " . $url);
                }  
              }
               
              unset($image);
            }
          }
        }          
      }
    }

    public function ImportImagesBoutiques() {
      if(isset($this->images['stores']) && count($this->images['stores'])) {
        foreach ($this->images['stores'] as $id_store => $positions) {
          if(is_array($positions)) {
            ksort($this->images['stores'][$id_store]);
            foreach ($positions as $position => $file) {
              if((int) $position == 1) {
                if(Store::storeExists($id_store)) {
                  // ajout de l'image au store
                  // copie dans le repertoire img/s      
                  $extension =  $file['regex'][4];            
                  if (!self::copyImg($id_store, null, $file['file']->getRealPath(), 'stores', true, $extension)) {
                    $this->log("[PRODUIT IMAGE] Image Store non ajoutée (id:".$id_store.")");
                  } else { 
                    $this->log("[PRODUIT IMAGE] Image Store ajoutée (id:".$id_store.")");
                  }
                }
              }
            }
          }
        }
      }
    }

    public static function copyImg($id_entity, $id_image = null, $url = '', $entity = 'products', $regenerate = true, $type = 'jpg') {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
            print($tmpfile." image add\n");
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break; 
            case 'stores':
                $path = _PS_STORE_IMG_DIR_ . (int) $id_entity;
                break;
        }
        $orig_tmpfile = $tmpfile;
        if (Tools::copy($url, $tmpfile)) {
            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);
                return false;
            }

            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            //Copie du fichier dans repertoire de destination avec les tailles d'origines
            ImageManager::resize($tmpfile, $path . '.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height);
            //recup images types
            $images_types = ImageType::getImagesTypes($entity, true);

            if ($regenerate) {
                $previous_path = null;
                $path_infos = array();
                $path_infos[] = array($tgt_width, $tgt_height, $path . '.jpg');
                foreach ($images_types as $image_type) {
                    //$tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize(
                                    $tmpfile, $path . '-' . stripslashes($image_type['name']) . '.jpg', $image_type['width'], $image_type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height
                            )) {
                        // the last image should not be added in the candidate list if it's bigger than the original image
                        /*if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = array($tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg');
                        }*/
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $id_entity . '_' . (int) Context::getContext()->shop->id . '.jpg');
                            }
                        }
                    }
                }
            }
        } else {
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);
        return true;
    }

    /**
     * [readFile Lis la liste des fichiers images] 
     * @param  [type] $dirname [nom du repertoire]
     * @return [type]           [boolean]
     */
    protected function readDir($dirname = '') {
        if(!is_dir($this->dirImportFile)) {
            $this->log("ERREUR le dossier " . $this->dirImportFile . " n\'a pas été trouvé");
            return false;
        }
        if(!is_dir($this->dirImportFile.'/'.$dirname)) {
            $this->log("ERREUR le dossier " . $this->dirImportFile .'/'. $dirname ." n\'a pas été trouvé");
            return false;
        }
        $this->images[$dirname] = array();
        $dir_iterator = new RecursiveDirectoryIterator($this->dirImportFile.'/'.$dirname);
        $iterator = new RecursiveIteratorIterator($dir_iterator);  
        foreach ($iterator as $file) { 
          // Recupération des fichiers de type images dans tout les sous repertoires
          if($file->isFile()) {
            $filename = $file->getFilename();
            if(preg_match('/^([0-9]+)-(.+)-([0-9]+)\.(jpg|png|jpeg)$/i', $filename, $results)){

              if(count($results) == 5) {         
                $id_produit = (int) $results[1];
                $position = (int) $results[3];
                //creation du tableau s'il n'est pas encore créé
                if(!isset($this->images[$dirname][$id_produit])) {
                  $this->images[$dirname][$id_produit] = array();
                }

                $this->images[$dirname][$id_produit][$position] = array(
                    'file' => $file,
                    'regex' => $results,
                );
              }
            } 
          }
        }
        return true;
    }

    private function checkDir($dir = false) {      
        $this->log('Verification du repertoire '.$dir);
        if($dir) {
            if(!is_dir($dir)) {              
              $this->log('Création du repertoire '.$dir);
              if(mkdir($dir)) {
                $this->log('SUCCESS');
              } else {                
                $this->log('FAIL');
              }
            }
            return is_dir($dir);
        }        
        return false;
    }

}

$timestamp_debut = microtime(true);

$import = new ImportImages();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$import->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");
 
if(function_exists('opcache_reset')) {
  opcache_reset();
  $import->log('opcache_reset OK'."\n");
}
