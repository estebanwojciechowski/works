<?php

class ProductController extends ProductControllerCore
{
    public $id_product_attribute = 0;

    public function initContent()
    {
        parent::initContent();
        // Recupération des délais mini des différents transporteurs
        // Ne fonctionne pas sur les boutiques qui n'ont pas de magasin

        $carrierColissimo = $carrierCNC = $carrierVelo = $carrierVoiture = false;
        $carrierCNC = Carrier::getCarrierByAttributeFields(array('delai_clickandcollect'));

        if($this->product->delai_livraison_velo > 0)
            $carrierVelo = Carrier::getCarrierByAttributeFields(array('delai_livraison_velo'));

        if($this->product->delai_livraison_voiture > 0)
            $carrierVoiture = Carrier::getCarrierByAttributeFields(array('delai_livraison_voiture'));

        if($this->product->delai_colissimo > 0)
            $carrierColissimo = Carrier::getCarrierByAttributeFields(array('delai_colissimo'));

        $delais = array(
            'clickandcollect' => false,
            'velo' => false,
            'voiture' => false,
            'colissimo' => false,
        );

        if(Validate::isLoadedObject($carrierCNC)) {
            $delais['clickandcollect'] = $carrierCNC->getProductDate($this->product);
            $delais['clickandcollect'] = Tools::getDateFromTimestamp($delais['clickandcollect']);
        }

        if(Validate::isLoadedObject($carrierVelo)) {
            $delais['velo'] = $carrierVelo->getProductDate($this->product);
            $delais['velo'] = Tools::getDateFromTimestamp($delais['velo']);
        }

        if(Validate::isLoadedObject($carrierVoiture)) {
            $delais['voiture'] = $carrierVoiture->getProductDate($this->product);
            $delais['voiture'] = Tools::getDateFromTimestamp($delais['voiture']);
        }

        if(Validate::isLoadedObject($carrierColissimo)) {
            $delais['colissimo'] = $carrierColissimo->getProductDate($this->product);
            $delais['colissimo'] = Tools::getDateFromTimestamp($delais['colissimo']);
        }

        // Liste des id_feature a afficher dans le nav tab
        $display_features = array(1,2,3,7);


        // recupération des images de l'attribut par défaut des formules
        // si elles existent
       if($id_product_attribute = Tools::getValue('id_product_attribute')) {
            // verification si le produit existe avec cet attribut
            // sinon redirection vers attribut par défaut
            // Product::getAttributesParams est mis en cache donc j'utilise cette fonction
            $params = Product::getAttributesParams($this->product->id, $id_product_attribute);
            if(empty($params)) {
                // redirection
                Tools::redirect($this->context->link->getProductLink($this->product));
            }
            $this->id_product_attribute = $id_product_attribute;
        } else {
            $this->id_product_attribute = Product::getDefaultAttribute($this->product->id);
        }
        $infos = array(
            'images' => array(),
            'features' => array(),
        );

        if($this->product->isFormule()) {
            // recupération des attributs
            $attributes = $this->product->getAttributes($this->id_product_attribute, $this->context->language->id);
            $infos = Product::getInfosFromAttributes($this->product->id, $attributes);
        }

        $this->context->smarty->assign(
            array(
                'delais' => $delais,
                'display_features' => $display_features,
                'attributes_images' => $infos['images'],
                'attributes_features' => $infos['features'],
                'formules' => !empty($infos['images'])
            )
        );


    }

    public function displayAjaxRefreshFormule()
    {
        $errors = array();
        $selected_attributes = Tools::getValue('attributes');
        $attributes = array();
        if($selected_attributes && count($selected_attributes)) {
            foreach ($selected_attributes as $id_attribute_group => $id_attribute) {
                if(in_array($id_attribute_group, Product::$formules_id_attribute_groups)) {
                    $attribute = new Attribute($id_attribute, $this->context->language->id);
                    if(Validate::isLoadedObject($attribute)) {
                        $attributes[$id_attribute_group] = (array)$attribute;
                    }
                }
            }

            if(count($attributes)) {

                // recupération des features et images
                $infos = Product::getInfosFromAttributes($this->product->id, $attributes);

                //recupération des templates images
                $this->context->smarty->assign('attributes_images', $infos['images']);

                $cover = $this->context->smarty->fetch('catalog/_partials/product-formule-cover-thumbnails.tpl');

                $product_images_modal = $this->context->smarty->fetch('catalog/_partials/product-formule-images-modal.tpl');

                //recupération des templates features
                $this->context->smarty->assign('part', 'top');
                $product_nav_tabs = $this->context->smarty->fetch('catalog/_partials/formule-features.tpl');

                $this->context->smarty->assign('part', 'content');
                $tab_content = $this->context->smarty->fetch('catalog/_partials/formule-features.tpl');

                header('Content-Type: application/json');
                $this->ajaxDie(
                    Tools::jsonEncode(
                        array(
                            'success' => true,
                            'cover' => $cover,
                            'product_images_modal' => $product_images_modal,
                            'product_nav_tabs' => $product_nav_tabs,
                            'tab_content' => $tab_content
                        )
                    )
                );
            }
        }

        $errors[] = 'Les attributs n\'ont pas été retrouvés';

        header('Content-Type: application/json');
        $this->ajaxDie(
            Tools::jsonEncode(
                array(
                    'error' => true,
                    'errors' => $errors,
                )
            )
        );

    }


    // ESTEBANW INSITACTION 06122018
    // Performance atroces pour un nombre de déclis élevés
    // Override pour gestion des formules
    /**
     * Assign template vars related to attribute groups and colors.
     */
    protected function assignAttributesGroups($product_for_template = null)
    {
        $groups = array();
        if($this->product->isFormule()) {
            $attributes_groups = $this->product->getDistinctAttributesGroups($this->context->language->id);
            if (is_array($attributes_groups) && $attributes_groups) {
                foreach ($attributes_groups as $key => $attributes_group) {

                    $id_attribute_group = $attributes_group['id_attribute_group'];
                    $id_attribute = $attributes_group['id_attribute'];


                    //initialisation des infos du groupe d'attribut
                    if(!is_array($groups[$id_attribute_group]))
                        $groups[$id_attribute_group] = array(
                            'group_name'    => $attributes_group['group_name'],
                            'name'          => $attributes_group['public_group_name'],
                            'group_type'    => $attributes_group['group_type'],
                            'default'       => -1,
                            'attributes'    => array()
                        );

                    //initialisation de la liste des attributs
                    if(!is_array($groups[$id_attribute_group]['attributes'][$id_attribute])) {
                        $selected = (isset($product_for_template['attributes'][$attributes_group['id_attribute_group']]['id_attribute']) && $product_for_template['attributes'][$attributes_group['id_attribute_group']]['id_attribute'] == $attributes_group['id_attribute']) ? true : false;

                        $groups[$id_attribute_group]['attributes'][$id_attribute] = array(
                            'name'                  => $attributes_group['attribute_name'],
                            'html_color_code'       => $attributes_group['attribute_color'],
                            'texture'               => '',
                            'selected'              => $selected,
                        );
                    }
                }
            }
            $this->context->smarty->assign(array(
                'groups' => $groups,
                'colors' => false,
                'combinations' => $this->combinations,
                'combinationImages' => array(),
            ));
        } else {
            parent::assignAttributesGroups($product_for_template);
        }

    }
}