{* <div id="custom-text-bloc4">
  	{$post_bloc nofilter}
</div> *}

<div class="storeloc__container parallax-window" data-parallax="scroll" data-speed="0.3" data-image-src="{$urls.base_url}{$smarty.const._THEME_DIR_}assets/img/storelocation-2.jpg">
	<a href="{$urls.base_url}boulangeries" class="storeloc__content">
		<b>{l s="Proche de vous"}</b>
		<div class="storeloc__title">
			<small class="todo">@todo</small>{l s="Trouvez votre boulangerie"}
		</div>
	</a>
</div>