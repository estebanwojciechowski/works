{**
 * 2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 *
 *} 

<input type="checkbox" name="{$params.multishop_group_prefix|escape:'htmlall':'UTF-8'}_multishop_override_enabled[]" value="{$params.name|escape:'htmlall':'UTF-8'}"
       id="conf_helper_{$params.name|escape:'htmlall':'UTF-8'}" {if !$params.disabled} checked="checked"{/if}
       onclick="{$params.name|escape:'htmlall':'UTF-8'}_toggleMultishopDefaultValue()">
<script>
    $(document).ready(function(){
        {$params.name|escape:'htmlall':'UTF-8'}_toggleMultishopDefaultValue();
    });

    function {$params.name|escape:'htmlall':'UTF-8'}_toggleMultishopDefaultValue() {
        var obj = $('#conf_helper_{$params.name|escape:'htmlall':'UTF-8'}');
        var key = '{$params.name|escape:'htmlall':'UTF-8'}';

        if (!$(obj).prop('checked') || $('.'+key).hasClass('isInvisible')) {
            $('.conf_id_'+key+' input, .conf_id_'+key+' textarea, .conf_id_'+key+' select, .conf_id_'+key+' button').attr('disabled', true);
            $('.conf_id_'+key+' label.conf_title').addClass('isDisabled');
        } else {
            $('.conf_id_'+key+' input, .conf_id_'+key+' textarea, .conf_id_'+key+' select, .conf_id_'+key+' button').attr('disabled', false);
            $('.conf_id_'+key+' label.conf_title').removeClass('isDisabled');
        }
        $('.conf_id_'+key+' input[name^=\'{$params.multishop_group_prefix|escape:'htmlall':'UTF-8'}_multishop_override_enabled\']').attr('disabled', false);
    }
</script>