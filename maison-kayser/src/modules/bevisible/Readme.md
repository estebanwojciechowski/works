Thank you for using our module. For the best user experience we provide some information how to use ths module.
If you need more help, please feel free to contact us.


Installation

The module can be installed in the same way that any PrestaShop module.

1. Via PrestaShop back office
 - Navigate to the "Modules and Services" in the admin.
 - In the upper-right hand corner click "Add a new module", the upload form will visible.
 - Select the compressed module file (.zip, .tar, .tar.gz, .tgz) by click "Choose a file" and then click "Upload this module".
 - Currently the module is uploaded, now click "Install" on the uploaded module.
 - Now the module is installed and you can start using it.

2. Via FTP
 - Upload the uncompressed module folder to the "modules" directory of your store.
 - Navigate to the "Modules and Services" in the admin.
 - Find the uploaded module in the module list and click "Install".
 - Now the module is installed and you can start using it.


Getting Started

When you have installed the module, you need to setup a Google Maps API Key to enable all the features.

Configure the module

1. General Settings
 - Google Maps API Key (Some features require)- Here you can get a Google Maps API Key
 - Google Maps Store Marker Icon
 - Google Maps Style - You can use a Javascript Style Array from Snazzy Maps or Google Maps Styling Wizzard.
 - Default store
 - Enable customer nearest store function - If a customer logged in and has an address the map tries to select the nearest store
 - Enable/Disable stores

2 .Home Page Settings
 - Block title - Can be empty
 - Map height - Required
 - Map zoom level - In automatic mode all the stores marker fits in the map viewport
 - Open info window - The default or nearest stores info window on the map automatically opened
 - Enable nearest store locator - Under the map an you can type an address find it's nearest store
 - Custom HTML content before and after the map - You can set any HTML content like image, text, etc

3. Custom Page Settings
 - Edit custom page - You can edit the custom page url (link rewrite), title, meta description, meta keywords
 - Map height - Required
 - Map zoom level - In automatic mode all the stores marker fits in the map viewport
 - Open info window - The default or nearest stores info window on the map automatically opened
 - Enable nearest store locator - Under the map an you can type an address find it's nearest store
 - Custom HTML content before and after the map - You can set any HTML content like image, text, etc


Configure store

Navigate to Shop Parameters -> Contact -> Stores. Here you can add, edit or remove store which is available for displaying on map. You can change name,
address, coordinates, status, picture and opening hours. This information appears in the maps info box.

To setup opening hours, please use the following formats:
 - 9:00 - 19:00
 - 09:00 - 19:00
 - 9:00AM - 7:00PM
 - 9:00am - 7:00pm
 - 9:00am - 1:00pm and 3:00pm - 7:00pm
 - 9:00am - 1:00pm, 3:00pm - 7:00pm
 - 9:00 - 13:00 | 15:00 - 19:00
 - For closed, just use any string or leave it empty

Be sure have at lease one space between each time block as in the examples and no space between time and the "am", "pm" modifier.


Store Importer

To start importing you need to prepare an import CSV file, for a start please use our Store Import Sample CSV. The store name behaves as a unique identifier,
so you can update exist stores with the importer. Please keep the columns' name.

The import is only successful if contains NO invalid values.

If an error occurs an error message displays the row number, column name and a invalid value as well, it helps you easily find and fix the invalid values.

Required Columns:

Name
 - The name of the store.
 - Must be a valid generic name.
 - Example: My Cool Store

Address
 - The address of the store.
 - Must be a valid address.
 - Example: Test st. 1

City
 - The city of the store.
 - Must be a valid city name.
 - Example: Miami

Postcode
 - The postcode of the store.
 - Must be a valid postcode regarding to the country's zip/postal code format.
 - Example: 33013

Country
 - The country of the store.
 - The valid country names listed here.
 - Example: United States

State
 - The state of the store.
 - Only required if the country requires a state, other way leave it blank.
 - The valid state names listed here.
 - Example: Florida

Active
 - The status of the store (0/1).
 - "0" - Inactive, "1" - Active


Optional Columns:

Address2
 - The second address line of the store.
 - Must be a valid address.
 - Example: Test st. 1

Latitude
 - The latitude (coordinate) of the store.
 - Must be a valid coordinate.
 - Example: 23.0243223

Longitude
 - The longitude (coordinate) of the store.
 - Must be a valid coordinate.
 - Example: -83.42123322

Phone
 - The phone number of the store.
 - Must be a valid phone number.
 - Example: +1 (800) 555-3247

Fax
 - The fax number of the store.
 - Must be a valid phone number.
 - Example: +1 (800) 555-3247

Email
 - The email address of the store.
 - Must be a valid email address.
 - You can add multiple email address by separating the address with "," (comma).

Note
 - The note of the store.
 - Must be a valid clean html text.

ImageURL
 - The image/logo of the store.
 - Must be a valid absolute url to the image, which is accessible through internet.
 - If you import images, please regenerate all image thumbnails for Stores here

Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
 - The opening hours of the store per day.
 - Must be a valid opening hour, please see "Configure store" section of the help.


ModuleFactory.co - PrestaShop Partner Agency
