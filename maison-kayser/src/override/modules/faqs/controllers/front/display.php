<?php

class FaqsDisplayModuleFrontControllerOverride extends FaqsDisplayModuleFrontController
{

  public function initContent()
  {
    parent::initContent();

    $this->_shopId = 1;

    $mostFaq = faqsPost::getFeaturedFaqs($this->_shopId, $this->_langId);
    $faqCategories = faqsCategory::getCategoriesFaq($this->_shopId, $this->_langId);

    if(!$faqCategories) {

      $faqCategories = array();

    }

    foreach($faqCategories as $key => $category) {
      $num_of_questions_in_category = faqsCategory::getNumberOfQuestionsInCategory($category['id_gomakoil_faq_category']);

      if ($num_of_questions_in_category <= 0) {
        unset($faqCategories[$key]);
        continue;
      }

      $faqsFromCategory = faqsPost::getAllActiveFaqsFromCategory($category['id_gomakoil_faq_category'], $this->_shopId, $this->_langId);

      $faqCategories[$key]['count'] = count($faqsFromCategory);
      $faqCategories[$key]['faqs'] = ($faqsFromCategory);
    }

    $faqCategoryId = Tools::getValue("category");

    if( !$faqCategoryId ){
      $faqCategoryId = false;
    }

    $faqId = Tools::getValue("question");
    $search = trim(Tools::getValue("search"));
    $search_val = '';
    $baseUrl = faqs::getBaseUrl();

    $faq = false;
    $questions = false;

    if ($faqCategoryId !== false) {
      if (_PS_VERSION_ >= 1.7) {
          /*
		  Insitaction : Disable left column
		  Context::getContext()->shop->theme->setPageLayouts(array("module-faqs-display" => "layout-left-column"));		  
		  */
      }

      $questions['content'] = faqsPost::getFaqsByUrl($this->_shopId, $this->_langId, $faqCategoryId);
      $cat = faqsCategory::getCategoryByName($this->_shopId, $this->_langId, $faqCategoryId);

      if (!empty($questions) && !empty($cat)) {
        $questions['name'] = $cat[0]['name'];
        $questions['color'] = $cat[0]['color'];

        if( $faqId !== false ){
          $faq = faqsPost::getFaqsByUrl($this->_shopId, $this->_langId, $faqCategoryId, $faqId);
          $faq = $faq[0];
          $questions = false;
        }
      }
    }

    if ($search) {
      if (_PS_VERSION_ >= 1.7) {
        Context::getContext()->shop->theme->setPageLayouts(array("module-faqs-display" => "layout-left-column"));
      }

     $search_val = $search;
     $search = faqsPost::searchFaqs($this->_shopId, $this->_langId, $search);
    }

    $template_vars = array(
        'faqCategories'     => $faqCategories,
        'faq'               => $faq,
        'mostFaq'           => $mostFaq,
        'questions'         => $questions,
        'faqUrl'            => $baseUrl,
        'search'            => $search,
        'search_val'        => $search_val,
        'rewrite_settings'  => faqs::getRewriteSettings(),
    );

    if (_PS_VERSION_ < 1.7) {
      if ($faq || $questions || $search_val) {
        $template_vars['hide_left_column'] = false;
        $template_vars['HOOK_LEFT_COLUMN'] = Module::getInstanceByName('faqs')->hookDisplayLeftColumn();

        $category_name = null;

        if (isset($cat) && isset($cat[0]['name'])) {
          $category_name = $cat[0]['name'];
        }

        $template_vars['path'] = $this->getBreadcrumbsPath16($faqCategoryId, $category_name, $faqId, trim(Tools::getValue("search")));
      } elseif (!$faq && !$questions && !$search_val) {
        $template_vars['hide_left_column'] = true;
        $this->display_column_left = false;
        $template_vars['path'] = $this->getBreadcrumbsPath16();
      }
    }


    $this->context->smarty->assign($template_vars);

    $template_path = _PS_VERSION_ >= 1.7 ? 'module:faqs/views/templates/front/1.7/display.tpl' : '1.6/display.tpl';
    $this->setTemplate($template_path);
  }
}
