<?php

class CheckoutDeliveryStep extends CheckoutDeliveryStepCore
{
    public function handleRequest(array $requestParams = array())
    {
        if (isset($requestParams['delivery_option'])) {             
           
            $this->getCheckoutSession()->setDeliveryOption(
                $requestParams['delivery_option']
            );

            //ADD BY ESTEBANW INSITACTION 15032018
            //Ajout de la date de livraison ou de collecte
            $has_date_picker = false;
            $current_carrier = intval(current($requestParams['delivery_option']));
            if(isset($requestParams['datepicker_'.$current_carrier])) { 
                $has_date_picker = true;
                $this->getCheckoutSession()->setDeliveryDate($requestParams['datepicker_'.$current_carrier]);
            }

            $this->getCheckoutSession()->setRecyclable(
                isset($requestParams['recyclable']) ? $requestParams['recyclable'] : false
            );
            $this->getCheckoutSession()->setGift(
                isset($requestParams['gift']) ? $requestParams['gift'] : false,
                (isset($requestParams['gift']) && isset($requestParams['gift_message'])) ? $requestParams['gift_message'] : ''
            );
        }

        if (isset($requestParams['delivery_message'])) { 
            //ADD BY ESTEBANW 06042018
            //recupération du message saisi au transporteur choisi
            $message = '';
            if(isset($requestParams['delivery_message'][$current_carrier])) {
                $message = $requestParams['delivery_message'][$current_carrier];
            } 
            $this->getCheckoutSession()->setMessage($message);
        }

        if ($this->step_is_reachable && isset($requestParams['confirmDeliveryOption'])) {
            // we're done if
            // - the step was reached (= all previous steps complete)
            // - user has clicked on "continue"
            // - there are delivery options
            // - the is a selected delivery option
            // - the module associated to the delivery option confirms
            $deliveryOptions = $this->getCheckoutSession()->getDeliveryOptions();
            $this->step_is_complete =
                !empty($deliveryOptions) && $this->getCheckoutSession()->getSelectedDeliveryOption() && $this->isModuleComplete($requestParams)
            ;
            //ADD BY ESTEBANW INSITACTION
            //Est ce que la date est validée
            if($has_date_picker) {
                $selected_date = $this->getCheckoutSession()->getDeliveryDate();
                 $this->step_is_complete =  $this->step_is_complete && $selected_date && is_numeric($selected_date) && $selected_date > time();
            }
        }

        $this->setTitle($this->getTranslator()->trans('Shipping Method', array(), 'Shop.Theme.Checkout'));

        Hook::exec('actionCarrierProcess', array('cart' => $this->getCheckoutSession()->getCart()));
    }

    public function render(array $extraParams = array())
    {
        return $this->renderTemplate(
            $this->getTemplate(),
            $extraParams,
            array(
                'hookDisplayBeforeCarrier' => Hook::exec('displayBeforeCarrier', array('cart' => $this->getCheckoutSession()->getCart())),
                'hookDisplayAfterCarrier' => Hook::exec('displayAfterCarrier', array('cart' => $this->getCheckoutSession()->getCart())),
                'id_address' => $this->getCheckoutSession()->getIdAddressDelivery(),
                'delivery_options' => $this->getCheckoutSession()->getDeliveryOptions(),
                'delivery_option' => $this->getCheckoutSession()->getSelectedDeliveryOption(),
                'delivery_option_date' => $this->getCheckoutSession()->getDeliveryDate(),
                'recyclable' => $this->getCheckoutSession()->isRecyclable(),
                'recyclablePackAllowed' => $this->isRecyclablePackAllowed(),
                'delivery_message' => $this->getCheckoutSession()->getCarrierMessage(),
                'gift' => array(
                    'allowed' => $this->isGiftAllowed(),
                    'isGift' => $this->getCheckoutSession()->getGift()['isGift'],
                    'label' => $this->getTranslator()->trans(
                        'I would like my order to be gift wrapped %cost%',
                        array('%cost%' => $this->getGiftCostForLabel()),
                        'Shop.Theme.Checkout'
                    ),
                    'message' => $this->getCheckoutSession()->getGift()['message'],
                ),
            )
        );
    }
}