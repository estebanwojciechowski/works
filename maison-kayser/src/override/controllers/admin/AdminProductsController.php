<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */


/**
 * @property Product $object
 */
class AdminProductsController extends AdminProductsControllerCore
{
	// ADD BY ESTEBANW INSITACTION 13062018
	// Correction de l'insertion des custom features
	// HS sur presta 1.7...
	public function processFeatures($id_product = null)
	{
		if (!Feature::isFeatureActive()) {
			return;
		}
		$defaultValue = '';
		$id_product = (int) $id_product ? $id_product : (int)Tools::getValue('id_product');

		if (Validate::isLoadedObject($product = new Product($id_product))) {
			// delete all objects
			// commenté pour eviter la disparition des caractérisitiques 
			// si une erreur se produit
			// $product->deleteFeatures();

			// add new objects
			$languages = Language::getLanguages(false);
			$form = Tools::getValue('form', false);

			// ADD BY ESTEBANW 07092018
			// un champ input hidden hidden_feature_info est ajouté 
			// pour retrouver les features existantes et 
			// les supprimer 1 a 1
			
			// recupération des product_features existantes avant enregistrement
			$existants_features_values = DB::getInstance()->executeS('
				SELECT id_feature_value FROM `'._DB_PREFIX_.'feature_product`  
				WHERE id_product = '.$id_product
			);
			if($existants_features_values) {
				$existants_features_values = array_column($existants_features_values, 'id_feature_value');
			}
			// tableau qui stock les id feature value qui seront effacés
			$deleted_features_value = array();
			// recupération des input hidden pour faire la liaison entre les features
			// récup en post et leur id_feature_value
			$existants_features = Tools::getValue('hidden_feature_info');
			if (false !== $form) {
				$features = isset($form['step1']['features']) ? $form['step1']['features'] : array();
				 if (is_array($features)) {
					foreach ($features as $key => $feature) {
						$defaultValue = $this->checkFeatures($languages, $feature);
						// recupération de l'id feature correspondant s'il existe
						$current_features_infos = array();
						if($existants_features && isset($existants_features[$key])) {
							// 0 => id_feature
							// 1 => id_feature_value
							// 2 => custom ? 0 ou 1
							$current_features_infos = explode('_', $existants_features[$key]);
							if(count($current_features_infos) == 3) {
								$product->deleteFeature($current_features_infos[1]);
								$deleted_features_value[] = $current_features_infos[1];
							}
						}
						// VERIFICATION Qu'il n'y ait pas de valeurs custom
						$has_custom = false;                        
						foreach ($languages as $language) {
							$valueToAdd = (isset($feature['custom_value'][$language['id_lang']]))
								? $feature['custom_value'][$language['id_lang']]
								: $defaultValue;
							if(strlen(trim($valueToAdd))) {
								$has_custom = true;
								break;
							}
						}
						if (!empty($feature['value']) && !$has_custom) {
							$product->addFeaturesToDB($feature['feature'], $feature['value']);
						} elseif ($defaultValue) {
							$idValue = $product->addFeaturesToDB($feature['feature'], 0, 1);
							foreach ($languages as $language) {
								$valueToAdd = (isset($feature['custom_value'][$language['id_lang']]))
									? $feature['custom_value'][$language['id_lang']]
									: $defaultValue;
								$product->addFeaturesCustomToDB($idValue, (int)$language['id_lang'], $valueToAdd);
							}
						}
					}

					// Verification si des feature value ont été enlevées
					$toDelete = array_diff($existants_features_values, $deleted_features_value);
					if($toDelete) {
						foreach ($toDelete as $key => $id_feature_value) {
							 $product->deleteFeature($id_feature_value);
						}
					}
				}
			}
			$this->updateLivraisonFeatures();
		} else {
			$this->errors[] = $this->trans('A product must be created before adding features.', array(), 'Admin.Catalog.Notification');
		}
	}

	// ADD BY ESTEBANW INSITACTION 13062018
	// $rules['sizeLang']['value'] pas set quand un id feature value n'est 
	// pas renseigné
	protected function checkFeatures($languages, $featureInfo)
	{
		$rules = call_user_func(array('FeatureValue', 'getValidationRules'), 'FeatureValue');
		$feature = Feature::getFeature((int)Configuration::get('PS_LANG_DEFAULT'), $featureInfo['feature']);

		foreach ($languages as $language) {
			if (isset($featureInfo['custom_value'][$language['id_lang']])) {
				$val = $featureInfo['custom_value'][$language['id_lang']];
				$current_language = new Language($language['id_lang']);
				if (isset($rules['sizeLang']['value']) && Tools::strlen($val) > $rules['sizeLang']['value']) {
					$this->errors[] = $this->trans(
						'The name for feature %1$s is too long in %2$s.',
						array(
							' <b>' . $feature['name'] . '</b>',
							$current_language->name
						),
						'Admin.Catalog.Notification'
					);
				} elseif (!call_user_func(array('Validate', $rules['validateLang']['value']), $val)) {
					$this->errors[] = $this->trans(
						'A valid name required for feature. %1$s in %2$s.',
						array(
							' <b>'.$feature['name'].'</b>',
							$current_language->name
						),
						'Admin.Catalog.Notification'
					);
				}
				if (count($this->errors)) {
					return 0;
				}
				// Getting default language
				if ($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT')) {
					return $val;
				}
			}
		}
		return 0;
	}

	/**
	// ADD BY ESTEBANW INSITACTION 19062018
	// Gestion des caractéristiques transporteur 
	 * [updateLivraisonFeatures A l'ajout des transporteurs
	 * on ajoute les caractéristiques de livraison correspondantes
	 * pour affichage sur le moteur à facettes]
	 * @param [type] $product [description]
	 */

	protected function updateLivraisonFeatures() {
		if (!isset($product)) {
			$product = new Product((int)Tools::getValue('id_product'));
		}

		if (Validate::isLoadedObject($product)) {
			$carriers = array();

			if (Tools::getValue('selectedCarriers')) {
				$carriers = Tools::getValue('selectedCarriers');
			}

			$id_ref_colissimo = Configuration::get('COLISSIMO_ID_REFERENCE');
			if(!$id_ref_colissimo) {
				$id_ref_colissimo = 5;
			}
			$feature_livraison_group_id = 11;
			$carriersTofeatures = array(
				1   => 492,     // clickandcollect
				51  => 492,     // clickandcollect
				6   => 491,     // Livraison vélo 
				12  => 491,     // Livraison voiture
				$id_ref_colissimo   => 493,     // Colissimo
			);
			if(empty($carriers)) {
				// ajout de toutes les valeurs
				$carriers = array_keys($carriersTofeatures);
			} 
			// Suppression des valeurs actuelles pour les livraisons
			DB::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'feature_product` 
										WHERE `id_feature` = 11 
										AND `id_product` = '.$product->id);

			$traites = array();
			foreach ($carriers as $id_carrier) {
				if(isset($carriersTofeatures[$id_carrier])
					&& !in_array($carriersTofeatures[$id_carrier], $traites)) {
					
					if(Product::addFeatureProductImport($product->id, $feature_livraison_group_id, $carriersTofeatures[$id_carrier])) {

						$traites[] = $carriersTofeatures[$id_carrier];
					}

				}
			}
		}
	}

}
