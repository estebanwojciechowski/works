<?php
/**
 * @property Product $object
 */
class AdminAttributeGeneratorController extends AdminAttributeGeneratorControllerCore
{

	private $wanted_quantity;

	public function generateCombinations($attributes_list = array(), $product = false, $wanted_quantity = 1, $instance = false)
    {
    	if($instance) {
    		$instance->log('generateCombinations START');
    	}

        if (!is_array($attributes_list)) {
            $this->errors[] = $this->trans('Please select at least one attribute.', array(), 'Admin.Catalog.Notification');
        } else {

            if (count($attributes_list) && Validate::isLoadedObject($product)) {
				if($instance) {
		    		$instance->log("generateCombinations Paramètres OK \n attributes_list :\n".json_encode($attributes_list)."\n\n id produit:".$product->id);
		    	}
                $this->product = $product;
                $this->wanted_quantity = $wanted_quantity;

                // suppression des précédentes déclinaisons
                $this->product->deleteProductAttributes();

                // initialisation des combinaisons
                $this->combinations = array_values(AdminAttributeGeneratorController::createCombinations($attributes_list));

                $values = array_values(array_map(array($this, 'addAttributeGenerator'), $this->combinations));

                // @since 1.5.0
                if ($this->product->depends_on_stock == 0) {
                    $attributes = Product::getProductAttributesIds($this->product->id, true);
                    foreach ($attributes as $attribute) {
                        StockAvailable::removeProductFromStockAvailable($this->product->id, $attribute['id_product_attribute'], Context::getContext()->shop);
                    }
                }

                SpecificPriceRule::disableAnyApplication();

                $this->product->generateMultipleCombinations($values, $this->combinations);

                // Reset cached default attribute for the product and get a new one
                Product::getDefaultAttribute($this->product->id, 0, true);
                Product::updateDefaultAttribute($this->product->id);

                // @since 1.5.0
                if ($this->product->depends_on_stock == 0) {
                    $attributes = Product::getProductAttributesIds($this->product->id, true);
                    $quantity = $wanted_quantity;
                    foreach ($attributes as $attribute) {
                        if (Shop::getContext() == Shop::CONTEXT_ALL) {
                            $shops_list = Shop::getCompleteListOfShopsID();
                            if (is_array($shops_list)) {
                                foreach ($shops_list as $current_shop) {
                                    if (isset($current_shop['id_shop']) && (int)$current_shop['id_shop'] > 0) {
                                        StockAvailable::setQuantity($this->product->id, (int)$attribute['id_product_attribute'], $quantity, (int)$current_shop['id_shop']);
                                    }
                                }
                            }
                        } else {
                            StockAvailable::setQuantity($this->product->id, (int)$attribute['id_product_attribute'], $quantity);
                        }
                    }
                } else {
                    StockAvailable::synchronize($this->product->id);
                }

                SpecificPriceRule::enableAnyApplication();
                SpecificPriceRule::applyAllRules(array((int)$this->product->id));
            } else {
                $this->errors[] = $this->trans('Unable to initialize these parameters. A combination is missing or an object cannot be loaded.', array(), 'Admin.Catalog.Notification');
            }
        }
        return $this->errors;
    }

    protected function addAttributeGenerator($attributes, $price = 0, $weight = 0)
    {
        if ($this->product->id) {
            return array(
                'id_product' => (int)$this->product->id,
                'price' => (float)$price,
                'weight' => (float)$weight,
                'ecotax' => 0,
                'quantity' => $this->wanted_quantity,
                'reference' => pSQL($this->product->reference),
                'default_on' => 0,
                'available_date' => '0000-00-00'
            );
        }
        return array();
    }
}
