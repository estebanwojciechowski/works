<?php


if (!defined('_PS_VERSION_')) {
    exit;
}

class Ps_GoogleanalyticsOverride extends Ps_Googleanalytics
{
    /**
     * ADD BY ESTEBANW INSITACTION 23052018
     * 
     * [getAgreementsToRGPD verifie si les cookies ont été acceptés]
     * @return [bool] [accepté ou non]
     */
    public static function getAgreementsToRGPD() {

        if(isset($_COOKIE['cookieconsent_status']) 
            && isset($_COOKIE['cookieconsent_status']) == 'dismiss') {
            return true;
        }
        return false;
    }

    public function hookdisplayOrderConfirmation($params)
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookdisplayOrderConfirmation($params);
        }
    }

    public function hookdisplayHeader($params, $back_office = false)
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookdisplayHeader($params, $back_office = false);
        }
    }

    public function hookdisplayFooter()
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookdisplayFooter();
        }
    }

    public function hookdisplayHome()
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookdisplayHome();
        }
    }

    public function hookdisplayFooterProduct($params)
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookdisplayFooterProduct($params);
        }
    }

    public function hookactionCartSave()
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookactionCartSave();
        }
    }

    public function hookactionCarrierProcess($params)
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookactionCarrierProcess($params);
        }
    }
}
