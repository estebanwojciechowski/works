<?php
/**
 *  Override pour permettre d'afficher le menu du shop par défaut sur tous les sites
 *
 *  @author    Nicolas Picart
 *  @copyright 2018 Insitaction
 */

class IqitMenuTabOverride extends IqitMenuTab{

    /**
     * Modification pour ajout du shop par défaut
     * @param $menu_type
     * @param $css_generator
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getTabsFrontend($menu_type, $css_generator)
    {
        $context = Context::getContext();
        $id_shop = $context->shop->id;
        $id_shop = (int) Configuration::get('PS_SHOP_DEFAULT');
        $id_lang = $context->language->id;

        $tabs = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT hs.`id_tab` as id_tab, hssl.`title`, hssl.`label`, hssl.`url`,
			hss.`position`,  hss.`active_label`, hss.`url_type`, hss.`id_url`, hss.`icon_type`, hss.`icon_class`, hss.`icon`, hss.`legend_icon`,
			hss.`new_window`, hss.`float`, hss.`submenu_type`, hss.`submenu_content`, hss.`submenu_width`, hss.`group_access`
			' . ($css_generator ? ', hss.`bg_color`, hss.`txt_color`,  hss.`h_bg_color`, hss.`h_txt_color`, hss.`labelbg_color`, hss.`labeltxt_color`,
			hss.`submenu_bg_color`, hss.`submenu_image`, hss.`submenu_repeat`, hss.`submenu_bg_position`,
			hss.`submenu_link_color`, hss.`submenu_hover_color`, hss.`submenu_link_color`, hss.`submenu_hover_color`, hss.`submenu_title_color`,
			hss.`submenu_title_colorh`, hss.`submenu_titleb_color`, hss.`submenu_border_t`, hss.`submenu_border_r`, hss.`submenu_border_b`,
			hss.`submenu_border_l`, hss.`submenu_border_i` ' : '') . '
			FROM ' . _DB_PREFIX_ . 'iqitmegamenu_tabs_shop hs
			LEFT JOIN ' . _DB_PREFIX_ . 'iqitmegamenu_tabs hss ON (hs.id_tab = hss.id_tab)
			LEFT JOIN ' . _DB_PREFIX_ . 'iqitmegamenu_tabs_lang hssl ON (hss.id_tab = hssl.id_tab)
			WHERE id_shop = ' . (int) $id_shop . ' AND menu_type = ' . (int) $menu_type . ' AND active = 1
			AND hssl.id_lang = ' . (int) $id_lang . '
			ORDER BY hss.position');

        if (Context::getContext()->customer) {
            foreach ($tabs as $key => $tab) {
                if ($userGroups = Context::getContext()->customer->getGroups()) {
                    $tmpLinkGroups = unserialize($tab['group_access']);
                    $linkGroups = array();

                    foreach ($tmpLinkGroups as $groupID => $status) {
                        if ($status) {
                            $linkGroups[] = $groupID;
                        }
                    }

                    $intersect = array_intersect($userGroups, $linkGroups);
                    if (!count($intersect)) {
                        unset($tabs[$key]);
                    }
                }
            }

            return $tabs;
        } else {
            return $tabs;
        }
    }

}
