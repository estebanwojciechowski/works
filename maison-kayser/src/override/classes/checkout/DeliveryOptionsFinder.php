<?php

use Symfony\Component\Translation\TranslatorInterface;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\ObjectPresenter;

class DeliveryOptionsFinder extends DeliveryOptionsFinderCore {

	private $context;
    private $objectPresenter;
    private $translator;
    private $priceFormatter;

    public function __construct(
        Context $context,
        TranslatorInterface $translator,
        ObjectPresenter $objectPresenter,
        PriceFormatter $priceFormatter
    ) {
        $this->context = $context;
        $this->objectPresenter = $objectPresenter;
        $this->translator = $translator;
        $this->priceFormatter = $priceFormatter;
        parent::__construct($context, $translator, $objectPresenter, $priceFormatter);
    }

    private function isFreeShipping($cart, array $carrier)
    {
        $free_shipping = false;

        if ($carrier['is_free']) {
            $free_shipping = true;
        } else {
            foreach ($cart->getCartRules() as $rule) {
                if ($rule['free_shipping'] && !$rule['carrier_restriction']) {
                    $free_shipping = true;
                    break;
                }
            }
        }

        return $free_shipping;
    }

    public function getSelectedDeliveryOption()
    {
        return current($this->context->cart->getDeliveryOption(null, false, false));
    }

     public function getDeliveryOptions()
    { 
        $delivery_option_list = $this->context->cart->getDeliveryOptionList();
        $include_taxes = !Product::getTaxCalculationMethod((int) $this->context->cart->id_customer) && (int) Configuration::get('PS_TAX');
        $display_taxes_label = (Configuration::get('PS_TAX') && !Configuration::get('AEUC_LABEL_TAX_INC_EXC'));

        $carriers_available = array();

        if (isset($delivery_option_list[$this->context->cart->id_address_delivery])) {
            foreach ($delivery_option_list[$this->context->cart->id_address_delivery] as $id_carriers_list => $carriers_list) {
                foreach ($carriers_list as $carriers) {
                    if (is_array($carriers)) {
                        foreach ($carriers as $carrier) {

                            $carrier = array_merge($carrier, $this->objectPresenter->present($carrier['instance']));
                            $delay = $carrier['delay'][$this->context->language->id];


                            // ADD BY INSITACTION ESTEBANW - 12032018
                            $carrier['cart_date_delivery'] = $carrier['instance']->getCartDate($this->context->cart); 

                            $id_ref_colissimo = Configuration::get('COLISSIMO_ID_REFERENCE');
                            if(!$id_ref_colissimo)
                                $id_ref_colissimo = 5;
                            
                            if($carrier['instance']->id_reference == $id_ref_colissimo) {
                                $hasGlutenFreeProduct = $this->context->cart->hasGlutenFreeProduct();
                                $carrier['cart_date_delivery_format'] = Tools::getDateColissimo($hasGlutenFreeProduct);
                            } else {
                                if($carrier['instance']->is_module == 1 && $carrier['instance']->external_module_name ==  'kayserdelivery' ) {
                                    $carrier['cart_date_delivery_format'] = Tools::getDateFromTimestamp($carrier['cart_date_delivery']);
                                }
                            }
                            $carrier['delay'] = $delay;

                            $carrier['attributes'] = $carrier['instance']->attributes;
                            $carrier['datepicker'] = $carrier['instance']->datepicker; 
                            $carrier['velo'] =  $carrier['instance']->velo;

                            unset($carrier['instance']);

                            if ($this->isFreeShipping($this->context->cart, $carriers_list)) {
                                $carrier['price'] = $this->translator->trans(
                                    'Free', array(), 'Shop.Theme.Checkout'
                                );
                            } else {
                                if ($include_taxes) {
                                    $carrier['price'] = $this->priceFormatter->format($carriers_list['total_price_with_tax']);
                                    if ($display_taxes_label) {
                                        $carrier['price'] = sprintf(
                                            $this->translator->trans(
                                                '%s tax incl.', array(), 'Shop.Theme.Checkout'
                                            ),
                                            $carrier['price']
                                        );
                                    }
                                } else {
                                    $carrier['price'] = $this->priceFormatter->format($carriers_list['total_price_without_tax']);
                                    if ($display_taxes_label) {
                                        $carrier['price'] = sprintf(
                                            $this->translator->trans(
                                                '%s tax excl.', array(), 'Shop.Theme.Checkout'
                                            ),
                                            $carrier['price']
                                        );
                                    }
                                }
                            }

                            if (count($carriers) > 1) {
                                $carrier['label'] = $carrier['price'];
                            } else {
                                $carrier['label'] = $carrier['name'].' - '.$carrier['delay'].' - '.$carrier['price'];
                            }

                            // If carrier related to a module, check for additionnal data to display
                            $carrier['extraContent'] = '';
                            if ($carrier['is_module']) {
                                if ($moduleId = Module::getModuleIdByName($carrier['external_module_name'])) {
                                    $carrier['extraContent'] = Hook::exec('displayCarrierExtraContent', array('carrier' => $carrier), $moduleId);
                                }
                            }

                            $carriers_available[$id_carriers_list] = $carrier;
                        }
                    }
                }
            }
        }

        return $carriers_available;
    }
}