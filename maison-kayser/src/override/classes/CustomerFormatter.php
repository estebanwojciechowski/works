<?php
/**
 *  2018 Insitaction
 *
 *  @author    Florent Vatin
 *  @copyright 2018 Insitaction 
 */

if (!defined('_PS_VERSION_')) {
    exit;
}  

use Symfony\Component\Translation\TranslatorInterface;

class CustomerFormatter extends CustomerFormatterCore
{
    public function getFormat()
    {
        $this->context = Context::getContext();
        $format = [];

        $format['id_customer'] = (new FormField)
            ->setName('id_customer')
            ->setType('hidden')
        ;

        $genderField = (new FormField)
            ->setName('id_gender')
            ->setType('radio-buttons')
            ->setLabel(
                $this->translator->trans(
                    'Social title', [], 'Shop.Forms.Labels'
                )
            )
            ->setRequired(true)
        ;
        foreach (Gender::getGenders($this->language->id) as $gender) {
            $genderField->addAvailableValue($gender->id, $gender->name);
        }
        $format[$genderField->getName()] = $genderField;

        $format['firstname'] = (new FormField)
            ->setName('firstname')
            ->setLabel(
                $this->translator->trans(
                    'First name', [], 'Shop.Forms.Labels'
                )
            )
            ->setRequired(true)
        ;

        $format['lastname'] = (new FormField)
            ->setName('lastname')
            ->setLabel(
                $this->translator->trans(
                    'Last name', [], 'Shop.Forms.Labels'
                )
            )
            ->setRequired(true)
        ;

        if (Configuration::get('PS_B2B_ENABLE')) {
            $format['company'] = (new FormField)
                ->setName('company')
                ->setType('text')
                ->setLabel($this->translator->trans(
                    'Company', [], 'Shop.Forms.Labels'
                ));
            $format['siret'] = (new FormField)
                ->setName('siret')
                ->setType('text')
                ->setLabel($this->translator->trans(
                    // Please localize this string with the applicable registration number type in your country. For example : "SIRET" in France and "Código fiscal" in Spain.
                    'Identification number', [], 'Shop.Forms.Labels'
                ));
        }

        $format['email'] = (new FormField)
            ->setName('email')
            ->setType('email')
            ->setLabel(
                $this->translator->trans(
                    'Email', [], 'Shop.Forms.Labels'
                )
            )
            ->setRequired(true)
        ;

        // TODO : Revoir système de traduction
        if($this->context->customer->id == null):

        $format['emailConf'] = (new FormField)
        ->setName('emailConf')
        ->setType('emailConf')
        ->setLabel(
            $this->translator->trans(
                'Confirmation E-mail', [], 'Shop.Forms.Labels'
            )
        )
        ->setRequired(true)
        ;

            endif;

        if ($this->ask_for_password) {
            $format['password'] = (new FormField)
                ->setName('password')
                ->setType('password')
                ->addAvailableValue('title', $this->translator->trans(
                        'Le mot de passe doit contenir au moins 5 caractères', [], 'Shop.Forms.Errors'
                    )
                )
                ->addAvailableValue('showPassword', true)
                ->setLabel(
                    $this->translator->trans(
                        'Password', [], 'Shop.Forms.Labels'
                    )
                )
                ->setRequired($this->password_is_required)
            ;
        }

        if ($this->ask_for_password) {
            $format['passwordconf'] = (new FormField)
                ->setName('passwordConf')
                ->setType('password')
                ->addAvailableValue('title', $this->translator->trans(
                        'Le mot de passe doit contenir au moins 5 caractères', [], 'Shop.Forms.Errors'
                    )
                )
                ->addAvailableValue('showPassword', true)
                ->setLabel(
                    $this->translator->trans(
                        'Confirmation Mot de passe', [], 'Shop.Forms.Labels'
                    )
                )
                ->setRequired($this->password_is_required)
            ;
        }


        if ($this->ask_for_new_password) {
            $format['new_password'] = (new FormField)
                ->setName('new_password')
                ->setType('password')
                ->setLabel(
                    $this->translator->trans(
                        'New password', [], 'Shop.Forms.Labels'
                    )
                )
                ->addAvailableValue('showPassword', true)
            ;
        }

        
        $format['phone'] = (new FormField)
            ->setName('phone')
            ->setLabel(
                $this->translator->trans(
                    'Téléphone', [], 'Shop.Forms.Labels'
                )
            )
            ->setRequired(true)
        ;

        if ($this->ask_for_birthdate) {
            $format['birthday'] = (new FormField)
                ->setName('birthday')
                ->setType('text')
                ->setLabel(
                    $this->translator->trans(
                        'Birthdate', [], 'Shop.Forms.Labels'
                    )
                )
                ->addAvailableValue('placeholder', Tools::getDateFormat())
                ->addAvailableValue(
                    'comment',
                    $this->translator->trans('(E.g.: %date_format%)', array('%date_format%' => Tools::formatDateStr('31 May 1970')), 'Shop.Forms.Help')
                )
            ;
        }

        if ($this->ask_for_partner_optin) {
            $format['optin'] = (new FormField)
                ->setName('optin')
                ->setType('checkbox')
                ->setLabel(
                    $this->translator->trans(
                        'Receive offers from our partners', [], 'Shop.Theme.Customeraccount'
                    )
                )
                ->setRequired($this->partner_optin_is_required)
            ;
        }

        // ToDo, replace the hook exec with HookFinder when the associated PR will be merged
        $additionalCustomerFormFields = Hook::exec('additionalCustomerFormFields', array(), null, true);

        if (is_array($additionalCustomerFormFields)) {
            foreach ($additionalCustomerFormFields as $moduleName => $additionnalFormFields) {
                foreach ($additionnalFormFields as $formField) {
                    $formField->moduleName = $moduleName;
                    $format[$moduleName.'_'.$formField->getName()] = $formField;
                }
            }
        }
        // TODO: TVA etc.?
        return $this->addConstraints($format);
    }

}