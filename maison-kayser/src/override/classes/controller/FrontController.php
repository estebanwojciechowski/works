<?php

class FrontController extends FrontControllerCore {

    private $display_column_right;

    /**
     * Load Wordpress Configuration Data
     */
    public function init() {
        global $useSSL, $cookie, $smarty, $cart, $iso, $defaultCountry, $protocol_link, $protocol_content, $link, $css_files, $js_files, $currency, $wpdb, $wp, $aWordpressData;

        parent::init();

        $this->commonInit();

        // ADD BY ESTEBANW INSITACTION 30042018
        // A ce moment la, si on est sur Wordpress $this->context->shop est déjà associé
        // à la boutique préférée (plugins ins_prestapress_wp)
        if($my_shop = $this->context->customer->getMyShop()) {
            $current_url = $this->context->shop->getBaseURL(true, false).$_SERVER['REQUEST_URI'];
            //redirection si une boulangerie préférée a été sélectionnée, et est différente de l'actuelle
            if($my_shop != $this->context->shop->id) {
                $url_store_loc = false;
                if(class_exists('BeVisible')) {
                    $metas = BeVisible::getMetas();
                    $url_store_loc = $this->context->shop->getBaseURL(true).$metas['url_rewrite'];
                    $iso = Language::getIsoById($this->context->language->id);
                    $url_store_loc_lang = $this->context->shop->getBaseURL(true).$iso.'/'.$metas['url_rewrite'];
                }
                // empeche la redirection si on est sur la page du store loc
                // pour autoriser un choix de boulangerie différent
                $my_shop = new Shop($my_shop);
                if($url_store_loc && strpos($current_url, $url_store_loc) === false
                    && strpos($current_url, $url_store_loc_lang) === false) {
                    if(Validate::isLoadedObject($my_shop)) {
                        $this->context->logToOtherShop($this->context->customer, $my_shop);
                        $url = $my_shop->getBaseURL(true, true);
                        $request = ltrim($_SERVER['REQUEST_URI'], '/');
                        if(strlen($this->context->shop->virtual_uri)) {
                            //contexte autre boutique sélectionnée
                            $request =  str_replace($this->context->shop->virtual_uri, '', $request);
                        }
                        $url .= $request;
                        Tools::redirect($url);
                    }
                } elseif(strlen(trim($my_shop->virtual_uri)) > 0) {
                    // redirection vers le store loc de la boulangerie en cours pour
                    // conserver l'affichage du panier
                    $url = $my_shop->getBaseURL(true).ltrim($_SERVER['REQUEST_URI'], '/');
                    Tools::redirect($url);
                }
            }
        }/* elseif(!$this->context->customer->isLogged() && $this->context->shop->id > 1) {
            // si pas loggé et pas dans la boutique internationale on redirige
            $url = Tools::getShopDomainSsl(true).str_replace($this->context->shop->virtual_uri, '', $_SERVER['REQUEST_URI']);
            Tools::redirect($url);
        }*/

        // ADD BY ESTEBANW INSITACTION 19042018
        // ajout d'un cookie pour conserver le panier lors du changement vers wp
        setcookie("wp_shop", $this->context->shop->id, time()+3600, '/');

        // Load Datas
        $id_shop = $this->context->shop->id;
        $iso_code = $this->context->language->iso_code;
        $aFusionData = array(
            'id_blog' => 1
        );
        $sPsConfPath = _WORDPRESS_DIR_NAME_;
        $sWordpressTablePrefix = _WORDPRESS_DB_PREFIX_;

        $bLoadWordpress = Tools::getValue('wpload');
        $bLoadWordpress = (empty($bLoadWordpress) || ($bLoadWordpress != false && strtolower($bLoadWordpress) != 'false')) ? true : false;

        if($this->context->controller->php_self == 'cart' && $this->ajax){
            $bLoadWordpress = false;
        }

        if (!empty($sWordpressTablePrefix) && $bLoadWordpress) {
            /* INIT WORDPRESS */
            $wordpressConfPath = $sPsConfPath . '/';
            $wordpressPath = _PS_ROOT_DIR_ . '/' . $wordpressConfPath;

            // Fix Wordpress URI based multi-site loader
            $server_query = $_SERVER['REQUEST_URI'];
            $_SERVER['REQUEST_URI'] = '/'._WORDPRESS_DIR_NAME_.'/';
            require_once( $wordpressPath . 'wp-load.php' );

            $_SERVER['REQUEST_URI'] = $server_query;

            if ($aFusionData['id_blog'] != false) :
                $blogWordpressId = $aFusionData['id_blog'];
            endif;

            $sMasterWordpressTablePrefix = $sWordpressTablePrefix;
            if ($blogWordpressId != 1)
                $sWordpressTablePrefix .= $blogWordpressId . '_';

            $aWordpressData = array(
                'table_prefix' => $sWordpressTablePrefix,
                'master_table_prefix' => $sMasterWordpressTablePrefix,
                'id_blog' => $blogWordpressId,
                'server_path' => $wordpressConfPath,
                'wp_base_url' => network_home_url(),
            );

            $this->context->wordpress = $aWordpressData;

            $this->context->smarty->registerPlugin('function', 'wp_head', 'wp_head');
            $this->context->smarty->registerPlugin('function', 'wp_footer', 'wp_footer');

            $this->context->smarty->assign(
                array(
                    'langage_selector' => $this->get_language_selector_flags()
                )
            );
        }
    }

    /**
     * Init for Wordpress
     */
    public function initWp() {
        global $useSSL, $cookie, $smarty, $cart, $iso, $wpdb, $wp_query, $wp, $blog_id, $aWordpressData;

        parent::init();

       $this->commonInit();

        //$this->loggedInAccount();

        $this->context->smarty->registerPlugin('function', 'wp_head', 'wp_head');

        $this->context->smarty->registerPlugin('function', 'wp_footer', 'wp_footer');
    }

    public function commonInit() {
         // Ajout du magasin correspondant au shop si il existe
        $this->context->shop->getStore();
        $my_boulangerie_link = false;
        if($this->context->shop->id > 1) {
            $my_boulangerie_link = $this->context->link->getModuleLink(
                        'bevisible',
                        'map-store',
                        [
                            'id_store' => $this->context->shop->id,
                            'wanted_url' => Tools::link_rewrite($this->context->shop->name)
                        ]
                    );

        }

        $this->context->smarty->assign([
            'active_store' => $this->context->shop->store,
            'my_boulangerie_link' => $my_boulangerie_link,
        ]);

    }

    private function loggedInAccount() {
        global $wpdb, $wp, $aWordpressData;

        if ($this->context->customer->isLogged()) :
            $wp_user = get_user_by('email',$this->context->customer->email);
            wp_set_current_user($wp_user->ID, $wp_user->user_login);
            wp_set_auth_cookie($wp_user->ID);
            do_action('wp_login', $wp_user->user_login);
        endif;

    }

    public static function getElementorPage($idPage){
        return do_shortcode('[INSERT_ELEMENTOR id=' . $idPage . ']');

    }

    public function displayWpHeader($display = true) {

        global $bInWordpress, $wp_query;

        parent::initHeader();
        parent::initContent();
        $hook_header = Hook::exec('displayHeader');

        if ((Configuration::get('PS_CSS_THEME_CACHE') || Configuration::get('PS_JS_THEME_CACHE')) && is_writable(_PS_THEME_DIR_ . 'cache')) {
            // CSS compressor management
            if (Configuration::get('PS_CSS_THEME_CACHE'))
                $this->css_files = Media::cccCSS($this->css_files);
            //JS compressor management
            if (Configuration::get('PS_JS_THEME_CACHE'))
                $this->js_files = Media::cccJs($this->js_files);
        }

        $this->context->smarty->assign(array(
            'HOOK_HEADER' => $hook_header,
            'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),
            'langage_selector' => $this->get_language_selector_flags(true)
        ));

        $this->display_header = $display;

        if( !empty($bInWordpress) && !empty($wp_query) )
            $wpClasses = get_body_class();

        $this->context->smarty->assign(
                array(
                        'body_class'    => implode(' ',$wpClasses),
                        'page_name'     => 'wordpress',
                        'css_files'     => $this->css_files,
                        'js_files'      => array_unique($this->js_files),
                        'isWP'          => '1',
                        'stylesheets'   => $this->getStylesheets(),
                        'javascript'    => $this->getJavascript(),
                        'js_custom_vars' => Media::getJsDef(),
                )
        );

        $this->smartyOutputContent(_PS_THEME_DIR_ . 'templates/_partials/header-wp.tpl');
    }

    public function get_language_selector_flags($wordpress = false) {
        // n'affiche pas le selecteur si il n'y a pas 2 langues activées
        // sur prestashop et wordpress
        $languages = Language::getLanguages();
        $wp_langs = icl_get_languages('skip_missing=0&orderby=code');
        $lang_intersect = array();
        $exclude = array();
        $images = array();
        // recupération des images des flags wordpress
        foreach ($wp_langs as $key => $wp_lang) {
            // check si la langue existe
            if(!in_array($wp_lang['code'], $lang_intersect)) {
                $exclude[$wp_lang['code']] = $wp_lang['code'];
                foreach ($languages as $key => $presta_lang) {
                    if($presta_lang['iso_code'] == $wp_lang['code']) {
                        $lang_intersect[] = $presta_lang['iso_code'];
                        // si trouvé on ne l'exclu pas
                        unset($exclude[$wp_lang['code']]);
                        break;
                    }
                }
            }
            $images[$wp_lang['code']] = $wp_lang['country_flag_url'];
        }

        if(count($lang_intersect) < 2)
            return false;

        if($wordpress) {
            return get_language_selector_flags($exclude);
        }

        $html = '';
        if(!empty($languages)) {
            foreach($languages as $l) {
                $url = $this->context->link->getLanguageLink($l['id_lang']);
                $active = $this->context->language->id == $l['id_lang'];
                if(!$active) $html .= '<a href="'.$url.'">';
                $html .= '<img src="'.$images[$l['iso_code']].'" height="12" alt="'.$l['iso_code'].'" width="18" />';
                if(!$active) $html .= '</a>';
            }
        }
        return $html;
    }

    public function displayWpFooter()
    {

        $javascript = $this->getJavascript();

        /**
         * TODO DEV PROBLEME des JS dans le blog
         */
        $this->context->smarty->assign(
            array(
                    'page_name'     => 'wordpress',
                    'css_files'     => $this->css_files,
                    'js_files'      => array_unique($this->js_files),
                    'isWP'          => '1',
                    'stylesheets' => $this->getStylesheets(),
                    'javascript' => $javascript,
                    'js_custom_vars' => Media::getJsDef(),
            )
        );

        $this->smartyOutputContent(_PS_THEME_DIR_ . 'templates/_partials/footer-wp.tpl');
    }

    public function getTemplateVarUrls()
    {
        $urls = parent::getTemplateVarUrls();

        $main_shop_current_url = Tools::getShopDomainSsl(true).str_replace($this->context->shop->virtual_uri, '', $_SERVER['REQUEST_URI']);

        $urls['main_shop_current_url'] = $main_shop_current_url;
        $urls['base_blog_url'] = Tools::getShopDomainSsl(true).'/'._WORDPRESS_DIR_NAME_;


        return $urls;
    }

    // FONCTIONS GARDEES AU BESOIN

    /**
     * Init for Wordpress
     */
    /*public function initWp() {
        global $useSSL, $cookie, $smarty, $cart, $iso, $wpdb, $wp_query, $wp, $blog_id, $aWordpressData;

        parent::init();

        //$this->loggedInAccount();

        $this->context->smarty->registerPlugin('function', 'wp_head', 'wp_head');

        $this->context->smarty->registerPlugin('function', 'wp_footer', 'wp_footer');
    }*/

    /*private function loggedInAccount() {
        global $wpdb, $wp, $aWordpressData;

        if ($this->context->customer->isLogged()) :
            $wp_user = get_user_by('email',$this->context->customer->email);
            wp_set_current_user($wp_user->ID, $wp_user->user_login);
            wp_set_auth_cookie($wp_user->ID);
            do_action('wp_login', $wp_user->user_login);
        endif;

    }

    public function displayWpHeader($display = true) {

        global $bInWordpress, $wp_query;

        parent::initHeader();
        parent::initContent();
        $hook_header = Hook::exec('displayHeader');

        if ((Configuration::get('PS_CSS_THEME_CACHE') || Configuration::get('PS_JS_THEME_CACHE')) && is_writable(_PS_THEME_DIR_ . 'cache')) {
            // CSS compressor management
            if (Configuration::get('PS_CSS_THEME_CACHE'))
                $this->css_files = Media::cccCSS($this->css_files);
            //JS compressor management
            if (Configuration::get('PS_JS_THEME_CACHE'))
                $this->js_files = Media::cccJs($this->js_files);
        }

        $this->context->smarty->assign(array(
            'HOOK_HEADER' => $hook_header,
            'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),
        ));

        $this->display_header = $display;

        if( !empty($bInWordpress) && !empty($wp_query) )
            $wpClasses = get_body_class();

        $this->context->smarty->assign(
                array(
                        'body_class'    => implode(' ',$wpClasses),
                        'page_name'     => 'wordpress',
                        'css_files'     => $this->css_files,
                        'js_files'      => array_unique($this->js_files),
                        'isWP'          => '1',
						'stylesheets' => $this->getStylesheets(),
						'javascript' => $this->getJavascript(),
                        'js_custom_vars' => Media::getJsDef(),
                )
        );

        $this->smartyOutputContent(_PS_THEME_DIR_ . 'templates/_partials/header-wp.tpl');
    }

    public function displayWpFooter()
    {

        $javascript = $this->getJavascript();

        /**
         * TODO DEV PROBLEME des JS dans le blog
         */
        /*$this->context->smarty->assign(
            array(
                    'page_name'     => 'wordpress',
                    'css_files'     => $this->css_files,
                    'js_files'      => array_unique($this->js_files),
                    'isWP'          => '1',
                    'stylesheets' => $this->getStylesheets(),
                    'javascript' => $javascript,
                    'js_custom_vars' => Media::getJsDef(),
            )
        );

        $this->smartyOutputContent(_PS_THEME_DIR_ . 'templates/_partials/footer-wp.tpl');
    }*/

    /*public static function loadScriptElementor(){

		return array(
	        '/blog/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.js',
	        '/blog/wp-content/plugins/elementor/assets/lib/imagesloaded/imagesloaded.js',
	        '/blog/wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.js',
	        '/blog/wp-content/plugins/elementor/assets/lib/swiper/swiper.jquery.js',
	        '/blog/wp-content/plugins/elementor/assets/lib/dialog/dialog.js',
	        '/blog/wp-content/plugins/elementor/assets/js/editor.min.js',
	        '/blog/wp-content/plugins/elementor/assets/js/frontend.min.js',
	    );

	}

    public static function loadStyleElementor($idPost){

		$wp_upload_dir 	= wp_upload_dir( null, false );
		$url_post 		= set_url_scheme( $wp_upload_dir['baseurl'] . sprintf( '%s/%s.css', '/elementor/css', 'post-' . $idPost ) );
		$url_global 	= set_url_scheme( $wp_upload_dir['baseurl'] . sprintf( '%s/%s.css', '/elementor/css', 'global') );

		return array(
			'/blog/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css',
			'/blog/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css',
			'/blog/wp-content/plugins/elementor/assets/css/animations.min.css',
			'/blog/wp-content/plugins/elementor/assets/css/frontend.min.css',
			str_replace(Tools::getShopDomainSsl(true), '', $url_global),
            str_replace(Tools::getShopDomainSsl(true), '', $url_post)
        );

    }*/

    /**
     * Adds jQuery UI component(s) to queued JS file list
     *
     * @param string|array $component
     * @param string $theme
     * @param bool $check_dependencies
     */
    public function addJqueryUI($component, $theme = 'base', $check_dependencies = true)
    {
        $css_theme_path = '/js/jquery/ui/themes/'.$theme.'/minified/jquery.ui.theme.min.css';
        $css_path = '/js/jquery/ui/themes/'.$theme.'/minified/jquery-ui.min.css';
        $js_path = '/js/jquery/ui/jquery-ui.min.js';

        $this->registerStylesheet('jquery-ui-theme', $css_theme_path, ['media' => 'all', 'priority' => 95]);
        $this->registerStylesheet('jquery-ui', $css_path, ['media' => 'all', 'priority' => 40]);
        $this->registerJavascript('jquery-ui', $js_path, ['position' => 'bottom', 'priority' => 40]);
    }

    public function addCookieConsent() {
        $path = __PS_BASE_URI__.'js/cookieconsent/';
        $this->registerStylesheet('cookieconsent-theme', $path.'cookieconsent.min.css', ['media' => 'all', 'priority' => 95]);
        $this->registerJavascript('cookieconsent', $path.'cookieconsent.min.js', ['position' => 'bottom', 'priority' => 40]);
    }

    public function addJqueryCircle() {
        $path = __PS_BASE_URI__.'js/jquery-circle-progress/';
        $this->registerJavascript('circleprogress', $path.'circle-progress.min.js', ['position' => 'bottom', 'priority' => 5]);
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCookieConsent();
        $this->addJqueryCircle();
    }
}