{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="_desktop_cart">
  <div class="blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if}" data-refresh-url="{$refresh_url}">
    <div class="header">
      {if $cart.products_count > 0}
        <span class="quickcart__picto quickcart__picto--open">
      {/if}
        <span class="picto shopping-cart quickcart__picto quickcart__picto--open"></span>
        {if $cart.products_count > 0}<span class="cart-products-count"><span>{$cart.products_count}</span></span>{/if}
      {if $cart.products_count > 0}
        </span>
      {/if}
    </div>
  </div>  
  <div id="quick-cart">
    <div class="quick-cart__title">
      <div class="blockcart__quickcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if}" data-refresh-url="{$refresh_url}">
        <div class="header">
          {if $cart.products_count > 0}
            <span class="quickcart__picto quickcart__picto--open">
          {/if}
            <span class="picto shopping-cart quickcart__picto quickcart__picto--open"></span>
           {if $cart.products_count > 0}<span class="cart-products-count"><span>{$cart.products_count}</span></span>{/if}
          {if $cart.products_count > 0}
            </span>
          {/if}
        </div>
      </div>  
      <div class="quick-cart__title--cart">
        <span>Mon</span>
        panier
      </div>
    </div>
    
	<ul class="media-list cart-overview-quickcart">
	{if $cart.products}
	  {foreach from=$cart.products item=product}
		<li class="media">
		  {block name='cart_detailed_product_line_quickcart'}
			{include file='checkout/_partials/cart-detailed-product-line-quickcart.tpl' product=$product}
		  {/block}
		</li>
		{if $product.customizations|count >1}<hr>{/if}
	  {/foreach}
	{else}
	  <li class="media"><span class="no-items">{l s='There are no more items in your cart' d='Shop.Theme.Checkout'}</span></li>
	{/if}
	</ul>
	
    <div class="quickcart__container__bottom">
      <div class="quickcart__container__bas">
        {block name='cart_summary_subtotals'}
          {foreach from=$cart.subtotals item="subtotal"}
            {if $subtotal && $subtotal.type !== 'tax'}
              <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-{$subtotal.type}">
                <span class="label">{$subtotal.label}</span>
                <span class="value">{$subtotal.value}</span>  
              </div>
            {/if}
          {/foreach}
        {/block}    
        <span class="quickcart__picto quickcart__picto--close"></span>
        <a rel="nofollow" href="{$cart_url}" class="quickcart__button">
          <button class="ins-button-bg"> Voir mon panier </button>
          <!-- <span class="cart-products-count">({$cart.products_count})</span> -->
        </a>
      </div>
    </div>
  </div>
</div>