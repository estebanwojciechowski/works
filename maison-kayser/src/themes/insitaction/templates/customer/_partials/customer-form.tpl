{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='customer_form'}
  {block name='customer_form_errors'}
    {include file='_partials/form-errors.tpl' errors=$errors['']}
  {/block}

<form action="{block name='customer_form_actionurl'}{$action}{/block}" id="customer-form" class="js-customer-form formulaire-inscription" method="post">

  {if !$customer.is_logged}
  <p class="already-account">{l s='Already have an account?' d='Shop.Theme.Customeraccount'}
    <a href="{$urls.pages.authentication}">{l s='Log in instead!' d='Shop.Theme.Customeraccount'}</a>
  </p>
  {/if}

  <section>
    <div class="form-group form-rgpd  ">
      <div class="input-rgpd">
        <span class="checkbox-rgpd checkbox">
          <input name="rgpd" id="rgpd" type="checkbox" value="1" required>
            <label for="rgpd" class="box-rgpd">{l s='Les informations recueillies via ce formulaire permettront la création de votre compte client et le traitement de vos commandes. Tous les champs doivent être remplis, sinon votre compte client ne pourra pas être créé et vos commandes traitées. Les données que vous aurez mentionnées dans ces champs ainsi que celles que vous aurez précisées dans vos commandes constituent vos données personnelles sur lesquelles vous bénéficiez de droits d\'interrogation, d\'accès, de limitation, de rectification, d\'opposition au traitement et d\'effacement. Pour plus d\'informations, notre politique de protection des données personnelles est accessible en cliquant sur le lien suivant :' d='Shop.Theme.Customeraccount' }<br>
            <a href="{url entity='cms' id=7 id_lang=1}" target="_blank">{l s="Politique de données personnelles" d='Shop.Theme.Global'}</a>
        </span>
      </div>
      <div class=" form-control-comment">
      </div>
    </div>
    {block "form_fields"}
      {foreach from=$formFields item="field"}
        {block "form_field"}
          {if $field.name == 'newsletter'}
            <br/><p>{l s='Si vous souhaitez également recevoir notre newsletter, vous pouvez cocher la case ci-après.' d='Shop.Theme.Customeraccount'}</p>
          {/if}
          {form_field field=$field}
        {/block}
      {/foreach}
      {$hook_create_account_form nofilter}
    {/block}
    <p>
      {l s='Vous pouvez vous désinscrire à tout moment, vous trouverez pour cela nos informations de contact dans les conditions d\'utilisation du site.' d='Shop.Theme.Customeraccount'}
    </p>
  </section>

  {block name='customer_form_footer'}
    <footer class="form-footer clearfix">
      <input type="hidden" name="submitCreate" value="1">
      {block "form_buttons"}
        <button class="btn ins-button-bg btn-primary form-control-submit float-xs-center" data-link-action="save-customer" type="submit">
          {l s='Valider' d='Shop.Theme.Actions'}
        </button>
      {/block}
    </footer>
  {/block}

</form>
{/block}
