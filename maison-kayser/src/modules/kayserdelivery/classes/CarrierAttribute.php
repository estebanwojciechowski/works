<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

if (!defined('_PS_VERSION_')) {
    exit;
}  

class CarrierAttribute extends ObjectModel
{
	public $id_carrier_attribute;
    
    public $name;

    public $field;
 
    public $object; 

    public $datepicker; 

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'carrier_attribute',
        'primary' => 'id_carrier_attribute', 
        'fields' => array( 
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true, 'size' => 256),
            'field' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true, 'size' => 256),
            'object' => array('type' => self::TYPE_STRING ),
            'datepicker' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
        )
    ); 

    public static function getIDByField($field) {
		return DB::getInstance()->getValue(
            'SELECT * FROM `'._DB_PREFIX_.'carrier_attribute` g
            WHERE `field` = "'.pSQL($field).'"'
        ); 
    }

    public static function getAttributes() {  
        return DB::getInstance()->executeS(
            'SELECT * FROM `'._DB_PREFIX_.'carrier_attribute` g'  
        ); 
    }
}
