{if isset($part)}	
	{if $part == 'top'}    
      {assign var=first_tab value=true}
      {foreach from=$attributes_features key=formule_id_feature item=current_feature}
        {if in_array($formule_id_feature, $display_features)}	
		    {foreach from=$current_feature key=$feature_id_product item=feature}
            <li class="nav-item">
                <a
                  class="nav-link{if $first_tab} active{/if}"
                  data-toggle="tab"
                  href="#formule_tab_{$formule_id_feature}"
                  role="tab"
                  aria-controls="description"
                  {if $first_tab} aria-selected="true"{/if}>{$feature.name}</a>
                  {assign var=first_tab value=false}                
            </li>
            {break}
      		{/foreach}
        {/if}
       {/foreach}
	{elseif $part == 'content'}
		<div class="tab-content" id="tab-content">        
          {assign var=first_tab value=true}
          {foreach from=$attributes_features key=formule_id_feature item=current_feature}
	        {if in_array($formule_id_feature, $display_features)}	
              {if $formule_id_feature == 1}
                {assign var=classnav value='composition'}

              {elseif $formule_id_feature == 2}
                {assign var=classnav value='allergenes'}

              {elseif $formule_id_feature == 3}
                {assign var=classnav value='valeurnutritionnelles'}

              {elseif $formule_id_feature == 7}
                {assign var=classnav value='description'}

              {/if}        
              <div class="tab-pane in{if $first_tab} active{/if}" id="formule_tab_{$formule_id_feature}" role="tabpanel">
                <div class="product-{$classnav} product-info">
                  <div class="product-{$classnav}__content product-info_content">
          			{assign var=first_product value=true}
		          	{foreach from=$current_feature key=$feature_id_product item=feature}
		          		{if $first_product}
			          		<div class="product-{$classnav}__title product-info_title">
		                      {$feature.name}
		                    </div>
		                    <br/>
		                {/if}
	                    <div class="{$classnav}">
	                    	<span class="h4">{$feature.product_name} : </span><br/>
	                      	{$feature.value}
	                    </div>
	                    <br/>
          				{assign var=first_product value=false}
		          	{/foreach}       
		            {assign var=first_tab value=false} 
                  </div>
                </div>
              </div>           
		    {/if}
          {/foreach}   
        </div>  
	
	{/if}
	
{/if}