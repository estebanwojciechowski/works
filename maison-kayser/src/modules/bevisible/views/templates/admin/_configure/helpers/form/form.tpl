{**
 * 2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 *
 *} 

{extends file="helpers/form/form.tpl"}
{block name="input"}
    {if $input.type == 'select'}
        {if isset($input.select2) && $input.select2}
            {capture name="parent_select"}
                {$smarty.block.parent}
            {/capture}
            {$smarty.capture.parent_select|escape:'html':'UTF-8'|bevisibleCorrectTheMess|bevisibleRemoveClass:'fixed-width-xl'}
            <script type="text/javascript">
                $(document).ready(function(){
                    $('select#{$input.name|escape:'html':'UTF-8'}').select2({
                        escapeMarkup: function (markup) { return markup; },
                        templateResult: function(item) {
                            if (item.loading) { return item.text; }
                            return BV.select2.markMatch(item.text, $('.select2-search__field').val());
                        }
                    });
                });
            </script>
        {else}
            {$smarty.block.parent}
        {/if}
    {elseif $input.type == 'textarea'}
        {if isset($input.editors) && $input.editors}
            {capture name="parent_textarea"}
                {$smarty.block.parent}
            {/capture}
            {$smarty.capture.parent_textarea|escape:'html':'UTF-8'|bevisibleCorrectTheMess|bevisibleOnLangChange:'BV.helperForm.languageChanged()'}
            {if !(isset($input.lang) && $input.lang)}<br />{/if}
            <div class="btn-group" data-toggle="buttons"{if isset($input.hide_selector) && $input.hide_selector} style="display:none;"{/if}>
                <label class="btn btn-default active" onclick="switch_editor_{$input.name|escape:'html':'UTF-8'}('none')">
                    <input type="radio" name="{$input.name|escape:'html':'UTF-8'}_editor" id="{$input.name|escape:'html':'UTF-8'}_editor_none" value="none" autocomplete="off"> {l s='Plain Text (No Editor)' mod='fsstoresmap'}
                </label>
                {if isset($input.editors.tinymce.basic) && $input.editors.tinymce.basic}
                    <label class="btn btn-default" onclick="switch_editor_{$input.name|escape:'html':'UTF-8'}('tinymce')">
                        <input type="radio" name="{$input.name|escape:'html':'UTF-8'}_editor" id="{$input.name|escape:'html':'UTF-8'}_editor_tinymce" value="tinymce" autocomplete="off"> {l s='Basic WYSIWYG editor' mod='fsstoresmap'}
                    </label>
                {/if}
                {if isset($input.editors.tinymce.advanced) && $input.editors.tinymce.advanced}
                    <label class="btn btn-default" onclick="switch_editor_{$input.name|escape:'html':'UTF-8'}('tinymceadvanced')">
                        <input type="radio" name="{$input.name|escape:'html':'UTF-8'}_editor" id="{$input.name|escape:'html':'UTF-8'}_editor_tinymceadvanced" value="tinymceadvanced" autocomplete="off"> {l s='Advanced WYSIWYG editor' mod='fsstoresmap'}
                    </label>
                {/if}
                {if isset($input.editors.codemirror) && $input.editors.codemirror}
                    <label class="btn btn-default" onclick="switch_editor_{$input.name|escape:'html':'UTF-8'}('codemirror')">
                        <input type="radio" name="{$input.name|escape:'html':'UTF-8'}_editor" id="{$input.name|escape:'html':'UTF-8'}_editor_codemirror" value="codemirror" autocomplete="off"> {l s='Code Editor' mod='fsstoresmap'}
                    </label>
                {/if}
            </div>
            {if isset($input.desc) && !(isset($input.hide_selector) && $input.hide_selector)}
            <br /><br />
            {/if}
            <script type="text/javascript">
                function switch_editor_{$input.name|escape:'html':'UTF-8'}(editor) {
                    var selector = '{$input.selector|escape:'html':'UTF-8'}';
                    BV.helperForm.trigger('switch_editor', {
                        selector: selector,
                        editor: editor,
                        {if isset($input.editors.tinymce)}
                        tinymce: {$input.editors.tinymce|bevisibleJsonEncode|escape:'html':'UTF-8'|bevisibleCorrectTheMess},
                        {/if}
                        {if isset($input.editors.codemirror)}
                        codemirror: {$input.editors.codemirror|bevisibleJsonEncode|escape:'html':'UTF-8'|bevisibleCorrectTheMess},
                        {/if}
                    });
                }

                {assign var='editor_field' value=implode('_', array($input['name'], 'editor'))}
                {if isset($input.editors) && $input.editors}
                $(document).ready(function(){
                    $('#{$input.name|escape:'html':'UTF-8'}_editor_{if isset($fields_value[$editor_field]) && $fields_value[$editor_field]}{$fields_value[$editor_field]|escape:'html':'UTF-8'}{else}none{/if}').parent().click();
                });
                {/if}
            </script>
        {else}
            {$smarty.block.parent}
        {/if}
    {else}
        {$smarty.block.parent}
    {/if}
{/block}

{block name="label"}
    {$smarty.block.parent}
{/block}

{block name="legend"}
    {$smarty.block.parent}
    {if isset($field.show_multishop_header) && $field.show_multishop_header}
        <div class="well clearfix">
            <label class="control-label col-lg-3">
                <i class="icon-sitemap"></i> {l s='Multistore' mod='fsstoresmap'}
            </label>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="help-block">
                            <strong>{l s='You are editing this page for a specific shop or group.' mod='fsstoresmap'}</strong><br />
                            {l s='If you check a field, change its value, and save, the multistore behavior will not apply to this shop (or group), for this particular parameter.' mod='fsstoresmap'}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    {/if}
{/block}
