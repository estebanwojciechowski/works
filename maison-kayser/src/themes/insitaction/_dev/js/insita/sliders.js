import $ from 'jquery';
import Swiper from 'swiper/dist/js/swiper'; // plugins 

export default class Sliders {
    
    constructor(el) {}

    init(){

        let defaultConfig = {
            // Optional parameters
            loop: false,
            slideClass: 'swiper-slide',
            slidesPerView: 3,
            breakpoints: {
                1140: {
                    slidesPerView: 2,
                    grabCursor: true,
                    draggable: true,
                }, 
                960: { 
                    slidesPerView: 1, 
                    grabCursor: true,
                    draggable: true,
                },
                768: {
                    slidesPerView: 2,
                    grabCursor: true,
                    draggable: true,
                },
                500: { 
                    slidesPerView: 1, 
                    grabCursor: true,
                    draggable: true,
                },
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        }
        let jobConfig = {
            // Optional parameters
            loop: false,
            slideClass: 'swiper-slide',
            slidesPerView: 4,
            breakpoints: {
                1140: {
                    slidesPerView: 3,
                    grabCursor: true,
                    draggable: true,
                }, 
                960: { 
                    slidesPerView: 3, 
                    grabCursor: true,
                    draggable: true,
                },
                768: {
                    slidesPerView: 2,
                    grabCursor: true,
                    draggable: true,
                },
                500: { 
                    slidesPerView: 1, 
                    grabCursor: true,
                    draggable: true,
                },
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        }
        let temoignageConfig = {
            // Optional parameters
            loop: false,
            slideClass: 'swiper-slide',
            slidesPerView: 3,
            breakpoints: {
                1140: {
                    slidesPerView: 3,
                    grabCursor: true,
                    draggable: true,
                }, 
                960: { 
                    slidesPerView: 2, 
                    grabCursor: true,
                    draggable: true,
                },
                768: {
                    slidesPerView: 2,
                    grabCursor: true,
                    draggable: true,
                },
                500: { 
                    slidesPerView: 1, 
                    grabCursor: true,
                    draggable: true,
                },
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        }
        let productConfig = {
            // Optional parameters
            loop: false,
            slideClass: 'swiper-slide',
            slidesPerView: 4,
            // centeredSlides: true,
            breakpoints: {
                1140: {
                    slidesPerView: 3,
                    grabCursor: true,
                    draggable: true,
                }, 
                960: { 
                    slidesPerView: 2, 
                    grabCursor: true,
                    draggable: true,
                },
                768: {
                    slidesPerView: 2,
                    grabCursor: true,
                    draggable: true,
                },
                500: { 
                    slidesPerView: 2, 
                    grabCursor: true,
                    draggable: true,
                },
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        }
        
        if($('.swiper--reassurance-hp').length)
            new Swiper ($('.swiper--reassurance-hp'), defaultConfig);
            
        if($('.swiper--featured-products').length)
            new Swiper ($('.swiper--featured-products'), productConfig);

        if($('.our-jobs').length)
            new Swiper ($('.swiper--our-jobs'), jobConfig);

        if($('.temoignages').length)
            new Swiper ($('.swiper--temoignages'), temoignageConfig);
    }
}