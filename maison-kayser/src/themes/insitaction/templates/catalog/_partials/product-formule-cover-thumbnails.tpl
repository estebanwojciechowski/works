{if isset($attributes_images) && count($attributes_images)}
<div id="formules_images" class="row">
  	{foreach from=$attributes_images item=id_image}
    <div class="col-sm-12 col-md-6">
      <img id="formule_image_{$id_image}" class="formule-cover js-qv-product-cover" src="{$link->getImageLink($product.link_rewrite, $id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{$product.name}" title="{$product.cover.legend}" style="width:100%;" itemprop="image">
      <div class="layer formule-layer hidden-sm-down" data-toggle="modal" data-target="#product-modal" data-id-image="{$id_image}">
        <i class="material-icons zoom-in">&#xE8FF;</i>
      </div>
    </div>
	{/foreach}
</div>
{/if}