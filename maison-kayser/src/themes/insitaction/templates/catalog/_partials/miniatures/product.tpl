{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='product_miniature_item'}

  {*
      ********* RAJOUTER LA CLASSE not--available sur la div thumbnail-container si le produit est indisponible *********
  *}

  <article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
    <a href="{$product.url}">

    <div class="thumbnail-container">
      {block name='product_thumbnail'}
        <div class="thumbnail product-thumbnail">
          {if $product.cover.bySize.home_default.url}
			  <img
				  src = "{$product.cover.bySize.home_default.url}"
				  alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
				  data-full-size-image-url = "{$product.cover.large.url}"
				/>
		  {else}
			  {* patch pour forcer l'image par défaut si pas d'image, certains modules ne renseigne pas $product.cover*}
			  <img
				  src = "{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"
				  alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
				  data-full-size-image-url = "{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"
				/>

		  {/if}
        </div>
      {/block}

      <div class="product-description">
        {block name='product_name'}
          <h3 class="product-title" itemprop="name" data-mh="product-name"><span>{$product.name|truncate:30:'...'}</span>
          {if isset($product.features) && count($product.features)}
            {foreach from=$product.features item="feature"}
              {if $feature.id_feature == 5}
                <div class="product--piece">{$feature.value} {if $feature.value > 1}{l s='personnes' d='Shop.Theme.Global'}{else}{l s='personne' d='Shop.Theme.Global'}{/if}</div>
              {/if}
            {/foreach}
          {/if}
          </h3>
        {/block}

        {block name='product_price_and_shipping'}
          {if $product.show_price}
            <div class="product-price-and-shipping">
              {if $product.has_discount}
                {hook h='displayProductPriceBlock' product=$product type="old_price"}

                <span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
                <span class="regular-price">{$product.regular_price}</span>
                {if $product.discount_type === 'percentage'}
                  <span class="discount-percentage">{$product.discount_percentage}</span>
                {/if}
              {/if}

              {hook h='displayProductPriceBlock' product=$product type="before_price"}

              <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
              <span itemprop="price" class="price">{$product.price}</span>

              {hook h='displayProductPriceBlock' product=$product type='unit_price'}

              {hook h='displayProductPriceBlock' product=$product type='weight'}
            </div>
          {/if}
        {/block}

        <div class="delivery__pictos">
          <span class="retrait__boulangerie{if is_null($product.delai_clickandcollect)} disabled{/if}"></span>
          <span class="livraison__rdv{if !($product.delai_livraison_velo > 0 || $product.delai_livraison_voiture > 0)} disabled{/if}"></span>
          <span class="livraison__coli{if  is_null($product.delai_colissimo) || $product.delai_colissimo == '0'} disabled{/if}"></span>
        </div>

        {block name='product_reviews'}
          {hook h='displayProductListReviews' product=$product}
        {/block}
      </div>

      {block name='product_flags'}
        {if isset($product.flags) && count($product.flags)}
          <ul class="product-flags">
            {foreach from=$product.flags item=flag}
              <li class="product-flag {$flag.type}"><span>{$flag.label}</span></li>
            {/foreach}
          </ul>
        {elseif isset($product.features) && count($product.features)}
          <ul class="product-flags product-page-flags">
            {foreach from=$product.features item=feature}
              {if $feature.id_feature == 14}
              <li class="product-flag-kayser new"><span>{$feature.value}</span></li>
              {/if}
            {/foreach}
          </ul>
        {/if}
      {/block}

      <div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
        {block name='quick_view'}
          <div class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> {l s='Quick view' d='Shop.Theme.Actions'}
          </div>
        {/block}

        {block name='product_variants'}
          {if $product.main_variants}
            {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
          {/if}
        {/block}
      </div>

    </div>
    </a>
    <style type="text/css">
      .retrait_boulangerie_info,
      .livraison_rdv_info,
      .livraison_coli_info {

        display: none;
        position: absolute;
        width:100%;
        visibility: hidden;
      }
      .retrait_boulangerie_info.is--open,
      .livraison_rdv_info.is--open,
      .livraison_coli_info.is--open {

        display: block;
        position: absolute;
        width: 100%;
        visibility: visible;
      }
    </style>
    <div class="desc__livraison is--open">
          <span class="retrait_boulangerie_info">{l s ="Retrait dans votre boulangerie"}
          {if $active_store}
            <span class="bakery">{$active_store->name}</span>
            <span><a href="{$store_locator_url}?cnc=1&back={if isset($urls) && isset($urls.main_shop_current_url)}{urlencode($urls.main_shop_current_url)}{/if}" class="change__bakery">{l s ="Changer de boulangerie"}</a></span>
          {else}
            <span><a href="{$store_locator_url}?cnc=1&back={if isset($urls) && isset($urls.main_shop_current_url)}{urlencode($urls.main_shop_current_url)}{/if}" class="change__bakery">{l s ="Choisir une boulangerie"}</a></span>
          {/if}
          </span>
          <span class="livraison_rdv_info">{l s ="Livraison sur rendez-vous"}</span>
          <span class="livraison_coli_info">{l s ="Livraison à domicile en France"}</span>
      </div>
  </article>
{/block}