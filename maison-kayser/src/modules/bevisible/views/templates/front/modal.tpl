 
<div id="choose-store-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title h6 text-sm-center" id="myModalLabel">
          {count($products)} {if count($products) > 1}{l s='produits indisponibles'}{else}{l s='produit indisponible'}{/if}  
        </h4>
      </div>
      <div class="modal-body">
        <div class="row"> 
            <div class="col-sm-12" style="text-align: center">
              <p class="h4">
                {l s='Votre panier va être modifié si vous confirmez votre choix.'}
                {if $store && strlen($store->name)}
                  <br/>
                  <span class="h3">{$store->name}</span>
                {/if}
              </p>
               <form action="{$action_url}" method="post" id="confirm-change">
                  <input type="hidden" name="token" value="{$static_token}"/>
                  <input type="hidden" name="id_store" value="{$store->id}" id="confirm-change_id_store"/>
                  <input type="hidden" name="method" value="confirm-change-submit"/>
                  <input type="hidden" name="back" value="{$back}"/>
                  <button
                        class="btn btn-primary choose-store"
                        name="confirm-change-submit"
                        type="submit" 
                      >
                        {l s="Choisir cette boulangerie" d='Shop.Theme.Actions'} 
                    </button>
                </form>
                <hr/>
              <p>
                {if count($products) > 1}
                  {l s='Ces produits vont être supprimés de votre panier.'}
                {else}
                  {l s='Ce produit va être supprimé de votre panier.'}
                {/if}                 
              </p>  

              <section id="products">
                <div class="products">  
                 {foreach from=$products item="product"}
                    <article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
                      <a href="{$link->getProductLink($product.id_product)}">
                        
                      <div class="thumbnail-container">
                        {block name='product_thumbnail'}
                          <div class="thumbnail product-thumbnail">
                          <img
                            src = "{$product.cover}"
                            alt = "{$product.name|truncate:30:'...'}"
                            class = "img-responsive"
                          />
                          </div>
                        {/block}

                        <div class="product-description">
                          {block name='product_name'}
                            <h2 class="h3 product-title" itemprop="name" data-mh="product-name"><span>{$product.name}</span></h2>
                          {/block}
                        </div>
                      </div>
                    </article>
                  {/foreach}
                </div>
              </section>              
            </div> 
        </div>
      </div>
    </div>
  </div>
</div>
