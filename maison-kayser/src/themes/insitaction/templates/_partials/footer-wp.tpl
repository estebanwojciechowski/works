
{* </div> <!-- END .container --> *}
</div> <!-- END #wrapper -->
    {block name='cookies'}
      {include file='_partials/cookies.tpl'}
    {/block}
 <footer id="footer">

    {block name="footer"}
      {include file="_partials/footer.tpl"}
    {/block}
  </footer>
   	{block name='javascript_head'}
	  {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
	{/block}