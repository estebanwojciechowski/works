import $ from 'jquery';

export default class Search {
    
    constructor(el) {}

    init(){
        $('.search__picto').on('click' , (e) => {
            if( $(e.currentTarget).hasClass('search__picto--open') ){
                $('#search_widget').addClass('is--open');
                setTimeout(function(){
                    $('#input__search').focus();
                },500);
            } else if( $(e.currentTarget).hasClass('search__picto--close') ){
                $('#search_widget').removeClass('is--open');
            }
        });

        $('.blog-search').on('click' , (e) => {
            $('.search-blog__container').toggleClass('is--open');
            setTimeout(function(){
                $('.search-field').focus();
            },500);
        });

        $('.search__picto--close').on('click', function() {
            $('.search-blog__container').removeClass('is--open');   
        });

        $('.listOffres__li').hover(function(e){
            e.preventDefault();
            var mqh = window.matchMedia( "(min-width: 900px)" );    
            if (mqh.matches) {
                $('.search_categories').removeClass('is--open');
                $(this).find('.search_categories').toggleClass('is--open');
            }
        }, function(e){
            e.preventDefault();
            var mqh = window.matchMedia( "(min-width: 900px)" );
            if (mqh.matches) {
                $('.search_categories').removeClass('is--open');    
            }
        });

        $('.listeOffre_filtres').on('click', function(e){
            e.preventDefault();
            var mq = window.matchMedia( "(max-width: 900px)" );
            if (mq.matches) {
                $('.search_categories').removeClass('is--open--mobile');
                $(this).parent().find('.search_categories').toggleClass('is--open--mobile');
            }
        });
    }
}