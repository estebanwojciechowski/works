<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

use PrestaShop\PrestaShop\Adapter\Cart\CartPresenter;

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Ps_ShoppingcartOverride extends Ps_Shoppingcart
{

    public function getWidgetVariables($hookName, array $params)
    {
        $cart_url = $this->getCartSummaryURL();

        return array(
            'cart' => (new CartPresenter)->present(isset($params['cart']) ? $params['cart'] : $this->context->cart),
            'link' => $this->context->link,
            'refresh_url' => $this->context->link->getModuleLink('ps_shoppingcart', 'ajax', array(), null, null, null, true),
            'cart_url' => $cart_url
        );
    }
	
	private function getCartSummaryURL()
    {
        return $this->context->link->getPageLink(
            'cart',
            null,
            $this->context->language->id,
            array(
                'action' => 'show'
            ),
            false,
            null,
            true
        );
    }

    public function renderModalStore($back = false)
    {  
        $meta = Meta::getMetaByPage('module-bevisible-map', $this->context->language->id);

        $title = '';
        if ($meta && isset($meta['title']) && $meta['title']) {
            $title = $meta['title'];
        }
        $store_url = Tools::getHttpHost(true).'/'.$meta['url_rewrite'];  
        $this->smarty->assign(array( 
            'store_url' => $store_url,
            'back' => $back,
        ));

        return $this->fetch('module:ps_shoppingcart/modal_store.tpl');
    }

}
