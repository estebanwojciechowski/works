<?php
 
  
include_once(dirname(__FILE__).'/../../config/config.inc.php');
include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_Clients.php');
include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_Commandes.php');

$webservice = new Insitaction_Webservice_Clients(); 
$customersIDS = $webservice->cronGetIds('customer.csv');
//print_r($customersIDS);
$start = time();
$webservice->log('Debut traitement cron customer : '.count($customersIDS));
if($customersIDS && count($customersIDS)) {
	$liste_traites = array();
	$liste_non_traites = array(); 
	foreach ($customersIDS as $customersID) {	 
		$customersID = (int) $customersID;
		//on evite de traiter 2 fois le même id au cas où	
		if(in_array($customersID, $liste_traites)) {
			$webservice->log($customersID.' déja traité');
			continue;
		}
		$webservice->log('Cron customer : '.$customersID);
		$method = 'degrade';
		$customer = new Customer($customersID);
		if($customer && $customer instanceof Customer && !is_null($customer->id)) {
			//verification si le client existe deja
			$reponse = $webservice->identification('email', array('email_client' => $customer->email));
			$parsed_customer = false;
			if($reponse) {  
				if(isset($reponse['WSVC_CLIENTS']) && isset($reponse['WSVC_CLIENTS']['code_client']) && $reponse['WSVC_CLIENTS']['code_client']) {
					$method = 'update';
					$parsed_customer = Insitaction_Webservice_Clients::parseCustomer($customer);
					$parsed_customer['Code_client'] = $reponse['WSVC_CLIENTS']['code_client'];
				}
			}
			$reponse = $webservice->crud($method, ($parsed_customer ? $parsed_customer : $customer)); 
			if($reponse && isset($reponse['code_retour']) && $reponse['code_retour'] == 1) {
				$liste_traites[] = $customersID;
				$webservice->log('REUSSI');
			} else {
				$liste_non_traites[] = $customersID;
				$webservice->log('ECHEC : '.json_encode($reponse));
			}
		} else {
			$liste_traites[] = $customersID;
			$webservice->log('N\'existe pas dans la base client ID#'.$customersID);
		}
	} 
	$webservice->cronRefresh('customer.csv', $liste_non_traites, $liste_traites);
}
$webservice->log('Fin traitement cron customer');


$webservice = new Insitaction_Webservice_Commandes(); 
$ordersIDS = $webservice->cronGetIds('order.csv');
//print_r($ordersIDS);
$webservice->log('Debut traitement cron order : '.count($ordersIDS));
if($ordersIDS && count($ordersIDS)) {
	$liste_traites = array();
	$liste_non_traites = array(); 
	foreach ($ordersIDS as $ordersID) {
		$ordersID = (int) $ordersID;
		if(in_array($ordersID, $liste_traites))
			continue;
		$webservice->log('Cron order : '.$ordersID);
		$order = new Order($ordersID);
		if($order && $order instanceof Order && !is_null($order->id)) {		
			$reponse = $webservice->ajoutCommande($order, 'async'); 
			if($reponse && isset($reponse['WSVC_STATE']) && $reponse['WSVC_STATE'] == 1
						&& isset($reponse['WSVC_COMMANDES']) && $reponse['WSVC_COMMANDES'] == 1) {
				$liste_traites[] = $ordersID;
				$webservice->log('REUSSI');
			} else {
				$liste_non_traites[] = $ordersID;
				$webservice->log('ECHEC : '.json_encode($reponse));
			}
		} else {
			$webservice->log('N\'existe pas dans la base Order ID#'.$ordersID);
		}
	} 
	$webservice->cronRefresh('order.csv', $liste_non_traites, $liste_traites);
}
$webservice->log('Fin traitement cron order');
$webservice->log('Temps traitement cron '.(time() - $start).'s');