{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}


{block name='page_content'}
    {block name='login_form_container'}
      <div class="header-connexion parallax-window" data-parallax="scroll" data-speed="0.7" data-image-src="{$urls.base_url}{$smarty.const._THEME_DIR_}img/pain-connexion.jpg">&nbsp;</div>
      <div class="formulaires container">
        <div class="row">

          {* Bloc je me connecte *}
          <div class="col-xs-12 col-sm-6 col-md-6 col-xl-5 offset-lg-1">
            <section id="content" class="connexion page-content card card-block">
              <section class="login-form">
                <div class="title-form">
                  <h2>{l s='Je me connecte' d='Shop.Theme.Global'}</h2>
                </div>
                {render file='customer/_partials/login-form.tpl' ui=$login_form}
              </section>
              {block name='display_after_login_form'}
                {hook h='displayCustomerLoginFormAfter'}
              {/block}
            </section>
          </div>

          {* Bloc je m'inscris *}
          <div class="col-xs-12 col-sm-6 col-md-6 col-xl-5">
            <section id="content1" class="inscription connexion page-content card card-block">
              <section class="login-form login-form-inscription">
                <div class="title-form">
                  <h2>{l s="Je m'inscris" d='Shop.Theme.Global'}</h2>
                </div>
                {render file='customer/_partials/login-form-subscribe.tpl' ui=$login_form}
              </section>
              {block name='display_after_login_form'}
                {hook h='displayCustomerLoginFormAfter'}
              {/block}
            </section>
          </div>

        </div>
      </div>
    {/block}
{/block}
