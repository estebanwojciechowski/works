{**
 * 2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 *
 *} 
 
<strong>{$bevisible_store.name}</strong><br />
{$bevisible_store.address1}
{if strlen(trim($bevisible_store.arrondissement)) > 0}
	<br />{$bevisible_store.address2}
{/if}
{*if strlen(trim($bevisible_store.arrondissement))}
    {$bevisible_store.arrondissement} {l s='arrondissement'}
{/if*} 
{if strlen(trim($bevisible_store.quartier))}
    <br />{$bevisible_store.quartier}
{/if}  
<br />{$bevisible_store.postcode} {$bevisible_store.city}{if $bevisible_store.state_iso_code}, {$bevisible_store.state_iso_code}{/if}<br /><br />
<a href="tel:{$bevisible_store.phone}" class="shop__tel">{$bevisible_store.phone}</a>
<br />
{foreach $bevisible_store.hours as $key => $day}
    <br /><strong>{$bevisible_days_translations.$key}</strong> {if !$day}{l s='Fermé'}{else}{$day}{/if}
{/foreach}
<br />
{*<a href="{$bevisible_get_direction_url nofilter}" data-href="{$bevisible_get_direction_url nofilter}" onclick="bevisible.extendDirectionLink(this);" target="_blank">
    {l s='Get directions' mod='fsstoresmap'}
</a>*}
{assign var=is_current_store value=false}
{if isset($smarty.get.id_store)}
	{assign var=is_current_store value=$smarty.get.id_store==$bevisible_store.id_store}
{/if}
{if !$is_current_store && isset($link)}
<br />
<a href="{$link->getModuleLink(
            'bevisible', 
            'map-store', 
            [
                'id_store' => $bevisible_store.id_store,
                'wanted_url' => Tools::link_rewrite($bevisible_store.name)
            ]
        )}{if $back}?back={$back}{/if}" class="button">{l s="Voir la fiche"}</a> 
{/if}