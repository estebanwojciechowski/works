<?php

use PrestaShop\PrestaShop\Adapter\Cart\CartPresenter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;

class CartController extends CartControllerCore
{

    public $show_store_modal = 0;

    /**
     * Override pour ajouter une requête ajax custom de mise à jour des message et couverts
     * @throws PrestaShopException
     */
    public function postProcess(){

        // Si requête ajax de mise à jour des message et des couverts
        if (Tools::getIsset('updateMessageCouverts'))
            $this->displayAjaxUpdateMessageCouverts();
        else // Sinon comportement normal
            $this->updateCart();

    }

    /**
     * Override pour regarder si des produits possèdent des couverts
     * @see FrontController::initContent()
     */
    public function initContent()
    {

        if (Configuration::isCatalogMode() && Tools::getValue('action') === 'show') {
            Tools::redirect('index.php');
        }

        $presenter = new CartPresenter();
        $presented_cart = $presenter->present($this->context->cart, $shouldSeparateGifts = true);
        
        if (count($presented_cart['products']) > 0){
            $couverts = $this->checkCouverts($presented_cart['products']);
            $this->context->smarty->assign([
                'couverts' => ($couverts == true) ? 1 : 0,
            ]);
        }

        if ($old_message = Message::getMessageByCartId((int)$this->context->cart->id)){
            $message = new Message((int)$old_message['id_message']);
            $this->context->smarty->assign([
                'msg_destinataire' => $message->message,
            ]);
        }

        $accessoires = $this->crossCart($presented_cart);
        
        $SYSTEMPAY_STD_AMOUNTS = @unserialize(Configuration::get('SYSTEMPAY_STD_AMOUNTS'));
        if(!empty($SYSTEMPAY_STD_AMOUNTS)
            && isset($SYSTEMPAY_STD_AMOUNTS[0]['max_amount'])) {
            $max_amount = $SYSTEMPAY_STD_AMOUNTS[0]['max_amount'];
        } else {
            $max_amount = Configuration::get('MAX_AMOUNT');
        }   
        $this->context->smarty->assign(array(
            'accessories' => $accessoires,
            'amountmax' => $max_amount, //1499,
        ));

        parent::initContent();
    }

    /**
     * Vérification de l'existance des couverts en tant qu'accessoire sur les produits
     * @param $productsInCart
     * @return bool
     */
    public function checkCouverts($productsInCart){

        $isCouvertNecessary = false;

        foreach ($productsInCart as $product){
            $accessories = Product::getAccessoriesLight($this->context->language->id,$product['id_product']);
            if(count($accessories) > 0){
                foreach ($accessories as $accessory){
                    if($accessory['reference'] == '123456789'){
                        $isCouvertNecessary = true;
                        break 2;
                    }
                }
            }
        }

        return $isCouvertNecessary;

    }

    /**
     * Ajout du message au destinataire dans le panier
     * @throws PrestaShopException
     */
    public function displayAjaxUpdateMessageCouverts(){

        $couverts           = Tools::getValue('couverts');
        $idProductCouvert   = Product::getProductByRef('123456789');

        if(!is_null($idProductCouvert)){

            // Si demande de couvert coché
            if($couverts == '1'){

                if($this->context->cart->containsProduct($idProductCouvert))        // S'il contient déjà des couverts
                    $this->context->cart->deleteProduct($idProductCouvert);         // On supprime le produit couvert ( car le client a peut être ajouté plusieurs couverts )

                $this->context->cart->updateQty(1, $idProductCouvert);      // On ajoute un seul couvert

            } else { // Si demande de couvert non coché

                if($this->context->cart->containsProduct($idProductCouvert))        // S'il contient des couverts dans le panier
                    $this->context->cart->deleteProduct($idProductCouvert);         // On supprime les couverts

            }

        }

        $id_message         = false;

        // Si message déjà enregistré on récupère son ID
        if ($old_message = Message::getMessageByCartId((int)$this->context->cart->id))
            $id_message = $old_message['id_message'];

        // On charge le vieux message ou un nouveau
        $message = new Message((int)$id_message);

        if ($message_content = Tools::getValue('comment')) {    // Si message en POST
            if (Validate::isMessage($message_content)) {            // Et si le message est valide
                $message->message       = $message_content;         // On met à jour les données du message
                $message->id_cart       = (int)$this->context->cart->id;
                $message->id_customer   = (int)$this->context->cart->id_customer;
                $message->save();
            }
        } elseif (Validate::isLoadedObject($message)) {             // Si pas de message on supprime l'ancien message
            $message->delete();
        }

        // Envoi de la réponse de succès
        $this->ajaxDie(Tools::jsonEncode([
            'success' => true,
        ]));


    }
 
    public function displayAjaxUpdate()
    {

        if (Configuration::isCatalogMode()) {
            return;
        }

        if($this->show_store_modal) {

            $this->ajaxDie(Tools::jsonEncode([
                'hasError' => true,
                'errors' => $this->errors, 
                'show_store_modal' => $this->show_store_modal
            ]));

        }

        $productsInCart = $this->context->cart->getProducts();
        $updatedProducts = array_filter($productsInCart, array($this, 'productInCartMatchesCriteria'));
        list(, $updatedProduct) = each($updatedProducts);
        $productQuantity = $updatedProduct['quantity'];

        if (!$this->errors) {
            $cartPresenter = new CartPresenter();
            $this->ajaxDie(Tools::jsonEncode([
                'success' => true,
                'id_product' => $this->id_product,
                'id_product_attribute' => $this->id_product_attribute,
                'quantity' => $productQuantity,
                'cart' => $cartPresenter->present($this->context->cart),
            ]));
        } else {
            $this->ajaxDie(Tools::jsonEncode([
                'hasError' => true,
                'errors' => $this->errors,
                'quantity' => $productQuantity,
            ]));
        }

        //parent::displayAjaxUpdate();
    }

    /**
     * This process add or update a product in the cart
     */
    protected function processChangeProductInCart()
    {
        // avant tout on vérifie le contexte de la boutique, est elle choisie ?
        $store = $this->context->shop->getStore(); 
        // récupération des infos produits, magasin physique requis ou non ?
        $id_product = (int) Tools::getValue('id_product'); 
        $colissimo = Product::isColissimoReady($id_product); 
        // si l'une des 2 condition est valide no pb
        if($colissimo || $store) {
            // traitement normal
            parent::processChangeProductInCart();            
        } else {
            //affichage popup choix du magasin 
            $this->show_store_modal = 1;
            $this->errors[] = $this->trans('Le produit nécessite le choix d\'une boutique.', array(), 'Shop.Notifications.Error');
        }        
    }

    private function crossCart($presented_cart){
        
        if(!$presented_cart)
            return array();

        $products = $presented_cart['products'];
        if(!$products)
            return array();

        $tab = array();

        foreach($products as $product){
            $oProduct = new Product($product['id_product'], 'false', $this->context->language->id, $this->context->shop->id);
            $accessoires = $oProduct->getAccessories($this->context->language->id);

            if($accessoires){
                foreach($accessoires as $accessoire){
                    $tab[$accessoire['id_product']] = $accessoire;
                }
            }
        }

        if (is_array($tab)) {

            $ProductPresenterFactory = new ProductPresenterFactory($this->context, new TaxConfiguration());
            $presentationSettings = $ProductPresenterFactory->getPresentationSettings();

            $presenter = new ProductListingPresenter(
                new ImageRetriever(
                    $this->context->link
                ),
                $this->context->link,
                new PriceFormatter(),
                new ProductColorsRetriever(),
                $this->getTranslator()
            );

            foreach ($tab as &$accessory) {
                $accessory = $presenter->present(
                    $presentationSettings,
                    Product::getProductProperties($this->context->language->id, $accessory, $this->context),
                    $this->context->language
                );
            }
            unset($accessory);
        }else{
            return array();
        }

        return $tab;

    }
 
}
