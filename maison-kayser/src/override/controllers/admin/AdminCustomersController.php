<?php

class AdminCustomersController extends AdminCustomersControllerCore
{
    public function renderForm()
    {
        /** @var Customer $obj */
        if (!($obj = $this->loadObject(true))) {
            return;
        }
        parent::renderForm();

        $civilite = array_shift($this->fields_form[0]['form']['input']);

        array_unshift(
            $this->fields_form[0]['form']['input'], 
            array(
                'type' => 'text',
                'label' => $this->trans('Telephone', array(), 'Admin.Orderscustomers.Feature'),
                'name' => 'phone',
                
            )
        );
        array_unshift(
            $this->fields_form[0]['form']['input'], 
            $civilite
        );


        $this->fields_value['phone'] = Tools::getValue('phone') ? Tools::getValue('phone') : $obj->phone;
        $this->tpl_form_vars['phone'] = '';

        $helper = new HelperForm($this);
        $this->setHelperDisplay($helper);
        $helper->fields_value = $this->fields_value;
        $helper->submit_action = $this->submit_action;
        $helper->tpl_vars = $this->getTemplateFormVars();
        $helper->show_cancel_button = (isset($this->show_form_cancel_button)) ? $this->show_form_cancel_button : ($this->display == 'add' || $this->display == 'edit');

        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) {
            $back = self::$currentIndex.'&token='.$this->token;
        }
        if (!Validate::isCleanHtml($back)) {
            die(Tools::displayError());
        }

        $helper->back_url = $back;
        !is_null($this->base_tpl_form) ? $helper->base_tpl = $this->base_tpl_form : '';
        if ($this->access('view')) {
            if (Tools::getValue('back')) {
                $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue('back'));
            } else {
                $helper->tpl_vars['back'] = Tools::safeOutput(Tools::getValue(self::$currentIndex.'&token='.$this->token));
            }
        }
        $form = $helper->generateForm($this->fields_form);

        return $form;
    }

}
