<?php
/**
* @author    Esteban Wojciechowski <estebanw@insitaction.com>
 * @copyright 2017 INSITACTION
 */


class ClickNCollectSessionCore
{
    private $context; 

    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    public function customerHasLoggedIn()
    {
        return $this->context->customer->isLogged();
    }

    public function getCustomer()
    {
        return $this->context->customer;
    }

    public function getCart()
    {
        return $this->context->cart;
    }

    public function resetCart()
    {
        $this->context->cart = $this->context->cart->resetCart($this->getCustomer());
        return $this;
    }
    
    public function getIDStore()
    {   
        return $this->context->cart->getIDStore();   
    }

    public function setIDStore($id_store)
    {
        return $this->context->cart->setIDStore($id_store);   
    }

     public function getMailSend()
    {   
        return $this->context->cart->getMailSend();   
    }

    public function setMailSend($value)
    {
        return $this->context->cart->setMailSend($value);   
    }

    public function setMessage($message)
    {
        $this->_updateMessage(Tools::safeOutput($message));

        return $this;
    }

    public function isGuestAllowed()
    {
        return Configuration::get('PS_GUEST_CHECKOUT_ENABLED');
    }

    public function getClickNCollectURL()
    {
        $url = $this->context->link->getPageLink('clickncollect');
        if($content_only = Tools::getValue('content_only'))
            $url .= '?content_only='.$content_only;
        return $url;
    }
}
