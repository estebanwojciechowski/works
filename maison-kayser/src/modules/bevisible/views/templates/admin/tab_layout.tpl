{**
 * 2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 *
 *} 
<div class="row">
    <div id="bevisible_tabs" class="col-lg-2 col-md-3">
        <div class="list-group">
            {foreach from=$bevisible_tab_layout item=bevisible_tab}
                <a class="list-group-item{if $bevisible_active_tab == $bevisible_tab.id} active{/if}"
                   href="#{$bevisible_tab.id|escape:'htmlall':'UTF-8'}"
                   aria-controls="{$bevisible_tab.id|escape:'htmlall':'UTF-8'}" role="tab" data-toggle="tab">
                    {$bevisible_tab.title|escape:'htmlall':'UTF-8'}
                </a>
            {/foreach}
        </div>
    </div>
    <div class="col-lg-10 col-md-9">
        <div class="tab-content">
            {foreach from=$bevisible_tab_layout item=bevisible_tab}
                <div role="tabpanel" class="tab-pane{if $bevisible_active_tab == $bevisible_tab.id} active{/if}" id="{$bevisible_tab.id|escape:'htmlall':'UTF-8'}">
                    {$bevisible_tab.content|escape:'html':'UTF-8'|bevisibleCorrectTheMess|bevisibleResolveEscape}
                </div>
            {/foreach}
        </div>
    </div>
    <div class="clearfix"></div>
</div>