class Cap {
	showError(response) {
    	if(typeof response.messages != 'undefined' && response.messages != '') {
			jQuery(this.currentFieldId + '_error').text(response.messages);
			jQuery(this.currentFieldId + '_error').fadeIn();
		} else {
			jQuery(this.currentFieldId + '_error').text('');
			jQuery(this.currentFieldId + '_error').fadeOut();
		}
    }

    hideErrors(domObject) {
    	if(domObject) {
    		var currentId = domObject.attr('id');
    		jQuery('.cap_errors').each(function(index, el) {
    			if(currentId != jQuery(this).attr('id'))
    				jQuery(this).hide();
    		});
    	} else {
    		jQuery('.cap_errors').hide();
    	}
    }

    showPropositions(content) {
    	if(jQuery(this.currentFieldId + '_propositions').length > 0) {
    		jQuery(this.currentFieldId + '_propositions').html(content);
    		if(content != '')
    			jQuery(this.currentFieldId + '_propositions').fadeIn();
    		else
    			jQuery(this.currentFieldId + '_propositions').fadeOut();
    	}
    }

    hidePropositions() {
    	if(jQuery('.cap_propositions').length > 0) {
    		jQuery('.cap_propositions').hide();
    	}
    }


    // DOM & Style

    addFields(domObject) {
    	domObject.attr('autocomplete', 'off');
    	this.addErrorField(domObject);
    	this.addPropositionsField(domObject);
    }

    addErrorField(domObject) {

    	var parentObject = domObject.parent();
    	var errorDivId = domObject.attr('id') + '_error';

    	if(!parentObject.find('.cap_errors').length) {

	    	var div = jQuery('<div/>');
	    	div.attr({
	    		id: errorDivId,
	    		class: 'cap_errors'
	    	});
	    	div.css({
	    		opacity: '0.8',
	    		backgroundColor: '#fa0000',
	    		color: '#fff',
	    		width: domObject.outerWidth() - 10,
	    		display: 'none',
	    		padding: '3px 5px'
	    	});

	    	parentObject.prepend(div);
	    }
    }

    addPropositionsField(domObject) {

    	var parentObject = domObject.parent();
    	var propositionsDivId = domObject.attr('id') + '_propositions';

    	if(!parentObject.find('.cap_propositions').length) {

	    	var div = jQuery('<div/>');
	    	div.attr({
	    		id: propositionsDivId,
	    		class: 'cap_propositions'
	    	});
	    	div.css({
	    		backgroundColor: '#fff',
	    		border: '1px solid #eee',
	    		width: domObject.outerWidth() - 2,
	    		display: 'none',
	    	});

	    	parentObject.append(div);
	    }
    }
}

class CapAdresse extends Cap{

	constructor(configs) {
		super();
		// configs par defaut
		this.fiedPrefix = 'address';
		this.fiedIndexPrefix = '_item';
		// correspondance des champs formulaires
		this.addressesFields = {
			postalCode: 		'postcode',
			locality: 			'city',
			streetName: 		'street.0',
			streetNumber: 		'street.1',
 			buildingName: 		'street.2',
			localitySynonym: 	'street.3',
			country: 			'country_id',
		};

		// par défaut
        this.country = false;
        this.countryISO3 = false;
        this.isoLang = false;

		// configs
		if(configs) {
			this.setConfigParams(configs);
		}

		this.field = false;
		this.fieldIndex = false;
		this.currentIndex = false;
		this.currentFieldId = false;

		this.reset = false;
		this.inputChangedByCode = false;

		// on stockera toutes les infos des adresses ici
		this.addresses = {};
    }

    setConfigParams(configs) {

    	if(typeof configs.fiedPrefix != 'undefined')
			this.fiedPrefix = configs.fiedPrefix;

    	if(typeof configs.fiedIndexPrefix != 'undefined')
			this.fiedIndexPrefix = configs.fiedIndexPrefix;

    	if(typeof configs.addressesFields != 'undefined') {
    		for (var key in configs.addressesFields) {
    			this.addressesFields[key] = configs.addressesFields[key];
    		}
    	}

    	if(typeof configs.country != 'undefined')
			this.country = configs.country;

    	if(typeof configs.countryISO3 != 'undefined')
			this.countryISO3 = configs.countryISO3;

    	if(typeof configs.isoLang != 'undefined')
			this.isoLang = configs.isoLang;
    }

	// Core functions

    callApi(url, params, callback) {
    	var object = this;
    	jQuery.ajax({
			type: "POST",
			url: url,
			dataType: "json",
			data: params,
			success: function(response) {
				// traitements communs
				object.showError(response);

				if(callback !== false) {
					callback(object, response);
				}
			}
		});
    }

    doRequest(domObject) {
    	// set l'objet dom courant
		this.setFieldInfos(domObject);
    	this.setCurrentField(domObject.attr('id'));
    	// si il n'y a pas de champs pour accueiller un message d'erreur
    	// et des propositions on les ajoute
    	this.addFields(domObject);

    	if(!this.isCountrySet() && this.field != this.addressesFields.country) {
    		var response = {
    			messages: 'Veuillez commencer par choisir un pays'
    		};
	    	this.showError(response);
    	} else {
    		switch (this.field) {
    			case this.addressesFields.postalCode:
    				this.reset = true;
    				return this.postalCodeRequest(domObject);
    				break;
    			case this.addressesFields.locality:
    				this.reset = true;
    				return this.localityRequest(domObject);
    				break;
    			case this.addressesFields.streetName:
    				this.reset = true;
    				return this.streetNameRequest(domObject);
    				break;
    			case this.addressesFields.streetNumber:
    				return this.streetNumberRequest(domObject);
    				break;
    			case this.addressesFields.country:
    				this.reset = true;
    				return this.countryRequest(domObject);
    				break;

    			return true;
    		}
    	}
    	return false;
    }

    // Fonctions de gestion des configs

    isCountrySet() {
    	return this.country !== false && this.countryISO3 !== false;
    }

    setCountry(country_code, ISO3) {
    	// non utilisé mais si evol ic récupérer l'iso3 car non fourni par défaut
    	this.country = country_code;
    	this.countryISO3 = ISO3;
    }

    setCurrentField(name) {
    	this.currentFieldId = '#' + name;
    }

    setFieldInfos(domObject) {
    	var inputName = domObject.attr('name');

		var regex = /address\[_item([0-9]+)\]\[(\w+)\]\[?([0-9]?)\]?/;
		var matches = regex.exec(inputName);

		if(matches) {
			this.currentIndex = matches[1];
			this.field = matches[2];
			this.fieldIndex = false;
			if(matches[3] != '') {
				this.field += '.' + matches[3];
				this.fieldIndex = matches[3];
			}
		}
    }

    resetFieldsInfos() {
    	this.currentIndex = false;
    	this.field = false;
    	this.fieldIndex = false;
		this.currentIndex = false;
		this.currentFieldId = false;
		this.inputChangedByCode = false;
    }

    // getter et setter des paramètres validés par capadresse

	setAddressValidatedParams(params) {

		if(typeof this.addresses[this.currentIndex] == 'undefined')
			this.addresses[this.currentIndex] = {};

		for (var key in params) {
			var field = params[key];
			this.addresses[this.currentIndex][key] = field;
		}
	}

	getAddressValidatedParams(params) {
		var args = {};
		if(typeof this.addresses[this.currentIndex] != 'undefined') {
			for (var i = params.length - 1; i >= 0; i--) {
				if(typeof this.addresses[this.currentIndex][params[i]] != 'undefined')
					args[params[i]] = this.addresses[this.currentIndex][params[i]];
			}
		}
		return args;
	}
	/**
	 * reinit les infos validés
	 * et conserv  uniquement les paramètres
	 * requis aux étapes précédent celle en parametres
	 * @param  {string} step étape
	 */
	resetAddressValidatedParams(step) {

		if(typeof this.addresses[this.currentIndex] == 'undefined')
			return;

		var args = {};

		switch (step) {
			case 'streetname':
				args = {
					postcode: this.addresses[this.currentIndex]['postcode'],
					locality: this.addresses[this.currentIndex]['locality'],
					insee: this.addresses[this.currentIndex]['insee'],
					type: this.addresses[this.currentIndex]['type']
				};
				break;
			case 'streetnumber':
				args = {
					mat: this.addresses[this.currentIndex]['mat'],
					streetname: this.addresses[this.currentIndex]['streetname'],
					streetnumber: this.addresses[this.currentIndex]['streetnumber'],
					buildingName: this.addresses[this.currentIndex]['buildingName'],
					postcode: this.addresses[this.currentIndex]['postcode'],
					locality: this.addresses[this.currentIndex]['locality'],
					insee: this.addresses[this.currentIndex]['insee'],
					type: this.addresses[this.currentIndex]['type']
				};
				break;
		}
		this.addresses[this.currentIndex] = args;
	}

    // gestion verif code postal

    postalCodeRequest(domObject) {
    	if(!this.inputChangedByCode) {
	    	if(this.reset) {
	    		this.resetFields(['locality', 'streetName', 'streetNumber', 'buildingName', 'localitySynonym']);
	    		this.resetAddressValidatedParams('postcode');
	    		this.hideErrors(domObject);
	    	}

	    	var url =  '/capadresse/index/checkCp';

			var params = {
				cp:  domObject.val()
			};

	    	this.callApi(url, params, this.postalCodeSuccess);
	    }
    }

    postalCodeSuccess(capObject, response) {
    	var options = '';
    	if (jQuery.isArray(response.propositions) && response.propositions.length != 0) {
			response.propositions.each(function(proposition) {
				if (proposition.iType == 0 || proposition.iType == 2) {
					options += '<span data-method="postalCode" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + '" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLoc + '</span>';
				} else if (proposition.iType == 1) {
					options += '<span data-method="postalCode" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + ' (' + proposition.sLieuDit + ')" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLieuDit + '</span>';
				} else if (proposition.iType == 3) {
					if (proposition.sLieuDit != '') {
						options += '<span data-method="postalCode" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + ' (' + proposition.sLieuDit + ')" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLoc + ' (' + proposition.sLieuDit + ')</span>';
					} else {
						options += '<span data-method="postalCode" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + '" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLoc + '</span>';
					}
				}
			});

			// affichage des propositions
			capObject.showPropositions(options);

		} else if (response.propositions.length == 0) {
			capObject.showPropositions('');
		}
    }

    // gestion verif locality

    localityRequest(domObject) {
    	if(!this.inputChangedByCode) {
	    	if(this.reset) {
	    		this.resetFields(['postalCode', 'streetName', 'streetNumber', 'buildingName', 'localitySynonym']);
	    		this.resetAddressValidatedParams('locality');
	    	}

	    	var url =  '/capadresse/index/checkCp';

			var params = {
				cp:  domObject.val()
			};

	    	this.callApi(url, params, this.localitySuccess);
	    }
    }

     localitySuccess(capObject, response) {
    	var options = '';
    	if (jQuery.isArray(response.propositions) && response.propositions.length != 0) {
			response.propositions.each(function(proposition) {
				if (proposition.iType == 0 || proposition.iType == 2) {
					options += '<span data-method="locality" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + '" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLoc + '</span>';
				} else if (proposition.iType == 1) {
					options += '<span data-method="locality" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + ' (' + proposition.sLieuDit + ')" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLieuDit + '</span>';
				} else if (proposition.iType == 3) {
					if (proposition.sLieuDit != '') {
						options += '<span data-method="locality" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + ' (' + proposition.sLieuDit + ')" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLoc + ' (' + proposition.sLieuDit + ')</span>';
					} else {
						options += '<span data-method="locality" data-cp="' + proposition.sCp +'" data-ville="' + proposition.sLoc + '" data-insee="' + proposition.sInsee + '" data-type="' + proposition.iType +'">' + proposition.sCp + ' ' + proposition.sLoc + '</span>';
					}
				}
			});

			// affichage des propositions
			capObject.showPropositions(options);

		} else if (response.propositions.length == 0) {
			capObject.showPropositions('');
		}
    }

    // gestion verif voie

    streetNameRequest(domObject) {
    	if(!this.inputChangedByCode) {
	    	if(this.reset) {
	    		this.resetFields(['streetNumber', 'buildingName', 'localitySynonym']);
	    		this.resetAddressValidatedParams('streetname');
	    	}

	    	var url =  '/capadresse/index/checkVoie';

	    	var args = this.getAddressValidatedParams(['insee', 'type']);

	    	// si un paramètre de validation est manquant
	    	// on demande la saisie
	    	if(
	    		typeof args['insee'] == 'undefined'
	    		|| typeof args['type'] == 'undefined'
	    	) {
	    		var response = {
	    			messages: 'Veuillez commencer par saisir le code postal ou la ville'
	    		};
	    		this.showError(response);
	    	} else {
		    	//!\ l'ordre est important
				var params = {
					voie: domObject.val(),
					insee: args['insee'],
					type: args['type']
				};
	    		this.callApi(url, params, this.streetNameSuccess);
			}
		}
    }

    streetNameSuccess(capObject, response) {
    	var options = '';

		if (jQuery.isArray(response.propositions) && response.propositions.length != 0) {
			response.propositions.each(function(prod) {
				options += '<span data-method="streetName" data-mat="' + prod.sMat + '" data-insee="' + prod.sInsee + '" data-cp="' + prod.sCp + '" data-type="'+prod.streetFlags+'"';
				if (prod.sLieuDit != '') {
					options += ' data-ville="' + prod.sLieuDit + '"';
				} else {
					options += ' data-ville="' + prod.sLoc + '"';
				}
				if (prod.sL4 == '' && prod.sOldL4 != '') {
					options += ' data-street="' + prod.sOldL4 + '" data-street2=""';
				} else {
					options += ' data-street="' + prod.sL4 + '" data-street2=""';
				}
				if (prod.sNum != '') {
					options += ' data-num="' + prod.sNum + '"';
				} else {
					options += ' data-num=""';
				}
				options += '>';
				if (prod.sNum != '') {
					options += prod.sNum + ' ';
				}
				if (prod.sL4 == '' && prod.sOldL4 != '') {
					options += prod.sOldL4;
				} else {
					options += prod.sL4;
				}

				var inputPostcode = capObject.getFieldInput('postalCode');
				if(inputPostcode) {
					inputPostcode = jQuery('input[name$="' + inputPostcode + '"]');
					if(inputPostcode.length > 0) {
						if (prod.sCp != inputPostcode.val()) {
							options += ' (' + prod.sCp + ')';
						}
					}
				}
				if (prod.sLieuDit != '') {
					options += ' (' + prod.sLieuDit + ')';
				}
				options += '</span>';
			});
			// affichage des propositions
			capObject.showPropositions(options);

		} else if (response.propositions.length == 0) {
			capObject.showPropositions('');
		}

    }

    streetNumberRequest(domObject) {
    	if(!this.inputChangedByCode) {

	    	var url =  '/capadresse/index/checkNum';

	    	var args = this.getAddressValidatedParams(['insee', 'type', 'mat']);

	    	// si un paramètre de validation est manquant
	    	// on demande la saisie
	    	if(
	    		typeof args['insee'] == 'undefined'
	    		|| typeof args['type'] == 'undefined'
	    	) {

	    		var response = {
	    			messages: 'Veuillez commencer par saisir le code postal'
	    		};
	    		this.showError(response);

	    	} else if(typeof args['mat'] == 'undefined') {

	    		var response = {
	    			messages: 'Veuillez commencer par saisir votre rue'
	    		};
	    		this.showError(response);

	    	} else {

		    	//!\ l'ordre est important
				var params = {
					champ: domObject.val(),
					sMat: args['mat'],
					sInsee: args['insee'],
					type: args['type']
				};
	    		this.callApi(url, params, this.streetNumberSuccess);
			}
		}
    }

    streetNumberSuccess(capObject, response) {
    	var options = '';
		// Si on a des propositions
		if (jQuery.isArray(response.propositions) && response.propositions.length != 0 && response.propositions.length != 1) {
			response.propositions.each(function(prod) {
				options += '<span data-method="streetNumber" data-cp="' + prod.sCp + '" data-hcnum="' + prod.sHcNum + '">' + prod.sNum +'</span>';
			});
			// affichage des propositions
			capObject.showPropositions(options);

		} else if (response.propositions.length == 0) {
			capObject.showPropositions('');
		}

    }

    countryRequest(domObject) {
    	if(!this.inputChangedByCode) {
	    	if(this.reset) {
	    		this.resetFields(['postalCode', 'locality', 'streetName', 'streetNumber', 'buildingName', 'localitySynonym']);
	    		this.resetAddressValidatedParams('postcode');
	    		this.hidePropositions();
	    		this.hideErrors();

	    	}

	    	if(domObject.val() != '') {

		    	var url =  '/capadresse/index/getCountryInfos';

				var params = {
					iso:  domObject.val()
				};

		    	this.callApi(url, params, this.countrySuccess);
		    } else {
		    	this.setCountry(false, false);
		    }
	    }
    }

	countrySuccess(capObject, response) {
		capObject.setCountry(response['iso'], response['iso3']);
	}
    // Fonctions de gestion du clic sur la proposition valide

    selectProposition(proposition, input, value) {
    	var method = proposition.data('method');

		if(typeof method != 'undefined') {
			// methode définie
			var callback = 'selectProposition' + method.charAt(0).toUpperCase() + method.slice(1);
			var callbackFunction = this[callback];
			// on teste si la fonction existe dans la classe
			if(typeof this[callback] == 'function') {
				// ajout des paramètres
				var args = [proposition, input, value];
				// appel de la fonction
				this[callback].apply(this, args);
			}
		}
		// recharge les infos de visualisations des adresses magento
		this.refreshMagentoAddress();
		this.resetFieldsInfos();
		this.hideErrors(false);
    }

    selectPropositionPostalCode(proposition, input, value) {
		var cp 		= proposition.data('cp');
		var ville 	= proposition.data('ville');
		var insee 	= proposition.data('insee');
		var type 	= proposition.data('type');
		// prevent reset
		this.inputChangedByCode = true;
		// MAJ des champs input
		input.val(cp);
		this.setFieldInputValue('locality', ville);
		// Assigne les variables de validation
		var params = {
			postcode: cp,
			locality: ville,
			insee: insee + '' + cp,
			type: type
		};
		this.setAddressValidatedParams(params);

		// Met le focus sur le champ suivant
		var inputStreet = this.getFieldInput('streetName');
		if(inputStreet) {
			inputStreet = jQuery('input[name$="' + inputStreet + '"]');
			if(inputStreet.length > 0) {
				inputStreet.focus();
			}
		}

    }

     selectPropositionLocality(proposition, input, value) {
		var cp 		= proposition.data('cp');
		var ville 	= proposition.data('ville');
		var insee 	= proposition.data('insee');
		var type 	= proposition.data('type');
		// prevent reset
		this.inputChangedByCode = true;
		// MAJ des champs input
		input.val(ville);
		this.setFieldInputValue('postalCode', cp);
		// Assigne les variables de validation
		var params = {
			postcode: cp,
			locality: ville,
			insee: insee + '' + cp,
			type: type
		};
		this.setAddressValidatedParams(params);

		// Met le focus sur le champ suivant
		var inputStreet = this.getFieldInput('streetName');
		if(inputStreet) {
			inputStreet = jQuery('input[name$="' + inputStreet + '"]');
			if(inputStreet.length > 0) {
				inputStreet.focus();
			}
		}

    }

    selectPropositionStreetName(proposition, input, value) {
		var cp = proposition.attr('data-cp');
		var ville = proposition.attr('data-ville');
		var mat = proposition.attr('data-mat');
		var street = proposition.attr('data-street');
		var street2 = proposition.attr('data-street2');
		var num = proposition.attr('data-num');
		var insee = proposition.attr('data-insee');
		var type = proposition.attr('data-type');
		// prevent reset
		this.inputChangedByCode = true;
		// Mise à jour des input avec les valeurs validées
		input.val(street);
		this.setFieldInputValue('buildingName', street2);
		this.setFieldInputValue('locality', ville);
		this.setFieldInputValue('postalCode', cp);
		if(num)
			this.setFieldInputValue('streetNumber', num);

		// Assigne les variables de validation
		var params = {
			mat: mat,
			streetname : street,
			streetnumber : num,
			buildingName: street2,
			postcode: cp,
			locality: ville,
			insee: insee,
			type: type
		};

		this.setAddressValidatedParams(params);

		if(num) {
			// Met le focus sur le champ street2
			var inputStreet2 = this.getFieldInput('buildingName');
			if(inputStreet2) {
				inputStreet2 = jQuery('input[name$="' + inputStreet2 + '"]');
				if(inputStreet2.length > 0) {
					inputStreet2.focus();
				}
			}
		} else {
			// Met le focus sur le champ street2
			var inputStreetNumber = this.getFieldInput('streetNumber');
			if(inputStreetNumber) {
				inputStreetNumber = jQuery('input[name$="' + inputStreetNumber + '"]');
				if(inputStreetNumber.length > 0) {
					inputStreetNumber.focus();
				}
			}
		}
    }

    selectPropositionStreetNumber(proposition, input, value) {
		var hcnum 	= proposition.data('hcnum');
		// prevent reset
		this.inputChangedByCode = true;
		// MAJ des champs input
		input.val(proposition.text());
		// Assigne les variables de validation
		var params = {
			hcnum: hcnum,
		};
		this.setAddressValidatedParams({hcnum: hcnum});

    }

    // getter et setter des champs front

    getFieldInput(inputName) {

    	if(typeof this.addressesFields[inputName] != 'undefined') {
    		var currentField = this.addressesFields[inputName];
    		if(currentField.indexOf('.') > 0) {

    			var tmp = currentField.split('.');
				currentField = '';
				for (var i = 0; i < tmp.length; i++) {
				  currentField = currentField + '[' + tmp[i] + ']';
				}

    		} else {
				currentField = '[' + currentField + ']';
			}

			if(this.fiedIndexPrefix)
				currentField = '[' + this.fiedIndexPrefix + this.currentIndex + ']' + currentField;
			else
				currentField = '[' + this.currentIndex + ']' + currentField;

			if(this.fiedPrefix)
				currentField = this.fiedPrefix + currentField ;

			return currentField;
    	}

    	return false;
    }

    setFieldInputValue(inputName, value) {
		var input = this.getFieldInput(inputName);
		if(input) {
			input = jQuery('input[name$="' + input + '"]');
			if(input.length > 0) {
				input.val(value);
			}
		}
    }

    // Fonctions communes

    resetFields(fieldlist) {
    	var currentField;
    	var tmp;
    	for (var index in fieldlist) {
    		if(typeof this.addressesFields[fieldlist[index]] != 'undefined') {
	    		currentField = this.addressesFields[fieldlist[index]];
	    		// test si il y a un point alors il y a un index dans l'input
	    		if(currentField.indexOf('.') > 0) {

	    			var tmp = currentField.split('.');
	    			currentField = '';
	    			for (var i = 0; i < tmp.length; i++) {
					  currentField = currentField + '[' + tmp[i] + ']';
					}

	    		} else {
	    			currentField = '[' + currentField + ']';
	    		}


	    		if(this.fiedIndexPrefix)
	    			currentField = '[' + this.fiedIndexPrefix + this.currentIndex + ']' + currentField;
	    		else
	    			currentField = '[' + this.currentIndex + ']' + currentField;

	    		if(this.fiedPrefix)
	    			currentField = this.fiedPrefix + currentField ;

	    		jQuery('input[name$="'+currentField+'"], select[name$="'+currentField+'"]').val('');

	    	}
    	}
    	// evite des appels multiples
    	this.reset = false;
    }

    // Fonction spécifique a magento
    refreshMagentoAddress() {

		if(typeof customerAddresses != 'undefined') {
			customerAddresses.onItemFormFieldChange();
		}
    }

}


/***
* EXEMPLE D'APPEL POUR
* LA MISE EN PLACE DE CAPADRESSE

jQuery(document).ready(function($) {
	// input name : address[_item([0-9])][*]
	var config = {
		fiedPrefix: 'address',
		fiedIndexPrefix: '_item',
		addressesFields: {
			postalCode: 		'postcode',
			locality: 			'city',
			streetName: 		'street.0',
			streetNumber: 		'street.1',
			buildingName: 		'street.2',
			localitySynonym: 	'street.3',
			country: 			'country_id',
		},
		// par defaut dans le formulaire
		// pays est sélectionné à FR
		country: 'FR',
        countryISO3: 'FRA',
        isoLang:'fr'
	}

	var capAdresse = new CapAdresse(config);

	// evenement lors de la saisie
	$(document).on('keyup', '#address_form_container .fieldset input.input-text', function(e) {

		capAdresse.doRequest($(this));

	});

	// evenement lors du changement de pays
	$(document).on('change', '#address_form_container .fieldset select[name$="address[_item1][country_id]"]', function(e) {

		capAdresse.doRequest($(this));

	});

	// evenement lors du choix de proposition
	$(document).on('click', '.cap_propositions span', function() {
		var value = $(this).text();
		var container = $(this).closest('.cap_propositions');
		var input = container.parent().find('input');
		capAdresse.selectProposition($(this), input, value);
		container.hide();
	});

});


***/