<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a trade license awarded by
 * Garamo Online L.T.D.
 *
 * Any use, reproduction, modification or distribution
 * of this source file without the written consent of
 * Garamo Online L.T.D It Is prohibited.
 *
 *  @author    Reaction Code <info@reactioncode.com>
 *  @copyright 2015-2017 Garamo Online L.T.D
 *  @license   Commercial license
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Rc_PgTagManagerOverride extends Rc_PgTagManager
{

    /**
     * ADD BY ESTEBANW INSITACTION 23052018
     * EMPECHE L'UTILISATION D'ANALYTICS SI LES RGPD NE SONT PAS ACCEPTEES
     */
    
    public function hookHeader()
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookHeader();
        }
    }
    
    public function hookProductFooter($params)
    {
        if(self::getAgreementsToRGPD()) {
            parent::hookProductFooter($params);
        }
    }

    public function hookOrderConfirmation($params)
    {
        if(self::getAgreementsToRGPD()) {
            parent::hookOrderConfirmation($params);
        }
    }

    public function hookDisplayBeforeBodyClosingTag()
    {
        if(self::getAgreementsToRGPD()) {
            return parent::hookDisplayBeforeBodyClosingTag();
        }
    }

    public function hookUpdateOrderStatus($params)
    {
        if(self::getAgreementsToRGPD()) {
            parent::hookUpdateOrderStatus($params);
        }
    }

    public function ajaxCall($params)
    {
        if(self::getAgreementsToRGPD()) {
            parent::ajaxCall($params);
        }
    }

    /**
     * ADD BY ESTEBANW INSITACTION 23052018
     * 
     * [getAgreementsToRGPD verifie si les cookies ont été acceptés]
     * @return [bool] [accepté ou non]
     */
    public static function getAgreementsToRGPD() {

        if(isset($_COOKIE['cookieconsent_status']) 
            && isset($_COOKIE['cookieconsent_status']) == 'dismiss') {
            return true;
        }
        return false;
    }
}
