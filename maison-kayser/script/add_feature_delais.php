<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Class d'import produit initial // pas de maj détruit toute les données si le test du csv est bon
 */
class AddCaracDelaisLivraison {

    protected static $carac_mode_livraison_clickandcollect_id = 11;
    protected static $carac_mode_livraison_rdv_id = 14;
    protected static $carac_mode_livraison_colissimo_id = 15;
    protected static $carac_clickandcollect = 492; //'Retrait en boulangerie';
    protected static $carac_no_clickandcollect = 439; //'Retrait en boulangerie impossible';
    protected static $carac_rdv = 491; //434 // 'Livraison sur rendez-vous';
    protected static $carac_no_rdv = 440; //'Livraison sur rendez-vous';
    protected static $carac_colissimo = 493;// 438; //'Livraison Colissimo';   
    protected static $carac_no_colissimo = 441; //'Livraison Colissimo';   

    protected $default_id_lang;

    private $_logger;

    protected $errors = array();


    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {

        $cnc = $rdv = $colissimo = 0;
      $sql = 'SELECT * FROM `'._DB_PREFIX_.'product`';
      $results = DB::getInstance()->executeS($sql); 
      if(is_array($results) && $results) {
        $this->log(count($results).' Produits trouvés');


        foreach ($results as $key => $result) {

          $id_product = $result['id_product'];

          $delai_colissimo = !is_null($result['delai_colissimo']) && $result['delai_colissimo'] > 0;
          // clickandcollect pour tout produit
          $delai_clickandcollect = !is_null($result['delai_clickandcollect']);
          $delai_livraison_velo = !is_null($result['delai_livraison_velo']) && $result['delai_livraison_velo'] > 0;
          $delai_livraison_voiture = !is_null($result['delai_livraison_voiture']) && $result['delai_livraison_voiture'] > 0;

          if($delai_colissimo) { 
            $this->log('Produit #'.$id_product.' Ajout feature Colissimo');
            $colissimo++;
            $this->addFeature(self::$carac_mode_livraison_clickandcollect_id, $id_product, self::$carac_colissimo);
          } else {
            // $this->addFeature(self::$carac_mode_livraison_colissimo_id, $id_product, self::$carac_no_colissimo);
          }

          if($delai_clickandcollect) {
            $this->log('Produit #'.$id_product.' Ajout feature Click and collect');
            $cnc++;
           $this->addFeature(self::$carac_mode_livraison_clickandcollect_id, $id_product, self::$carac_clickandcollect);
          } else {
           // $this->addFeature(self::$carac_mode_livraison_clickandcollect_id, $id_product, self::$carac_no_clickandcollect);
          }

          if($delai_livraison_velo || $delai_livraison_voiture) {
            $this->log('Produit #'.$id_product.' Ajout feature RDV');      
            $rdv++;
            $this->addFeature(self::$carac_mode_livraison_clickandcollect_id, $id_product, self::$carac_rdv);
          } else {
            // $this->addFeature(self::$carac_mode_livraison_rdv_id, $id_product, self::$carac_no_rdv);
          }
        }
      }
      $this->log('Produit Colissimo#'.$colissimo.' Click and Collect#'.$cnc.' RDV#'.$rdv);    
    }

    protected function addFeature($id_feature = false, $id_product = false, $feature_value = '') {
      if($id_feature && $id_product && strlen(trim($feature_value))) {
      // if($id_feature && $id_product && $feature_value) {
        $id_feature_value = $feature_value;
        
        //$id_feature_value = FeatureValue::addFeatureValueImport(
                                                    //   $id_feature, 
                                                    //   $feature_value, 
                                                    //   $id_product, 
                                                    //   $this->default_id_lang
                                                    // ); 
        if($id_feature_value) {
          //test si feature existante
          $sql = 'SELECT * FROM `mk_feature_product` WHERE `id_feature` = '.$id_feature.' AND `id_product` = '.$id_product.' AND `id_feature_value` = '.$id_feature_value;
          $this->log('Feature value ID#'.$id_feature_value);
          if($result = DB::getInstance()->getRow($sql)) {
            
            $this->log('Existe pour le produit ID#'.$id_product.' et la valeur #'.$id_feature_value);

          } else {
            $this->log('n\'existe pas pour le produit ID#'.$id_product.' et la valeur #'.$id_feature_value);

            if(Product::addFeatureProductImport($id_product, $id_feature, $id_feature_value)) {
              $this->log('La feature #'.$id_feature.' "'.$feature_value.'" a été ajouté au produit #'.$id_product);
            } else {
              $this->log('ERREUR La feature #'.$id_feature.' n\'a pas été ajouté au produit #'.$id_product);
            }
          }
         
        } else {
          $this->log('ERREUR id_feature_value non retrouvé');
        }     
      
      } else {
        $this->log('ERREUR parametres incorrects');
      }
    }

}

$timestamp_debut = microtime(true);

$import = new AddCaracDelaisLivraison();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$import->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");
  