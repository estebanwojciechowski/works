<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php');

class generateDeclis {

    protected static $id_attributes = array(
      3 => 3, // boissons
      4 => 4, // plats
      5 => 5, // desserts
      9 => 9, // 2nd plats
    );

    protected static $id_product = 381;

    protected $attribute_values = array();
    protected $product;
    protected $context;

    protected $default_id_lang;

    private $_logger;

    protected $errors = array();


    public function __construct() {
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques
        $context = Context::getContext();
        $context->shop = new Shop();
        $context->employee = new Employee(1);
        $context->language = new Language($this->default_id_lang);
        $this->context = $context;
        define('_PS_ADMIN_DIR_', true);
        Shop::setContext(Shop::CONTEXT_ALL);
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO);


    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {

      $this->product = new Product(self::$id_product, true);
      if(!Validate::isLoadedObject($this->product)) {
        $this->log('L\'objet produit n\'a pas été chargé correctement.');
        return false;
      }

      // Le prix n'est pas instancié au chargement on le force sur la valeur globale
      $price = DB::getInstance()->getValue('SELECT price FROM `'._DB_PREFIX_.'product` WHERE id_product = '.self::$id_product);
      $this->product->price = $price;
      $this->product->id_default_category = $this->product->getDefaultCategory();
      $this->product->active = 0;
      $this->product->show_price = 1;
      $this->product->available_for_order = 1;

      if(!is_array(self::$id_attributes) || empty(self::$id_attributes)) {
        $this->log('Les attributs sont manquants.');
        return false;
      }
      // recupération des features values correspondantes aux features sélectionnées
      foreach (self::$id_attributes as $key => $id_attribute_group) {
        $parsed_attribute_values = array();
        $attribute_values = DB::getInstance()->executeS('
          SELECT id_attribute FROM `'._DB_PREFIX_.'attribute`
          WHERE id_attribute_group = '.$id_attribute_group
        );

        foreach ($attribute_values as $attribute_value) {
          $parsed_attribute_values[$attribute_value["id_attribute"]] = $attribute_value["id_attribute"];
        }
        $this->attribute_values[] = $parsed_attribute_values;
      }

      $controller = new AdminAttributeGeneratorController();
      $default_quantity = 1;

      $errors = $controller->generateCombinations(
        $this->attribute_values,
        $this->product,
        $default_quantity,
        $this
      );

      if(!empty($errors)) {
        $this->log(json_encode($errors));
      } else {
        $this->log('Tout s\'est bien déroulé');
        // activer product_attribute default_on pour id_attibute default
      }
    }

    public function generateCache() {

      // on recupére tout les id product_attribute du produit
      $sql = '  SELECT id_product_attribute FROM `'._DB_PREFIX_.'product_attribute`
                WHERE id_product = '.self::$id_product;
      $ids = DB::getInstance()->executeS($sql);
      if($ids) {
        foreach ($ids as $key => $idpa) {
          $params = Product::getAttributesParams(self::$id_product, $idpa['id_product_attribute']);
          if($params) {
            $this->log("\n\nCACHE GENERE:\n ".$idpa['id_product_attribute'].' '.json_encode($params)."\n\n");
          }
        }
      }
    }

    public function generateStock() {
      $shops_list = Shop::getCompleteListOfShopsID();
      // on recupére tout les id product_attribute du produit
      $sql = '  SELECT id_product_attribute FROM `'._DB_PREFIX_.'product_attribute`
                WHERE id_product = '.self::$id_product;
      $ids = DB::getInstance()->executeS($sql);
      if($ids) {
          $id_product = self::$id_product;
          // enleve les stock
          $sql = 'DELETE FROM `'._DB_PREFIX_.'stock_available`
                  WHERE  id_product = '.self::$id_product;
          DB::getInstance()->execute($sql);

           $attributes_not_empty = array();
          foreach ($ids as $key => $idpa) {

            $id_product_attribute = (int) $idpa['id_product_attribute'];

            $sql = 'INSERT INTO `'._DB_PREFIX_.'stock_available`
                  (id_stock_available, id_product , id_product_attribute, id_shop, id_shop_group, quantity,
                    physical_quantity, reserved_quantity, depends_on_stock, out_of_stock)
                  VALUES ';

            foreach ($shops_list as $key => $id_shop) {
              $sql .= ' ( NULL, '.$id_product.', '.$id_product_attribute.', '.$id_shop.', 0, 0, 0, 0, 0, 2),';
              if(!isset($attributes_not_empty[$id_product]) && $id_product_attribute > 0) {
                $sql .= ' ( NULL, '.$id_product.', 0, '.$id_shop.', 0, 0, 0, 0, 0, 2),';
              }
            }
             //ajout de l'id produit pour ne pas ajouter une ligne id_product_attr a 0 pour la prochaine decli
            if(!isset($attributes_not_empty[$id_product]) && $id_product_attribute > 0) {
              $attributes_not_empty[$id_product] = $id_product;
            }

            $sql = substr($sql, 0, -1);

            if(DB::getInstance()->execute($sql)) {
              $this->log('Stock ajouté '.$id_product.' '.$id_product_attribute);
            } else {
              $this->log('ERREUR Stock non ajouté '.$id_product.' '.$id_product_attribute);
            }

          }
      }

    }

}

$timestamp_debut = microtime(true);

$import = new generateDeclis();
// $import->start();
// $import->generateCache();
$import->generateStock();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$import->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");
