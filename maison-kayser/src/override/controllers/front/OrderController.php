<?php

class OrderController extends OrderControllerCore
{

    public function setMedia()
    {
    	parent::setMedia();

        $this->registerJavascript('shipping', '/assets/js/shipping.js', ['position' => 'bottom', 'priority' => 1000]);

        $carrier_velo_hours = Configuration::get('KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES');

        $this->context->smarty->assign(
        	array(
        		'id_carrier_velo' => Carrier::getVeloCarrier(),
        		'carrier_velo_hours' => $carrier_velo_hours
        	)
        );

        return true;
    }

}