<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

class BevisibleMapModuleFrontController extends ModuleFrontController
{
    /** @var BeVisibleMap */
    public $module;

    public function initContent()
    {         
        parent::initContent(); 
        $metas = BeVisible::getMetas();
        $title = '';
        if ($metas && isset($metas['title']) && $metas['title']) {
            $title = $metas['title'];
        }
        $bevisible_url = $metas['url_rewrite']; 
        $bevisible_full_url = $this->context->shop->getBaseURL(true).$bevisible_url;
        $back = Tools::getValue('back', false);

        $this->module->bvhelper->smartyAssign(array(
            'bevisible_title' => $title,
            'bevisible_content' => Configuration::get('BEVISIBLE_CUSTOM_CONTENT', $this->context->language->id),
            'bevisible_content_before' => Configuration::get('BEVISIBLE_CUSTOM_CONTENT_BEFORE', $this->context->language->id),
            'bevisible_content_after' => Configuration::get('BEVISIBLE_CUSTOM_CONTENT_AFTER', $this->context->language->id),
            'bevisible_locator' => $this->module->hasGoogleMapsApiKey() && Configuration::get('BEVISIBLE_CUSTOM_LOCATOR'),
            'bevisible_url' => $bevisible_full_url,
            'back' => $back, 
            'link' => $this->context->link 
            )
        );

        $content = ''; 
        $store = $this->context->shop->store; 
        $stores = $this->module->getStoresFront(); 
        $template = 'module:'.BeVisibleMapHelper::MODULE_NAME.'/views/templates/front/map.tpl';

        if($id_zone = Tools::getValue('id_zone')) { 
            $zone = new BeVisibleGroup($id_zone, $this->context->language->id); 
            
            $id_country = str_replace('-', '', Tools::getValue('id_country')); 
            $country = new Country($id_country, $this->context->language->id); 
            if(!Validate::isLoadedObject($country))
                $country = false;

            // etape intermédiaire, listing de pays a afficher si c'est pas une zone france
            $countries = false;
            if(!$country && !$zone->france)
                $countries = BeVisibleGroup::getCountriesByGroupID($id_zone, $this->context->language->id);


            // redirect with the good name
            $url_to_redirect = $this->context->link->getModuleLink(
                                        'bevisible', 
                                        'map-zone', 
                                        [
                                            'id_zone' => $id_zone,
                                            'id_country' => ($country ? '-'.$country->id : false),
                                            'wanted_url' => Tools::link_rewrite(($country ? $country->name : $zone->name))
                                        ]
                                    );
            //redirection verls l'url avec le bon nom de magasin
            $this->redirectionExtraExcludedKeys[] = 'id_zone';
            $this->redirectionExtraExcludedKeys[] = 'id_country';
            $this->redirectionExtraExcludedKeys[] = 'module';
            parent::canonicalRedirection($url_to_redirect);

            // reorder stores by postcode and names   
            usort($stores, function($a, $b) {
                if(isset($a['postcode']) && isset($b['postcode']))
                    if($a['postcode'] == $b['postcode']) {
                        //si un chiffre est présent on teste le chiffre
                        if(preg_match('/^([0-9]+).*$/', $a['name'], $a_nb)) {
                            if(preg_match('/^([0-9]+).*$/', $b['name'], $b_nb)) {
                                if(isset($a_nb[1]) && isset($b_nb[1]))
                                    return $a_nb[1] > $b_nb[1];
                            }
                        }
                        return strcmp($a['name'], $b['name']);
                    }
                    return $a['postcode'] > $b['postcode'];
                return false;
            });

            $this->module->bvhelper->smartyAssign(array(
                    'zone' => $zone,
                    'stores' => $stores,
                    'country' => $country, 
                    'countries' => $countries
                )
            );
            $content = $this->module->bvhelper->smartyFetch('front/zone.tpl', true);  
            /**
             * Charge le contenu de la zone, du magasin ou le listing des groupes
             */
            $this->module->bvhelper->smartyAssign(array(
                    'content' => $content, 
                )
            );            

        } elseif($id_store = Tools::getValue('id_store')) {
            $template = 'module:'.BeVisibleMapHelper::MODULE_NAME.'/views/templates/front/store.tpl';
            if(count($stores)) {
                $store = current($stores); 
            } else {                
                Tools::redirect($bevisible_full_url);
            } 
            // redirect with the good name

            //le magasin dispose t il d'une boutique
            $shop = new Shop($id_store);
            $has_shop = false;
            $store_choose_action = false; 
            if(Validate::isLoadedObject($shop) ) {
                if($shop->active) {
                    $has_shop = true;
                    if($this->context->shop->id != $shop->id) {
                        $store_choose_action = $shop->getBaseURL(true).$bevisible_url; 
                    }
                }
            }

            // ecrase les metas
            $page_vars = $this->context->smarty->getTemplateVars('page');

            $page_vars['meta']['title'] = $this->l('Boulangerie').' '.$store['name'];
            $page_vars['meta']['description'] = strip_tags(html_entity_decode($store['description']));


            $this->context->smarty->assign(array(
                'page' => $page_vars,
            ));

            $bg_image = false;
            if(file_exists(_PS_STORE_IMG_DIR_.$id_store.'.jpg'))
                $bg_image = _THEME_STORE_DIR_.$id_store.'.jpg';

            if($id_store != $store['id_store']) {
                //si le store set est celui par défaut on affiche la 404
                $url_to_redirect =  $this->context->link->getPageLink('PageNotFoundController');
            } else {
                $url_to_redirect  = $this->context->link->getModuleLink(
                                    'bevisible', 
                                    'map-store', 
                                    [
                                        'id_store' => $id_store,
                                        'wanted_url' => Tools::link_rewrite($store['name'])
                                    ]
                                );
            }
            //redirection verls l'url avec le bon nom de magasin
            $this->redirectionExtraExcludedKeys[] = 'id_store';
            $this->redirectionExtraExcludedKeys[] = 'module';
            parent::canonicalRedirection($url_to_redirect);


            $has_services = Store::hasServices($store['id_store']);
            $this->module->bvhelper->smartyAssign(array(
                    'bg_image' => $bg_image,
                    'store' =>  $store,
                    'store_choose_action' => $store_choose_action,
                    'has_services' => $has_services,
                    'has_shop' => $has_shop
                )
            );
        } else {
            if($cnc = Tools::getValue('cnc')) {
                //si sélection depuis popup choisir boulangerie
                 // reorder stores by postcode and names   
                usort($stores, function($a, $b) {
                if(isset($a['postcode']) && isset($b['postcode']))
                        if($a['postcode'] == $b['postcode']) {
                            //si un chiffre est présent on teste le chiffre
                            if(preg_match('/^([0-9]+).*$/', $a['name'], $a_nb)) {
                                if(preg_match('/^([0-9]+).*$/', $b['name'], $b_nb)) {
                                    if(isset($a_nb[1]) && isset($b_nb[1]))
                                        return $a_nb[1] > $b_nb[1];
                                }
                            }
                            return strcmp($a['name'], $b['name']);
                        }
                        return $a['postcode'] > $b['postcode'];
                    return false;
                });

                $this->module->bvhelper->smartyAssign(array(
                        'zone' => false,
                        'stores' => $stores,
                        'country' => false, 
                        'countries' => false
                    )
                );
                $content = $this->module->bvhelper->smartyFetch('front/zone.tpl', true);  
                /**
                 * Charge le contenu de la zone, du magasin ou le listing des groupes
                 */
                $this->module->bvhelper->smartyAssign(array(
                        'content' => $content, 
                    )
                );    
            } else {
                // listing classique                
                $id_countries = array();
                foreach ($stores as $key => $store) {
                    if(isset($id_countries[$store['id_country']]))
                        continue;
                    $id_countries[$store['id_country']] = $store['id_country'];
                }        

                $zones = BeVisibleGroup::getGroupsWithCountries($this->context->language->id, $id_countries); 
                $zones_france = BeVisibleGroup::getGroupsInFrance($this->context->language->id);
                //add countries 
                $this->module->bvhelper->smartyAssign(array(
                        'zones' => $zones,
                        'zones_france' => $zones_france
                    )
                ); 
                $content = $this->module->bvhelper->smartyFetch('front/zones.tpl', true);
            }
            /**
             * Charge le contenu de la zone, du magasin ou le listing des groupes
             */
            $this->module->bvhelper->smartyAssign(array(
                    'content' => $content, 
                )
            );
        }

        $this->setTemplate($template);        

    }

    /**
     * [postProcess Getion du choix de boulangerie + migration panier]
     * @return [type] [description]
     */
    public function postProcess() { 
        $errors = array();
        $metas = BeVisible::getMetas();  
        $post_datas = Tools::getAllValues();  
        if( isset($post_datas['method']) && 
            ($post_datas['method'] == 'choose-store-submit' 
                || $post_datas['method'] == 'confirm-change-submit')
        ) {
            $token = $post_datas['token'];
            $method = $post_datas['method'];
            $back = Tools::getValue('back'); 
            if($token == Tools::getToken(false)) { 
               $id_store = $post_datas['id_store']; 
               // verification qu'une boutique est présent pour cette boulangerie
               $shop = new Shop($id_store); 
               if(Validate::isLoadedObject($shop)) {
                    //redirect if success
                    $url = $shop->getBaseURL(true);
                    if($back) {
                        $url =str_replace(
                                $this->context->shop->getBaseURL(true), 
                                $url, 
                                $back
                            );                    
                    } else {
                        $langues =  Language::getLanguages();
                        if(count($langues) > 1) {
                            //add lang
                            $url .= Language::getIsoById($this->context->language->id).'/';
                        }
                    }
                    $modal = false; 
                    if(Validate::isLoadedObject($this->context->cart)) { 
                        $confirm_change = ($method == 'confirm-change-submit'); 
                        $products = $this->context->cart->getProducts();
                        $products_unavailable = array();
                        // affichage modal si un produit n'est pas dispo lors de la migration panier
                        if(is_array($products) && count($products)) {
                            foreach ($products as $key => &$product) {  
                                $unavailable = false;
                                // produit actif dans la boutique cible ?
                                $product_dest_shop = new Product($product['id_product'], false, $this->context->language->id, $id_store); 
                                if(!Validate::isLoadedObject($product_dest_shop) 
                                    || !$product_dest_shop->active) {
                                    $unavailable = true;
                                } elseif(isset($product['out_of_stock'])) { 
                                    // si le produit n'est pas commandable hors stock
                                    if(!Product::isAvailableWhenOutOfStock($product['out_of_stock'])) {  
                                        // regarde le stock dans la boutique cible                   
                                        $quantity_available = StockAvailable::getStockAvailableIdByProductId($product['id_product'], $product['id_product_attribute'], $id_store);
                                        if($quantity_available <= 0) {
                                            $unavailable = true;
                                        }
                                    } 
                                }

                                if($unavailable) {
                                    $cover = Product::getCover($product['id_product']);
                                    $product['cover'] = $this->context->link->getImageLink($product['name'], $cover['id_image'], 'home_default');

                                    $products_unavailable[$product['id_product'].'_'.$product['id_product_attribute']] = $product;
                                }                                
                            }

                            if($confirm_change || empty($products_unavailable)) {
                                // defini la boulangerie choisie comme préférée si connecté
                                $this->context->customer->setMyShop($id_store);
                                
                                if($this->context->cart->changeShop($id_store, $products_unavailable)) {

                                    if($this->ajax) {
                                        ob_end_clean();
                                        header('Content-Type: application/json');
                                        die(json_encode([
                                            'success' => true,
                                            'redirect' => $url,
                                        ]));
                                    }
                                    Tools::redirect($url);
                                } else {
                                    $errors[] = 'Erreur lors de la migration du panier';
                                }                           
                                
                            } elseif(!empty($products_unavailable)) { 
                                $modal = $this->renderModal(
                                                    $id_store, 
                                                    $products_unavailable, 
                                                    Tools::getValue('action_url'),                     
                                                    Tools::getValue('back')
                                                );
                                if($this->ajax) {
                                    ob_end_clean();
                                    header('Content-Type: application/json');
                                    die(json_encode([
                                        'success' => true,
                                        'modal' => $modal,
                                    ]));
                                }
                            }
                        } 
                    }

                    // defini la boulangerie choisie comme préférée si connecté
                    $this->context->customer->setMyShop($id_store);
                    // Pas de panier, redirect
                    if($this->ajax) {
                        ob_end_clean();
                        header('Content-Type: application/json');
                        die(json_encode([
                            'success' => true,
                            'redirect' => $url,
                        ]));
                    }
                } else {
                    $errors[] = 'Aucune boutique pour cette boulangerie';
                }
            } else {
                $errors[] = 'Token invalide';
            }  

            if($this->ajax) {
                ob_end_clean();
                header('Content-Type: application/json');
                die(json_encode([
                    'hasError' => true,
                    'error' => $errors
                ]));
            }     
        }
    }

    
    public function renderModal($id_store, $products_unavailable, $action_url, $back)
    {    
        $store = new Store($id_store); 

        $this->context->smarty->assign(array( 
            'products' => $products_unavailable,
            'store' => $store,
            'action_url' => $action_url,
            'link' => $this->context->link,
            'back' => $back,
            'static_token' => Tools::getToken(false)
        ));

        return $this->context->smarty->fetch('module:bevisible/views/templates/front/modal.tpl');
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->module->l('Our stores', 'map'),
            'url' => $this->context->link->getModuleLink('bevisible', 'map'),
        );

        return $breadcrumb;
    }
}
