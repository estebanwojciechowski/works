<?php

class Product extends ProductCore
{

    // VARIABLE QUI PERMET DE DESACTIVER LE DELAI DE REFABRICATION
    private static $refabrication = true;
    // VARIABLE QUI PERMET DE FORCER LE DELAI DE REFABRICATION MEME SI LE PRODUIT EST EN STOCK
    private static $force_refabrication = true;

    /** @var string Tax name */

    /**
     * $_nbSpecificProduct int
     * définit le nombre maximum de produits specifique (Pour combler les accessoires)
     */
    private $_nbSpecificProduct = 4;

    public $id_sage;

    public $delai_fabrication_avant_11h;

    public $delai_fabrication_apres_11h;

    public $delai_clickandcollect;

    public $delai_livraison_velo;

    public $delai_livraison_voiture;

    public $delai_colissimo;

    // GROUPE D'ATTRIBUTS DES FORMULES
    public static $formules_id_attribute_groups = array(
        'plats'             => 4,
        'desserts'          => 5,
        'boissons'          => 3,
        'second_plats'      => 9,
    );

    public static $definition = array(
        'table' => 'product',
        'primary' => 'id_product',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
        	/* Identifiant caisse */
        	'id_sage' 						=> 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 32),

        	/* delais */
        	'delai_fabrication_avant_11h' 	=> 	array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
 			'delai_fabrication_apres_11h' 	=> 	array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
 			'delai_clickandcollect' 		=> 	array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isUnsignedFloat'),
 			'delai_livraison_velo' 			=> 	array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
 			'delai_livraison_voiture' 		=> 	array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
 			'delai_colissimo' 				=> 	array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),

            /* Classic fields */
            'id_shop_default' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_manufacturer' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_supplier' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'reference' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 32),
            'supplier_reference' =>        array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 32),
            'location' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 64),
            'width' =>                        array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'height' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'depth' =>                        array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'weight' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'quantity_discount' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'ean13' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isEan13', 'size' => 13),
            'isbn' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isIsbn', 'size' => 32),
            'upc' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isUpc', 'size' => 12),
            'cache_is_pack' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'cache_has_attachments' =>        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'is_virtual' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'state' =>                     array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),

            /* Shop fields */
            'id_category_default' =>        array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'id_tax_rules_group' =>        array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'on_sale' =>                    array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'online_only' =>                array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'ecotax' =>                    array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'minimal_quantity' =>            array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'price' =>                        array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice', 'required' => true),
            'wholesale_price' =>            array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'unity' =>                        array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isString'),
            'unit_price_ratio' =>            array('type' => self::TYPE_FLOAT, 'shop' => true),
            'additional_shipping_cost' =>    array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'customizable' =>                array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'text_fields' =>                array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'uploadable_files' =>            array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'active' =>                    array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'redirect_type' =>                array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isString'),
            'id_type_redirected' =>        array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'available_for_order' =>        array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'available_date' =>            array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDateFormat'),
            'show_condition' =>            array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'condition' =>                    array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isGenericName', 'values' => array('new', 'used', 'refurbished'), 'default' => 'new'),
            'show_price' =>                array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'indexed' =>                    array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'visibility' =>                array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isProductVisibility', 'values' => array('both', 'catalog', 'search', 'none'), 'default' => 'both'),
            'cache_default_attribute' =>    array('type' => self::TYPE_INT, 'shop' => true),
            'advanced_stock_management' =>    array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'date_add' =>                    array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDate'),
            'date_upd' =>                    array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDate'),
            'pack_stock_type' =>            array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),

            /* Lang fields */
            'meta_description' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 655),
            'meta_keywords' =>                array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 320),
            'meta_title' =>                array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 320),
            'link_rewrite' =>    array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isLinkRewrite',
                'required' => false,
                'size' => 128,
                'ws_modifier' => array(
                    'http_method' => WebserviceRequest::HTTP_POST,
                    'modifier' => 'modifierWsLinkRewrite'
                )
            ),
            'name' =>                        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCatalogName', 'required' => false, 'size' => 128),
            'description' =>                array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'description_short' =>            array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'available_now' =>                array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'available_later' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'IsGenericName', 'size' => 255),
        ),
        'associations' => array(
            'manufacturer' =>                array('type' => self::HAS_ONE),
            'supplier' =>                    array('type' => self::HAS_ONE),
            'default_category' =>            array('type' => self::HAS_ONE, 'field' => 'id_category_default', 'object' => 'Category'),
            'tax_rules_group' =>            array('type' => self::HAS_ONE),
            'categories' =>                    array('type' => self::HAS_MANY, 'field' => 'id_category', 'object' => 'Category', 'association' => 'category_product'),
            'stock_availables' =>            array('type' => self::HAS_MANY, 'field' => 'id_stock_available', 'object' => 'StockAvailable', 'association' => 'stock_availables'),
        ),
    );

    public static function isColissimoReady($product = null) {
        if(!is_null($product) && $product) {
            if(is_numeric($product)) {
                $product = new Product($product);
            }
            if(Validate::isLoadedObject($product) && $product instanceof Product) {
                return !is_null($product->delai_colissimo) && $product->delai_colissimo;
            }
        }
        return false;
    }

    public function getProductDelay($attribute = false, $with_refab = false) {
        $product_delay = 0;
        //si un attribut de transporteur est passé en parametre
        //on ajoute le delai de préparation
        if($attribute && isset($attribute['field'])
            && isset($attribute['object']) && $attribute['object'] == 'product') {
            //on verifie que le champ existe, si il existe le délai est exprimé en heures
            if(property_exists($this, $attribute['field'])) {

                if($attribute['field'] != 'delai_clickandcollect' && (is_null($this->{$attribute['field']}) ||  !($this->{$attribute['field']} > 0) )) {
                    //pas dispo avec cet attribut
                    return false;
                }
                //on ajoute le delai de péparation au produit
                $product_delay += $this->{$attribute['field']} * 3600;
            }
        }
        if($with_refab) {
            $product_delay += $this->getRefabDelay();
        }
        return $product_delay;
    }

    /**
     * [getRefabDelay recupération du delai de fabrication]
     * @param  boolean $delay_after_opening [force le delai de refabrication au matin pour report à l'ouverture de la boulangerie (= ouverture boulangerie + delai refabrication avant mise a dispo)]
     * @return [type]                       [description]
     */
    public function getRefabDelay($delay_after_opening = false) {
        $quantity = self::getQuantity($this->id);
        if(($quantity <= 0 && self::$refabrication) || self::$force_refabrication) {
            // si pas de stock il y a un delai de refabrication
            // est il 11h ?
            $heure = date('H');
            if($heure < 11 || $delay_after_opening) {
                return $this->delai_fabrication_avant_11h * 3600;
            } else {
                return $this->delai_fabrication_apres_11h * 3600;
            }
        }
        return 0;
    }

    /**
     * Récupération du produit en fonction de sa référence
     * @return ID du produit
     */
    public static function getProductByRef($reference){

        $row = Db::getInstance()->getRow('
		SELECT `id_product`
		FROM `'._DB_PREFIX_.'product` p
		WHERE p.reference = "'.pSQL($reference).'"');

        return isset($row['id_product']) ? $row['id_product'] : null;

    }

    /**
     * Get product accessories
     *
     * @param int $id_lang Language id
     * @return array Product accessories
     */
    public function getAccessories($id_lang, $active = true)
    {

        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, pl.`link_rewrite`,
					pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`,
					image_shop.`id_image` id_image, il.`legend`, m.`name` as manufacturer_name, cl.`name` AS category_default, IFNULL(product_attribute_shop.id_product_attribute, 0) id_product_attribute,
					DATEDIFF(
						p.`date_add`,
						DATE_SUB(
							"'.date('Y-m-d').' 00:00:00",
							INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
						)
					) > 0 AS new
				FROM `'._DB_PREFIX_.'accessory`
				LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = `id_product_2`
				'.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
					ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$this->id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
					p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
				)
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (
					product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
				)
				LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
					ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$this->id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.`id_manufacturer`= m.`id_manufacturer`)
				'.Product::sqlStock('p', 0).'
				WHERE `id_product_1` = '.(int)$this->id.
                ($active ? ' AND product_shop.`active` = 1 AND product_shop.`visibility` != \'none\'' : '').'
                GROUP BY product_shop.id_product';

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if(Context::getContext()->controller->php_self == 'cart'){
            return $this->getNativeAccessories($result, $id_lang);
        }

        if(count($result) >= $this->_nbSpecificProduct ){ //si le produit possède au moins 4 ou + accessoires, on laisse la méthode native de PS
            return $this->getNativeAccessories($result, $id_lang);
        }else{ //si le produit possède moins de 4 accessoires, on comble
            return $this->getSpecificAccessories($result, $id_lang, $active);
        }

        return array();

    }

    /**
     * on ajoute les produits spé aux accessoires déjà présent
     */
    private function getSpecificAccessories($result, $id_lang, $active){
        $nb = count($result);
        $limit = $this->_nbSpecificProduct-$nb; // limit servant à savoir le nombre de produits que l'ont veut en +

        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, pl.`link_rewrite`,
                pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`,
                image_shop.`id_image` id_image, il.`legend`, m.`name` as manufacturer_name, cl.`name` AS category_default, IFNULL(product_attribute_shop.id_product_attribute, 0) id_product_attribute,
					DATEDIFF(
						p.`date_add`,
						DATE_SUB(
							"'.date('Y-m-d').' 00:00:00",
							INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
						)
					) > 0 AS new
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
					ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$this->id_shop.')
                LEFT JOIN `' . _DB_PREFIX_ . 'category_product` catp ON p.id_product = catp.id_product
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
					p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
                )
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (
					product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
				)
                LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
					ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$this->id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.`id_manufacturer`= m.`id_manufacturer`)
                '.Product::sqlStock('p', 0).'
                WHERE p.`id_product` != '.(int)$this->id.
                ($active ? ' AND product_shop.`active` = 1 AND product_shop.`visibility` != \'none\'' : '').'
                AND catp.id_category = '.(int)$this->id_category_default.'
                ORDER BY p.`price` DESC
                LIMIT 0,'.$limit.'
                ';

        $resultSpe = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if($resultSpe){ // si résultat, on combine
            $resultMerge = array_merge($result, $resultSpe);

            return $this->getNativeAccessories($resultMerge, $id_lang);
        }else{
            if(sizeof($result) > 0){
                return $this->getNativeAccessories($result, $id_lang);
            }else{
                return array();
            }

        }

    }

    public static function getAttributesParams($id_product, $id_product_attribute)
    {



        $id_lang = (int)Context::getContext()->language->id;
        $cache_id = 'Product::getAttributesParams_'.(int)$id_product.'-'.(int)$id_product_attribute.'-'.(int)$id_lang;

        if (!Cache::isStored($cache_id)) {

            $result = Db::getInstance()->executeS('
            SELECT a.`id_product`, a.`id_attribute`, a.`id_attribute_group`, al.`name`, agl.`name` as `group`, pa.`reference`, pa.`ean13`, pa.`isbn`,pa.`upc`
            FROM `'._DB_PREFIX_.'attribute` a
            LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
                ON (al.`id_attribute` = a.`id_attribute` AND al.`id_lang` = '.(int)$id_lang.')
            LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
                ON (pac.`id_attribute` = a.`id_attribute`)
            LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                ON (pa.`id_product_attribute` = pac.`id_product_attribute`)
            '.Shop::addSqlAssociation('product_attribute', 'pa').'
            LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
                ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)$id_lang.')
            WHERE pa.`id_product` = '.(int)$id_product.'
                AND pac.`id_product_attribute` = '.(int)$id_product_attribute.'
                AND agl.`id_lang` = '.(int)$id_lang);
            Cache::store($cache_id, $result);
        } else {

            $result = Cache::retrieve($cache_id);
        }

        return $result;
    }

    /**
     * Accessoire natif PS
     */
    private function getNativeAccessories($result, $id_lang){
        foreach ($result as $k => &$row) {
            if (!Product::checkAccessStatic((int)$row['id_product'], false)) {
                unset($result[$k]);
                continue;
            } else {
                $row['id_product_attribute'] = Product::getDefaultAttribute((int)$row['id_product']);
            }
        }

        return $this->getProductsProperties($id_lang, $result);
    }

     /**
     * Sets carriers assigned to the product
     * and to all shop
     * if admin CONTEXT_ALL
     */
    public function setCarriers($carrier_list)
    {
        $data = array();
        $context = Context::getContext();
        if (
            $context && $context->shop->getContext() == Shop::CONTEXT_ALL
        ) {
            $shops = Shop::getContextListShopID();
            foreach ($shops as $key => $id_shop) {
                foreach ($carrier_list as $carrier) {
                    $data[] = array(
                        'id_product' => (int)$this->id,
                        'id_carrier_reference' => (int)$carrier,
                        'id_shop' => (int)$id_shop
                    );
                }

                Db::getInstance()->execute(
                    'DELETE FROM `'._DB_PREFIX_.'product_carrier`
                    WHERE id_product = '.(int)$this->id.'
                    AND id_shop = '.(int)$id_shop
                );

                $unique_array = array();
                foreach ($data as $sub_array) {
                    if (!in_array($sub_array, $unique_array)) {
                        $unique_array[] = $sub_array;
                    }
                }

                if (count($unique_array)) {
                    Db::getInstance()->insert('product_carrier', $unique_array, false, true, Db::INSERT_IGNORE);
                }

            }
        } else {
            foreach ($carrier_list as $carrier) {
                $data[] = array(
                    'id_product' => (int)$this->id,
                    'id_carrier_reference' => (int)$carrier,
                    'id_shop' => (int)$this->id_shop
                );
            }
            Db::getInstance()->execute(
                'DELETE FROM `'._DB_PREFIX_.'product_carrier`
                WHERE id_product = '.(int)$this->id.'
                AND id_shop = '.(int)$this->id_shop
            );

            $unique_array = array();
            foreach ($data as $sub_array) {
                if (!in_array($sub_array, $unique_array)) {
                    $unique_array[] = $sub_array;
                }
            }

            if (count($unique_array)) {
                Db::getInstance()->insert('product_carrier', $unique_array, false, true, Db::INSERT_IGNORE);
            }
        }
    }

    public function copyProductToOtherShop($id_shop) {
        $this->id_shop = $id_shop;
        return $this->save();
    }

    // ADD BY ESTEBANW INSITACTION 07092018
    // #20106
    // Permet la suppression des features une a une pour eviter
    // la perte des données

    public function deleteFeature($id_feature_value)
    {
        $all_shops = Context::getContext()->shop->getContext() == Shop::CONTEXT_ALL ? true : false;

        // List products features
        $feature = Db::getInstance()->executeS('
            SELECT p.*, f.*
            FROM `'._DB_PREFIX_.'feature_product` as p
            LEFT JOIN `'._DB_PREFIX_.'feature_value` as f ON (f.`id_feature_value` = p.`id_feature_value`)
            '.(!$all_shops ? 'LEFT JOIN `'._DB_PREFIX_.'feature_shop` fs ON (f.`id_feature` = fs.`id_feature`)' : null).'
            WHERE `id_product` = '.(int)$this->id.'
            AND  p.`id_feature_value` = '.$id_feature_value
                .(!$all_shops ? ' AND fs.`id_shop` = '.(int)Context::getContext()->shop->id : '')
        );
        if($feature) {
            foreach ($feature as $tab) {
                // Delete product custom features
                if ($tab['custom']) {
                    Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'feature_value` WHERE `id_feature_value` = '.(int)$id_feature_value);
                    Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'feature_value_lang` WHERE `id_feature_value` = '.(int)$id_feature_value);
                }
            }
        }
        // Delete product features
        $result = Db::getInstance()->execute('
            DELETE `'._DB_PREFIX_.'feature_product` FROM `'._DB_PREFIX_.'feature_product`
            WHERE `id_product` = '.(int)$this->id.'
            AND `id_feature_value` = '.$id_feature_value.(!$all_shops ? '
                AND `id_feature` IN (
                    SELECT `id_feature`
                    FROM `'._DB_PREFIX_.'feature_shop`
                    WHERE `id_shop` = '.(int)Context::getContext()->shop->id.'
                )' : ''));

        SpecificPriceRule::applyAllRules(array((int)$this->id));
        return ($result);
    }
    //END

    public static function getInfosFromAttributes($id_product = null, $attributes =array(), $id_lang = null) {

        if(is_null($id_lang)) {
            $id_lang = Context::getContext()->language->id;
        }

        $infos = array(
            'images' => array(),
            'features' => array()
        );

        if(is_numeric($id_product) && is_array($attributes) && count($attributes)) {
            foreach ($attributes as $key => $attribute) {
                // recupération des images des formules
                $id_cover_image = self::getImgFromAttributes($id_product, $attribute);
                if($id_cover_image) {
                    $infos['images'][$attribute['id_attribute_group']] = $id_cover_image;
                }

                // recupération des features des formules
                $features = self::getFeaturesFromAttributes($id_product, $attribute, $id_lang);
                if($features) {
                    $last_id_feature = false;
                    foreach ($features as $id_feature => $feature) {
                        if(isset($attribute['name'])) {
                            $feature['product_name'] = $attribute['name'];
                        }
                        $infos['features'][$id_feature][$attribute['id_attribute_group']] = $feature;

                        if(!$last_id_feature) {
                            $last_id_feature = $id_feature;
                        } elseif($last_id_feature != $id_feature) {
                            // reorganisation par groupe d'attribut :
                            // boisson - plat - dessert
                            ksort($infos['features'][$last_id_feature], SORT_NUMERIC);
                            $last_id_feature = $id_feature;
                        }
                    }
                    if($last_id_feature) {
                        // reorganisation par groupe d'attribut :
                        // boisson - plat - dessert
                        ksort($infos['features'][$last_id_feature], SORT_NUMERIC);

                    }

                }
            }

            if($infos['images']) {
                ksort($infos['images'], SORT_NUMERIC);
            }
            if($infos['features']) {
                ksort($infos['features'], SORT_NUMERIC);
            }
        }

        return $infos;
    }

    public static function getImgFromAttributes($id_product = null, $attribute = array()) {
        if(is_numeric($id_product) &&  is_array($attribute)
            && isset($attribute['id_attribute_group']) && isset($attribute['id_product'])
        ) {
            // recupération des images des formules uniquement
            if(in_array($attribute['id_attribute_group'], self::$formules_id_attribute_groups)) {
                if(
                    $attribute['id_product']
                        && Validate::isUnsignedInt($attribute['id_product'])
                ){
                    $cover = Product::getCover($attribute['id_product']);
                    if(
                        $cover['id_image']
                            && Validate::isUnsignedInt($cover['id_image'])
                    ) {
                        return (int) $cover['id_image'];
                    }
                }
            }
        }
        return false;
    }

    public static function getFeaturesFromAttributes($id_product = null, $attribute = array(), $id_lang = null)
    {
        if(is_numeric($id_product) &&  is_array($attribute)
            && isset($attribute['id_attribute_group']) && isset($attribute['id_product'])
        ) {
            // recupération des images des formules uniquement
            if(in_array($attribute['id_attribute_group'], self::$formules_id_attribute_groups)) {
                if(
                    $attribute['id_product']
                        && Validate::isUnsignedInt($attribute['id_product'])
                ){
                    $features = Product::getFrontFeaturesStatic($id_lang, $attribute['id_product']);
                    if(
                        $features
                            && count($features)
                    ) {
                        // add key to feature array
                        $orderer_features = array();
                        foreach ($features as $key => $feature) {
                           $orderer_features[$feature['id_feature']] = $feature;
                        }
                        ksort($orderer_features, SORT_NUMERIC);
                        return $orderer_features;
                    }
                }
            }
        }
        return false;
    }

    public function getAttributes($id_product_attribute = null, $id_lang = null) {
        $results = array();
        if($id_product_attribute && Validate::isUnsignedInt($id_product_attribute)) {

            $sql = 'SELECT a.*, al.name
                    FROM '._DB_PREFIX_.'product_attribute_combination pac
                    INNER JOIN '._DB_PREFIX_.'attribute a ON (pac.id_attribute = a.id_attribute AND pac.id_product_attribute='.$id_product_attribute.')
                    '. ($id_lang ?
                        '   LEFT JOIN '._DB_PREFIX_.'attribute_lang al
                            ON (al.id_attribute = a.id_attribute)
                            AND al.id_lang = '.$id_lang
                        : '').'
                    ';

            $results = DB::getInstance()->executeS($sql);
        }
        return $results;
    }

    public function getDistinctAttributesGroups($id_lang)
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }
        $sql = 'SELECT ag.`id_attribute_group`, ag.`is_color_group`, agl.`name` AS group_name, agl.`public_name` AS public_group_name,
                    a.`id_attribute`, al.`name` AS attribute_name, a.`color` AS attribute_color,
                    IFNULL(stock.quantity, 0) as quantity, product_attribute_shop.`price`, product_attribute_shop.`ecotax`, product_attribute_shop.`weight`,
                    product_attribute_shop.`default_on`, pa.`reference`, product_attribute_shop.`unit_price_impact`,
                    product_attribute_shop.`minimal_quantity`, product_attribute_shop.`available_date`, ag.`group_type`
                FROM `'._DB_PREFIX_.'product_attribute` pa
                '.Product::sqlStock('pa', 'pa').'
                '.Shop::addSqlAssociation('product_attribute', 'pa').'
                LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
                LEFT JOIN `'._DB_PREFIX_.'attribute` a ON (a.`id_attribute` = pac.`id_attribute`)
                LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON (ag.`id_attribute_group` = a.`id_attribute_group`)
                LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute`)
                LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group`)
                WHERE pa.`id_product` = '.(int)$this->id.'
                    AND al.`id_lang` = '.(int)$id_lang.'
                    AND agl.`id_lang` = '.(int)$id_lang.'
                GROUP BY id_attribute_group, id_attribute
                ORDER BY ag.`position` ASC, a.`position` ASC, agl.`name` ASC';
        return Db::getInstance()->executeS($sql);
    }

    public function isFormule() {
        if($this->hasAttributes()) {
            if($this->getDefaultCategory() == 58) {
                return true;
            }
        }
        return false;
    }

    public static function getMainShopCanonical($id_product, $canonical_url) {
        // product exists in MKI ?
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'product_shop`
                WHERE active = 1
                AND id_shop = 1
                AND visibility IN ("both", "catalog", "search")
                AND id_product = '.$id_product;

        $exists = DB::getInstance()->getRow($sql);
        if($exists) {
            $context = Context::getContext();
            return $context->link->getProductLink(
                        $id_product,
                        null,
                        null,
                        null,
                        $context->language->id,
                        1
            );
        }
        return $canonical_url;

    }
}



?>