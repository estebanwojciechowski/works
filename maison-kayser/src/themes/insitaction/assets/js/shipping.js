function initData(hours) {
	var data = {
		horaires: [],
		closed: []
	};

	if(hours) {
		hours = JSON.parse(hours);
		for (var i in hours) {
			day = parseInt(i)+1;
			if(i == 6) {
				day = 0;
			}

			data['horaires'][day] = hours[i];
			if(!hours[i] || typeof hours[i]['start'] == 'undefined') {
					data['closed'].push(day);
			}
		}
	}
	return data ;
}

function getSpecialdaysInfos(currentDateJs) {
	var originTimeStamp = new Date(currentDateJs.getTime());
	originTimeStamp.setHours(0);
	originTimeStamp.setMinutes(0);
	originTimeStamp.setSeconds(0);
	originTimeStamp = originTimeStamp.getTime();

	var storeSpecialDays = $('input[name$="store_special_days"]').val();
	if(storeSpecialDays) {
		storeSpecialDays = JSON.parse(storeSpecialDays);
	}
	var currentSpecialDay = false;
	if(typeof storeSpecialDays[originTimeStamp/1000] != 'undefined') {
		// si c'est un jour de fermeture c'est pas censé être sélectionné
		// si ça passe par là = problème on a pu clique sur une date disabled
		if(storeSpecialDays[originTimeStamp/1000].fermeture == 0) {
			var tmpSpecialDay = storeSpecialDays[originTimeStamp/1000];
			tmpSpecialDay = tmpSpecialDay.hours;
			tmpSpecialDay = tmpSpecialDay.split('-');

			if(tmpSpecialDay.length == 2) {

				var minHours = tmpSpecialDay[0].split(':');
				var maxHours = tmpSpecialDay[1].split(':');

				if(minHours.length == 2 && maxHours.length == 2) {
					var minMinutes = parseInt(minHours[1]);
					var minHours = parseInt(minHours[0]);

					var maxMinutes = parseInt(maxHours[1]);
					var maxHours = parseInt(maxHours[0]);

					currentSpecialDay = storeSpecialDays[originTimeStamp/1000];
					currentSpecialDay.start = new Array();
					currentSpecialDay.start['hours'] = minHours;
					currentSpecialDay.start['minutes'] = minMinutes;

					currentSpecialDay.end = new Array();
					currentSpecialDay.end['hours'] = maxHours;
					currentSpecialDay.end['minutes'] = maxMinutes;
				}
			}
		}
	}
	return currentSpecialDay;
}

function getMaxMinutes(datepickerCurrentHour, dateDayEnd) {
	var maxHours = dateDayEnd['hours'];
	var maxMinutes = dateDayEnd['minutes'];
	if(maxHours == datepickerCurrentHour) {
		return parseInt(maxMinutes);
	}

	return 59;
}

function getMinMinutes(datepickerCurrentHour, dateDayStart) {
	var minHours = dateDayStart['hours'];
	var minMinutes = dateDayStart['minutes'];
	if(minHours == datepickerCurrentHour) {
		return parseInt(minMinutes);
	}
	return 0;
}

function updateDatePickerMinutes(datepicker, d, datepickers, horairesIndex, currentIdentifier) {

	var currentSpecialDay = getSpecialdaysInfos(d);

	if(currentSpecialDay == false) {
		var maxMinutes = getMaxMinutes(d.getHours(), datepickers[horairesIndex]['horaires'][currentIdentifier]['end']);
	    var minMinutes = getMinMinutes(d.getHours(), datepickers[horairesIndex]['horaires'][currentIdentifier]['start']);
	} else {
		var maxMinutes = getMaxMinutes(d.getHours(), currentSpecialDay['end']);
	    var minMinutes = getMinMinutes(d.getHours(), currentSpecialDay['start']);
	}
    //maj du date picker
	datepicker.update({
			minMinutes: minMinutes,
			maxMinutes: maxMinutes,
	});
}

function updateDatePicker(datepicker, d, datepickers, horairesIndex, currentIdentifier) {

	var currentSpecialDay = getSpecialdaysInfos(d);

	if(currentSpecialDay == false) {
		//recupération depuis les infos fournies précédemments
		var minHours = parseInt(datepickers[horairesIndex]['horaires'][currentIdentifier]['start']['hours']);
		var maxHours = parseInt(datepickers[horairesIndex]['horaires'][currentIdentifier]['end']['hours']);

		var maxMinutes = getMaxMinutes(d.getHours(), datepickers[horairesIndex]['horaires'][currentIdentifier]['end']);
		var minMinutes = getMinMinutes(d.getHours(), datepickers[horairesIndex]['horaires'][currentIdentifier]['start']);
	} else {
		//recupération depuis les infos fournies précédemments
		var minHours = parseInt(currentSpecialDay['start']['hours']);
		var maxHours = parseInt(currentSpecialDay['end']['hours']);

		var maxMinutes = getMaxMinutes(d.getHours(), currentSpecialDay['end']);
		var minMinutes = getMinMinutes(d.getHours(), currentSpecialDay['start']);
	}

	//maj du date picker
	datepicker.update({
		 	minHours: minHours,
			minMinutes: minMinutes,
			maxHours: maxHours,
			maxMinutes: maxMinutes,
	});
}

function getSelectTimePicker(carrierId, selectedDate, datepickers, horairesIndex, currentIdentifier, pas, dateStart) {
	if($('select[name$="timepicker_'+carrierId+'"]').length > 0) {

		var currentSpecialDay = getSpecialdaysInfos(selectedDate);

		if(typeof(datepickers[horairesIndex]['horaires'][currentIdentifier]) != 'undefined'
			|| currentSpecialDay != false) {

			if(currentSpecialDay) {
				var today = currentSpecialDay;
			} else {
				var today = datepickers[horairesIndex]['horaires'][currentIdentifier];
			}

			if(typeof(today['start']) != 'undefined'
				&& typeof(today['end']) != 'undefined') {


				var day = selectedDate.getDate();
				var month = selectedDate.getMonth() + 1;
				var year = selectedDate.getFullYear();
				var selectedDateTimeStamp = selectedDate.getTime();
				// date today at 00:00:00
				var originTimeStamp = new Date(year+'/'+month+'/'+day);
				originTimeStamp = originTimeStamp.getTime();
				// timestamp de la date de depart
				var dateStartTimeStamp = dateStart.getTime();

				var storeSpecialDays = $('input[name$="store_special_days"]').val();
				if(storeSpecialDays) {
					storeSpecialDays = JSON.parse(storeSpecialDays);
				}

				if(currentSpecialDay == false) {
					// selection des horaires mini et max selon clickncollect ou magasin
					var minHours = parseInt(datepickers[horairesIndex]['horaires'][currentIdentifier]['start']['hours']);
					var maxHours = parseInt(datepickers[horairesIndex]['horaires'][currentIdentifier]['end']['hours']);
					var maxMinutes = parseInt(datepickers[horairesIndex]['horaires'][currentIdentifier]['end']['minutes']);
					var minMinutes = parseInt(datepickers[horairesIndex]['horaires'][currentIdentifier]['start']['minutes']);
				} else {
					// selection des horaires mini et max selon clickncollect ou magasin
					var minHours = parseInt(currentSpecialDay['start']['hours']);
					var maxHours = parseInt(currentSpecialDay['end']['hours']);
					var maxMinutes = parseInt(currentSpecialDay['end']['minutes']);
					var minMinutes = parseInt(currentSpecialDay['start']['minutes']);
				}


				var idCarrierVelo = getIdCarrierVelo();
				var carrierRefId = getIdCarrierReference(carrierId);

				if(carrierRefId == idCarrierVelo && carrierRefId) {
					var horairesVelo = getHorairesVelo();
					if(horairesVelo && horairesVelo.length == 2) {
						var matin = horairesVelo[0].split(':');
						if(matin.length == 2) {
							minHours = parseInt(matin[0]);
							minMinutes = parseInt(matin[1]);
						}
					}
				}

				minHoursSecond = minHours * 3600 * 1000; // on parcourt en secondes
				maxHoursSecond = maxHours * 3600 * 1000;
				minMinutesSecond = minMinutes * 60 * 1000;
				maxMinutesSecond = maxMinutes * 60 * 1000;

				startTimeStamp =  originTimeStamp + minHoursSecond + minMinutesSecond;
				endTimeStamp = originTimeStamp + maxHoursSecond + maxMinutesSecond;

				// on arrondi la date a la 1/2 heure supérieure si ça n'est pas le cas
				var modulo = Math.ceil(startTimeStamp / pas);
				startTimeStamp =  modulo * pas;
				// on arrondi la date a la 1/2 heure supérieure si ça n'est pas le cas
				modulo = Math.ceil(dateStartTimeStamp / pas);
				dateStartTimeStamp = modulo * pas;
				// si la date mini delivraison n'est pas atteinte on affiche uniquement les
				// horaires disponnibles ensuite
				if(dateStartTimeStamp > startTimeStamp) {
					startTimeStamp = dateStartTimeStamp;
				}
				var currentDate = new Date(startTimeStamp);

				var heure = currentDate.getHours();
				var minutes = currentDate.getMinutes();

				var select = $('select[name$="timepicker_'+carrierId+'"]');
				var checked = parseInt(select.val());

				if(isNaN(checked)) {
					//1er chargement on récupére la valeur min ou celle déja enregistrée
					//si elle a déja été validée
					checked = parseInt($('input[name$="datepicker_'+carrierId+'"]').val()) * 1000;
				} else {
					//on conserve la valeur précedemment selectionnée
					checked = new Date(checked);
					var checkedH = checked.getHours();
					checkedH = (checkedH < 10 ? '0'+checkedH : checkedH);
					var checkedMin = checked.getMinutes();
					checkedMin = (checkedMin < 10 ? '0'+checkedMin : checkedMin);
					checked = originTimeStamp + checkedH * 3600000 + checkedMin * 60000;
				}
				//si le timestamp checked n'est pas dans la plage d'ouverture du jour on l'enleve
				if(!(checked >= startTimeStamp && checked <= endTimeStamp)) {
					checked = false;
				}

				var options = select.find('option');
				if(options.length > 0)
					options.remove();

				var firstOption = false;
				for (var h = startTimeStamp; h <= endTimeStamp; h = h+pas) {
						var currentDate = new Date(h);
						var heure = currentDate.getHours();
						heure = (heure < 10 ? '0'+heure : heure);
						var minutes = currentDate.getMinutes();
						minutes = (minutes < 10 ? '0'+minutes : minutes);

						var option = $('<option />');
						option.val(h);
						option.text(heure+':'+minutes);
						if(checked == false) {
							checked = true;
							option.prop('selected', 'selected');
				        	$('input[name$="datepicker_'+carrierId+'"]').val(h/1000);
						} else if(checked ==  h) {
							option.prop('selected', 'selected');
						}
						select.append(option);
						if(!firstOption) {
							firstOption = option;
						}
				}

				var currentSelectedtimestamp = select.val() / 1000;
				var currentTimestamp = $('input[name$="datepicker_'+carrierId+'"]').val();
				if(currentTimestamp != currentSelectedtimestamp) {
					$('input[name$="datepicker_'+carrierId+'"]').val(currentSelectedtimestamp);
				}

			}
		}
	}
}

function formatNumber(number) {
	if(parseInt(number) < 10) {
		return '0'+parseInt(number);
	}
	return parseInt(number);
}

function getFormattedDate(date) {
	return formatNumber(date.getDate())+'/'+formatNumber(date.getMonth() + 1)+'/'+date.getFullYear()+' '+formatNumber(date.getHours())+':'+formatNumber(date.getMinutes())
}

function getIdCarrierReference(carrier) {
	if($('input[name$="carrier_ref_'+carrier+'"]').length)
		return parseInt($('input[name$="carrier_ref_'+carrier+'"]').val());
	return false;
}

function getIdCarrierVelo() {
	if($('input[name$="carrier_velo"]').length) {
		return parseInt($('input[name$="carrier_velo"]').val());
	}
	return false;

}

function getHorairesVelo() {
	var horairesVelo = $('input[name$="velo_hours"]').val();
	if(horairesVelo) {
		return horairesVelo.split('-');
	}
	return false;
}

$(document).ready(function(){
	if($('.date-heure').length > 0) {

		// events on select
		// selectionne la date dans le datetimepicker
		$('#checkout-delivery-step select.timepicker').on('change', function(e){
			var idCarrier = $(this).attr('name');
			idCarrier = idCarrier.replace('timepicker_', '');
			var currentDatepicker = $('#datepicker_'+idCarrier).data('datepicker');
			var selectdate = new Date(parseInt($(this).val()));
			currentDatepicker.selectDate(selectdate);
		});

		var datepickers = {};
		//recup des heures d'ouverture magasin et clickandcollect
		dataClickNCollect = $('input[name$="store_hours_clickandcollect"]').val();
		data = $('input[name$="store_hours"]').val();
		datepickers['store'] = initData(data);
		datepickers['clickandcollect'] = initData(dataClickNCollect);
		var storeSpecialDays = $('input[name$="store_special_days"]').val();
		if(storeSpecialDays) {
			storeSpecialDays = JSON.parse(storeSpecialDays);
		}
		var maxDate = $('input[name$="store_max_date"]').val();
		if(maxDate) {
			maxDate = new Date(maxDate);
		}

		$('.date-heure').each(function(index){
			var datepicker = $(this).find('.datepicker-here');
			if(datepicker.length > 0) {
				var carrierId = datepicker.attr('id').replace('datepicker_', '');
				var clickandcollect = $('input[name$="clickandcollect_'+carrierId+'"]');
				var horairesIndex = 'store';
	        	if(clickandcollect.length > 0) {
	        		if(clickandcollect.val() == 1)
	        			horairesIndex = 'clickandcollect';
	        	}
	        	//desactivation des jours fermés
	        	if(datepickers[horairesIndex]['closed']) {
					var disabledDays = datepickers[horairesIndex]['closed'];
				}
				// timestamp js = timestamp php x 1000
				var startDateTimestamp = parseInt($(this).find('input[name$="start_date_'+carrierId+'"]').val()) * 1000;
				var dateToSelect = parseInt($(this).find('input[name$="datepicker_'+carrierId+'"]').val()) * 1000;

				if(dateToSelect < startDateTimestamp) {
					// maj de la date avec le timestamp mini actuel
					dateToSelect = startDateTimestamp;
				}
				dateToSelect = new Date(dateToSelect);
				// Date de départ mini du timepicker heure et minutes incluses
				var dateFromTimestamp = new Date(startDateTimestamp);
				currentIdentifier = '' + dateFromTimestamp.getDay();

				var minDay = datepickers[horairesIndex]['horaires'][currentIdentifier]['start'];
				var maxDay = datepickers[horairesIndex]['horaires'][currentIdentifier]['end'];

				datepickers['datepicker_'+carrierId] = {};
				datepickers['datepicker_'+carrierId]['current'] = false;

				// Initialisation du select avec les bonnes valeurs
				var pas = 30* 60 * 1000; // affiche un horaires toutes les 30 minutes (timestamp js en ms)
				//un triger du select a lieu dés le début car on set la date déja selectionnée ou mini
				//getSelectTimePicker(carrierId, dateFromTimestamp, datepickers, horairesIndex, currentIdentifier, pas, dateFromTimestamp);

 				datepicker.datepicker({
			        timepicker: true,
			        language: 'fr',
			        minDate: dateFromTimestamp,
			        maxDate: maxDate,
			        minHours: minDay['hours'],
			        minMinutes: 0,
			        maxHours: maxDay['hours'],
			        maxMinutes: 59,
			        onRenderCell: function (date, cellType) {
			        	// rend les jours fermés non sélectionnables
				        if (cellType == 'day') {
				            var day = date.getDay();
				            // todo vérification horaires exceptionnels
				            // ouverture et horaires
				            var timestamp = date.getTime() / 1000;

				            var isDisabled = disabledDays.indexOf(day) != -1;
				            if(!isDisabled) {
				            	// jour ouvert, on verifie si fermeture exceptionnelle
				            	if(typeof storeSpecialDays[timestamp] != 'undefined') {
			        				if(storeSpecialDays[timestamp].fermeture == 1){
			        					isDisabled = true;
			        				} else {
			        					// horaires d'ouvertures exceptionnels
			        				}
				            	}
				            } else if(typeof storeSpecialDays[timestamp] != 'undefined') {
				            	if(storeSpecialDays[timestamp].fermeture == 0) {
		        					// horaires d'ouvertures exceptionnels
		        					isDisabled = false;
				            	}
				            }
				            return {
				                disabled: isDisabled
				            }
				        }
				    },
			        onSelect: function (fd, d, picker) {
			        	// si on selectionne le meme jour le selectedDates est undefined
			        	if(typeof picker.selectedDates[0] != 'undefined') {
				        	var datepickerID = $(picker.el).attr('id');
				        	var idCarrier = datepickerID.replace('datepicker_', '');
				        	var currentDate = picker.selectedDates[0];
				        	var currentIdentifier = ''+picker.selectedDates[0].getDay();
							var clickandcollect = $('input[name$="clickandcollect_'+carrierId+'"]');
				        	var horairesIndex = 'store';
				        	if(clickandcollect.length > 0) {
				        		if(clickandcollect.val() == 1)
				        			horairesIndex = 'clickandcollect';
				        	}
				        	//ne fait rien si selection d'un jour antérieur
				        	//verification si on est un autre jour que le jour courant
				        	if(currentIdentifier == datepickers[datepickerID]['current']) {
			        			// on est au jour en cours, changement d'heure et minutes pas besoin de tout mettre à jour
			        			updateDatePickerMinutes(picker, d, datepickers, horairesIndex, currentIdentifier);
			        		} else if(typeof datepickers[horairesIndex]['horaires'][currentIdentifier] != 'undefined') {
			        			// on change de jour, on met tout à jour et on change l'index courant
					        	updateDatePicker(picker, d, datepickers, horairesIndex, currentIdentifier);
				                datepickers[datepickerID]['current'] = currentIdentifier;
				        	}

				        	getSelectTimePicker(idCarrier, currentDate, datepickers, horairesIndex, currentIdentifier, pas, dateFromTimestamp);

				        	//maj de l'affichage du rdv et de l'input hidden
				        	// $('#rdv_'+idCarrier+' span').html(getFormattedDate(currentDate));
				        	$('input[name$="'+datepickerID+'"]').val(currentDate.getTime()/1000);

				        }
			        }
			    });
			    if(typeof dateToSelect != 'undefined') {
				    // initialistion du premier affichage
				  	var currentDatepicker = datepicker.data('datepicker');
				  	// selection de la date enregistre ou de la date mini sur le timepicker
				  	currentDatepicker.selectDate(dateToSelect);
				  	// $('#rdv_'+carrierId+' span').html(getFormattedDate(dateToSelect));
				}
			}
		});
	}
});


