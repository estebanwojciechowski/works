
import $ from 'jquery';

export default class AirDatepicker {
    constructor(el) { }
    init(){        
        // Initialization
            $('#datepicker-here').datepicker({
                language: 'fr',
                navTitles : {
                    days: 'MM'
                }
            });
            // Access instance of plugin
            $('#datepicker-here').data('datepicker');
    }
}