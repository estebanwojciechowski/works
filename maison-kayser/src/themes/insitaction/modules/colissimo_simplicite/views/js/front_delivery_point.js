
$(document).ready(function ()
{
    $('a#showColissimoModal').on('click', function(){
        if (!$('#widget-container').html()) {
           generateMap();
        }
    });
    $('a#removeColissimoRelais').on('click', function(){
       deleteDeliveryPoint(moduleLink);
       return false;
    });
});

var generateMap = function () {
    $(function () {
        $('#widget-container').frameColiposteOpen({
            "ceLang": soInputs.ceLang,
            "callBackFrame": "callBackFrame",
            "ceCountryList": "FR",
            "dyPreparationTime": soInputs.dyPreparationTime,
            "ceAddress": soInputs.ceAddress,
            "ceZipCode": soInputs.ceZipCode,
            "ceTown": soInputs.ceTown,
            "ceCountry": soInputs.cePays,
            "token": soInputs.token,
            "point": soInputs.point
        });

    });
}

function callBackFrame(point) {
    saveDeliveryPoint(point, moduleLink);
}