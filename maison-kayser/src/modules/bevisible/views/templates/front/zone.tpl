{if $zone}
    <h2>{if $country}{$country->name}{else}{$zone->name}{/if}</h2>
{/if}
{if isset($countries) && $countries}
    {foreach from=$countries item=country}
        <ul>
            <li>
                {assign var=country_id value='-'|cat:$country->id}
                <h3>
                    <a href="{$link->getModuleLink(
                                'bevisible', 
                                'map-zone', 
                                [
                                    'id_zone' => $zone->id_bevisible_group,
                                    'id_country' => $country_id,
                                    'wanted_url' => Tools::link_rewrite(current($country->name))
                                ]
                            )}{if $back}?back={$back}{/if}">
                        {current($country->name)}
                    </a>
                </h3>
            </li>
        </ul>
    {/foreach}
{elseif !empty($stores)}
    {foreach from=$stores item=store} 
        <div class="shop__item">
            <div class="picto picto--location"></div>
            <h3 class="shop__title">
                <a href="{$link->getModuleLink(
                                    'bevisible', 
                                    'map-store', 
                                    [
                                        'id_store' => $store.id_store,
                                        'wanted_url' => Tools::link_rewrite($store.name)
                                    ]
                                )}{if $back}?back={$back}{/if}">
                            {$store.name}
                </a>
            </h3>
            <div class="shop__adresse">
                {$store.address1}
                {if strlen(trim($store.address2))}
                    <br/>{$store.address2}
                {/if}  
                <br/>{$store.postcode} {$store.city}
            </div>
            <div class="shop__horaire">
                {assign var=today value=($smarty.now|date_format:'n')-1}
                {if $today < 0}
                    {if $today == -1}
                        {assign var=today value=5}
                    {else}
                        {assign var=today value=6}
                    {/if}
                {/if} 
                
                {assign var=closed value=true}
                {if isset($store['hours'][$today])}
                    {if !$store['hours'][$today]}
                        {l s='Fermé le'} {$bevisible_days_translations[$today]}
                    {else}
                        {assign var=closed value=false}
                        {l s='Ouvert de'} {$store['hours'][$today]}
                    {/if}
                {/if}
                {if !$closed}
                {foreach $store.hours as $key => $day}
                    {if !$day}
                        {l s=', fermé le'} {$bevisible_days_translations.$key}
                    {/if}
                {/foreach}
                {/if}               
            </div> 
            <a href="{$link->getModuleLink(
                                    'bevisible', 
                                    'map-store', 
                                    [
                                        'id_store' => $store.id_store,
                                        'wanted_url' => Tools::link_rewrite($store.name)
                                    ]
                                )}{if $back}?back={$back}{/if}" class="button button--secondary">
                            {l s="Voir la fiche"}</a>
        </div>
    {/foreach}
{else}
    {if isset($services) && $services}
        <p class="h3 text-center">
        {if isset($services.cafe) && $services.cafe}
            {l s='Aucune boulangerie avec café n\'a été trouvée.'}
        {elseif isset($services.resto) && $services.resto}
            {l s='Aucune boulangerie avec restaurant n\'a été trouvée.'}
        {elseif isset($services.sansgluten) && $services.sansgluten}
            {l s='Aucune boulangerie avec corner sans gluten n\'a été trouvée.'}
        {/if}
        </p>
    {else}
        <p class="h3 text-center">{l s='Aucune boulangerie n\'a été trouvée.'}</p>
    {/if}
{/if}
 