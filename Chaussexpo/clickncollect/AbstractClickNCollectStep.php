<?php
/**
 * @author    Esteban Wojciechowski <estebanw@insitaction.com>
 * @copyright 2017 INSITACTION
 */


use Symfony\Component\Translation\TranslatorInterface;

abstract class AbstractClickNCollectStepCore implements ClickNCollectStepInterface
{
    private $smarty;
    private $translator;
    private $clickncollectProcess;

    private $title;

    protected $step_is_reachable = false;
    protected $step_is_complete = false;
    protected $step_is_current = false;
    protected $context;

    protected $template;
    protected $content_only = false;
    protected $unreachableStepTemplate = 'clickncollect/_partials/steps/unreachable.tpl';

    public function __construct(Context $context, TranslatorInterface $translator)
    {
        $this->context = $context;
        $this->smarty = $context->smarty;
        $this->translator = $translator;
    }

    public function setTemplate($templatePath)
    {
        $this->template = $templatePath;

        return $this;
    }

    public function getTemplate()
    {
        if ($this->isReachable()) {
            return $this->template;
        } else {
            return $this->unreachableStepTemplate;
        }
    }

    protected function getTranslator()
    {
        return $this->translator;
    }

    public function renderType(){
        return $this->content_only;
    }

    public function setRenderType($content_only)
    {
        $this->content_only = $content_only;

        return $this;
    }

    protected function renderTemplate($template, array $extraParams = array(), array $params = array())
    {
        $defaultParams = array(
            'title' => $this->getTitle(),
            'step_is_complete' => (int) $this->isComplete(),
            'step_is_reachable' => (int) $this->isReachable(),
            'step_is_current' => (int) $this->isCurrent(),
            'content_only' => (int) $this->renderType()
        );

        $scope = $this->smarty->createData(
            $this->smarty
        );

        $scope->assign(array_merge($defaultParams, $extraParams, $params));

        $tpl = $this->smarty->createTemplate(
            $template,
            $scope
        );

        return $tpl->fetch();
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setClickNCollectProcess(ClickNCollectProcess $clickncollectProcess)
    {
        $this->clickncollectProcess = $clickncollectProcess;

        return $this;
    }

    public function getClickNCollectProcess()
    {
        return $this->clickncollectProcess;
    }

    public function getClickNCollectSession()
    {
        return $this->getClickNCollectProcess()->getClickNCollectSession();
    }

    public function setReachable($step_is_reachable)
    {
        $this->step_is_reachable = $step_is_reachable;

        return $this;
    }

    public function isReachable()
    {
        return $this->step_is_reachable;
    }

    public function setComplete($step_is_complete)
    {
        $this->step_is_complete = $step_is_complete;

        return $this;
    }

    public function isComplete()
    {
        return $this->step_is_complete;
    }

    public function setCurrent($step_is_current)
    {
        $this->step_is_current = $step_is_current;

        return $this;
    }

    public function isCurrent()
    {
        return $this->step_is_current;
    }

    public function getIdentifier()
    {
        // SomeClassNameLikeThis => some-class-name-like-this
        return Tools::camelCaseToKebabCase(get_class($this));
    }

    public function getDataToPersist()
    {
        return array();
    }

    public function restorePersistedData(array $data)
    {
        return $this;
    }
}
