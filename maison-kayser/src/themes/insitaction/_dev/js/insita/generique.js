import $ from 'jquery';

class Generique {
    
    constructor() {
        this.scrollTo = new ScrollTo();
    }

    init(){
        $("a.scroll-to[href^='#']").on("click", (e)=>{
            e.preventDefault();
            this.scrollTo.animate($($(e.currentTarget).attr("href")), 500);
        });

        //Lien bloc visual composer
        $('.shp__professionnels, .shp__cocktails__et__rec, .shp__hotels__et__restau').on('click', function() {
            window.location = $(this).find('a').attr('href');
        });

        $('.job').on('click', function() {
            window.location = $(this).find('a').attr('href');
        });
    }
}

class ScrollTo {
    constructor() { }
    animate(element, speed) {
        if( element == "undefined" )
            return false;

        if( typeof element == "string" )
            element = $(element);

        if( speed == "undefined" )
            speed = 500;

        // Position top of element default
        let positionTop = element.offset().top;

        // If header is fixed -- change position top of element - header height
        /*
        if( $('#header').css('position') == "fixed" ){
            let headerHeight = $('#header').height();
            positionTop = positionTop - headerHeight;
        }
        */
        
        // Animate top function
        $('body, html').animate({
            scrollTop: positionTop
        }, speed);
    }
}


export { Generique, ScrollTo }