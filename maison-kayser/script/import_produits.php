<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
include_once 'vendor/autoload.php'; // logger
include_once(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Class d'import produit initial // pas de maj détruit toute les données si le test du csv est bon
 */
class Import {

    private static $fields = array(
        0   => 'id_product',                    // Identifiant
        1   => 'id_sage',                       // Identifiant SAGE
        2   => 'name',                          // Nom produit 
        3   => 'main_category',                 // Famille 
        4   => 'sub_category',                  // Sous famille  
        5   => 'description',                   // Description 
        6   => 'CARAC_COMPOSITION',             // Composition 
        7   => 'CARAC_ALLERGENES',              // Allergènes  
        8   => 'CARAC_VALEURS_NUTRITIONNELLES', // Valeurs Nutritionnelles 
        9   => 'CARAC_NB_PIECES',               // Nb pièces 
        10  => 'CARAC_NB_CONVIVES',             // Nb convives 
        11  => 'CARAC_POIDS_NET',               // Poids net (g) 
        12  => 'weight',                        // Poids total (g) 
        13  => 'depth',                         // Longueur colis (cm) 
        14  => 'width',                         // Largeur colis (cm)  
        15  => 'height',                        // Hauteur Colis (cm)  
        16  => 'formule',                       // Formule
        17  => 'delai_fabrication_avant_11h',   // Délai de re-fabrication (en h) Commande AVANT 11h  
        18  => 'delai_fabrication_apres_11h',   // Délai de re-fabrication (en h) Commande APRES 11h
        19  => 'delai_clickandcollect',         // Délai Click&Collect 
        20  => 'delai_livraison_velo',          // Délai livraison (vélo)  
        21  => 'delai_livraison_voiture',       // Délai livraison (Voiture) 
        22  => 'delai_colissimo',               // Délai Colissimo 
        23  => 'CARAC_CONSERVATION',            // Conseil de conservation 
        24  => 'CARAC_VEGETARIEN',              // Végétarien  
        25  => 'CARAC_VEGAN',                   // Vegan 
        26  => 'CARAC_GLUTENFREE',              // Sans gluten 
        27  => 'DECLI_TRANCHE',                 // Tranché 
        28  => 'DECLI_CHAUD_FROID',             // Chaud / froid
        29  => 'active',                        // Actif ?
    );

    private static $price_fields = array(
        0   => 'id_product',                    // Identifiant produit
        1   => 'name',                          // nom produit
        2   => 'id_stores',                     // Identifiant boutique
        3   => 'prix_ttc',                      // Prix ttc 
        4   => 'tva',                           // TVA en %
    );
 
    private static $constantes = array(
        'CARAC_COMPOSITION'             => 1,
        'CARAC_ALLERGENES'              => 2,
        'CARAC_VALEURS_NUTRITIONNELLES' => 3,
        'CARAC_NB_PIECES'               => 4,
        'CARAC_NB_CONVIVES'             => 5,
        'CARAC_POIDS_NET'               => 6,
        'CARAC_CONSERVATION'            => 7,
        'CARAC_VEGETARIEN'              => 8,
        'CARAC_VEGAN'                   => 9,
        'CARAC_GLUTENFREE'              => 10, 

        'DECLI_CHAUD_FROID'             => 1,
        'DECLI_TRANCHE'                 => 2,

        // ID TAX RULES_GROUP
        'TAX_FR_20'                     => 1,
        'TAX_FR_10'                     => 2,
        'TAX_FR_5.5'                    => 3,
        'TAX_FR_2.1'                    => 4,
        'TAX_FR_0'                      => 0,
    );
    

    private $_logger;

    protected $errors = array();
    protected $products = array();
    protected $products_prices = array();
    protected $products_max_prices = array();
    protected $categories = array();
    protected $sub_categories = array();
    protected $default_id_lang;
    protected $attributes_by_groups = array();

    public $dirImportFile = ''; 
    public $fileList = array(
        'produits.csv',
        'prix.csv',
    );

    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  

        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $results = DB::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'attribute` WHERE `id_attribute_group` IN ('. self::$constantes['DECLI_CHAUD_FROID'].', '. self::$constantes['DECLI_TRANCHE'].')');
        if($results && count($results)) {
          foreach ($results as $key => $value) {
            if(!isset($this->attributes_by_groups[$value['id_attribute_group']]))
              $this->attributes_by_groups[$value['id_attribute_group']] = array();
            $this->attributes_by_groups[$value['id_attribute_group']][$value['id_attribute']] = array('id_attribute' => $value['id_attribute']);
          }
        } 
        $this->dirImportFile = dirname(__FILE__) . '/../import/';
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {
        if($this->readFiles()) { 
            if(count($this->products)) {   

            //import from scratch
            // $this->doImportFromScratch();

            // Maj des prix
              $this->updatePricesOnly();
          }
        }         
    }

    private function doImportFromScratch() {
      $result = $this->cleanBDD();
      if($result) {
        //la bdd a été nettoyé on commence les imports
        if($this->addCategories()) {
          $this->addProducts(); 
        }
      }
    }

    private function updatePricesOnly() {    

        foreach ($this->products_prices as $id_product => &$product) {
          $this->log("Mise a jour des prix produits ID#".$id_product);
          $this->updateShopsPrices($id_product, true);
        }
    }

    private function cleanBDD() {

        // Clean BDD products // categories
        $clean = DB::getInstance()->execute(
                  // reinit des categories
                ' TRUNCATE `'._DB_PREFIX_.'category`;
                  TRUNCATE `'._DB_PREFIX_.'category_group`;
                  TRUNCATE `'._DB_PREFIX_.'category_lang`;
                  TRUNCATE `'._DB_PREFIX_.'category_product`;
                  TRUNCATE `'._DB_PREFIX_.'category_shop`;

                  TRUNCATE `'._DB_PREFIX_.'product`; 
                  TRUNCATE `'._DB_PREFIX_.'product_attachment`; 
                  TRUNCATE `'._DB_PREFIX_.'product_attribute`; 
                  TRUNCATE `'._DB_PREFIX_.'product_attribute_combination`; 
                  TRUNCATE `'._DB_PREFIX_.'product_attribute_image`; 
                  TRUNCATE `'._DB_PREFIX_.'product_attribute_shop`; 
                  TRUNCATE `'._DB_PREFIX_.'product_carrier`; 
                  TRUNCATE `'._DB_PREFIX_.'product_country_tax`; 
                  TRUNCATE `'._DB_PREFIX_.'product_download`; 
                  TRUNCATE `'._DB_PREFIX_.'product_group_reduction_cache`; 
                  TRUNCATE `'._DB_PREFIX_.'product_lang`; 
                  TRUNCATE `'._DB_PREFIX_.'product_sale`; 
                  TRUNCATE `'._DB_PREFIX_.'product_shop`; 
                  TRUNCATE `'._DB_PREFIX_.'product_supplier`; 
                  TRUNCATE `'._DB_PREFIX_.'product_tag`;

                  TRUNCATE `'._DB_PREFIX_.'feature_value`; 
                  TRUNCATE `'._DB_PREFIX_.'feature_value_lang`; 
                  TRUNCATE `'._DB_PREFIX_.'feature_product`;

                  ALTER TABLE  `'._DB_PREFIX_.'feature_value_lang`
                  MODIFY COLUMN `value` TEXT; 
                  '
            );
        if(!$clean) {
          $this->log("ERREUR Clean BDD KO");
          return false;
        }
        $this->log("Clean BDD OK");
        return true;
    }

    protected function readFiles() {
        $this->log("Lecture des informations contenues dans le CSV");
        foreach ($this->fileList as $file) { 
            if(!$this->readFile($this->dirImportFile . $file, $file)) {
              return false;
            }
        }
        $this->log('Produits initialisés');
        return true;
    }

    /**
     * [readFile Lis la liste des fichiers]
     * @param  [type] $file     [chemin complet du fichier]
     * @param  [type] $fileName [nom du fichier]
     * @return [type]           [boolean]
     */
    protected function readFile($file, $fileName) {

        if (!file_exists($file)) {
            $this->log("ERREUR le fichier " . $fileName . " n\'a pas été trouvé");
            return false;
        }

        $this->log("le fichier " . $fileName . " a été trouvé");
        $fp = file($file);  

        if (($handle = fopen($file, "r")) !== false) { 
            $row = 0;
            while (($data = fgetcsv($handle, 0, ";")) !== false) { 
              if(is_array($data) && count($data) == count(self::$fields)) {
                //traitement squelette des produits -> nom / description ...
                $this->initProduct($data, $row);
              } elseif(is_array($data) && count($data) == count(self::$price_fields)) {
                //traitement association des produits aux boutique + tarif
                 $this->initPrices($data, $row);
              } else {
                $msg = 'ERREUR : le nombre de colonnes du csv ne correspond pas. CSV : '.count($data).' / Config : '.count(self::$fields);
                 $this->log($msg);
                 $this->errors[] = $msg;
                 return false;
              }
              $row++; 
            }
            fclose($handle);
            unset($handle); 
        } else {
            $msg = "ERREUR Impossible de lire le fichier " . $fileName;
            $this->errors[] = $msg;
            $this->log($msg);
            unset($fp);
            return false;
        }
        unset($fp);
        return true;
    }

    /**
     * [initProduct assigne les elements du csv dans un tableau de produit et test les champs transmis]
     * @param  [type]  $csv_row [ligne de csv]
     * @param  integer $nb      [numero de ligne] 
     */
    protected function initProduct($csv_row, $nb = 0) {
        $this->log('Initialisation du produit');
        $errors = array(); 
        //objet product utilisé pour la validation des champs
        $product = new Product();
        $product_array = array();
        $definition = Product::$definition['fields'];
        $id_product = null;
        $main_category = ''; 
        foreach ($csv_row as $key => $value) {  
          if(self::$fields[$key] == 'id_product') {
            $id_product = (int) $value; 
            if(!$id_product) {
              // si l'id produit est invalide on stoppe le traitement
              // on empeche pas l'import on assume, c'est des lignes vides..
              // bon on log quand même on sait jamais
              $msg =  'LIGNE '.$nb.' '.self::$fields[$key].' invalide : "'.addslashes($value).'"';
              $errors[self::$fields[$key]] = $msg; 
              break;
            }
            $product_array = array(
                                'product' => array('id_product' => $id_product),
                                'features' => array(),
                                'attributes' => array(),
                                'category' => array(),
                                'sub_category' => array()
                              );
            continue;
          }
          // si la propriété existe il n'y a pas d traitement spécifique
          if(property_exists($product, self::$fields[$key])) {            
            $lang = (isset($definition[self::$fields[$key]]) && isset($definition[self::$fields[$key]]['lang']) && $definition[self::$fields[$key]]['lang'] ? true : false);
            // force le type des champs numériques
            if(in_array(self::$fields[$key], array('delai_clickandcollect', 'width', 'depth', 'height', 'weight'))) {
              // si c'est non on laisse a null
              if(strtolower(trim($value)) == 'non') {
                continue;
              }
              $value = floatval(str_replace(',', '.', $value));
            } elseif(in_array(self::$fields[$key], array('delai_fabrication_avant_11h', 'delai_fabrication_apres_11h', 'delai_livraison_velo', 'delai_livraison_voiture', 'delai_colissimo'))) {
              // si c'est non on laisse a null
              if(strtolower(trim($value)) == 'non') {
                continue;
              }
              $value = intval($value);
            }
            // test validation du champ
            $id_lang = ($lang ? $this->default_id_lang : null); 
            if($product->validateField(self::$fields[$key], $value, $id_lang)) {
              $product_array['product'][self::$fields[$key]] = $value; 
            } else {
              $msg =  'ERREUR LIGNE '.$nb.' '.self::$fields[$key].' invalide : "'.addslashes($value).'"';
              $errors[self::$fields[$key]] = $msg;
              $this->log($msg);
            }  
          } else {
              // traitement des features et attributes
              if(strpos(self::$fields[$key], 'CARAC_') !== false) {
                // feature 
                if(isset(self::$constantes[self::$fields[$key]])) {
                  $id_feature = self::$constantes[self::$fields[$key]];
                  if(Validate::isGenericName($value)) {
                    $tmp_val = strtolower(trim($value));
                    if(strlen(trim($tmp_val))) { 
                      if($tmp_val == 'non' && in_array(self::$fields[$key], array('CARAC_NB_CONVIVES', 'CARAC_NB_PIECES'))) {
                        //si pas de nombre de convive ou pieces on laisse à null
                        continue;
                      }
                      $product_array['features'][$id_feature] = $value;
                    }
                  } else {                         
                    $msg =  'ERREUR LIGNE '.$nb.' '.self::$fields[$key].' feature invalide';
                    $errors[self::$fields[$key]] = $msg;
                    $this->log($msg);
                  }
                } else {                  
                  $msg =  'ERREUR LIGNE '.$nb.' '.self::$fields[$key].' feature non définie';
                  $errors[self::$fields[$key]] = $msg;
                  $this->log($msg);
                }
              } elseif(strpos(self::$fields[$key], 'DECLI_') !== false) {
                // attribute
                if(isset(self::$constantes[self::$fields[$key]])) {
                  $id_attribute = self::$constantes[self::$fields[$key]];
                  if(strtolower($value) == 'oui') {
                    // decli a créer
                    $product_array['attributes'][$id_attribute] = true;
                  } 
                } else {                  
                  $msg =  'ERREUR LIGNE '.$nb.' '.self::$fields[$key].' attribute non défini';
                  $errors[self::$fields[$key]] = $msg;
                  $this->log($msg);
                }
              } elseif(strpos(self::$fields[$key], '_category') !== false) {
                // category 
                if(!Validate::isGenericName($value)) {                      
                  $msg =  'ERREUR LIGNE '.$nb.' '.self::$fields[$key].' categorie invalide';
                  $errors[self::$fields[$key]] = $msg;
                  $this->log($msg);
                  continue;
                }
                // si valide on continue
                if(self::$fields[$key] == 'main_category') {
                  $main_category = $value; 
                  $product_array['category'] = $value;
                  if(!in_array($value, $this->categories))
                    $this->categories[] = $value; 
                } else {
                  // pas d'id on les créera dans un 2eme temps
                  $product_array['sub_category'] = $value;
                  $this->sub_categories[] = array(
                      'name' => $value,
                      'parent' => $main_category,
                      'id_product' => $id_product,
                  );
                }                
              } elseif(self::$fields[$key] == 'formule') {
                // TODO Champ ignoré on les fera à la main
              } else {
                $msg =  'ERREUR LIGNE '.$nb.' '.self::$fields[$key].' champ indéfini';
                $errors[self::$fields[$key]] = $msg;
                $this->log($msg);
                print('existe pas : '. self::$fields[$key]."\n");
              }
          }
        } 
        if(empty($errors)) {
          $this->log('Ligne '.$nb.' Produit '.$id_product.' OK');
          $this->products[$id_product] = $product_array;
        } else {
          $this->log('ERREUR Ligne '.$nb.' Produit '.$id_product." KO \nERREURS :\n".print_r($errors, true)."\nLIGNE:\n".print_r($csv_row, true));
          $this->errors[] = $errors;
        } 
        unset($product);
        unset($errors);
        unset($definition); 
    }

    protected function initPrices($csv_row, $row) {
      $this->log('Initialisation des prix produit');
      $no_id = false;
      $id_product = false;
      $name = array();
      $id_stores = false; 
      $prix_ttc = false; 
      $tva = false; 

      foreach ($csv_row as $key => $value) {  
        if(isset(self::$price_fields[$key])) {
          switch (self::$price_fields[$key]) {
            case 'id_product':
              if(!$value || !is_numeric($value)) {
                $no_id = true;
                break 2;
              }
              $id_product = trim($value);
              break;

            case 'id_stores':
              $id_stores = explode(',', $value);
              break;
            
            case 'name':
              $name[$this->default_id_lang] = trim($value);
              break;

            case 'prix_ttc':
              $prix_ttc = floatval(str_replace(',', '.', $value));
              break;

            case 'tva':
              $tva = floatval(str_replace(',', '.', $value));
              break;
 
          }
        } 
      }

      if($id_product && $id_stores && $prix_ttc !== false && $tva !== false) {
        $data = array();
        $price = $prix_ttc / (1 + (abs($tva) / 100));

        if(!isset($this->products_max_prices[$id_product])) {
          $this->products_max_prices[$id_product] = array(
                      'max_ttc' => $prix_ttc,
                      'max_ht' => $price
          );
        } elseif($this->products_max_prices[$id_product]['max_ttc'] < $prix_ttc) {
          $this->products_max_prices[$id_product] = array(
                      'max_ttc' => $prix_ttc,
                      'max_ht' => $price
          );
        }

        //recup ID TVA
        $id_tva = 0;
        if(isset(self::$constantes['TAX_FR_'.$tva])) {
            $id_tva = self::$constantes['TAX_FR_'.$tva];  
        } else {
          $msg = 'La TVA '.$tva.' est introuvable dans la BDD. Produit#'.$id_product.' ligne#'.$row;
          $this->log($msg);
          $this->errors[] = $msg;  
        }

        // on ajoute la tax_rule_group et le price au main_product 
        if(isset($this->products[$id_product]) 
            && isset($this->products[$id_product]['product'])) {
          //si un id_tva est set on l'ajout au produit sinon ça reste à null
          if($id_tva)
            $this->products[$id_product]['product']['id_tax_rules_group'] = $id_tva;
          //on assigne le 1er prix produit au produt global - il n'est pas affiché sur la boutique internationale
          if($price && !isset($this->products[$id_product]['product']['price']))
            $this->products[$id_product]['product']['price'] = number_format($price, 6);
        }

        $shops = Shop::getShops(false); 

        foreach ($id_stores as $key => $id_store) { 
          $id_store = trim($id_store);
          if(!isset($shops[$id_store])) { 
            $msg = 'ERREUR ligne#'.$row.' ID Produit#'.$id_product.' La boutique#'.$id_store.' est inexistante'; 
            $this->log($msg);
            continue;
          }

          $data[$id_store] = array(
                                  'price' => $price,
                                  'price_wt' => $prix_ttc,
                                  'id_tva' => $id_tva,
                                  'tva' => $tva,
                                  'name' => $name,                                              
                                );
        }

        $data[1] = array( 
                      'price' => $this->products_max_prices[$id_product]['max_ht'],
                      'price_wt' => $this->products_max_prices[$id_product]['max_ttc'],
                      'id_tva' => $id_tva,
                      'tva' => $tva,
                      'name' => $name,                                              
                    );
        if(isset($this->products_prices[$id_product])) {
          foreach ($data as $id_store => $value) {
              if(isset($this->products_prices[$id_product][$id_store]) && $id_store > 1) {
                $this->log('ERREUR Ligne#'.$row.' prix produit#'.$id_product.' id_store:'.$id_store.' DOUBLON');
              }
             $this->products_prices[$id_product][$id_store] = $value;
          } 
        } else {
          $this->products_prices[$id_product] = $data;
        }
        $this->log('Initialisation des prix produit#'.$id_product.' OK');
      } else {
          if($no_id) {
            $msg = 'ERREUR Ligne#'.$row.' pas d\'ID Produit';          
          } else {
            $msg = 'ERREUR Produit#'.$id_product.' ligne#'.$row.' prix impossible à initialiser.';
          }
          $this->log($msg);
          $this->errors[] = $msg;  
      }
    }

    protected function addCategories() {
      $this->log('Début d\'ajout des catégories root et accueil');      
      // ajout des catégories root et accueil
      // /!\ l'id parent passe automatiquement a 1
      $category = new Category;
      $category->id_parent = 0;
      $category->is_root_category = 1;
      $category->name = array($this->default_id_lang => 'Racine');
      $category->link_rewrite = array($this->default_id_lang => 'racine');
      if(!$category->save()) {
        $this->log('ERREUR Impossible de créer la catégorie root');
        return false;
      }
      $sql = 'UPDATE `'._DB_PREFIX_.'category` SET `id_parent` = 0 WHERE id_category = 1';
      DB::getInstance()->execute($sql);
      unset($category);
      $category = new Category;
      $category->id_parent = 1;
      $category->name = array($this->default_id_lang => 'Nos Produits');
      $category->link_rewrite = array($this->default_id_lang => 'nos-produits');
      if(!$category->save()) {
        $this->log('ERREUR Impossible de créer la catégorie Nos Produits');
        return false;
      }
      unset($category);
      $this->log('Fin d\'ajout des catégories root et accueil');
 
      if($this->addMainCategories())
        if($this->addSubCategories())
          return true;
      return false;
    }

    protected function addMainCategories() {
      if(count($this->categories)) {
       $this->log('Début d\'ajout des catégories principales');
        $categories = array();
        foreach ($this->categories as $key => $value) {
          $category = new Category;
          $category->id_parent = 2;
          $category->name = array($this->default_id_lang => $value);
          $category->link_rewrite = array($this->default_id_lang => Tools::link_rewrite($value));
          if($category->save()) {
            $categories[$category->id] = $value;
            $this->log('Ajout Catégorie #'.$category->id.' "'.addslashes($value).'" OK');
          } else {
            $msg = 'ERREUR Ajout Catégorie "'.addslashes($value).'" KO';
            $this->errors[] = $msg;
            $this->log($msg);     
            return false;   
          }
        }
        // maj des id catégories en clé du tableau
        $this->categories = $categories;
        $this->log('Fin d\'ajout des catégories principales');
      }
      return true; 
    }

    protected function addSubCategories() { 
      if(count($this->sub_categories)) { 
        $subcategories_by_parent = array();
        $this->log('Début d\'ajout des sous catégories');
        foreach ($this->sub_categories as $key => $subcategory) {
          $parent_id = array_search($subcategory['parent'], $this->categories);
          if($parent_id === false) {
            $msg = 'ERREUR recherche catégorie parente "'.addslashes($subcategory['parent']).'" pou la catégorie fille '.addslashes($subcategory['name']).' KO';
            $this->errors[] = $msg;
            $this->log($msg);   
            break;
          }
          // si on a déjà ajouté la sous catégorie on skip
          if(isset($subcategories_by_parent[$parent_id]) && in_array($subcategory['name'], $subcategories_by_parent[$parent_id]))
            continue;
          $this->log('Catégorie parente : '.$parent_id.' '.$this->categories[$parent_id]);
          $category = new Category;
          $category->id_parent = $parent_id;
          $category->name = array($this->default_id_lang => $subcategory['name']);
          $category->link_rewrite = array($this->default_id_lang => Tools::link_rewrite($subcategory['name']));
          if($category->save()) {  

            if(!isset($subcategories_by_parent[$parent_id]))
              $subcategories_by_parent[$parent_id] = array();
            $subcategories_by_parent[$parent_id][$category->id] = $subcategory['name'];

            $this->log('Ajout Catégorie #'.$category->id.' "'.addslashes($subcategory['name']).'" OK');
          } else {
            $msg = 'ERREUR Ajout Catégorie "'.addslashes($subcategory['name']).'" KO';
            $this->errors[] = $msg;
            $this->log($msg);       
            return false;      
          }
          $this->sub_categories = $subcategories_by_parent;
        }
        $this->log('Fin d\'ajout des sous catégories');
      }
      return true; 
    }

    protected function addProducts() {
      if(count($this->products)) { 
        $this->log('Début d\'ajout des produits');
        //pour etre sur de démarrer au bon auto increment 
        $auto_increment = key($this->products);
        $sql = 'ALTER TABLE `'._DB_PREFIX_.'product` AUTO_INCREMENT='. $auto_increment;
        if(DB::getInstance()->execute($sql)) {
          $this->log('La table `'._DB_PREFIX_.'product` a été modifiée AUTO_INCREMENT='.$auto_increment);
        } else {
          $this->log('ERREUR La table `'._DB_PREFIX_.'product` n\'a été modifiée AUTO_INCREMENT='.$auto_increment);
          return false;
        }
        $last_index = false;
        foreach ($this->products as $id_product => &$product) {
          if($last_index && $id_product != ($last_index+1)) {
            // si un ID produit est manquant (on passe de 129 a 131 par exemple) 
            // on place l'auto increment au niveau de l'id produit courant
            $sql = 'ALTER TABLE `'._DB_PREFIX_.'product` AUTO_INCREMENT='. $id_product;
            DB::getInstance()->execute($sql); 
          }
          $last_index = $id_product;
          $id_category_default = array_search($product['category'], $this->categories);
          if($id_category_default !== false) {
            $product['product']['id_category_default'] = $id_category_default;
            if($obj = $this->addMainProduct($product['product'])) {  

              $this->associateCategory($obj, $id_category_default, $product['sub_category']); 

              //ajout des prix aux produits par boutique
              if(isset($this->products_prices[$id_product])) {
                // le booleen permet de mettre à jour uniquement le prix
                // le nom est present aussi dans le fichier csv des prix
                // on peut le mettre a jour en passant a false
                $this->updateShopsPrices($id_product, true);
              }
              //ajout des caracteristiques
              if(isset($product['features']) && count($product['features'])) {
                $this->addProductFeatures($obj, $product['features']); 
              }
              //ajout des déclis
              if(isset($product['attributes']) && count($product['attributes'])) {
                $this->addProductAttributes($obj, $product['attributes']); 
              }  

            } else {                  
              $this->log('ERREUR Le #'.$id_product.' n\'a pas été créé');
            }
          } else {
            $this->log('ERREUR La catégorie par défaut du produit #'.$id_product.' n\'a pas été trouvé');
          } 
        }
        $this->log('Fin d\'ajout des produits');
      }
      return true;
    }

     protected function updateShopsPrices($id_product, $price_only = false) {
      $this->log('Début updateShopsPrices');
      if(isset($this->products_prices[$id_product]) 
            && count($this->products_prices[$id_product])) {
        
        $sql = '';
        $sql_product = 'UPDATE `'._DB_PREFIX_.'product` SET ';
        $sql_shop = 'UPDATE `'._DB_PREFIX_.'product_shop` SET ';
        $sql_lang = 'UPDATE `'._DB_PREFIX_.'product_lang` SET ';

        // maj des prix produits avec le prix max si des boutiques ne sont pas renseignées dans le csv
        // le prix max sera appliqué comme sur la boutique internationale
        if(isset($this->products_prices[$id_product]) 
            && isset($this->products_prices[$id_product][1])) {
          
          $main_shop_values = $this->products_prices[$id_product][1];
         
          if(isset($main_shop_values['price']) && isset($main_shop_values['id_tva'])) {            
            // maj du prix du produit dans ps_product et ps_product_shopp
            // (l'import from scratch ne prenait pas le prix max à l'origine)
           
            $price = number_format($main_shop_values['price'], 6);

            $sql_main_shop = 'UPDATE `'._DB_PREFIX_.'product` SET `price` = '.$price.',  `id_tax_rules_group` = '.(!$main_shop_values['id_tva'] ? "'NULL'" : $main_shop_values['id_tva']).'
                        WHERE `id_product` = '.$id_product.'; ';
              
            $sql_main_shop .= 'UPDATE `'._DB_PREFIX_.'product_shop` SET `price` = '.$price.',  `id_tax_rules_group` = '.(!$main_shop_values['id_tva'] ? "'NULL'" : $main_shop_values['id_tva']).'
                        WHERE `id_product` = '.$id_product.'; ';

            if(DB::getInstance()->execute($sql_main_shop)) {
              $this->log('La mise à jour des prix produits au prix max a été effectuée avec succès.');
            } else {
              $this->log('ERREUR La mise à jour des prix produits au prix max fail.');
            }            
          }
        }

        // construction de la requete d'update
        foreach ($this->products_prices[$id_product] as $id_store => $fields) {
          if(isset($fields['price']) && isset($fields['id_tva'])) {
            $price = number_format($fields['price'], 6);
            $values = ' `price` = '.$price.',  `id_tax_rules_group` = '.(!$fields['id_tva'] ? "'NULL'" : $fields['id_tva']).'
                        WHERE `id_product` = '.$id_product.' AND `id_shop` = '.$id_store.'; ';
            $sql .= $sql_shop.$values;            
          }
          if(!$price_only && isset($fields['name']) && !empty($fields['name']) && isset($fields['name'][$this->default_id_lang])) {
            $values = ' `name` = "'.pSQL($fields['name'][$this->default_id_lang]).'"
                        WHERE `id_product` = '.$id_product.' AND `id_shop` = '.$id_store.'; ';
            $sql .= $sql_lang.$values;
          }
        }

        if(strlen($sql)) {
          if(DB::getInstance()->execute($sql)) {
            $this->log('La mise à jour des prix produits a été effectuée avec succès.');
          } else {
            $this->log('ERREUR La mise à jour des prix produits fail.');
          }
        }
      }
      $this->log('Fin updateShopsPrices');
    }

    protected function addMainProduct($product_fields = array()) {
      $product = new Product;
      $langues = Language::getLanguages();
      foreach ($product_fields as $key => $value) { 
        $product->{$key} = $value;
        if($key == 'name') {
          $link_rewrite = array();
          $name = array();
          foreach ($langues as $key => $langue) {
            $name[$langue['id_lang']] = $value;
            $link_rewrite[$langue['id_lang']] = $value;
          }
          $product->name = $name;
          $product->link_rewrite = $this->getLinkRewriteFromName($link_rewrite);
          $product->meta_title = $value;
        } elseif($key == 'description' && strlen(trim($value))) {
          $meta_description = array();
          foreach ($langues as $key => $langue) {
            $meta_description[$langue['id_lang']] = $value;
          }
          $product->meta_description = $this->getMetaDescription($meta_description);
        }
      } 

      if($product->save()){
        $this->log('Produit #'.$product_fields['id_product'].' ajouté à l\'ID#'.$product->id);
        return $product;
      }
      return false;
    }

    protected function getLinkRewriteFromName($name = array()) {
        $link_rewite = array();
        foreach ($name as $id_lang => $value) {
          $link_rewite[$id_lang] = Tools::link_rewrite($value);
        }
        return $link_rewite;
    }

    protected function getMetaDescription($description = array()) {
        $meta_description = array(); 
        foreach ($description as $id_lang => $value) {
          // les meta description ne peuvent pas avoir de retour chariot
          // de caracteres : ^<>={} 
          $value = str_replace(array("^", "<", ">", "=", "{", "}","\n", "\t", "\r"), '', $value);

          $meta_description[$id_lang] = mb_substr($value, 0, 240); 
        } 
        return $meta_description;
    }

    protected function associateCategory($product, $id_category_default = false, $subcategory = array()) {
      // recupére les sous catégories éligibles
      if(isset($this->sub_categories[$id_category_default])) {
        $id_subcategory = array_search($subcategory, $this->sub_categories[$id_category_default]);
        if($id_subcategory !== false) {
          if($product->addToCategories(array($id_category_default, $id_subcategory))) {
            $this->log('Le produit #'.$product->id_product.' a été ajouté à la catégorie #'.$id_subcategory.' '.$subcategory);
          } else {
            $this->log('ERREUR Le produit #'.$product->id_product.' n\'a pas été ajouté à la catégorie #'.$id_subcategory.' '.$subcategory);
          }
        } else {          
            $this->log('ERREUR Le produit #'.$product->id_product.' n\'a pas été ajouté à la catégorie '.$subcategory.', elle n\'a pas été trouvée pour la categorie parente #'.$id_category_default);
        }
      }
    }

    protected function addProductFeatures($product, $features = array()) {
      if(count($features)) {
        foreach ($features as $id_feature => $feature_value) { 
          $id_feature_value = FeatureValue::addFeatureValueImport($id_feature, $feature_value, $product->id_product, $this->default_id_lang); 
          // ajout de la feature si elle n'existe pas déjà
          if($id_feature_value) {
            if(Product::addFeatureProductImport($product->id_product, $id_feature, $id_feature_value)) {
              $this->log('La feature #'.$id_feature.' a été ajouté au produit #'.$product->id_product);
            } else {
              $this->log('ERREUR La feature #'.$id_feature.' n\'a pas été ajouté au produit #'.$product->id_product);
            }
          } else {
              $this->log('ERREUR pendant la création de la feature #'.$id_feature.' '.$feature_value);            
          }
        }
      }
    }

    protected function addProductAttributes($product, $attributes = array()) {
     if($nb_declis = count($attributes)) {
      $group_name = false;
      $attributes_combinations = array();    
      if($nb_declis == 2) {
        // genere les combinaisons de déclis possibles
        // au cas ou, chaud/froid + tranché    
        $group_name = 'Chaud/Froid + Tranché';
        $id_attribute_group = key($attributes);
        $id_attribute_group_2 = next($attributes);
        if(isset($this->attributes_by_groups[$id_attribute_group]) 
            && isset($this->attributes_by_groups[$id_attribute_group_2])) {
          $attributes_traites = array();
          foreach ($this->attributes_by_groups[$id_attribute_group] as $attribute) {
            foreach ($this->attributes_by_groups[$id_attribute_group_2] as $attribute_2) {
              if(!in_array($attribute['id_attribute'].'_'.$attribute_2['id_attribute'], $attributes_traites)){
               $attributes_combinations[] = array($attribute['id_attribute'], $attribute_2['id_attribute']);
               $attributes_traites[] = $attribute['id_attribute'].'_'.$attribute_2['id_attribute'];
              }
            }
          }
        }
      } elseif($nb_declis == 1) {
        // traitement si attribut tranché ou chaud froid uniquement
        $id_attribute_group = key($attributes);
        $group_name = $id_attribute_group == 1 ? 'Chaud/Froid' : 'Tranché';
        if(isset($this->attributes_by_groups[$id_attribute_group])) {
          foreach ($this->attributes_by_groups[$id_attribute_group] as $attribute) {
            $attributes_combinations[] = array($attribute['id_attribute']);
          }
        }
      }
      if(!empty($attributes_combinations)) {
          // la premiere décli est mise par défaut pour le produit
          $default = true;
          foreach ($attributes_combinations as $attributes) {
            //on ajoute une combinaison sans modification de tarif ni poids
            $price = 0;
            $weight = 0;
            if($combination_id = $product->addAttribute(
                                                  $price, 
                                                  $weight, 
                                                  0, 
                                                  0, 
                                                  array(), 
                                                  $product->reference, 
                                                  $product->ean13,
                                                  $default
                                                )
            ) {
              $combination = new Combination($combination_id);
              // affecte l'attribut en cours à la décli tout fraichement créée
              $combination->setAttributes($attributes); 
              $this->log('L\'attribut #'.implode(',', $attributes).' du groupe '.$group_name.' a été ajouté avec succès'); 
              $default = false;
            } else {
               $this->log('ERREUR L\'attribut #'.implode(',', $attributes).' du groupe '.$group_name.' n\'a pas été ajouté'); 
            }
          }
        } else {        
          // TODO non traités si plus de 2 déclis
        }
      }            
    }

   

    protected function addImages() {
      // TODO
    }

    public function update($args) {
        
        /**
         * Gestion de la valeur par défaut du délimiteur
         */
        if (!$args['delimiter'])
            $args['delimiter'] = 100;

        if ($args['datas']) {
            while (count($args['datas']) > 0) {
                echo 'UPDATE EN COURS';
                echo "\r\n";

                $sousSQL = '';
                $ID = array();
                $var = 0;

                //on boucle sur toutes les données
                foreach ($args['datas'] as $key => $data) { // BOUCLE SUR LES CLES
                    // on sort de la boucle while si il n'y a plus de datas à traiter
                    $var += sizeof($data);
                    if (!$var) {
                        $sortiBoucle = true;
                        break;
                    } else
                        $sortiBoucle = false;

                    // début de la gestion de la requetes
                    //on boucle sur toutes les clés
                    $sousSQL .= '
                        `' . $key . '` = CASE';

                    // on boucle sur les lignes d'updates par champs
                    $o = 0;
                    foreach ($data as $k => $d) {

                        if ($o == $args['delimiter'])
                            break;

                        $val = ($d[$key] === '') ? "''" : ((is_null($d[$key])) ? 'NULL' : "'{$d[$key]}'");
                        $ID[] = $d[$args['identifier']];
                        
                        $sousSQL .= '
                            WHEN `' . $args['identifier'] . '` = "' . $d[$args['identifier']] . '" THEN ' . $val . '
                        ';
                        $o++;
                        unset($data[$k]);
                        unset($args['datas'][$key][$k]);
                    }
                    $sousSQL .= 'END,
                    ';

                    $ID = array_unique($ID);
                    $ID_stringified = implode(', ', $ID);
                }

                if ($sortiBoucle)
                    break;

                $sousSQL = substr(trim($sousSQL), 0, -1);

                $sql = 'UPDATE `' . $args['table'] . '` SET
                    ' . $sousSQL . '
                    WHERE `' . $args['identifier'] . '` IN (' . $ID_stringified . ');';

                DB::getInstance()->executeS($sql);
            }
        }
    }
}

$timestamp_debut = microtime(true);

$import = new Import();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$import->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");
 
if(function_exists('opcache_reset')) {
  opcache_reset();
  $import->log('opcache_reset OK'."\n");
}
