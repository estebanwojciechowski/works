<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php'); 
  
class PaymentAsso {
  

    protected $default_id_lang;

    private $_logger;

    private static $payment_module_id = array(
      'systempay' => 62
      // 'systempay' => 86,
      // 'cheque' => 6
    );

    protected $errors = array();


    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {
      $sql = 'TRUNCATE `'._DB_PREFIX_.'module_carrier` ';
      $results = DB::getInstance()->execute($sql);

      $sql = 'SELECT `id_shop` FROM `'._DB_PREFIX_.'shop` ';
      $shops = DB::getInstance()->executeS($sql); 

      $sql = 'SELECT DISTINCT `id_reference` FROM `'._DB_PREFIX_.'carrier` ';
      $id_carriers = DB::getInstance()->executeS($sql); 

      foreach ($shops as $key => $shop) {
        $sql = 'INSERT INTO `'._DB_PREFIX_.'module_carrier` (id_module, id_shop, id_reference) VALUES ';
        foreach ($id_carriers as $key => $id_carrier) {
          foreach (self::$payment_module_id as $key => $id_module) {
              $sql .= ' ('.$id_module.', '.$shop['id_shop'].', '.$id_carrier['id_reference'].'),';
          }
        }
        $sql = rtrim($sql, ',');
        $results = DB::getInstance()->execute($sql);
        $this->log('INSERTION SHOP#'.$shop['id_shop'].' '.($results ? 'OK' : 'KO'));
      }     
        
    }

}

$timestamp_debut = microtime(true);

$import = new PaymentAsso();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
$import->log('Exécution du script : ' . $difference_ms . ' secondes.'."\n");
  