/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import 'expose-loader?Tether!tether';
import 'bootstrap/dist/js/bootstrap.min';
import 'flexibility';
import 'bootstrap-touchspin';

import './responsive';
import './checkout';
import './customer';
import './listing';
import './product';
import './cart';

import DropDown from './components/drop-down';
import Form from './components/form';
import ProductMinitature from './components/product-miniature';
import ProductSelect from './components/product-select';
import TopMenu from './components/top-menu';

import prestashop from 'prestashop';
import EventEmitter from 'events';

import './lib/bootstrap-filestyle.min';
import './lib/jquery.scrollbox.min';

import './components/block-cart';


/**
 *
 * INSITACTION
 *
 */

import './lib/air-datepicker/datepicker.min';
import './lib/air-datepicker/i18n/datepicker.fr';
import 'jquery-match-height'; // plugins
import AOS from 'aos/dist/aos'; // plugins
import 'jquery-parallax.js/parallax.min.js'; // plugins
import AirDatepicker from './insita/datepicker';
import PerfectScrollbar from 'perfect-scrollbar'; //plugins
import Search from './insita/search';
import Quickcart from './insita/quickcart';
import Menu from './insita/menu';
import { Generique } from './insita/generique';
import Sliders from './insita/sliders';
import Produit from './insita/produit';  


// "inherit" EventEmitter
for (var i in EventEmitter.prototype) {
  prestashop[i] = EventEmitter.prototype[i];
}
 
$(document).ready(() => {
  let dropDownEl = $('.js-dropdown');
  const form = new Form();
  let topMenuEl = $('.js-top-menu ul[data-depth="0"]');
  let dropDown = new DropDown(dropDownEl);
  let topMenu = new TopMenu(topMenuEl);
  let productMinitature = new ProductMinitature();
  let productSelect  = new ProductSelect();
  
  dropDown.init();
  form.init();
  topMenu.init();
  productMinitature.init();
  productSelect.init(); 
  
  $('p.submit.cookie_ope a').on('click', function(event) {
      if($(this).attr('href') == '#cookie_consent') {
      window.cookieconsent.utils.setCookie('cookieconsent_status', 'dismiss',  365, "", '/');
    } else {
      window.cookieconsent.utils.setCookie('cookieconsent_status', null,  null, null, null);
    }
    window.location.href = "/";
    return false;
  });

  $('.circle').circleProgress();

  /**
   *
   * INISTACTION
   *
   */
  let search = new Search();
  let quickcart = new Quickcart(); 
  let menu = new Menu();
  let generique = new Generique();
  let sliders = new Sliders();
  let produit = new Produit();  
  let airDatepicker = new AirDatepicker();
  search.init();
  quickcart.init();
  menu.init();
  generique.init();
  sliders.init();
  produit.init();

  AOS.init();
  airDatepicker.init();

  
  /**
   *
   * Init perfect scrollbal
   *
   */
  const containerScrollbar = document.querySelector('#containerScrollbar');
  if(containerScrollbar !== null){
    const ps = new PerfectScrollbar(containerScrollbar, {
      wheelSpeed: 2,
      wheelPropagation: true,
      minScrollbarLength: 20
    });
  
    let ww = $(window).width();
    if(ww < 990)
      ps.destroy();
  
    $(window).resize(function(){
      if(ww < 990)
        ps.destroy();
    });
  }
});
 