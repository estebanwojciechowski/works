{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='notifications'}{/block}

{block name='page_header_container'}
{/block}

{block name='page_content_container'}
  <div class="container">
    <div class="row">
      {if $page.page_name == "identity"}
        <div class="col-md-1 col-lg-2"></div>
        <div class="col-md-10 col-lg-8">
          <section id="content" class="page-content my-info">
            <div class="bloc-info">
              {block name='page_content_header_container'}
                {block name='page_title'}
                  <header class="page-header">
                    <h1>{$smarty.block.child}</h1>
                  </header>
                {/block}
              {/block}
              {block name='page_content_top'}
                {block name='customer_notifications'}
                  {include file='_partials/notifications.tpl'}
                {/block}
              {/block}
              {block name='page_content'}
                <!-- Page content -->
              {/block}
            </div>
          </section>
        </div>
      {elseif $page.page_name == "addresses" }
        <div class="col-xs-12">
          <section id="content" class="page-content my-info">
            <div class="bloc-info row">
              {block name='page_content_header_container'}
                {block name='page_title'}
                  <header class="page-header col-xs-12">
                    <h1>{$smarty.block.child}</h1>
                  </header>
                {/block}
              {/block}
              {block name='page_content_top'}
                {block name='customer_notifications'}
                  {include file='_partials/notifications.tpl'}
                {/block}
              {/block}
              {block name='page_content'}
                <!-- Page content -->
              {/block}
            </div>
            <div class="addresses-footer">
              <a href="{$urls.pages.address}" data-link-action="add-address">
                <i class="material-icons">&#xE145;</i>
                <span>{l s='Create new address' d='Shop.Theme.Actions'}</span>
              </a>
            </div>
          </section>
        </div>
      {elseif $page.page_name == "address"}
        <div class="col-md-1 col-lg-2"></div>
          <div class="col-md-10 col-lg-8">
          <section id="content" class="page-content my-info">
            <div class="bloc-info">
              {block name='page_content_header_container'}
                {block name='page_title'}
                  <header class="page-header">
                    <h1>{$smarty.block.child}</h1>
                  </header>
                {/block}
              {/block}
              {block name='page_content_top'}
                {block name='customer_notifications'}
                  {include file='_partials/notifications.tpl'}
                {/block}
              {/block}
              {block name='page_content'}
                <!-- Page content -->
              {/block}
            </div>
          </section>
        </div>
      {else}
        <section id="content" class="page-content">
          {block name='page_content_header_container'}
            {block name='page_title'}
              <header class="page-header">
                <h1>{$smarty.block.child}</h1>
              </header>
            {/block}
          {/block}
          {block name='page_content_top'}
            {block name='customer_notifications'}
              {include file='_partials/notifications.tpl'}
            {/block}
          {/block}
          {block name='page_content'}
            <!-- Page content -->
          {/block}
        </section>
      {/if}
    </div>
  </div>
{/block}

{block name='page_footer'}
  {block name='my_account_links'}
    {include file='customer/_partials/my-account-links.tpl'}
  {/block}
{/block}
