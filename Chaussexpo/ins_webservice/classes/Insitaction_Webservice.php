<?php

include_once(dirname(__FILE__) . '/../../../config/config.inc.php');

if(!class_exists('Katzgrau\KLogger\Logger')) {
    include_once(dirname(__FILE__) .'/vendor/autoload.php');
}


class Insitaction_Webservice extends ObjectModel {

	protected $url;

	protected $client;
	protected $method;

	protected $response = false; 
	protected $errors = array();
	protected $verbose = false;

    protected $_logger;

    private static $cronDir;

	public function __construct($verbose = false)
    {
        self::$cronDir = dirname(__FILE__).'/../mode_degrade/';
        $this->_logger = new Katzgrau\KLogger\Logger('/home/log/php/', Psr\Log\LogLevel::INFO);
    	$this->connectWS();
    	if(!$this->client)
    		$this->log('Insitaction_Webservice : Impossible d\'établir une connexion à l\'url '.$this->url);
    	$this->verbose = $verbose;
    }

    protected function setUrl($url) {
        if(stripos($url, '?wsdl') === false)
            $url = $url.'?WSDL';
    	$this->url = $url;
    	if($this->verbose)
    		print("\n".$this->url."\n");
    }

    protected function connectWS() {         
		try {
            $this->client = new \SoapClient(
            							$this->url, 
            							array(
            								'soap_version' => SOAP_1_2, 
            								"trace" => 1, 
            								"exception" => 0,
                                            "connection_timeout" => 10
            							)

            						);
        } catch (Exception $e) {
            $this->errors[] = 'Exception reçue : '.$e->getMessage();            
            $this->log('Exception reçue : '.$e->getMessage());
        } 
    }

    protected function getValueFromXML($xml_data, $field_name, $path) {
    	if($xml_data && $xml_data instanceof SimpleXMLElement) {
    		if($field_name && !empty(trim($field_name))) {
    			if($path && !empty(trim($path))) {
    				$result = $xml_data->xpath($path);  
				 	if($result && count($result)) {
				 		foreach ($result as $key => $value) {
				 			if(property_exists($value, $field_name)) { 
				 				$tmp = (array) $value->{$field_name};
				 				if($this->verbose) 
				 					print("\n".$field_name .' // Nb values : '.count($tmp));
				 				if(count($tmp) == 1) {
                                    $tmp =array_shift($tmp);
                                    if(is_object($tmp)) {
                                        $tmp = '';
                                    }
				 					return $tmp;
				 				} elseif(count($tmp) > 0) {
				 					$values = array(); 
				 					foreach ($tmp as $index => $val) {
				 						$values[$index] = $val;
				 					} 
				 					return $values;
				 				}
				 				return false;
				 			}
				 		}
				 	}
    			}
    		}
    	}
    	return false;
    }

    protected function parseResults($champ, $response = false) {
    	$response = (array) ($response ? $response : $this->response); 
    	$data = false;
    	if($response && count($response)) {
            if(isset($response[$champ])) { 
                return $response[$champ];
            }
	    	foreach ($response as $key => &$value) {  
                if(is_object($value)) {
    	  			if(property_exists($value, $champ)) {
    	  				$data = $value->{$champ}; 
    	  				break;
    	  			}
                } else if(is_array($value)) {
                    $data = $value[$champ]; 
                    break;
                }
	  		}    
            if(trim($data))
    		  $data = new SimpleXMLElement($data); 
    	}
    	return $data;
    }

    protected function getErrors() {
    	return $this->errors;
    }

   /* protected function log($message, $severity = 1, $object_type = null, $object_id = null) {
        PrestaShopLogger::addLog($message, $severity, null, $object_type, $object_id, true); 
    }*/

    public function log($message, $severity = 1, $object_type = null, $object_id = null) {
        $message = $severity."\n\n".$message."\n\n".($object_type ? $object_type."\t//\t".$object_id : '')."\n\n";         
        $this->_logger->info($message);
    }

    public function logErrors() {
        if(count($this->errors))
            foreach ($this->errors as $key => $value) {
                 $this->log($value, 2);
            }
    }

    private static function checkDir($dir = false) {
        if($dir) {
            if(!is_dir($dir))
                mkdir($dir);
            return is_dir($dir);
        }

        if(!is_dir(self::$cronDir))
           mkdir(self::$cronDir);
        
        return is_dir(self::$cronDir);
    }
	

    /**
     * [addCron ajout d'un objet à la liste a traiter]
     * @param [type] $object [description]
     */
    protected function addCron($object) {
        if(self::checkDir()) {
            if($object && $object instanceof Order && !is_null($object->id)) {
                $this->cronFileAdd('order.csv', $object->id);
            } elseif($object && $object instanceof Customer && !is_null($object->id)) {
                $this->cronFileAdd('customer.csv', $object->id);                
            } else {
                $this->log('Insitaction_Webservice AJOUT TACHE CRON FAILED, l\'objet n\'est pas défini');
            }
        } else {
            $this->log('Insitaction_Webservice la création du repertoire : '.self::$cronDir.' a échoué.');
        }
        return false;
    }

    protected function cronFileAdd($filename, $id) {  
        if(!$id) {
            $this->log('Insitaction_Webservice l\'écriture du fichier : '.self::$cronDir.$filename.' a échoué ID invalide');
            return false;
        }
        if($fp = fopen(self::$cronDir.$filename,'c+')) { 
            //check si l'id n'existe pas déja
            $id_exist = false;
            while($data = fgets($fp)) { 
                if((int)$id == (int)$data) {
                    $id_exist = true;
                    break;
                }
                unset($data);
            }
            if(!$id_exist) {
                if (fwrite($fp, $id.chr(13)) === FALSE)
                   $this->log('Insitaction_Webservice l\'écriture du fichier : '.self::$cronDir.$filename.' a échoué.');
                else {
                    $this->log('Insitaction_Webservice l\'écriture du fichier : '.self::$cronDir.$filename.' a reussi.');
                    fclose($fp);
                    return true;
                }
            } else {
                $this->log('Insitaction_Webservice pas d\'écriture du fichier : '.self::$cronDir.$filename.' L\'ID#'.$id.' existe deja');
                return true;
            }

        } else{
            $this->log('Insitaction_Webservice l\'acces au fichier : '.self::$cronDir.$filename.' a échoué.');
        }
        fclose($fp);
        return false;
    }

    /**
     * [cronGetIds recupération sous forme de tableau de tout les ids dans le fichier a traiter]
     * @param  [type] $filename [description]
     * @return [type]           [description]
     */
    public function cronGetIds($filename) {  
        if(file_exists(self::$cronDir.$filename)) {
            if($fp = fopen(self::$cronDir.$filename,'r')) { 
                $ids = array();
                while($id = fgets($fp)) {
                    if((int)$id > 0) 
                        $ids[] = $id;
                    unset($id);
                } 
                fclose($fp);
                return $ids;
            } else{
                $this->log('Insitaction_Webservice l\'acces au fichier : '.self::$cronDir.$filename.' a échoué.');
            }
            fclose($fp);
        }
        return array();
    }

    /**
     * [cronRefresh Refresh du fichier queue et archive un fichier avec les commandes traites]
     * @param  [type] $filename          [description]
     * @param  array  $liste_non_traites [description]
     * @param  array  $liste_traites     [description]
     * @return [type]                    [description]
     */
    public function cronRefresh($filename, $liste_non_traites = array(), $liste_traites = array()) {
        if($fp = fopen(self::$cronDir.$filename,'w')) { 
            if(is_array($liste_non_traites) && count($liste_non_traites)) {
                fwrite($fp, implode('', $liste_non_traites));
                $this->log('Insitaction_Webservice liste cron fichier : '.$filename.' non traités, en erreur : '.implode(', ', $liste_non_traites));
            } 
            fclose($fp);
            if(is_array($liste_traites) && count($liste_traites)) {
                $archive_dir = self::$cronDir.'old/';
                if(self::checkDir($archive_dir)) {
                    $archive_name = time().'_'.$filename;
                    if($fp = fopen($archive_dir.$archive_name,'c')) {
                        fwrite($fp, implode('', $liste_traites));
                        $this->log('Insitaction_Webservice liste cron fichier : '.$filename.' traités en dégradé : '.implode(', ', $liste_traites));
                    } else {
                        $this->log('Insitaction_Webservice l\'acces au fichier : '.$archive_dir.$archive_name.' a échoué.');
                    }
                    fclose($fp);
                }
                $this->log('Insitaction_Webservice la création du repertoire : '.$archive_dir.' a échoué.');
            } 
        } else{
            $this->log('Insitaction_Webservice l\'acces au fichier : '.self::$cronDir.$filename.' a échoué.');
        }
    }

    public static function getPointure4WS($pointure) {
        // verification du format pointure
        // des pointures sont retournés en format xx/yy (chaussettes par exemples)
        // on ne conserve que xx
        $pointure_array = explode('/', $pointure);
        if(is_array($pointure_array) && isset($pointure_array[0])) {
            $pointure = $pointure_array[0];
        }
        if(intval($pointure) == 0) {
            $pointure = '00';
        }
        return $pointure;
    }
}