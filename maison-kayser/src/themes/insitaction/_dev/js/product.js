/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import $ from 'jquery';

$(document).ready(function () {
  createProductSpin();
  createInputFile();
  coverImage();
  imageScrollBox();

  if($('#formules_images').length)
    manageFormule();

  prestashop.on('updatedProduct', function (event) {
    createInputFile();
    coverImage();
    if (event && event.product_minimal_quantity) {
      const minimalProductQuantity = parseInt(event.product_minimal_quantity, 10);
      const quantityInputSelector = '#quantity_wanted';
      let quantityInput = $(quantityInputSelector);

      // @see http://www.virtuosoft.eu/code/bootstrap-touchspin/ about Bootstrap TouchSpin
      quantityInput.trigger('touchspin.updatesettings', {min: minimalProductQuantity});
    }
    imageScrollBox();
    $($('.tabs .nav-link.active').attr('href')).addClass('active').removeClass('fade');
    $('.js-product-images-modal').replaceWith(event.product_images_modal);
  });

  function coverImage() {
    $('.js-thumb').on(
      'click',
      (event) => {
        $('.js-modal-product-cover').attr('src',$(event.target).data('image-large-src'));
        $('.selected').removeClass('selected');
        $(event.target).addClass('selected');
        $('.js-qv-product-cover').prop('src', $(event.currentTarget).data('image-large-src'));
      }
    );
  }

  function imageScrollBox()
  {
    if ($('#main .js-qv-product-images li').length > 2) {
      $('#main .js-qv-mask').addClass('scroll');
      $('.scroll-box-arrows').addClass('scroll');
        $('#main .js-qv-mask').scrollbox({
          direction: 'h',
          distance: 113,
          autoPlay: false
        });
        $('.scroll-box-arrows .left').click(function () {
          $('#main .js-qv-mask').trigger('backward');
        });
        $('.scroll-box-arrows .right').click(function () {
          $('#main .js-qv-mask').trigger('forward');
        });
    } else {
      $('#main .js-qv-mask').removeClass('scroll');
      $('.scroll-box-arrows').removeClass('scroll');
    }
  }

  function createInputFile()
  {
    $('.js-file-input').on('change', (event) => {
      let target, file;

      if ((target = $(event.currentTarget)[0]) && (file = target.files[0])) {
        $(target).prev().text(file.name);
      }
    });
  }

  function createProductSpin()
  {
    let quantityInput = $('#quantity_wanted');
    quantityInput.TouchSpin({
      verticalbuttons: true,
      verticalupclass: 'material-icons touchspin-up',
      verticaldownclass: 'material-icons touchspin-down',
      buttondown_class: 'btn btn-touchspin js-touchspin',
      buttonup_class: 'btn btn-touchspin js-touchspin',
      min: parseInt(quantityInput.attr('min'), 10),
      max: 1000000
    });

    var quantity = quantityInput.val();
    quantityInput.on('keyup change', function (event) {
      const newQuantity = $(this).val();
      if (newQuantity !== quantity) {
        quantity = newQuantity;
        let $productRefresh = $('.product-refresh');
        $(event.currentTarget).trigger('touchspin.stopspin');
        $productRefresh.trigger('click', {eventType: 'updatedProductQuantity'});
      }
      event.preventDefault();

      return false;
    });
  }

  function manageFormule() {
    formuleProductZoom();
    formuleRefreshInfos();
  }

  function formuleRefreshInfos() {

    // au refresh des declinaisons formules on récupére les images
    prestashop.on('updateProduct', function (event) {
      if(event.eventType != 'updatedProductCombination')
        return false;

      var form = $('form#add-to-cart-or-refresh');
      var attributes = new Object();
      form.find('.product-variants select').each(function (index, el) {
        var idGroup = parseInt($(this).data('productAttribute'));
        attributes[idGroup] = $(this).val();
      });

      $.ajax({
        url: event.reason.productUrl,
        type: 'POST',
        dataType: 'json',
        data: {attributes: attributes, ajax: 1,action: 'refreshFormule' },
        beforeSend: function(){
          hideProductInfos('productInfosLoader');
        }
      }).done(function (resp) {
        if (typeof resp.success != 'undefined' && resp.success) {
          $('#formules_images').replaceWith(resp.cover);
          $('.js-product-formule-images-modal').replaceWith(resp.product_images_modal);
          $('#product_nav_tabs').html(resp.product_nav_tabs);
          $('#tab-content').replaceWith(resp.tab_content);
        }
        removeLoader('productInfosLoader');
      });
      // refresh aside in modal
    });
  }

  function removeLoader(divId) {
    $('div#'+divId).remove();
  }

  function hideProductInfos(divId) {
  var target = $('div.product--info');
    var div = $('<div/>');
    div.attr({
       id: divId
    });
    div.css({
       position: 'absolute',
       top: 0,
       left: 0,
       zIndex: 99,
       background: '#fff',
       opacity: 0.7,
       width: '100%',
       height: (target.outerHeight()-2)+'px'
    });
    var loader = $('<div/>');
    loader.attr({
       class: "swiper-lazy-preloader"
    });
    div.append(loader);
    target.append(div);
  }

  function formuleProductZoom() {
    if(
        $('#formules_images img.formule-cover').length
          && $('#product-modal').length
            && $('.formule-layer').length
      ) {

      $(document).on('click', '.formule-layer', function() {
        var modal = $('#product-modal');
        var idImage = $(this).data('idImage');
        var img = modal.find('aside .product-images img#formule_thumb_' + idImage);
        if(img.length) {
          img.click();
        }
      });

    }
  }

});
