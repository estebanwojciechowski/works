<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

use \PrestaShop\PrestaShop\Core\Module\WidgetInterface;

if (!defined('_PS_VERSION_')) {
    exit;
}

$class_folder = dirname(__FILE__).'/classes/';
require_once($class_folder.'BeVisibleMapHelper.php');
require_once($class_folder.'BeVisibleMapHelperFormAbstract.php');
require_once($class_folder.'BeVisibleMapHelperConfig.php'); 
require_once($class_folder.'BeVisibleMapModel.php');
require_once($class_folder.'BeVisibleMapStore.php');
require_once($class_folder.'BeVisibleMapValidate.php');

require_once($class_folder.'BeVisibleGroup.php'); 

class BeVisible extends Module implements WidgetInterface
{
    const FRONT_PAGE_NAME = 'module-bevisible-map';

    public $contact_us_url;

    public $bvhelper;

    public static $stores;
    
    public static $all_stores;

    private $microCachedShops = array();

    public function __construct()
    { 
        $this->bvhelper = BeVisibleMapHelper::getInstance();
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->bootstrap = true;
        $this->author = 'Insitaction - EstebanW';

        $this->name = 'bevisible';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->ps_versions_compliancy['min'] = '1.7';
        $this->displayName = $this->l('Be Visible');
        $this->description = $this->l('Affiche les magasins sur une page gmap personnalisée.');

        $this->bvhelper->addHooks(array(
            'displayHeader',
            'moduleRoutes'
        ));

        $default_id_store = '0';
        $store_ids = array();
        $store_collection = new PrestaShopCollection('Store');
        $stores = $store_collection->getResults();
        if ($stores) {
            $default_id_store = $stores[0]->id;
            foreach ($stores as $store) {
                $store_ids[] = $store->id;
            }
        }

        //Ajout des tables pour la gestion des Zones et des groupes de Zones
        $this->addBDD(); 

        $this->bvhelper->addDefaultConfig(array(
            'BEVISIBLE_STORE_DEFAULT' => $default_id_store,
            'BEVISIBLE_STORES_ENABLE' => json_encode($store_ids),
            'BEVISIBLE_MAP_API_KEY' => '',
            'BEVISIBLE_MAP_STYLE' => '',
            'BEVISIBLE_GEOCODE_CUSTOMER' => '0',
            'BEVISIBLE_HOME_TITLE' => $this->bvhelper->generateMultilangField('Our Stores'),
            'BEVISIBLE_HOME_MAP_HEIGHT' => '400',
            'BEVISIBLE_HOME_MAP_ZOOM' => 'auto',
            'BEVISIBLE_HOME_OPEN_INFO_WINDOW' => '1',
            'BEVISIBLE_HOME_LOCATOR' => '0',
            'BEVISIBLE_HOME_CONTENT_BEFORE' => $this->bvhelper->generateMultilangField(''),
            'BEVISIBLE_HOME_CONTENT_AFTER' => $this->bvhelper->generateMultilangField(''),
            'BEVISIBLE_HOME_CLASS_WRAPPER' => 'card',
            'BEVISIBLE_HOME_CLASS_HEADER' => 'card-header',
            'BEVISIBLE_HOME_CLASS_BODY' => 'card-block',
            'BEVISIBLE_CUSTOM_MAP_HEIGHT' => '500',
            'BEVISIBLE_CUSTOM_MAP_ZOOM' => 'auto',
            'BEVISIBLE_CUSTOM_OPEN_INFO_WINDOW' => '1',
            'BEVISIBLE_CUSTOM_LOCATOR' => '0',
            'BEVISIBLE_CUSTOM_CONTENT' => $this->bvhelper->generateMultilangField(''),
            'BEVISIBLE_CUSTOM_CONTENT_BEFORE' => $this->bvhelper->generateMultilangField(''),
            'BEVISIBLE_CUSTOM_CONTENT_AFTER' => $this->bvhelper->generateMultilangField(''),
            'BEVISIBLE_CUSTOM_CSS' => '',
            'BEVISIBLE_ZONES' => '', 
            'group_name' =>  $this->bvhelper->generateMultilangField(''),
            'group_france' => ''
        ));
        /*
        
            'id_bevisible_group' => '',
            'zone_stores' =>  '',
         */

        //init des valeurs des id groups et stores groups
        $id_bevisible_groups = array();
        $stores_zones = array();
        $groups = BeVisibleGroup::getGroups();
        if($groups) {
            foreach ($groups as $key => $group) {
                $id_bevisible_groups['id_bevisible_group_'.$group->id_bevisible_group] = $group->id_bevisible_group;
                
                $stores_by_group = BeVisibleGroup::getStoresByGroupID($group->id_bevisible_group, null, true);
               
                $stores_zones['zone_stores_'.$group->id_bevisible_group] = json_encode($stores_by_group);
               

            }
            $this->bvhelper->addDefaultConfig($id_bevisible_groups); 
            $this->bvhelper->addDefaultConfig($stores_zones); 
        }

        $this->bvhelper->setTabSection(Tools::getValue(
            'tab_section',
            BeVisibleMapHelper::DEFAULT_TAB_SECTION
        ));

        parent::__construct();
    }

    public function hookModuleRoutes() { 
        $metas = self::getMetas();
        $routes = array(
            self::FRONT_PAGE_NAME.'-zone' => array(
                'controller' => 'map',
                'rule' =>  '{start}'.$metas['url_rewrite'].'/{id_zone}{id_country}-{wanted_url}', //the desired page URL contains parameters
                'keywords' => array( //in the Keywords section we describe every parameter
                    'start' => array('regexp' => '^(.*/)?'), 
                    'id_zone' => array('regexp' => '[0-9]+', 'param' => 'id_zone'), 
                    'id_country' => array('regexp' => '(-[0-9]+)?', 'param' => 'id_country'),
                    'wanted_url' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'bevisible',
                    'controller' => 'map',
                )
            ),
            self::FRONT_PAGE_NAME.'-store' => array(
                'controller' => 'map',
                'rule' => '{start}'.$metas['url_rewrite'].'/{id_store}-{wanted_url}.html', //the desired page URL contains parameters
                'keywords' => array( //in the Keywords section we describe every parameter
                    'start' => array('regexp' => '^(.*/)?'), 
                    'id_store' => array('regexp' => '[0-9]+', 'param' => 'id_store'), 
                    'wanted_url' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'bevisible',
                    'controller' => 'map',
                )
            ),
        );

        return $routes;
    }

    public function install()
    {
        $return = parent::install() && $this->bvhelper->install() && 
            $this->registerHook('moduleRoutes') && $this->installCustomPage();

        Configuration::updateValue('BEVISIBLE_INSTALLED', 1);

        return $return;
    }

    public function installCustomPage()
    {
        $exist = Meta::getMetaByPage(self::FRONT_PAGE_NAME, $this->context->language->id);
        if (!$exist) {
            $page = new Meta();
            $page->page = self::FRONT_PAGE_NAME;
            $page->title = $this->bvhelper->generateMultilangField('Our stores');
            $page->description = $this->bvhelper->generateMultilangField('Our stores');
            $page->url_rewrite = $this->bvhelper->generateMultilangField('our-stores');
            $page->add();
        }

        return true;
    }

    public function uninstall()
    {
        $return = $this->bvhelper->uninstall();
        $return = $return && parent::uninstall();
        return $return;
    }

    /**
     * [addBDD Ajout des tables nécessaires au module]
     */
    private function addBDD() {
        /**
         * GESTION DES ZONES
         */
        /**
         * GESTION DES GROUPES
         */ 
        $this->bvhelper->addSqlTable(
            array(
                'name' => 'bevisible_group',
                'columns' => array(
                    array(
                        'name' => 'id_bevisible_group',
                        'params' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
                        'is_primary_key' => true,
                    ),
                    array(
                        'name' => 'france',
                        'params' => 'int(1)', 
                    ),
                ), 
            )
        );

        $this->bvhelper->addSqlTable(
            array(
                'name' => 'bevisible_group_lang',
                'columns' => array(
                    array(
                        'name' => 'id_bevisible_group',
                        'params' => 'int(10) unsigned NOT NULL',
                        'is_primary_key' => true,
                    ),
                    array(
                        'name' => 'id_lang',
                        'params' => 'int(10) unsigned NOT NULL ', 
                        'is_primary_key' => true,
                    ),           
                    array(
                        'name' => 'name',
                        'params' => 'varchar(128) COLLATE utf8_unicode_ci NOT NULL', 
                    ),
                ), 
            )
        );
        // TABLE GROUPES SHOP
        $this->bvhelper->addSqlTable( 
            array(
                'name' => 'bevisible_group_store',
                'columns' => array(
                    array(
                        'name' => 'id_bevisible_group',
                        'params' => 'int(10) unsigned NOT NULL',
                        'is_primary_key' => true,
                    ),
                    array(
                        'name' => 'id_store',
                        'params' => 'int(10) unsigned NOT NULL',
                        'is_primary_key' => true,
                    ), 
                ), 
            )
        );
        
    }

    #################### ADMIN ####################

    public function getContent()
    {  
        $this->bvhelper->addCSS('admin.css');
        $this->bvhelper->addJS('admin.js');

        $html = $this->bvhelper->getMessagesHtml();

        $general_settings_form = $this->initGeneralSettingsForm(); 
        $custom_page_settings_form = $this->initCustomPageSettingsForm();
        $advanced_settings_form = $this->initAdvancedSettingsForm();
        $group_zone_settings_form = $this->initGroupZoneSettingsForm();  
 
        if ($general_settings_form->isSubmitted()) {
            if ($general_settings_form->postProcess()) {
                $this->bvhelper->addSuccessMessage($this->l('Update successful'));
            } else {
                $this->bvhelper->setTransferData($general_settings_form->getFieldsValue());
            }
            $this->bvhelper->redirect($this->bvhelper->getAdminModuleUrlTab($general_settings_form->getTabSection()));
        } elseif ($group_zone_settings_form->isSubmitted()) {
            $this->groupPostProcess($group_zone_settings_form);            
        } elseif ($custom_page_settings_form->isSubmitted()) {
            if ($custom_page_settings_form->postProcess()) {
                $this->bvhelper->addSuccessMessage($this->l('Update successful'));
            } else {
                $this->bvhelper->setTransferData($custom_page_settings_form->getFieldsValue());
            }
            $this->bvhelper->redirect($this->bvhelper->getAdminModuleUrlTab(
                $custom_page_settings_form->getTabSection()
            ));
        } else {
            if (Configuration::get('BEVISIBLE_INSTALLED')) {
                $this->bvhelper->setTabSection('bevisible_help_tab');
                Configuration::deleteByName('BEVISIBLE_INSTALLED');
            }

            $error_string = $this->l('Please turn off "%s" option in "%s" -> "%s" -> "%s" panel!');
            $menu_1 = $this->l('Advanced Parameters');
            $menu_2 = $this->l('Performance');
            $panel = $this->l('Debug Mode');

            if (Configuration::get('PS_DISABLE_NON_NATIVE_MODULE')) {
                $html .= $this->displayError(sprintf(
                    $error_string,
                    'Disable non PrestaShop modules',
                    $menu_1,
                    $menu_2,
                    $panel
                ));
            }

            $tab_content = array();
            $forms_fields_value = $this->bvhelper->getAdminConfig(); 
            $this->bvhelper->fillFromDefaultConfig($forms_fields_value); 

            $tab_content[] = array(
                'id' => $general_settings_form->getTabSection(),
                'title' => $this->l('Options générales'),
                'content' => $general_settings_form->setFieldsValue($forms_fields_value)->renderForm()
            ); 

            $tab_content[] = array(
                'id' => $custom_page_settings_form->getTabSection(),
                'title' => $this->l('Options de la Page'),
                'content' => $custom_page_settings_form->setFieldsValue($forms_fields_value)->renderForm()
            ); 


            $tab_content[] = $this->setZoneSettingsPage($group_zone_settings_form, $forms_fields_value);
 


            $this->bvhelper->smartyAssign(array(
                'bevisible_help_stores_url' => $this->context->link->getAdminLink('AdminStores'),
                'bevisible_help_countries_url' => $this->context->link->getAdminLink('AdminCountries'),
                'bevisible_help_states_url' => $this->context->link->getAdminLink('AdminStates'),
                'bevisible_help_images_url' => $this->context->link->getAdminLink('AdminImages'),
                'bevisible_help_sample_csv_url' => $this->bvhelper->getModuleBaseUrl().'tmp/sample.csv', 
            ));


            $html .= $this->renderTabLayout($tab_content, $this->bvhelper->getTabSection());
        }
        return $html;
    }

    public function renderTabLayout($layout, $active_tab)
    {
        $this->bvhelper->smartyAssign(array(
            'bevisible_tab_layout' => $layout,
            'bevisible_active_tab' => $active_tab
        ));

        return $this->bvhelper->smartyFetch('admin/tab_layout.tpl');
    }

    public function initGeneralSettingsForm()
    {
        $input_fields = array(0 => array());

        $gm = '';
        if (!$this->hasGoogleMapsApiKey()) {
            $gm = '<br /><br /><span class="bevisible-bold">';
            $gm .= $this->l(sprintf('To use this feature you need to setup a %s.', $this->l('Google Maps API Key')));
            $gm .= '</span>';
        }

        $maps_desc = 'If you experience map malfunction or the keyless quota reached, please create a %s.';
        $maps_help = '<a href="https://goo.gl/ojhzyo" target="_blank">'.$this->l('Google Maps API Key').'</a>';
        $maps_desc = sprintf($maps_desc, $maps_help);
        $input_fields[0][] = array(
            'type' => 'text',
            'label' => $this->l('Google Maps API Key').':',
            'name' => 'BEVISIBLE_MAP_API_KEY',
            'size' => 70,
            'desc' => $maps_desc,
            'auto_save' => true,
            'validate' => 'isGenericName',
            'error_message' => $this->l('Please enter a valid Google Maps API Key!'),
        );

        $input_fields[0][] = array(
            'type' => 'file',
            'label' => $this->l('Google Maps Marker Icon').':',
            'name' => 'bevisible_marker_icon',
            'files' => array(
                array(
                    'type' => 'image',
                    'file_type' => 'image/png',
                    'file_name' => 'map-marker.png',
                )
            ),
            'desc' => $this->l('For the best view, please upload a 30 x 30 pixel size PNG image file.'),
            'auto_save' => true,
            'validate' => 'isMatchedFileType',
            'error_message' => $this->l('Please upload a valid PNG image file!'),
        );

        $style_desc = 'You can use a Javascript Style Array from %s or %s.';
        $snazzy_link = '<a href="https://goo.gl/wBOSjx" target="_blank">'.$this->l('Snazzy Maps').'</a>';
        $google_link = '<a href="https://goo.gl/5jJrN8" target="_blank">'.$this->l('Google Maps Styling Wizzard');
        $google_link .= '</a>';
        $style_desc = sprintf($style_desc, $snazzy_link, $google_link);
        $input_fields[0][] = array(
            'type' => 'textarea',
            'label' => $this->l('Google Maps Style').':',
            'name' => 'BEVISIBLE_MAP_STYLE',
            'size' => 70,
            'desc' => $style_desc,
            'auto_save' => true,
            'validate' => 'isJson',
            'error_message' => $this->l('Please enter a valid Google Maps style!'),
        );

        $input_fields[0][] = array(
            'type' => 'select',
            'label' => $this->l('Default store').':',
            'name' => 'BEVISIBLE_STORE_DEFAULT',
            'options' => array(
                'query' => $this->getStores(),
                'id' => 'id_store',
                'name' => 'name',
            ),
            'desc' => $this->l('Store selector default value, also centers the map to this location.'),
            'auto_save' => true,
        );

        $input_fields[0][] = array(
            'type' => 'switch',
            'label' => $this->l('Display customer nearest store as default').':',
            'name' => 'BEVISIBLE_GEOCODE_CUSTOMER',
            'class' => 't',
            'is_bool' => true,
            'disabled' => !$this->hasGoogleMapsApiKey(),
            'values' => array(
                array(
                    'id' => 'BEVISIBLE_GEOCODE_CUSTOMER_on',
                    'value' => 1,
                    'label' => $this->l('Yes')
                ),
                array(
                    'id' => 'BEVISIBLE_GEOCODE_CUSTOMER_off',
                    'value' => 0,
                    'label' => $this->l('No')
                ),
            ),
            'desc' =>
                $this->l('If a customer logged in and has an address the map tries to select the nearest store.').$gm,
            'auto_save' => true,
        );

        $input_fields[0][] = array(
            'type' => 'checkbox',
            'class' => 'checkbox',
            'label' => $this->l('Enabled stores').':',
            'name' => 'BEVISIBLE_STORES_ENABLE',
            'values' => array(
                'query' => $this->getStores(),
                'id' => 'id_store',
                'name' => 'name',
            ), 
            'desc' => $this->l('Only selected stores visible on the map.').
                        '<a href="#select" onclick="var checkboxes = $(this).parent().parent().find(\'input.checkbox\');if(checkboxes.length){checkboxes.attr(\'checked\',\'checked\');};return false;">Selectionner tout</a>',
            'auto_save' => true,
        );

        $forms = array(0 => array());
        $forms[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('General Settings')
            ),
            'input' => $input_fields[0]
        );

        $helper = new BeVisibleMapHelperConfig();
        $helper->init($this);
        $helper->setIdentifier('bevisible_general_settings');
        $helper->setTabSection('bevisible_general_tab');
        $helper->setForms($forms);
        return $helper;
    }

    public function initCustomPageSettingsForm()
    {
        $input_fields = array(0 => array());

        $meta = Meta::getMetaByPage(self::FRONT_PAGE_NAME, $this->context->language->id);
        if ($meta) {
            $edit_url = $this->bvhelper->getAdminControllerUrl('AdminMeta', array(
                'id_meta' => $meta['id_meta'],
                'updatemeta' => 'true'
            ));

            $html_content = '<a href="'.$edit_url.'" target="_blank" class="btn btn-default btn-md">';
            $html_content .= $this->l('Click here to edit Title, Description, Link Rewrite for the custom page.');
            $html_content .= '</a>';

            if (Shop::isFeatureActive() && Shop::CONTEXT_SHOP != Shop::getContext()) {
                $html_content = '<span class="label label-info bevisible-label">';
                $html_content .= $this->l('Please select a shop!');
                $html_content .= '</span>';
            }

            $input_fields[0][] = array(
                'type' => 'html',
                'label' => $this->l('Edit custom page').':',
                'name' => '',
                'html_content' => $html_content,
                'disable_multistore' => true,
            );
        }

        $input_fields[0][] = array(
            'type' => 'text',
            'label' => $this->l('Map height').':',
            'name' => 'BEVISIBLE_CUSTOM_MAP_HEIGHT',
            'size' => 70,
            'required' => true,
            'suffix' => 'px',
            'desc' => $this->l('Recommended').': 500px',
            'auto_save' => true,
            'validate' => 'isInt',
            'error_message' => $this->l('Please enter a valid map height, must be number!'),
        );

        $input_fields[0][] = array(
            'type' => 'select',
            'label' => $this->l('Map zoom level').':',
            'name' => 'BEVISIBLE_CUSTOM_MAP_ZOOM',
            'options' => array(
                'query' => array(
                    array('id' => 'auto', 'name' =>  $this->l('Automatic')),
                    array('id' => '0', 'name' => '0'),
                    array('id' => '1', 'name' => '1'),
                    array('id' => '2', 'name' => '2'),
                    array('id' => '3', 'name' => '3'),
                    array('id' => '4', 'name' => '4'),
                    array('id' => '5', 'name' => '5'),
                    array('id' => '6', 'name' => '6'),
                    array('id' => '7', 'name' => '7'),
                    array('id' => '8', 'name' => '8'),
                    array('id' => '9', 'name' => '9'),
                    array('id' => '10', 'name' => '10'),
                    array('id' => '11', 'name' => '11'),
                    array('id' => '12', 'name' => '12'),
                    array('id' => '13', 'name' => '13'),
                    array('id' => '14', 'name' => '14'),
                    array('id' => '15', 'name' => '15'),
                    array('id' => '16', 'name' => '16'),
                    array('id' => '17', 'name' => '17'),
                    array('id' => '18', 'name' => '18'),
                ),
                'id' => 'id',
                'name' => 'name',
            ),
            'desc' => $this->l('Recommended').': '.$this->l('Automatic'),
            'auto_save' => true,
        );

        $input_fields[0][] = array(
            'type' => 'switch',
            'label' => $this->l('Open info window').':',
            'name' => 'BEVISIBLE_CUSTOM_OPEN_INFO_WINDOW',
            'class' => 't',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'BEVISIBLE_CUSTOM_OPEN_INFO_WINDOW_on',
                    'value' => 1,
                    'label' => $this->l('Yes')
                ),
                array(
                    'id' => 'BEVISIBLE_CUSTOM_OPEN_INFO_WINDOW_off',
                    'value' => 0,
                    'label' => $this->l('No')
                ),
            ),
            'desc' => $this->l('The default or nearest stores info window on the map automatically opened.'),
            'auto_save' => true,
        );

        $gm = '';
        if (!$this->hasGoogleMapsApiKey()) {
            $gm = '<br /><br /><span class="bevisible-bold">';
            $gm .= $this->l(sprintf('To use this feature you need to setup a %s.', $this->l('Google Maps API Key')));
            $gm .= '</span>';
        }

        $input_fields[0][] = array(
            'type' => 'switch',
            'label' => $this->l('Enable nearest store locator').':',
            'name' => 'BEVISIBLE_CUSTOM_LOCATOR',
            'class' => 't',
            'is_bool' => true,
            'disabled' => !$this->hasGoogleMapsApiKey(),
            'values' => array(
                array(
                    'id' => 'BEVISIBLE_CUSTOM_LOCATOR_on',
                    'value' => 1,
                    'label' => $this->l('Yes')
                ),
                array(
                    'id' => 'BEVISIBLE_CUSTOM_LOCATOR_off',
                    'value' => 0,
                    'label' => $this->l('No')
                ),
            ),
            'desc' => $this->l('An address search filed under the map.').$gm,
            'auto_save' => true,
        );

        $input_fields[0][] = array(
            'type' => 'textarea',
            'label' => $this->l('Description sous la liste des boulangeries').':',
            'name' => 'BEVISIBLE_CUSTOM_CONTENT',
            'size' => 70,
            'lang' => true,
            'auto_save' => true,
            'editors' => array(
                'tinymce' => array(
                    'basic' => true,
                    'advanced' => true,
                ),
                'codemirror' => array(
                    'mode' => 'htmlmixed',
                ),
            ),
            'hide_selector' => false,
        );

        $input_fields[0][] = array(
            'type' => 'textarea',
            'label' => $this->l('Contenu "en ce moment" de la page boulangerie').':',
            'name' => 'BEVISIBLE_CUSTOM_CONTENT_AFTER',
            'size' => 70,
            'lang' => true,
            'auto_save' => true,
            'editors' => array(
                'tinymce' => array(
                    'basic' => true,
                    'advanced' => true,
                ),
                'codemirror' => array(
                    'mode' => 'htmlmixed',
                ),
            ),
            'hide_selector' => false,
        );


        $input_fields[0][] = array(
            'type' => 'textarea',
            'label' => $this->l('Description de la boulangerie').':',
            'name' => 'BEVISIBLE_CUSTOM_CONTENT_BEFORE',
            'size' => 70,
            'lang' => true,
            'auto_save' => true,
            'editors' => array(
                'tinymce' => array(
                    'basic' => true,
                    'advanced' => true,
                ),
                'codemirror' => array(
                    'mode' => 'htmlmixed',
                ),
            ),
            'hide_selector' => false,
        );

        $forms = array(0 => array());
        $forms[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Custom Page Settings')
            ),
            'input' => $input_fields[0]
        );

        $helper = new BeVisibleMapHelperConfig();
        $helper->init($this);
        $helper->setIdentifier('bevisible_custom_page_settings');
        $helper->setTabSection('bevisible_custom_tab');
        $helper->setForms($forms);
        return $helper;
    }

    public function initAdvancedSettingsForm()
    {
        $input_fields = array(0 => array());
        $input_fields[0][] = array(
            'type' => 'text',
            'label' => $this->l('Home page block wrapper class').':',
            'name' => 'BEVISIBLE_HOME_CLASS_WRAPPER',
            'size' => 70,
            'auto_save' => true,
            'validate' => 'isGenericName',
            'error_message' => $this->l('Please enter a valid css class!'),
            'hint' => $this->l('Invalid characters:').' <>;=#{}',
        );

        $input_fields[0][] = array(
            'type' => 'text',
            'label' => $this->l('Home page block title class').':',
            'name' => 'BEVISIBLE_HOME_CLASS_HEADER',
            'size' => 70,
            'auto_save' => true,
            'validate' => 'isGenericName',
            'error_message' => $this->l('Please enter a valid css class!'),
            'hint' => $this->l('Invalid characters:').' <>;=#{}',
        );

        $input_fields[0][] = array(
            'type' => 'text',
            'label' => $this->l('Home page block body class').':',
            'name' => 'BEVISIBLE_HOME_CLASS_BODY',
            'size' => 70,
            'auto_save' => true,
            'validate' => 'isGenericName',
            'error_message' => $this->l('Please enter a valid css class!'),
            'hint' => $this->l('Invalid characters:').' <>;=#{}',
        );

        $input_fields[0][] = array(
            'type' => 'textarea',
            'label' => $this->l('Custom CSS').':',
            'name' => 'BEVISIBLE_CUSTOM_CSS',
            'size' => 70,
            'auto_save' => true,
            'editors' => array(
                'tinymce' => array(
                    'basic' => true,
                    'advanced' => true,
                ),
                'codemirror' => array(
                    'mode' => 'css', // htmlmixed, javascript, css, xml, smartymixed, smarty
                ),
            ),
            'hide_selector' => false,
            'default_editor' => 'codemirror',
        );

        $forms = array(0 => array());
        $forms[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Advanced Settings')
            ),
            'input' => $input_fields[0]
        );

        $helper = new BeVisibleMapHelperConfig();
        $helper->init($this);
        $helper->setIdentifier('bevisible_advanced_settings');
        $helper->setTabSection('bevisible_advanced_tab');
        $helper->setForms($forms);
        return $helper;
    }

    protected function groupPostProcess($group_zone_settings_form) {

        $datas = Tools::getAllValues();
        $post_keys = array_keys($datas);
        $id_bevisible_group_keys = preg_grep('/^id_bevisible_group_[0-9]+$/', $post_keys);
        $id_bevisible_group = false;
        if(is_array($id_bevisible_group_keys) && count($id_bevisible_group_keys) == 1) {
            $id_bevisible_group = intval($datas[current($id_bevisible_group_keys)]);
        } else {
            $id_bevisible_group = $group_zone_settings_form->bvhelper->getValue('id_bevisible_group'); 
        }
        $success = true;
        $message = '';

        if($id_bevisible_group) {  
            if(!empty(preg_grep('/^group_name_[0-9]+$/', $post_keys))) {
                //gestion groupe (edition)
                $group_zone = new BeVisibleGroup($id_bevisible_group);
                $group_name = $group_zone_settings_form->bvhelper->getValueMultilang('group_name');
                $group_france = preg_grep('/^group_france_[0-9]+$/', $post_keys); 
                $group_zone->name = $group_name;
                $group_zone->france = empty($group_france) ? 0 : 1;  
                $success = $group_zone->save();
            } else {
                // recuperation des magasins a ajouter à la zone
                $zone_stores_keys = preg_grep('/^zone_stores_[0-9]+_[0-9]+$/', $post_keys);
                $stores = array();
                if(!empty($zone_stores_keys)) {
                    foreach ($zone_stores_keys as $key => $value) {
                        if(preg_match('/^zone_stores_[0-9]+_([0-9]+)$/', $value, $matches)) {
                            // ajout des id_stores
                           $stores[] =  $matches[1];
                        }
                    }
                } 
                $group = new BeVisibleGroup($id_bevisible_group);
                $group->addStores($stores);                 
            } 
        } elseif(!empty(preg_grep('/^group_name_[0-9]+$/', $post_keys))) {
            //gestion groupe (Ajout)
            $group_zone = new BeVisibleGroup();
            $group_name = $group_zone_settings_form->bvhelper->getValueMultilang('group_name');
            $group_france = preg_grep('/^group_france_[0-9]+$/', $post_keys); 
            $group_zone->name = $group_name;
            $group_zone->france = empty($group_france) ? 0 : 1;  
            $success = $group_zone->save();
        } 
 


        if($success) {
             $this->bvhelper->addSuccessMessage(strlen($message) ? $message : $this->l('Update successful'));
        } else {
            $this->bvhelper->setTransferData($group_zone_settings_form->getFieldsValue());
        } 
        $this->bvhelper->redirect($this->bvhelper->getAdminModuleUrlTab($group_zone_settings_form->getTabSection()));
    }
    
    protected function setZoneSettingsPage($form, $forms_fields_value) {
        $title = '';
        $content = $list_content = '' ;
        $datas = Tools::getAllValues();
        if(isset($datas['id_bevisible_group'])) {
            //gestion des delete
            if(isset($datas['deletebevisible_group_zone'])) {
                
                $success = true;
                if(isset($datas['deletebevisible_group_zone'])) {
                    $group_zone = new BeVisibleGroup($datas['id_bevisible_group']);
                    if($group_zone instanceof BeVisibleGroup && !is_null($group_zone->id_bevisible_group)) {
                        $success = $group_zone->delete();
                    }
                } 

                if($success) {
                     $this->bvhelper->addSuccessMessage($this->l('Delete successful'));
                } else {
                    $this->bvhelper->setTransferData($form->getFieldsValue());
                } 
                $this->bvhelper->redirect($this->bvhelper->getAdminModuleUrlTab($form->getTabSection()));

            } elseif(isset($datas['viewbevisible_group_zone'])) {
                $group = new BeVisibleGroup($datas['id_bevisible_group'], (int)Configuration::get('PS_LANG_DEFAULT'));
                $title = '<h1>Groupe : '.$group->name.' #'.$group->id_bevisible_group.'</h1>';
                // Page vue des zones                 
                $content .= '<input type="hidden" name="add_group_zone" value="1" />';                

            } else {
                $content = 'id_group_page' ;
            }

        } elseif(Tools::getValue('id_bevisible_zone')) {
            $content = 'id_zone_page' ;
        } else {
            // listing group zone sur la page index
            $helper = new HelperList;
            $helper->actions = array('view', 'edit', 'delete');
            $helper->shopLinkType = '';     
            $helper->simple_header = true;
            $helper->identifier = 'id_bevisible_group';   
            $helper->table = 'bevisible_group_zone'; 
            $helper->token = Tools::getAdminTokenLite('AdminModules');    
            $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name."&tab_section=".$form->getTabSection();
             
            
            $fields_list = array(
                'id_bevisible_group' => array('title' => $this->l('ID'), 'align' => 'center', 'class' => 'fixed-width-xs'),
                'name' => array('title' => $this->l('Groupe')), 
                'france' => array('title' => $this->l('France'),'filter_key' => 'position', 'align' => 'center', 'class' => 'fixed-width-xs')
            );
            $group_zones = BeVisibleGroup::getGroups(null, null, false, true);
            $list_content = $helper->generateList($group_zones, $fields_list); 
        }
        $content = $title.$list_content.$form->setFieldsValue($forms_fields_value)->renderForm();

        return array(
            'id' => $form->getTabSection(),
            'title' => $this->l('Gestion des zones'),
            'content' => $content
            );

    }

    public function initGroupZoneSettingsForm() {
        $forms = array(0 => array()); 
        $datas = Tools::getAllValues();
        if(isset($datas['id_bevisible_group']) && $datas['id_bevisible_group']) {
            if(isset($datas['viewbevisible_group_zone'])) {
                $forms[0]['form'] = $this->getAddStoresToZoneForm($datas['id_bevisible_group']);
            } elseif(isset($datas['updatebevisible_group_zone'])) {
                $forms[0]['form'] = $this->getAddGroupZoneForm($datas['id_bevisible_group']);
            }            
        } else {
            $forms[0]['form'] = $this->getAddGroupZoneForm();
        }

        $helper = new BeVisibleMapHelperConfig();
        $helper->init($this);
        $helper->setObjectDefinition(BeVisibleGroup::$definition);
        $helper->setTabSection('bevisible_group_zone_tab'); 
        $helper->show_cancel_button = true; 
        $helper->url_back = AdminController::$currentIndex.'&configure='.$this->name."&tab_section=".$helper->getTabSection();  
        $helper->setForms($forms);  
        return $helper;
    }

    public function getAddGroupZoneForm($id_bevisible_group = false) {
        $input_fields = array(0 => array());
        $title = 'Ajout';
        if($id_bevisible_group) {
            $group = new BeVisibleGroup($id_bevisible_group); 
            if(Validate::isLoadedObject($group)) {
                $title = 'Modification';
                $config = array();
                foreach ($group->name as $key => $name) {
                    $config['group_name_'.$key] = $name;
                }
                $config['group_name'] = $group->name;
                $group_france = array();
                if($group->france) {
                    $group_france[] = 1;
                }
                $config['group_france'] = json_encode($group_france);

                $this->bvhelper->addDefaultConfig($config); 
                $input_fields[0][] = array(
                    'type' => 'hidden', 
                    'name' => 'id_bevisible_group_'.$id_bevisible_group,
                    'value' =>  $id_bevisible_group,
                );  
            }
        }
        $input_fields[0][] = array(
            'type' => 'text',
            'label' => $this->l('Nom de zone').':',
            'name' => 'group_name',
            'size' => 64,
            'lang' => true, 
            'validate' => 'isGenericName',
            'error_message' => $this->l('Please enter a valid name'),
            'hint' => $this->l('Invalid characters:').' <>;=#{}',
        );
        $input_fields[0][] = array(
            'type' => 'checkbox',
            'label' => $this->l('Appartient au groupe France').':',
            'name' => 'group_france', 
            'values' => array(
                'query' => array(
                   array(
                        'id' => '1',
                        'name' => '',
                        'value' => 1
                    ),
                ),
                'id' => 'id',
                'name' => 'name',
            ),
            'desc' => 'Cochez cette case si la zone appartient au groupe France', 
        );

        $form = array(
            'legend' => array(
                'title' => $this->l($title.' d\'un groupe de zones')
            ),
            'input' => $input_fields[0]
        );
 
        return $form;
    }

    public function getAddStoresToZoneForm($id_bevisible_group = false) {
        $input_fields = array(0 => array());
        $input_fields[0][] = array(
            'type' => 'hidden', 
            'name' => 'id_bevisible_group_'.$id_bevisible_group,
            'value' =>  '',
        );
        $input_fields[0][] = array(
            'type' => 'checkbox',
            'label' => $this->l('Enabled stores').':',
            'name' => 'zone_stores_'.$id_bevisible_group,
            'values' => array(
                'query' => $this->getStores(),
                'id' => 'id_store',
                'name' => 'name',
            ),
            'desc' => $this->l('Les boutiques sélectionnées seront affichée sur cette zone.'),
            'auto_save' => true,
        );

        $form = array(
            'legend' => array(
                'title' => $this->l('Lier des boutiques à la zone.')
            ),
            'input' => $input_fields[0]
        );
 
        return $form;
    }

    #################### FRONT HOOKS ####################

    public function hookDisplayHeader($params)
    { 
        $metas = BeVisible::getMetas();
        if(is_array($metas) && isset($metas['url_rewrite'])) {
            $store_locator_url =  $this->context->shop->getBaseURL(true, false).'/'.$metas['url_rewrite'];
            $this->context->smarty->assign('store_locator_url', $store_locator_url); 
        }         

        //ajout script et style pour la page storelocator
        if (isset($this->context->controller->page_name)
            && in_array($this->context->controller->page_name, array(self::FRONT_PAGE_NAME))) {
            $this->bvhelper->addCSS('bevisible-font-awesome.min.css');
            $this->bvhelper->addCSS('front.css');
            $this->bvhelper->addJS('front.js');

            $this->bvhelper->smartyAssign(array(
                'bevisible_params_hash' => sha1(json_encode($params))
            ));

            return $this->getCssAndJs('CUSTOM');
        }

        return '';
    } 

    public static function getMetas() {
        $context = Context::getContext();
        $meta = Meta::getMetaByPage(BeVisible::FRONT_PAGE_NAME, $context->language->id);        
        return $meta; 
    }

    public function renderWidget($hookName, array $configuration)
    {
        $options = $this->getWidgetVariables($hookName, $configuration);
        $this->bvhelper->smartyAssign(array(
            'bevisible_dev_null' => sha1(json_encode($options))
        ));

        return '';
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        return array($hookName, $configuration);
    }

    #################### FUNCTIONS ####################

    public function getCssAndJs($section)
    {
        $context = Context::getContext();
        $language_code_array = explode('-', $context->language->language_code);
        $language_code_for_map = 'en';
        if (isset($language_code_array[0])) {
            $language_code_for_map = $language_code_array[0];
        }

        $bevisible_css = array(
            'map_height' => Configuration::get('BEVISIBLE_'.$section.'_MAP_HEIGHT'),
            'custom' => Configuration::get('BEVISIBLE_CUSTOM_CSS')
        );

        
        $stores = $this->getStoresFront();

        $default_id_store = $this->getDefaultIdStore($stores);

        $open_info_window = 0;
        if (Configuration::get('BEVISIBLE_'.$section.'_OPEN_INFO_WINDOW')) {
            $open_info_window = 1;
        }

        $geocode_customer = 0;
        if ($this->hasGoogleMapsApiKey() && Configuration::get('BEVISIBLE_GEOCODE_CUSTOMER')) {
            $geocode_customer = 1;
        }

        $bevisible_js = array(
            'callback_url' => $context->link->getModuleLink(
                'bevisible',
                'geocode',
                array('ajax' => 1),
                Configuration::get('PS_SSL_ENABLED')
            ),
            'stores' => json_encode($stores),
            'all_stores' => json_encode(self::$all_stores),
            'selected_id_store' => $default_id_store,
            'marker_url' => $this->getMapMarkerIcon(),
            'marker_type' => 'icon',
            'map_zoom' => Configuration::get('BEVISIBLE_'.$section.'_MAP_ZOOM'),
            'map_styles' => (Configuration::get('BEVISIBLE_MAP_STYLE'))?Configuration::get('BEVISIBLE_MAP_STYLE'):'"none"',
            'language_code_for_map' => $language_code_for_map,
            'map_api_key' => trim(Configuration::get('BEVISIBLE_MAP_API_KEY')),
            'open_info_window' => $open_info_window,
            'geocode_customer' => $geocode_customer,
        );

        if ($this->getMapMarkerIcon()) {
            $bevisible_js['marker_type'] = 'image';
        }

        $this->bvhelper->smartyAssign(array(
            'bevisible_css' => $bevisible_css,
            'bevisible_js' => $bevisible_js,
        ));

        return $this->bvhelper->smartyFetch('front/css_js_17.tpl', true);
    }

    public function getMultiShopFileNameFront($file_name)
    {
        $path = dirname(__FILE__).'/views/img/';
        $s_file_name = 's-'.$this->context->shop->id.'-'.$file_name;
        $g_file_name = 'g-'.$this->context->shop->id_shop_group.'-'.$file_name;

        if (Shop::isFeatureActive() && file_exists($path.$s_file_name)) {
            return $s_file_name;
        }

        if (Shop::isFeatureActive() && file_exists($path.$g_file_name)) {
            return $g_file_name;
        }

        if (file_exists($path.$file_name)) {
            return $file_name;
        }

        if (file_exists($path.'default-'.$file_name)) {
            return 'default-'.$file_name;
        }

        return '';
    }

    public function getMapMarkerIcon()
    {
        $module_marker = 'map-marker.png';
        $module_marker = $this->getMultiShopFileNameFront($module_marker);

        if ($module_marker) {
            $module_img_uri = _MODULE_DIR_.$this->name.'/views/img/';
            return $this->context->link->getMediaLink($module_img_uri.$module_marker);
        }

        return '';
    }

    public function getDefaultIdStore($stores = array()) {
        $default_id_store = Configuration::get('BEVISIBLE_STORE_DEFAULT');
        if(!empty($stores)) {
            if($id_zone = Tools::getValue('id_zone')) {  
                $id_stores = array_keys($stores);
                if(!in_array($default_id_store, $id_stores)) {
                    $default_id_store = current($id_stores);
                } 
            } elseif($id_store = Tools::getValue('id_store')) {
                $default_id_store = key($stores);
            } 
        }
        return $default_id_store;
    }

    public function getStoresFront($return_all_stores = false, $services = array())
    {
        //TODO opti lors de la selection pays
        //if (!self::$stores) {
            $metas = BeVisible::getMetas();
            $context = context::getContext();
            $bevisible_url = $metas['url_rewrite']; 
            $bevisible_full_url = $this->context->shop->getBaseURL(true).$bevisible_url;
            $get_shops_only = Tools::getValue('cnc');

            $stores_enable = json_decode(Configuration::get('BEVISIBLE_STORES_ENABLE'), true);
            self::$stores = array();
            self::$all_stores = array();
            $stores = array();

            $collection_store = new PrestaShopCollection('Store');
            if (Configuration::get('BEVISIBLE_STORE_ORDER_NAME')) {
                $all_stores = $collection_store->orderBy('name')->getResults();
            } else {
                $all_stores = $collection_store->getResults();
            }
            
            if($id_zone = Tools::getValue('id_zone')) {                 
                $id_country = str_replace('-', '', Tools::getValue('id_country')); 
                $stores = BeVisibleGroup::getStoresByGroupID($id_zone, $id_country, false, true);   

            } elseif($id_store = Tools::getValue('id_store')) {
                $store = new Store($id_store);  
                if(Validate::isLoadedObject($store)) {
                    $stores = array($id_store => $store);
                    $get_shops_only = false;
                }
            }  

            if ($all_stores) {
                $days_translations = array(
                    0 => $this->l('Monday'),
                    1 => $this->l('Tuesday'),
                    2 => $this->l('Wednesday'),
                    3 => $this->l('Thursday'),
                    4 => $this->l('Friday'),
                    5 => $this->l('Saturday'),
                    6 => $this->l('Sunday'),
                );              

                foreach ($all_stores as $store) {
                    if (in_array($store->id, $stores_enable)) {

                        if($get_shops_only) {
                            //si le shop n'est pas retrouvé on zappe
                            if(!isset($this->microCachedShops[$store->id])) {
                                $shopObj = new Shop($store->id);
                                if(Validate::isLoadedObject($shopObj) && $shopObj->active) 
                                {                                    
                                    $this->microCachedShops[$store->id] = $shopObj;
                                } else {
                                    continue;
                                }
                            } 
                        }
 
                        if(isset($services['cafe']) && $services['cafe']) {
                            if(!$store->cafe)
                                continue;
                        }
                        if(isset($services['resto']) && $services['resto']) {
                            if(!$store->restaurant)
                                continue;
                        }
                        if(isset($services['sansgluten']) && $services['sansgluten']) {
                            if(!$store->sans_gluten)
                                continue;
                        }


                        $store_hours = Store::parseHours($store->hours);
                        $store_hours_cnc = Store::parseHours($store->hours_clickandcollect);

                        $tmp = array();
                        $tmp['id_country'] = $store->id_country;
                        $tmp['id_store'] = $store->id;
                        $tmp['name'] = $store->name;
                        $tmp['address1'] = $store->address1;
                        $tmp['address2'] = $store->address2;
                        $tmp['quartier'] = $store->quartier;
                        $tmp['arrondissement'] = $store->arrondissement;
                        $tmp['type_payment'] = $store->type_payment;
                        $tmp['identifiant_caisse'] = $store->identifiant_caisse;
                        $tmp['sans_gluten'] = $store->sans_gluten;
                        $tmp['restaurant'] = $store->restaurant;
                        $tmp['cafe'] = $store->cafe;
                        $tmp['terrasse'] = $store->terrasse;
                        $tmp['colissimo'] = $store->colissimo;
                        $tmp['clickandcollect'] = $store->clickandcollect;
                        $tmp['email'] = $store->email;
                        $tmp['note'] = $store->note;
                        $tmp['description'] = isset($store->description[$context->language->id]) ? $store->description[$context->language->id] : '';
                        $tmp['commercial'] = isset($store->commercial[$context->language->id]) ? $store->commercial[$context->language->id] : '';
                        $tmp['fax'] = $store->fax; 
                        $tmp['postcode'] = $store->postcode;
                        $tmp['city'] = $store->city;
                        $tmp['latitude'] = $store->latitude;
                        $tmp['longitude'] = $store->longitude;
                        $tmp['hours'] = $store_hours;
                        $tmp['hours_cnc'] = $store_hours_cnc;
                        $tmp['phone'] = $store->phone;
                        $tmp['state_iso_code'] = '';
                        $tmp['state_name'] = '';
 
                        if ($store->id_state) {
                            $state = new State($store->id_state);
                            $tmp['state_iso_code'] = $state->iso_code;
                            $tmp['state_name'] = $state->name;
                        }

                        $this->bvhelper->smartyAssign(array(
                            'bevisible_store' => $tmp,
                            'bevisible_days_translations' => $days_translations,
                            'bevisible_get_direction_url' => 'https://maps.google.com/maps?saddr=&daddr=('.
                                $tmp['latitude'].','.$tmp['longitude'].')',
                            'bevisible_url' => $bevisible_full_url,
                            'back' => Tools::getValue('back', false)
                        ));

                        $is_front_controller = true;
                        if ($this->context->controller instanceof AdminController ||
                            $this->context->controller instanceof AdminControllerCore) {
                            $is_front_controller = false;
                        }

                        if ($is_front_controller) {
                            $tmp['info_box_html'] = $this->bvhelper->smartyFetch('front/info_box_17.tpl', true);
                        } else {
                            $tmp['info_box_html'] = $this->bvhelper->smartyFetch('front/info_box.tpl');
                        }

                        $tmp['full_address'] = $tmp['address1'];
                        if ($tmp['address2']) {
                            $tmp['full_address'] .= ' '.$tmp['address2'];
                        }
                        $tmp['full_address'] .= ' '.$tmp['city'];
                        if ($tmp['state_iso_code']) {
                            $tmp['full_address'] .= ', '.$tmp['state_iso_code'];
                        }
                        $tmp['full_address'] .= ' '.$tmp['postcode'];

                        self::$all_stores[$store->id] = $tmp;
                        if(isset($stores[$store->id]) 
                            || $get_shops_only)
                            self::$stores[$store->id] = $tmp;
                    }
                }
            }
      //  }
      //  
        if(empty(self::$stores))
            self::$stores = self::$all_stores;

        if($return_all_stores) {
            return self::$all_stores;
        }
        return self::$stores;
    }

    public function getStores()
    {
        $collection_store = new PrestaShopCollection('Store');
        if (Configuration::get('BEVISIBLE_STORE_ORDER_NAME')) {
            $stores = $collection_store->orderBy('name')->getResults();
        } else {
            $stores = $collection_store->getResults();
        }

        $result = array();
        if ($stores) {
            foreach ($stores as $store) {
                $tmp = array();
                $tmp['id_store'] = $store->id;
                $tmp['val'] = $store->id;
                $tmp['name'] = $store->name;
                $tmp['state_iso_code'] = '';
                $tmp['state_name'] = '';

                if ($store->id_state) {
                    $state = new State($store->id_state);
                    $tmp['state_iso_code'] = $state->iso_code;
                    $tmp['state_name'] = $state->name;
                }

                $tmp['full_address'] = $store->address1;
                if ($store->address2) {
                    $tmp['full_address'] .= ' '.$store->address2;
                }
                $tmp['full_address'] .= ' '.$store->city;
                if ($tmp['state_iso_code']) {
                    $tmp['full_address'] .= ', '.$tmp['state_iso_code'];
                }
                $tmp['full_address'] .= ' '.$store->postcode;

                $result[$store->id] = $tmp;
            }
        }

        return $result;
    }

    

    public function getDistanceBetweenPointsNew($lat1, $lng1, $lat2, $lng2, $unit = 'km')
    {
        $theta = $lng1-$lng2;
        $distance = (sin(deg2rad($lat1))*sin(deg2rad($lat2))+
            (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($theta))));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;

        switch ($unit) {
            case 'km':
                $distance = $distance * 1.609344;
                break;
        }

        return (round($distance, 2));
    }

    public function hasGoogleMapsApiKey()
    {
        return (bool)Configuration::get('BEVISIBLE_MAP_API_KEY');
    } 

}
