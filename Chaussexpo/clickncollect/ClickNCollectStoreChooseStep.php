<?php
/**
 * @author    Esteban Wojciechowski <estebanw@insitaction.com>
 * @copyright 2017 INSITACTION
 */


use Symfony\Component\Translation\TranslatorInterface;

class ClickNCollectStoreChooseStepCore extends AbstractClickNCollectStep
{
    protected $template = 'clickncollect/_partials/steps/stores.tpl';
    private $stores;
    private $store;
    private $storeForm;

    public function __construct(
        Context $context,
        TranslatorInterface $translator
    ) {
        parent::__construct($context, $translator);
       // $this->getStores();
    }

    public function handleRequest(array $requestParameters = array())
    {
        // le choix du magasin is always reachable
        $this->step_is_reachable = true;

       /* ESTEBANW INSITACTION CLICKNCOLLECT
		@TODO affichage des magasins pour la selection OK
		infos client/adresses/magasins
	
		OK 1 - afficher les magasins (geoloc++ ou info adresse) via store locator // recup import + script import

		2 - check via import du matin si stock dispo pour les articles dans les magasins présents
*/
        if (isset($requestParameters['submitBoutique'])) {
            $id_store = intval(Tools::getValue('submitBoutique'));
            $currentIdStore = $this->getClickNCollectSession()->getIDStore();
            if ($currentIdStore && $this->getClickNCollectSession()->getIDStore() != $id_store) {
                $this->setCurrent(true);
                $this->getClickNCollectProcess()->invalidateAllStepsAfterCurrent();
            }

            if ($id_store && Store::storeExists($id_store) && $this->storeHasStock($id_store)) {
                $this->getClickNCollectSession()->setIDStore($id_store);
                $this->step_is_complete = true;
            }
        } 
        if (isset($requestParameters['content_only'])) {     
            $this->setRenderType($requestParameters['content_only']); 
        }  

        $this->setTitle(
            $this->getTranslator()->trans(
                'Nos Magasins',
                array(),
                'Shop.Theme.ClickNCollect'
            )
        );
    }

    public function ajaxRefreshStores($controller) {
        $errors = array();
        $html = false;
        /* Ne pas faire de Tools::GetValue() car il fait des traitements qui font sauter le JSON !!! */       
        $stores = $_POST['stores'];

        if($stores) {
            $stores = json_decode(html_entity_decode(urldecode($stores)), true);

            //sort stores by distance
            usort($stores, function ($item1, $item2) {
                if(!isset($item1['distance']) || !isset($item2['distance'])) {
                     $errors[] = $this->trans('Une distance n\'a pas été initialisée.');
                }
                if (floatval($item1['distance']) == floatval($item2['distance'])) return 0;
                    return floatval($item1['distance']) < floatval($item2['distance']) ? -1 : 1;
            });
            if(!$errors) {
                $distance_max = Configuration::get('FSSM_MAX_STORE_DISTANCE', null, null, null, 0);
                if($distance_max && !empty($stores)) {
                    foreach ($stores as $store_key => $store) {
                        if($store['distance'] > $distance_max) {
                            unset($stores[$store_key]);
                        }
                    }
                }
                $limit = Configuration::get('FSSM_MAX_STORE_DISPLAY', null, null, null, 0);
                if($limit && count($stores) > $limit) {
                     $stores = array_slice ($stores , 0, $limit);
                }
                //fill stores address
                $stores = $this->getTemplateVarStores($stores);

                $params = array(
                    'stores' => $stores,
                );

                $template = 'clickncollect/_partials/steps/stores-list.tpl';
                $context = Context::getContext();
                $tpl = $context->smarty->createTemplate(
                    $template,
                    $params
                );         

                $html = $tpl->fetch();
            }
        } else {
            $errors[] = 'Aucun magasin n\'a été trouvé.';
        }

        ob_end_clean();
        header('Content-Type: application/json');
        die(Tools::jsonEncode([
            'success' => empty($errors) ? true : false,
            'errors'  => $errors,
            'html' => $html,
        ]));
    }

    private function storeHasStock($id_store) {
        return true;
    }

    /**
     * [getDisponibilite recupere la disponibilité du panier selon un magasin]
     * @param  [type] $id_store [description]
     * @return [type]           [string 2H ou 48H]
     */
    public function getDisponibilite($id_store = null, $as_array = false) {
        $context = Context::getContext();
        $translator = $context->getTranslator();
        if(is_null($id_store)) {
            return $translator->trans('48H', array(), 'Shop.Theme.Global');
        }
        
        if(class_exists('Ins_Stock_Magasin')) {
            $stock = new Ins_Stock_Magasin();
            $cart = $context->cart; 
            $productStock = array();
            //verification des dispo pour chaque produits
            $products = $cart->getProducts();
            $hors_stock = 0;
            foreach ($products as $key => $product) { 
                $pointure = Ins_Stock_Magasin::getPointure($product['attributes']);
                $stock_produit = $stock->getStock($id_store, $product['reference'], $pointure); 
                $id_image = Product::getCover($product['id_product'], $context);
                if(is_array($id_image) && isset($id_image['id_image'])) {
                    $id_image = $id_image['id_image'];
                }

                $price_ttc = Product::getPriceStatic($product['id_product'], true, null, 6);
                $price_ttc = Tools::ps_round($price_ttc, 2);

                //si un des produits de la commande est hors stock dispo sous 48h
                $productStock[$product['reference'].'-'.$product['id_product_attribute']] = array(
                        'stock' => (int)$stock_produit - (int)$product['quantity'],
                        'id_product' => $product['id_product'],
                        'id_product_attribute' => $product['id_product_attribute'],
                        'id_image' =>  ($id_image ? $id_image : false),
                        'image_link' => ($id_image ? $context->link->getImageLink($product['name'], $id_image, 'medium_default') : false),
                        'quantity' => $product['quantity'],
                        'reference' => $product['reference'],
                        'pointure' => $pointure,
                        'name' => $product['name'],
                        'price' => $price_ttc,
                    ); 
                if(!$stock_produit || ($stock_produit - $product['quantity'] < 0)) {
                    $hors_stock++;
                    //break;
                }
            }
            if($hors_stock < count($products) && !$as_array) {

                return $translator->trans('2H', array(), 'Shop.Theme.Global');

            } elseif ($hors_stock == count($products) && !$as_array) {

               return $translator->trans('48H', array(), 'Shop.Theme.Global');

            }

        }
        if($as_array) { 
            return $productStock;
        }
        return $translator->trans('48H', array(), 'Shop.Theme.Global');
    }   

    public function getTemplateVarStores($stores = null)
    {
        if(!$stores) {
            $stores = Store::getStores();
        }
        $context = Context::getContext();
		$translator = $context->getTranslator();

        foreach ($stores as &$store) {
            //recuperation des disponibilite par magasin
            $store['disponibilite'] = $this->getDisponibilite($store['id_store']);
            $store['disponibilites'] = $this->getDisponibilite($store['id_store'], true);

            unset($store['active']);
            // Prepare $store.address
            $address = new Address();
            $store['address'] = [];
            $attr = ['address1', 'address2', 'postcode', 'city', 'id_state', 'id_country'];
            foreach ($attr as $a) {
                if(isset($store[$a])) {
                    $address->{$a} = $store[$a];
                    $store['address'][$a] = $store[$a];
                    unset($store[$a]);
                }
            }
            $store['address']['formatted'] = AddressFormat::generateAddress($address, array(), '<br />');

            // Prepare $store.business_hours
            // Required for trad            
            if(is_string($store['hours']))
                $temp = json_decode($store['hours'], true);
            else
                $temp = $store['hours'];

            unset($store['hours']);
            $store['business_hours'] = [
                [
                    'day' => $translator->trans('Monday', array(), 'Shop.Theme.Global'),
                    'hours' => $temp[0],
                ],[
                    'day' => $translator->trans('Tuesday', array(), 'Shop.Theme.Global'),
                    'hours' => $temp[1],
                ],[
                    'day' => $translator->trans('Wednesday', array(), 'Shop.Theme.Global'),
                    'hours' => $temp[2],
                ],[
                    'day' => $translator->trans('Thursday', array(), 'Shop.Theme.Global'),
                    'hours' => $temp[3],
                ],[
                    'day' => $translator->trans('Friday', array(), 'Shop.Theme.Global'),
                    'hours' => $temp[4],
                ],[
                    'day' => $translator->trans('Saturday', array(), 'Shop.Theme.Global'),
                    'hours' => $temp[5],
                ],[
                    'day' => $translator->trans('Sunday', array(), 'Shop.Theme.Global'),
                    'hours' => $temp[6],
                ],
            ];
        }

        return $stores;
    }

 	protected function getStores()
    {
        $this->stores = $this->getTemplateVarStores();
    }
    
    public function hasGoogleMapsApiKey()
    {
        return (bool)Configuration::get('FSSM_MAP_API_KEY');
    }


    public function render(array $extraParams = array())
    { 
        $distance_unit = Configuration::get('PS_DISTANCE_UNIT');
        if (!in_array($distance_unit, array('km', 'mi'))) {
            $distance_unit = 'km';
        }
        
        $address = false;
        if($this->context->customer && $this->context->customer->id) {
            $address = $this->context->customer->getAddresses($this->context->language->id);
            if($address && !empty($address)) {
                
                $address =  $address[0]['address1'].' '.
                            $address[0]['postcode'].' '.
                            $address[0]['city'].' '.
                            Country::getNameById($this->context->language->id, $address[0]['id_country']);
            }
        }
        $formUrl = $this->context->link->getPageLink('clickncollect');
        if(Tools::getValue('content_only'))
            $formUrl .= '?content_only=1';

        $params = array(
            'link' => $this->context->link,
            'storeStep' => $this->isReachable() && $this->isCurrent(),
            'empty_cart_on_logout' => !Configuration::get('PS_CART_FOLLOWING'),
            'formUrl' => $formUrl,
            'distance_unit' => $distance_unit,
            'map_api_key' => Configuration::get('FSSM_MAP_API_KEY'), 
            'fssm_stores' => $this->stores,
            'fssm_class_wrapper' => Configuration::get('FSSM_HOME_CLASS_WRAPPER'),
            'fssm_class_header' => Configuration::get('FSSM_HOME_CLASS_HEADER'),
            'fssm_class_body' => Configuration::get('FSSM_HOME_CLASS_BODY'),
            'address' => $address,
           // 'stores' => $this->stores,
        );

        return $this->renderTemplate($this->getTemplate(), $extraParams, $params);
    }
}
