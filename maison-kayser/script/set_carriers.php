<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
require 'vendor/autoload.php'; // logger
require(dirname(__FILE__) . '/../src/config/config.inc.php'); 

class SetProductLivraison {

    protected static $carrier_cnc_id_reference = 1;
    protected static $carrier_velo_id_reference = 6;
    protected static $carrier_voiture_id_reference = 12;
    protected static $carrier_colissimo_id_reference = 5;

    protected $default_id_lang;

    private $_logger;

    protected $errors = array();


    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
        // changement pour le module colissimo initialement non prévu
        // ne pas oublier de set la config 
        // (champ dans parametres généraux ou dans la table config directement)
        $carrier_colissimo_id_reference = Configuration::get('COLISSIMO_ID_REFERENCE');
        if($carrier_colissimo_id_reference) {
          self::$carrier_colissimo_id_reference = $carrier_colissimo_id_reference;
        }
        
    }

    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {

      $cnc = $velo = $voiture = $colissimo = 0;
      $sql = 'SELECT * FROM `'._DB_PREFIX_.'product`';
      $results = DB::getInstance()->executeS($sql); 
      if(is_array($results) && $results) {
        $this->log(count($results).' Produits trouvés');

        $sql = 'SELECT id_shop FROM `'._DB_PREFIX_.'shop`';
        $shops = DB::getInstance()->executeS($sql); 

        foreach ($results as $key => $result) {
          $carrier_list = array();
          $id_product = $result['id_product'];

          $delai_colissimo = !is_null($result['delai_colissimo']) && $result['delai_colissimo'] > 0;
          $delai_clickandcollect = !is_null($result['delai_clickandcollect']) && $result['delai_clickandcollect'] > 0;
          $delai_livraison_velo = !is_null($result['delai_livraison_velo']) && $result['delai_livraison_velo'] > 0;
          $delai_livraison_voiture = !is_null($result['delai_livraison_voiture']) && $result['delai_livraison_voiture'] > 0;

          if($delai_colissimo) { 
            $this->log('Produit #'.$id_product.' Ajout carrier Colissimo');
            $colissimo++;
            $carrier_list[] = self::$carrier_colissimo_id_reference;
          }

          if($delai_clickandcollect) {
            $this->log('Produit #'.$id_product.' Ajout carrier Click and collect');
            $cnc++;
            $carrier_list[] = self::$carrier_cnc_id_reference;
          }

          if($delai_livraison_velo) {
            $this->log('Produit #'.$id_product.' Ajout carrier RDV velo');
            $voiture++;
            $carrier_list[] = self::$carrier_velo_id_reference;
          } 

          if($delai_livraison_voiture) {
            $this->log('Produit #'.$id_product.' Ajout carrier RDV voiture');      
            $rdv++;
            $carrier_list[] = self::$carrier_voiture_id_reference;
          } 
          $this->log('Produit #'.$id_product.' '.print_r($carrier_list, true));   

          self::setCarriers($carrier_list, $id_product, $shops);
        }
      }
      $this->log('Carriers Colissimo#'.$colissimo.' Click and Collect#'.$cnc.' RDV#'.$rdv);    
    }

    private static function setCarriers($carrier_list, $id_product, $id_shops)
    {
        $data = array();

        foreach ($carrier_list as $carrier) {
          foreach ($id_shops as $key => $shop) {
            if(isset($shop['id_shop'])) {
              $data[] = array(
                  'id_product' => (int)$id_product,
                  'id_carrier_reference' => (int)$carrier,
                  'id_shop' => (int)$shop['id_shop']
              );
            }
          }
        }

        Db::getInstance()->execute(
            'DELETE FROM `'._DB_PREFIX_.'product_carrier`
            WHERE id_product = '.(int)$id_product
        );

        $unique_array = array();
        foreach ($data as $sub_array) {
            if (!in_array($sub_array, $unique_array)) {
                $unique_array[] = $sub_array;
            }
        }

        if (count($unique_array)) {
            Db::getInstance()->insert('product_carrier', $unique_array, false, true, Db::INSERT_IGNORE);
        }
    }
}

$timestamp_debut = microtime(true);

$import = new SetProductLivraison();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
echo 'Exécution du script : ' . $difference_ms . ' secondes.'."\n";
  