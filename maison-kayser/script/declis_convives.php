<?php

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');//au cas ou params serveur empeche le cli sans max_exec
include_once 'vendor/autoload.php'; // logger
include_once(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Class d'import produit initial // pas de maj détruit toute les données si le test du csv est bon
 */
class CreateDeclisConvives {

	private static $id_feature_convives = 5; 
	private static $id_attribute_group = 8; 
    private $_logger;

    private $combinations = array();
    private $products = array();
    private $prices_by_shop = array();
    private $price_impact = array();
    private $categories = array();
    private $attributes = array();

    protected $errors = array();
    protected $default_id_lang;



    public function __construct() {
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->employee = new Employee(1);
        define('_PRODUCT_SCRIPT_', true);
        define('_PS_ADMIN_DIR_', true);
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  
        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 

    }


    public function log($str) {
        echo $str . "\r\n";
        $this->_logger->info($str);
    }

    public function start() {
    	//recupération des produits disposant d'une feature convive
    	$sql = 'SELECT * FROM `'._DB_PREFIX_.'product` p
	    		INNER JOIN `'._DB_PREFIX_.'product_lang` pl
	    			ON pl.`id_product` = p.`id_product` 
	    			AND pl.`id_shop` = 1 AND pl.`id_lang` = '.$this->default_id_lang.'
    			INNER JOIN `'._DB_PREFIX_.'feature_product` fp 
	    			ON fp.`id_product` = p.`id_product` 
	    			AND fp.`id_feature` = '.self::$id_feature_convives.'
	    		INNER JOIN `'._DB_PREFIX_.'feature_value_lang` fvl
	    			ON fp.`id_feature_value` = fvl.`id_feature_value`
	    			AND fvl.`id_lang` = '.$this->default_id_lang.'
	    		INNER JOIN `'._DB_PREFIX_.'category_product` cp
	    			ON cp.`id_product` = p.`id_product`
	    			AND fvl.`id_lang` = '.$this->default_id_lang.'
    			';


    	$rows = DB::getInstance()->executeS($sql);
    	if(count($rows)) {
    		$products_ids = array();
    		$this->log('Création du tableau de déclinaisons');
    		foreach ($rows as $key => $row) {
    			if($row['value'] <= 1) {
    				continue;
    			}
    			// pour assembler les produits qui n'ont pas d'id, de ref ou autre communs
    			// on prend le nom
    			$identifier = Tools::link_rewrite($row['name']);

    			if(!isset($this->combinations[$identifier])) {
    				$this->combinations[$identifier] = array();
    				$this->products[$identifier] = $row;
    				$this->categories[$identifier] = array();
    			}
    			// init des attributs
    			if(!in_array($row['value'], $this->attributes)) {
    				$this->attributes[] = $row['value'];
    			}
    			if(!in_array($row['id_category'], $this->categories[$identifier])) {
    				$this->categories[$identifier][] = $row['id_category'];
    			}
    			
    			$products_ids[$row['id_product']] = $row['id_product'];

    			$this->combinations[$identifier][$row['value']] = $row;

    		}			
    		//debug
			// $this->combinations = array(
			// 	key($this->combinations) => current($this->combinations)
			// );
			

    		$sql = 'SELECT id_product, id_shop, price FROM `'._DB_PREFIX_.'product_shop` ps
    				WHERE id_product IN ('. implode(',', $products_ids) .')';
    		$prices = DB::getInstance()->executeS($sql);
    		$this->prices_by_shop = array();
    		foreach ($prices as $key => $product) {
    			if(!isset($this->prices_by_shop[$product['id_product']]))
    				$this->prices_by_shop[$product['id_product']] = array();
    			$this->prices_by_shop[$product['id_product']][$product['id_shop']] = $product['price'];
    		}

    		$this->log('Création des attributs');
			// création des attributs qui n'existent pas
			foreach ($this->attributes as $key => $value) {
				if(!is_numeric($value) || $value <= 1)
					continue;

				if(Attribute::isAttribute(self::$id_attribute_group, $value, $this->default_id_lang)) {
					$this->log('L\'Attribut '.$value.' existe déja.');
					
					// debug
					// permet la suppresion d'attributs
					// $curAttr = new Attribute($this->getAttributeId($value));
					// if($curAttr->delete()){
					// 	$this->log('L\'Attribut '.$value.' a été supprimé.');	
					// } else {
					// 	$this->log('L\'Attribut '.$value.' n\'a pas été supprimé.');				
					// }

				} else {
					$this->log('Création de l\'Attribut '.$value);
	    			$attribute = new Attribute();
	    			$attribute->id_attribute_group = self::$id_attribute_group;
	    			$attribute->name = array( $this->default_id_lang => $value);
	    			$attribute->color = false;
	    			if($attribute->save()) {
						$this->log('SUCCESS');
	    			} else {
	    				$this->log('FAIL');
	    			}
	    		}
				
			}

			$shops = Shop::getShops(false);

    		foreach ($this->combinations as $name_encoded => $combinations) {
    			if(isset($this->products[$name_encoded])) {    				
    				$this->log('Création des déclis '.$name_encoded.' #'.$this->products[$name_encoded]['id_product']);
    				$product = new Product($this->products[$name_encoded]['id_product'], true, $this->default_id_lang, 1);

    				$combinaisons = $product->getAttributeCombinations();

					$default = true;
    				$product_price = 0;
    				$product_weight = 0;

    				foreach ($combinations as $nb_convives => $row) {
						$combinaison_existante = false;
			           	if($default) {
			           		// le prix n'est pas set sans cart
			           		$product->price = $row['price'];
			           		$product->weight = $row['weight'];

			           		$product_price = $row['price'];
			           		$product_weight = $row['weight'];
			           	}

				        $attributes = (array) $this->getAttributeId($nb_convives); 
				        // verification que la combi ne soit pas déj) créée
			           	if(count($combinaisons)) {
			           		$default = false;
			           		foreach ($combinaisons as $key => $combinaison) {
			           			if($combinaison['id_attribute'] == current($attributes)) {
			           				$combinaison_existante = true;
			           				break;
			           			}
			           		}

			           		if($combinaison_existante) {
			           			continue 2;
			           		}
			           	}


			           	$price_impact = $row['price'] - $product_price;
			           	$weight_impact = $row['weight'] - $product_weight;

			           	try {

			           		if($combination_id = $product->addAttribute(
                                                  $price_impact, 
                                                  $weight_impact, 
                                                  0, 
                                                  0, 
                                                  array(), 
                                                  $row['id_sage'], 
                                                  $row['ean13'],
                                                  $default
	                                                )
				            ) {
				             
					           	if(!isset($this->price_impact[$row['id_product']])) {
					           		$this->price_impact[$row['id_product']] = array(
					           				'id_product_ref' => $product->id,
					           				'price' => $product_price,
					           				'id_product_attribute' => $combination_id,

					           		);
					           	}
					            $combination = new Combination($combination_id);
					             
					            // affecte l'attribut en cours à la décli tout fraichement créée
					            $combination->setAttributes($attributes); 
					            // ajout du stock
					            foreach ($shops as $key => $shop) {
					            	$sql = 'SELECT quantity FROM `'._DB_PREFIX_.'stock_available` 
					            			WHERE id_product = '.$row['id_product'].'
					            			AND id_shop = '. $shop['id_shop'];
					            	$quantity = DB::getInstance()->getValue($sql);
					            	if(!$quantity)
					            		$quantity = 0;
					            	StockAvailable::setQuantity($product->id, $combination_id, $quantity, $shop['id_shop']);
					            	$this->log('Ajout du Stock : '.$quantity.' SHOP#'. $shop['id_shop']);
					            }
				              
					           	$images = DB::getInstance()->executeS('SELECT id_image FROM  `'._DB_PREFIX_.'image` WHERE id_product = '.$row['id_product']);
						        $images = array_column($images, 'id_image');
					            if(!$default) {

						            $sql = '	UPDATE `'._DB_PREFIX_.'image` 
						              			SET id_product = '.$product->id.', 
						              			cover = NULL 
						              			WHERE id_product = '.$row['id_product'];
						            DB::getInstance()->execute($sql);

						            $sql = '	UPDATE `'._DB_PREFIX_.'image_shop` 
						              			SET id_product = '.$product->id.', 
						              				cover = NULL 
						              			WHERE id_product = '.$row['id_product'];
						            DB::getInstance()->execute($sql);
						        }

					            if($combination->setImages($images)) {
									$this->log('Images ajoutées : '.implode(',', $images));
					            } else {
									$this->log('ERREUR Images : '.implode(',', $images));
					            }
				              
					            $this->log('L\'attribut #'.implode(',', $attributes).' ('.$nb_convives.' convives) a été ajouté avec succès'); 
					            $default = false;

				              	// desactivation de l'ancien produit 
				              	if($product->id != $row['id_product']) {

					              	$sql = 'UPDATE `'._DB_PREFIX_.'product` 
					              			SET active = 0 
					              			WHERE id_product = '.$row['id_product'];

					              	if(DB::getInstance()->execute($sql)) {
										$this->log('Produit#'.$row['id_product'].' désactivé.');
					              	} else {
					              		$this->log('ERREUR Produit#'.$row['id_product'].' non désactivé.');
					              	}

					              	$sql = 'UPDATE `'._DB_PREFIX_.'product_shop`
					              			SET active = 0 
					              			WHERE id_product = '.$row['id_product'];

					              	if(DB::getInstance()->execute($sql)) {
										$this->log('Produit#'.$row['id_product'].' désactivé (all shops).');
					              	} else {
					              		$this->log('ERREUR Produit#'.$row['id_product'].' non désactivé (all shops).');
				              		}
				              	}
				              	// suppression convives
				              	$sql = 'DELETE FROM `'._DB_PREFIX_.'feature_product` 
				              			WHERE id_product = '.$row['id_product'].'
				              			AND id_feature = '.self::$id_feature_convives;
				              	if(DB::getInstance()->execute($sql)) {
									$this->log('Produit#'.$row['id_product'].' Feature convives enlevée.');
				              	} else {
				              		$this->log('ERREUR Produit#'.$row['id_product'].' Feature convives non supprimée.');
				              	}
				            } else {
				               $this->log('ERREUR L\'attribut #'.implode(',', $attributes).' ('.$nb_convives.' convives) n\'a pas été ajouté'); 
				            }
			           	} catch (Exception $e) {
			           		$this->log('ERREUR '.$e->getMessage());			           		
			           	}
    				}
    				// end foreach combi
    				// Ajout des catégories
    				$product->addToCategories($this->categories[$name_encoded]);
    			}
    		}
			// recupération des prix
			foreach ($this->prices_by_shop as $id_product => $shops) {
				if(isset($this->price_impact[$id_product])) {

					$id_product_ref = $this->price_impact[$id_product]['id_product_ref'];
					$id_product_attribute = $this->price_impact[$id_product]['id_product_attribute'];
					$impact = $this->price_impact[$id_product]['price'];

					$sql = '';
					foreach ($shops as $id_shop => $price) {

						$sql .= 'UPDATE `'._DB_PREFIX_.'product_attribute_shop` 
							SET price = '.($price - $impact).'
							WHERE id_product = '.$id_product_ref.'
							AND id_product_attribute = '.$id_product_attribute.'
							AND id_shop = '.$id_shop.'; ';
						
					}
					if(DB::getInstance()->execute($sql)) {
						$this->log('MAJ DES PRIX Product#'.$id_product.' P#'.$id_product_ref.' IPA#'.$id_product_attribute.' OK');
					} else {
						$this->log('ERREUR MAJ DES PRIX Product#'.$id_product.' P#'.$id_product_ref.' IPA#'.$id_product_attribute.' KO');
					}
				}
			}

    		// end foreach products
    	} else {
    		$this->log('Création du tableau de déclinaisons impossible, aucun produit correspondant trouvé');
    	}
    }

    private function getAttributeId($value) {
    	 return Db::getInstance()->getValue('
			SELECT a.`id_attribute`
			FROM `'._DB_PREFIX_.'attribute_group` ag
			LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
				ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int) $this->default_id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'attribute` a
				ON a.`id_attribute_group` = ag.`id_attribute_group`
			LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
				ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int) $this->default_id_lang.')
			'.Shop::addSqlAssociation('attribute_group', 'ag').'
			'.Shop::addSqlAssociation('attribute', 'a').'
			WHERE al.`name` = \''.pSQL($value).'\' AND ag.`id_attribute_group` = '.(int) self::$id_attribute_group.'
			ORDER BY agl.`name` ASC, a.`position` ASC
		');
    }
}

$instance = new CreateDeclisConvives();
$instance->start();