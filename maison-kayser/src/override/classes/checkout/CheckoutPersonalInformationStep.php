<?php


class CheckoutPersonalInformationStep extends CheckoutPersonalInformationStepCore
{
   
    public function handleRequest(array $requestParameters = array())
    {
        parent::handleRequest($requestParameters);
        // ADD BY ESTEBANW INSITACTION 09052018
        // EMPECHE LA REDIRECTION VER LA BOULANGERIE PREFEREE
        // CONTEXT : 
        // LE CLIENT A AJOUTE DES PRODUITS SUR UNE BOULANGERIE SANS ETRE CONNECTE, 
        // IL SE CONNECTE, POUR NE PAS PERDRE LE PANIER ON MET LA BOULANGERIE ACTUELLE EN PREFEREE
        if (isset($requestParameters['submitLogin'])
                && $this->step_is_complete) {
            $context = Context::getContext();
            $context->customer->setMyShop($context->shop->id);
        }
        // END
    }
}
