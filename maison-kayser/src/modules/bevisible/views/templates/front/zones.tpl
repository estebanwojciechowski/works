{* listing des groupes et zones *}
<h2>{l s='France'}</h2>
{if isset($zones_france) && count($zones_france)} 
	<ul>
    {foreach from=$zones_france item=zone} 
	    <li>
	    	<h3>
	    		<a href="{$link->getModuleLink(
							    		'bevisible', 
							    		'map-zone', 
							    		[
							    			'id_zone' => $zone.id_bevisible_group,
							    			'id_country' => false,
							    			'wanted_url' => Tools::link_rewrite($zone.name)
							    		]
							    	)}{if $back}?back={$back}{/if}" 
					class="bevisible-showzone" data-bvshow="{$zone.name}">	    	
					{$zone.name}
			    </a>
			</h3>
		</li>   
    {/foreach}
	</ul>
{/if} 

{if isset($zones) && count($zones)} 
	<h2>{l s='International'}</h2>
	<ul>
    {foreach from=$zones item=zone} 
	    <li>
	    	<h3>
	    		<a href="{$link->getModuleLink(
							    		'bevisible', 
							    		'map-zone', 
							    		[
							    			'id_zone' => $zone.id_bevisible_group,
							    			'id_country' => false,
							    			'wanted_url' => Tools::link_rewrite($zone.name)
							    		]
							    	)}{if $back}?back={$back}{/if}" 
					class="bevisible-showzone" data-bvshow="{$zone.name}">	    	
					{$zone.name}
			    </a>
			</h3>
		</li>   
    {/foreach}
	</ul>
{/if}  