{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{* <div class="container"> *}
  {* <section id="content" class="">
    {block name='page_content'}

      <div class="container-404">
        <div class="flex-50">
          <div class="container-404__inner container-404__left">
            <h4>{l s='Oups ! Erreur' d='Shop.Theme.Global'}</h4>
            <h4 class="big">{l s="404"}</h4>
            <p>{l s="Cette page n'existe plus." d='Shop.Theme.Global'}</p>

            <div class="btn__container">
              <a href="{$urls.base_url}" class="btn">{l s="Retour à la page d'accueil" d='Shop.Theme.Global'}</a>
            </div>
          </div>
        </div>

        <div class="flex-50">
          <div class="container-404__inner">
            <img src="{$urls.base_url}{$smarty.const._THEME_DIR_}img/404.jpg" alt="Erreur 404">
          </div>
        </div>
      </div>

    {/block}
  </section> *}
{* </div> *}

<section id="content" class="page-content page-not-found">
  {block name='page_content'}

    <div class="container-404">
      <div class="flex-50">
        <div class="container-404__inner container-404__left">
          <h4>{l s='Nous vous prions de nous excuser pour la gêne occasionnée.' d='Shop.Theme.Global'}</h4>
          <p>{l s='Search again what you are looking for' d='Shop.Theme.Global'}</p>

          {block name='search'}
            {hook h='displaySearch'}
          {/block}

          <div class="btn__container">
            <a href="{$urls.base_url}" class="btn">{l s="Retour à la page d'accueil" d='Shop.Theme.Global'}</a>
          </div>
        </div>
      </div>

      <div class="flex-50">
        <div class="container-404__inner">
          <img src="{$urls.base_url}{$smarty.const._THEME_DIR_}img/404.jpg" alt="Erreur 404">
        </div>
      </div>
    </div>

    {* {block name='search'}
      {hook h='displaySearch'}
    {/block} *}

    {block name='hook_not_found'}
      {hook h='displayNotFound'}
    {/block}

  {/block}
</section>
