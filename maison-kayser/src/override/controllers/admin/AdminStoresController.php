<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */


include_once(_PS_ROOT_DIR_.'/../script/import_stores.php');
/**
 * @property Store $object
 */
class AdminStoresController extends AdminStoresControllerCore
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'store';
        $this->className = 'Store';
        $this->lang = false;
        $this->toolbar_scroll = false;

        parent::__construct();

        if (!Tools::getValue('realedit')) {
            $this->deleted = false;
        }

        $this->fieldImageSettings = array(
            'name' => 'image',
            'dir' => 'st'
        );

        $this->fields_list = array(
            'id_store' => array('title' => $this->trans('ID', array(), 'Admin.Global'), 'align' => 'center', 'class' => 'fixed-width-xs'),
            'name' => array('title' => $this->trans('Name', array(), 'Admin.Global'), 'filter_key' => 'a!name'),
            'address1' => array('title' => $this->trans('Address', array(), 'Admin.Global'), 'filter_key' => 'a!address1'),
            'city' => array('title' => $this->trans('City', array(), 'Admin.Global')),
            'quartier' => array('title' => $this->trans('Quartier', array(), 'Admin.Global')),
            'arrondissement' => array('title' => $this->trans('Arrondissement', array(), 'Admin.Global')),
            'type_payment' => array('title' => $this->trans('Type de paiement', array(), 'Admin.Global')),
            'identifiant_caisse' => array('title' => $this->trans('Identifiant de caisse', array(), 'Admin.Global')),
            'postcode' => array('title' => $this->trans('Zip/postal code', array(), 'Admin.Global')),
            'state' => array('title' => $this->trans('State', array(), 'Admin.Global'), 'filter_key' => 'st!name'),
            'country' => array('title' => $this->trans('Country', array(), 'Admin.Global'), 'filter_key' => 'cl!name'),
            'phone' => array('title' => $this->trans('Phone', array(), 'Admin.Global')),
            'fax' => array('title' => $this->trans('Fax', array(), 'Admin.Global')),
            'active' => array('title' => $this->trans('Enabled', array(), 'Admin.Global'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
            'sans_gluten' => array('title' => $this->trans('Service sans gluten', array(), 'Admin.Global'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
            'restaurant' => array('title' => $this->trans('Service restaurant', array(), 'Admin.Global'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
            'cafe' => array('title' => $this->trans('Service cafe', array(), 'Admin.Global'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
            'terrasse' => array('title' => $this->trans('Service terrasse', array(), 'Admin.Global'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
            'colissimo' => array('title' => $this->trans('Livraison Colissimo', array(), 'Admin.Global'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
            'clickandcollect' => array('title' => $this->trans('Livraison Click and Collect', array(), 'Admin.Global'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
        );

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
                'icon' => 'icon-trash'
            )
        );

        $this->_buildOrderedFieldsShop($this->_getDefaultFieldsContent());
    }

    public function initPageHeaderToolbar() {

        $this->page_header_toolbar_btn['import'] = array(
                'href' => self::$currentIndex.'&importstores&token='.$this->token,
                'desc' => $this->trans('Import et Création Ecommerce par CSV', array(), 'Admin.Shopparameters.Feature'),
                'icon' => 'process-icon-import'
            );
        parent::initPageHeaderToolbar();

    }

    public function renderForm()
    {
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $image = _PS_STORE_IMG_DIR_.$obj->id.'.jpg';
        $image_url = ImageManager::thumbnail($image, $this->table.'_'.(int)$obj->id.'.'.$this->imageType, 350,
            $this->imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        $tmp_addr = new Address();
        $res = $tmp_addr->getFieldsRequiredDatabase();
        $required_fields = array();
        foreach ($res as $row) {
            $required_fields[(int)$row['id_required_field']] = $row['field_name'];
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Stores', array(), 'Admin.Shopparameters.Feature'),
                'icon' => 'icon-home'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->trans('Identifiant Magasin', array(), 'Admin.Global'),
                    'name' => 'identifiant',
                    'required' => false,
                    'hint' => array(
                        $this->trans('Laisser vide si inchangé', array(), 'Admin.Shopparameters.Feature')
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Name', array(), 'Admin.Global'),
                    'name' => 'name',
                    'required' => false,
                    'hint' => array(
                        $this->trans('Store name (e.g. City Center Mall Store).', array(), 'Admin.Shopparameters.Feature'),
                        $this->trans('Allowed characters: letters, spaces and %s', array(), 'Admin.Shopparameters.Feature')
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Address', array(), 'Admin.Global'),
                    'name' => 'address1',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Address (2)', array(), 'Admin.Global'),
                    'name' => 'address2'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Zip/postal code', array(), 'Admin.Global'),
                    'name' => 'postcode',
                    'required' => in_array('postcode', $required_fields)
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('City', array(), 'Admin.Global'),
                    'name' => 'city',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Quartier', array(), 'Admin.Global'),
                    'name' => 'quartier',
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Arrondissement', array(), 'Admin.Global'),
                    'name' => 'arrondissement',
                    'required' => false
                ),
                array(
                    'type' => 'select',
                    'label' => $this->trans('Country', array(), 'Admin.Global'),
                    'name' => 'id_country',
                    'required' => true,
                    'default_value' => (int)$this->context->country->id,
                    'options' => array(
                        'query' => Country::getCountries($this->context->language->id),
                        'id' => 'id_country',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->trans('State', array(), 'Admin.Global'),
                    'name' => 'id_state',
                    'required' => true,
                    'options' => array(
                        'id' => 'id_state',
                        'name' => 'name',
                        'query' => null
                    )
                ),
                array(
                    'type' => 'latitude',
                    'label' => $this->trans('Latitude / Longitude', array(), 'Admin.Shopparameters.Feature'),
                    'name' => 'latitude',
                    'required' => true,
                    'maxlength' => 12,
                    'hint' => $this->trans('Store coordinates (e.g. 45.265469/-47.226478).', array(), 'Admin.Shopparameters.Feature')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Place ID Google', array(), 'Admin.Global'),
                    'name' => 'note',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Phone', array(), 'Admin.Global'),
                    'name' => 'phone'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Fax', array(), 'Admin.Global'),
                    'name' => 'fax'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Email address', array(), 'Admin.Global'),
                    'name' => 'email'
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->trans('Description', array(), 'Admin.Global'),
                    'name' => 'description',
                    'autoload_rte' => true,
                    'lang' => true,
                    'hint' => $this->trans('Invalid characters:', array(), 'Admin.Notifications.Info').' <>;=#{}'
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->trans('En ce moment dans votre boulangerie', array(), 'Admin.Global'),
                    'name' => 'commercial',
                    'autoload_rte' => true,
                    'lang' => true,
                    'hint' => $this->trans('Invalid characters:', array(), 'Admin.Notifications.Info').' <>;=#{}'
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Active', array(), 'Admin.Global'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    ),
                    'hint' => $this->trans('Whether or not to display this store.', array(), 'Admin.Shopparameters.Help')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->trans('Picture', array(), 'Admin.Shopparameters.Feature'),
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'hint' => $this->trans('Storefront picture.', array(), 'Admin.Shopparameters.Help')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->trans('Type de paiement', array(), 'Admin.Global'),
                    'name' => 'type_payment',
                    //'list' => array('1' => 'Banque Populaire', '2' => 'Caisse d\'épargne'),
                    'options' => array(
                        'id' => 'id_payment',
                        'name' => 'name',
                        'query' => null, // TODO faire un Payment::getSystemPayType($this->context->language->id),
                    ),
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Identifiant de caisse', array(), 'Admin.Global'),
                    'name' => 'identifiant_caisse',
                    'required' => false
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Service Sans gluten', array(), 'Admin.Global'),
                    'name' => 'sans_gluten',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'sans_gluten_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'sans_gluten_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Service Restaurant', array(), 'Admin.Global'),
                    'name' => 'restaurant',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'restaurant_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'restaurant_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Service Cafe', array(), 'Admin.Global'),
                    'name' => 'cafe',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'cafe_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'cafe_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Service Terrasse', array(), 'Admin.Global'),
                    'name' => 'terrasse',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'terrasse_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'terrasse_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Livraison Colissimo', array(), 'Admin.Global'),
                    'name' => 'colissimo',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'colissimo_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'colissimo_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Livraison Click and Collect', array(), 'Admin.Global'),
                    'name' => 'clickandcollect',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'clickandcollect_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'clickandcollect_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
            ),
            'hours' => array(
            ),
            'hours_clickandcollect' => array(
            ),
            'special_days' => array(
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            )
        );

        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->trans('Shop association', array(), 'Admin.Global'),
                'name' => 'checkBoxShopAsso',
            );
        }

        $days = array();
        $days[1] = $this->trans('Monday', array(), 'Admin.Shopparameters.Feature');
        $days[2] = $this->trans('Tuesday', array(), 'Admin.Shopparameters.Feature');
        $days[3] = $this->trans('Wednesday', array(), 'Admin.Shopparameters.Feature');
        $days[4] = $this->trans('Thursday', array(), 'Admin.Shopparameters.Feature');
        $days[5] = $this->trans('Friday', array(), 'Admin.Shopparameters.Feature');
        $days[6] = $this->trans('Saturday', array(), 'Admin.Shopparameters.Feature');
        $days[7] = $this->trans('Sunday', array(), 'Admin.Shopparameters.Feature');

        $hours = array();

        $hours_temp = json_decode($this->getFieldValue($obj, 'hours'));
        if (!empty($hours_temp)) {
            foreach ($hours_temp as $k => $h) {
                if(is_array($h))
                    $h = implode(' | ', $h);
                $hours[] = $h;
                $this->fields_value['hours_'.$k] = $h;
            }
        }

        $hoursCandC = array();

        $hoursCandC_temp = json_decode($this->getFieldValue($obj, 'hours_clickandcollect'));
        if (!empty($hoursCandC_temp)) {
            foreach ($hoursCandC_temp as $k => $h) {
                if(is_array($h))
                    $h = implode(' | ', $h);
                $hoursCandC[] = $h;
                $this->fields_value['hours_clickandcollect_'.$k] = $h;
            }
        }

        if(Validate::isLoadedObject($obj))
            $special_days = StoreSpecialDay::getAll($obj->id);

        if(!$special_days)
            $special_days = array();

        $this->fields_value = array(
            'latitude' => $this->getFieldValue($obj, 'latitude') ? $this->getFieldValue($obj, 'latitude') : '',
            'longitude' => $this->getFieldValue($obj, 'longitude') ? $this->getFieldValue($obj, 'longitude') : '',
            'days' => $days,
            'hours' => $hours,
            'hours_clickandcollect' => $hoursCandC,
            'special_days' => $special_days
        );

        return AdminController::renderForm();
    }

    public function postProcess() {

        if (isset($_POST['submitAdd'.$this->table])) {
            /* Cleaning fields */
            foreach ($_POST as $kp => $vp) {
                if (!in_array($kp, array('checkBoxShopGroupAsso_store', 'checkBoxShopAsso_store', 'special_days', 'special_days_hours', 'special_days_fermeture'))) {
                    $_POST[$kp] = trim($vp);

                }
            }

            /* Rewrite latitude and longitude to 8 digits */
            $_POST['latitude'] = number_format((float)$_POST['latitude'], 8);
            $_POST['longitude'] = number_format((float)$_POST['longitude'], 8);

            /* If the selected country does not contain states */
            $id_state = (int)Tools::getValue('id_state');
            $id_country = (int)Tools::getValue('id_country');
            $country = new Country((int)$id_country);

            if ($id_country && $country && !(int)$country->contains_states && $id_state) {
                $this->errors[] = $this->trans('You\'ve selected a state for a country that does not contain states.', array(), 'Admin.Advparameters.Notification');
            }

            /* If the selected country contains states, then a state have to be selected */
            if ((int)$country->contains_states && !$id_state) {
                $this->errors[] = $this->trans('An address located in a country containing states must have a state selected.', array(), 'Admin.Shopparameters.Notification');
            }

            $latitude = (float)Tools::getValue('latitude');
            $longitude = (float)Tools::getValue('longitude');

            if (empty($latitude) || empty($longitude)) {
                $this->errors[] = $this->trans('Latitude and longitude are required.', array(), 'Admin.Shopparameters.Notification');
            }

            $postcode = Tools::getValue('postcode');
            /* Check zip code format */
            if ($country->zip_code_format && !$country->checkZipCode($postcode)) {
                $this->errors[] = $this->trans('Your Zip/postal code is incorrect.', array(), 'Admin.Notifications.Error').'<br />'.$this->trans('It must be entered as follows:', array(), 'Admin.Notifications.Error').' '.str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $country->zip_code_format)));
            } elseif (empty($postcode) && $country->need_zip_code) {
                $this->errors[] = $this->trans('A Zip/postal code is required.', array(), 'Admin.Notifications.Error');
            } elseif ($postcode && !Validate::isPostCode($postcode)) {
                $this->errors[] = $this->trans('The Zip/postal code is invalid.', array(), 'Admin.Notifications.Error');
            }
            /* Store hours */
            $_POST['hours'] = array();
            $hours = [];
            for ($i = 1; $i < 8; $i++) {
                $hours[] = explode(' | ', Tools::getValue('hours_'.(int)$i));
            }
            $_POST['hours'] = json_encode($hours);
            /* Store hoursCandC */
            $_POST['hours_clickandcollect'] = array();
            $hoursCandC = [];
            for ($i = 1; $i < 8; $i++) {
                $hoursCandC[] = explode(' | ', Tools::getValue('hours_clickandcollect_'.(int)$i));
            }
            $_POST['hours_clickandcollect'] = json_encode($hoursCandC);

            $this->processSpecialDays();

        }

        if (!count($this->errors)) {
            parent::postProcess();
        } else {
            $this->display = 'add';
        }
    }

    /**
     * [processSpecialDays gestion de l'enregistrement des jours de fermeture]
     * @return [type] [description]
     */
    private function processSpecialDays() {

        $special_days = Tools::getValue('special_days');
        $special_days_fermeture = Tools::getValue('special_days_fermeture');
        $special_days_hours = Tools::getValue('special_days_hours');

        $existants_special_days = StoreSpecialDay::getAll($this->id_object);
        $champs_vide = array();
        // vérification et enregistrement de toutes les dates valides
        foreach ($special_days as $key => $special_day) {
            $id_store_delay = preg_match('/^id_([0-9]+)/', $key, $matches);

            // Maj de l'entrée
            if($id_store_delay && $matches && isset($matches[1])) {

                $id_store_delay = $matches[1];
                $new_day = new StoreSpecialDay($id_store_delay);
                unset($existants_special_days[$id_store_delay]);
            } else {
                // Ajout d'une nouvelle entrée
                $new_day = new StoreSpecialDay();
            }
            // special_day = date
            // special_days_hours = horaire (si vide = fermeture exceptionnelle)
           $special_days_hours_value = str_replace('h', ':', trim($special_days_hours[$key]));
           if(in_array($special_days_hours, Store::$store_closed_value)) {
                $special_days_hours = '';
           }

           if(strlen(trim($special_day)) && !Validate::isDateFr($special_day)) {
                $this->errors[] = 'La date : '.$special_day.' est invalide.';
           } elseif(strlen($special_days_hours_value) && !Validate::isKayserHoraires($special_days_hours_value)) {
                $this->errors[] = 'L\'horaire de la date : '.$special_day.' est invalide : '.$special_days_hours_value.' (Format souhaité : 10:00-18:00)';
           } elseif (!strlen(trim($special_day))) {
                // date vide = dernier champ ou juste saisie a vide
                if($id_store_delay)
                    $champs_vide[] = $id_store_delay;
           } else {
                // fonctionnement normal
                $date = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $special_day)));

                $existant_day = StoreSpecialDay::getByDate($date, $this->id_object, $id_store_delay);

                if(!$existant_day) {
                    $new_day->id_store = $this->id_object;
                    $new_day->date = $date;
                    $new_day->fermeture = strlen($special_days_hours_value) ? 0 : 1;
                    $new_day->hours = $special_days_hours_value;

                    if(!$new_day->save()) {
                        $this->errors[] = 'Une erreur s\'est produite lors de l\'enregistrement de la date '.$special_day;
                    }
                } else {
                    $this->errors[] = 'La date spéciale '.$special_day.' existe déjà !';
                }
           }
        }

        if(empty($this->errors)) {
            if($existants_special_days && !empty($existants_special_days)) {

                foreach ($existants_special_days as $key => $value) {
                    $day = new StoreSpecialDay($value['id_special_day']);
                    $day->delete();
                }

            }

            if($champs_vide && !empty($champs_vide)) {

                foreach ($champs_vide as $key => $value) {
                    $day = new StoreSpecialDay($value);
                    $day->delete();
                }

            }

        }

    }

    protected function _getDefaultFieldsContent()
    {
        $this->context = Context::getContext();
        $countryList = array();
        $countryList[] = array('id' => '0', 'name' => $this->trans('Choose your country', array(), 'Admin.Shopparameters.Feature'));
        foreach (Country::getCountries($this->context->language->id) as $country) {
            $countryList[] = array('id' => $country['id_country'], 'name' => $country['name']);
        }
        $stateList = array();
        $stateList[] = array('id' => '0', 'name' => $this->trans('Choose your state (if applicable)', array(), 'Admin.Shopparameters.Feature'));
        foreach (State::getStates($this->context->language->id) as $state) {
            $stateList[] = array('id' => $state['id_state'], 'name' => $state['name']);
        }

        $formFields = array(
            'PS_SHOP_NAME' => array(
                'title' => $this->trans('Shop name', array(), 'Admin.Shopparameters.Feature'),
                'hint' => $this->trans('Displayed in emails and page titles.', array(), 'Admin.Shopparameters.Feature'),
                'validation' => 'isGenericName',
                'required' => true,
                'type' => 'text',
                'no_escape' => true,
            ),
            'PS_SHOP_EMAIL' => array('title' => $this->trans('Shop email', array(), 'Admin.Shopparameters.Feature'),
                'hint' => $this->trans('Displayed in emails sent to customers.', array(), 'Admin.Shopparameters.Help'),
                'validation' => 'isEmail',
                'required' => true,
                'type' => 'text'
            ),
            'PS_SHOP_DETAILS' => array(
                'title' => $this->trans('Registration number', array(), 'Admin.Shopparameters.Feature'),
                'hint' => $this->trans('Shop registration information (e.g. SIRET or RCS).', array(), 'Admin.Shopparameters.Help'),
                'validation' => 'isGenericName',
                'type' => 'textarea',
                'cols' => 30,
                'rows' => 5
            ),
            'PS_SHOP_ADDR1' => array(
                'title' => $this->trans('Shop address line 1', array(), 'Admin.Shopparameters.Feature'),
                'validation' => 'isAddress',
                'type' => 'text'
            ),
            'PS_SHOP_ADDR2' => array(
                'title' => $this->trans('Shop address line 2', array(), 'Admin.Shopparameters.Feature'),
                'validation' => 'isAddress',
                'type' => 'text'
            ),
            'PS_SHOP_CODE' => array(
                'title' => $this->trans('Zip/postal code', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
            'PS_SHOP_CITY' => array(
                'title' => $this->trans('City', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
            'PS_SHOP_QUARTIER' => array(
                'title' => $this->trans('Quartier', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
            'PS_SHOP_ARRONDISSEMENT' => array(
                'title' => $this->trans('Arrondissement', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
            'PS_SHOP_TYPEPAYMENT' => array(
                'title' => $this->trans('Type of payment', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
            'PS_SHOP_IDENTIFIANTCAISSE' => array(
                'title' => $this->trans('Identifiant de caisse', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
            'PS_SHOP_COUNTRY_ID' => array(
                'title' => $this->trans('Country', array(), 'Admin.Global'),
                'validation' => 'isInt',
                'type' => 'select',
                'list' => $countryList,
                'identifier' => 'id',
                'cast' => 'intval',
                'defaultValue' => (int)$this->context->country->id
            ),
            'PS_SHOP_STATE_ID' => array(
                'title' => $this->trans('State', array(), 'Admin.Global'),
                'validation' => 'isInt',
                'type' => 'select',
                'list' => $stateList,
                'identifier' => 'id',
                'cast' => 'intval'
            ),
            'PS_SHOP_PHONE' => array(
                'title' => $this->trans('Phone', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
            'PS_SHOP_FAX' => array(
                'title' => $this->trans('Fax', array(), 'Admin.Global'),
                'validation' => 'isGenericName',
                'type' => 'text'
            ),
        );

        return $formFields;
    }

    /**
     * @param ObjectModel $object
     * @return bool
     */
    protected function afterUpdate($object)
    {
        $id_store = Tools::getValue('identifiant');
        if($id_store) {
            if(Validate::isUnsignedInt($id_store)
                && Validate::isUnsignedInt($id_store)
            ) {
                if(!Store::storeExists($id_store)) {
                    $errors =  Store::changeStoreID($object->id, $id_store);
                    if(empty($errors)) {
                        return true;
                    } else {
                        $this->errors = $errors;
                    }
                } else {
                     $link = Context::getContext()->link->getAdminLink('AdminStores', true, array(), array('id_store' => $id_store, 'updatestore' => 1));
                    $this->errors[] = 'L\'identifiant '.$id_store.' est déjà utilisé. <a href="'.$link.'" target="_blank">Voir</a>';

                }
            } else {
                $this->errors[] = 'L\'identifiant '.$destID.' est invalide (nombre entier seulement).';
            }
        }
        return false;
    }


    public function initContent()
    {
        parent::initContent();

        if(isset($_GET['importstores'])) {

            $ligne = '<div class="panel">
                        <div class="panel-heading">
                            <i class="icon-home"></i>
                            Informations sur l\'import
                        </div>
                        <div class="form-wrapper">
                        <p><b>Les colonnes doivent impérativement être respectées : </b></p>
                        <p>Identifiant; Ouverture magasin; Géré par Paris; Nom de l\'établissement; Adresse; CP; Ville; Quartier; Arrondissement; Pays; Latitude; Longitude; Tel; Mail; Lundi; Mardi; Mercredi; Jeudi; Vendredi; Samedi; Dimanche; Lundi; Mardi; Mercredi; Jeudi; Vendredi; Samedi; Dimanche; Type paiement; Identifiant de caisse; sans gluten; restaurant; cafe; terrasse; Colissimo; Clic and Collect</p>
                        </div>
                        <br/>
                        <p>Les boutiques déja existantes ne seront pas mise à jour par csv. Veuillez les modifier directement ci dessous.</p>
                        <br/>
                        <p>Les boutiques seront automatiquement actives, si <b>Colissimo</b> ou <b>Clic and Collect</b> est à <b>oui</b> alors un site ecommerce sera généré. Si tout s\'est bien déroulé, vous pourrez le retrouver ici : <a href="'.$this->context->link->getAdminLink('AdminShop').'">Administration des sites e-commerce</a>
                    </div>

            ';

            $this->content = $ligne . $this->importStores() . $this->content;

            if (isset($_FILES['file'])) {

                $filename_prefix = date('YmdHis').'-';
                $file_full_name = $filename_prefix.'magasins.csv';
                $dir = self::getPath();
                if (isset($_FILES['file']) && !empty($_FILES['file']['error'])) {

                    switch ($_FILES['file']['error']) {
                        case UPLOAD_ERR_INI_SIZE:
                            $_FILES['file']['error'] = $this->trans('The uploaded file exceeds the upload_max_filesize directive in php.ini. If your server configuration allows it, you may add a directive in your .htaccess.', array(), 'Admin.Advparameters.Notification');
                            break;
                        case UPLOAD_ERR_FORM_SIZE:
                            $_FILES['file']['error'] = $this->trans('The uploaded file exceeds the post_max_size directive in php.ini. If your server configuration allows it, you may add a directive in your .htaccess, for example:', array(), 'Admin.Advparameters.Notification')
                            .'<br/><a href="'.$this->context->link->getAdminLink('AdminMeta').'" >
                            <code>php_value post_max_size 20M</code> '.
                            $this->trans('(click to open "Generators" page)', array(), 'Admin.Advparameters.Notification').'</a>';
                            break;
                        break;
                        case UPLOAD_ERR_PARTIAL:
                            $_FILES['file']['error'] = $this->trans('The uploaded file was only partially uploaded.', array(), 'Admin.Advparameters.Notification');
                            break;
                        break;
                        case UPLOAD_ERR_NO_FILE:
                            $_FILES['file']['error'] = $this->trans('No file was uploaded.', array(), 'Admin.Advparameters.Notification');
                            break;
                        break;
                    }
                } elseif (!preg_match('#([^\.]*?)\.(csv|xls[xt]?|o[dt]s)$#is', $_FILES['file']['name'])) {
                    $_FILES['file']['error'] = $this->trans('The extension of your file should be .csv.', array(), 'Admin.Advparameters.Notification');
                } elseif (!@filemtime($_FILES['file']['tmp_name']) ||
                    !@move_uploaded_file($_FILES['file']['tmp_name'], $dir.$file_full_name)) {
                    $_FILES['file']['error'] = $this->trans('An error occurred while uploading / copying the file.', array(), 'Admin.Advparameters.Notification');
                } else {
                    @chmod($dir.$file_full_name, 0664);
                    $_FILES['file']['filename'] = $file_full_name;
                }

                // lancer l'import sur le fichier si pas d'erreurs
                if(file_exists($dir.$file_full_name)) {
                    $timestamp_debut = microtime(true);

                    $import = new ImportStores($dir, array($file_full_name));
                    $import->start();

                    $timestamp_fin = microtime(true);
                    $difference_ms = $timestamp_fin - $timestamp_debut;

                    $errors = $import->getErrors();
                    if(empty($errors)) {
                        $this->confirmations[] = 'L\'import s\'est déroulé avec succés.';
                        @unlink($dir.$file_full_name);
                    } else {
                        foreach ($errors as $key => $error) {
                           $this->errors[] =  $error;
                        }
                    }
                    if(!empty($import->infos)) {
                        foreach ($import->infos as $key => $info) {
                           $this->warnings[] =  $info;
                        }
                    }
                }
            }


            $this->context->smarty->assign(array(
                'content' => $this->content,
            ));


        }
    }

    public static function getPath($file = '') {
        $dir = (defined('_PS_HOST_MODE_') ? _PS_ROOT_DIR_ : _PS_ADMIN_DIR_).DIRECTORY_SEPARATOR.'import'.DIRECTORY_SEPARATOR;
        if(Tools::checkDir($dir)) {
            $dir .= 'stores'.DIRECTORY_SEPARATOR;
            if(Tools::checkDir($dir)) {
                return $dir.$file;
            }
        }
        return false;
    }

    public function importStores() {
        return $this->importForm();
    }

    protected function importForm() {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Stores', array(), 'Admin.Shopparameters.Feature'),
                'icon' => 'icon-home'
            ),
            'input' => array(
                array(
                    'type' => 'file',
                    'label' => $this->trans('Fichier CSV', array(), 'Admin.Global'),
                    'name' => 'file',
                    'required' => true,
                ),
            ),

            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_importstores';
        $helper->currentIndex = self::$currentIndex.'&importstores';
        $helper->token = $this->token;

        $helper->tpl_vars = array(
            'fields_value' => array(), /* Add values for your inputs */
            'languages' => $this->getLanguages(),
            'id_language' => $this->context->language->id,
            'back' => self::$currentIndex.'&token='.$this->token
        );

       return $helper->generateForm(array(
                array(
                    'form' => $this->fields_form
                )
        ));
    }

    protected function beforeAdd($object)
    {
        $id_store = Tools::getValue('identifiant');
        if($id_store) {
            $object->id = $id_store;
            $object->force_id = true;
        }
    }
}
