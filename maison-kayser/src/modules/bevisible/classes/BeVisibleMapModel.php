<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

class BeVisibleMapModel extends ObjectModel
{
    /** @var integer BeVisiblemap id*/
    public $id;

    /** @var integer BeVisiblemap id order*/
    public $id_order;

    /** @var string BeVisiblemap id store */
    public $id_store;

    /** @var string BeVisiblemap creation date */
    public $date_pickup;

    /** @var string BeVisiblemap creation date */
    public $date_add;

    /** @var string BeVisiblemap last modification date */
    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'bevisible_map',
        'primary' => 'id_bevisiblemap',
        'multilang' => false,
        'fields' => array(
            'id_order' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_store' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'date_pickup' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
        )
    );

    public static function getIdByIdOrder($id_order)
    {
        $sql = 'SELECT id_BeVisiblemap FROM `'
            ._DB_PREFIX_.'BeVisiblemap` WHERE `id_order` = \''.pSQL($id_order).'\'';
        return Db::getInstance()->getValue($sql);
    }

    public static function getByIdOrder($id_order)
    {
        $id_BeVisiblemap = self::getIdByIdOrder($id_order);
        if ($id_BeVisiblemap) {
            return new self($id_BeVisiblemap);
        } else {
            return new self();
        }
    }
}
