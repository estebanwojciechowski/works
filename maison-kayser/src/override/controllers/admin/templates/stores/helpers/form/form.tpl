{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file="helpers/form/form.tpl"}

{block name=script}
	$(document).ready(function() {
		$('#latitude, #longitude').keyup(function() {
			$(this).val($(this).val().replace(/,/g, '.'));
		});
	});
{/block}

{block name="input"}
	{if $input.type == 'latitude'}
	<div class="row">
		<div class="col-lg-3">
			<input type="text"
				{if isset($input.size)}size="{$input.size}"{/if}
				{if isset($input.maxlength)}maxlength="{$input.maxlength}"{/if}
				name="latitude"
				id="latitude"
				value="{$fields_value[$input.name]|escape:'html':'UTF-8'}" />
		</div>
		<div class="col-lg-1">
			<div class="form-control-static text-center"> / </div>
		</div>
		<div class="col-lg-3">
			<input type="text"
				{if isset($input.size)}size="{$input.size}"{/if}
				{if isset($input.maxlength)}maxlength="{$input.maxlength}"{/if}
				name="longitude"
				id="longitude"
				value="{$fields_value['longitude']|escape:'html':'UTF-8'}" />
		</div>
	</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}

{block name="other_input"}
	{if $key == 'hours'}
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Hours:' d='Admin.Shopparameters.Feature'}</label>
				<div class="col-lg-9"><p class="form-control-static">{l s='e.g. 10:00AM - 9:30PM' d='Admin.Shopparameters.Help'}</p></div>
			</div>
			{foreach $fields_value.days as $k => $value}
			<div class="form-group">
				<label class="control-label col-lg-3">{$value}</label>
				<div class="col-lg-9"><input type="text" size="25" name="hours_{$k}" value="{if isset($fields_value.hours[$k-1])}{$fields_value.hours[$k-1]|escape:'html':'UTF-8'}{/if}" /></div>
			</div>
			{/foreach}
	{else if $key == 'hours_clickandcollect'}
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Horaires Click and Collect:' d='Admin.Shopparameters.Feature'}</label>
				<div class="col-lg-9"><p class="form-control-static">{l s='e.g. 10:00AM - 9:30PM' d='Admin.Shopparameters.Help'}</p></div>
			</div>
			{foreach $fields_value.days as $k => $value}
			<div class="form-group">
				<label class="control-label col-lg-3">{$value}</label>
				<div class="col-lg-9"><input type="text" size="25" name="hours_clickandcollect_{$k}" value="{if isset($fields_value.hours_clickandcollect[$k-1])}{$fields_value.hours_clickandcollect[$k-1]|escape:'html':'UTF-8'}{/if}" /></div>
			</div>
			{/foreach}
	{else if $key == 'special_days'}
		<hr/>
		<div class="form-group special_days">
			<label class="control-label col-lg-3">
				{l s='Fermetures et horaires exceptionnels' d='Admin.Shopparameters.Help'}
			</label>
			<div class="col-lg-9">
				<div>
					<div id="special_days_container">
					{if count($fields_value.special_days)}
						{foreach $fields_value.special_days as $k => $value}
						<div class="special_days_row">
							<div class="panel">
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<div class="col-lg-offset-3 col-lg-9">
													<label for="special_days_fermeture_{$k}" class="checkbox" onclick="toggleHorairesInput($(this));">
														<input type="checkbox" id="special_days_fermeture_{$k}" name="special_days_fermeture[id_{$k}]" value="1"{if $value.fermeture} checked="checked"{/if}/>
														{l s='Fermeture ?'}
													</label>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-3">
													{l s='Date :'}
												</label>
												<div class="col-lg-9">
													<input type="text" class="datepickerfr" name="special_days[id_{$k}]" value="{$value.date|date_format:'%d/%m/%Y'}" autocomplete="off"/>
												</div>
											</div>
											<div class="form-group horaires_exceptionnels"{if $value.fermeture} style="display:none"{/if}>
												<label class="control-label col-lg-3">
													{l s='Horaires de la journée :'}
												</label>
												<div class="col-lg-9">
													<input type="text" name="special_days_hours[id_{$k}]" value="{$value.hours}" autocomplete="off"/>
													<p class="form-control-static">Exemple : 10:00-21:30</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<a href="#" class="btn btn-default rm-special-days"
										onclick="return removeSpecialDay($(this));">
										<span class="icon-eraser"></span>
										{l s="Supprimer"}
									</a>
								</div>
							</div>
						</div>
						{/foreach}
					{else}
						<div class="special_days_row">
							<div class="panel">
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<div class="col-lg-offset-3 col-lg-9">
													<label class="checkbox" onclick="toggleHorairesInput($(this));">
														<input type="checkbox" name="special_days_fermeture[]" value="1"/>
														{l s='Fermeture ?'}
													</label>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-3">
													{l s='Date :'}
												</label>
												<div class="col-lg-9">
													<input type="text" class="datepickerfr" name="special_days[]" value="" autocomplete="off"/>
												</div>
											</div>
											<div class="form-group horaires_exceptionnels">
												<label class="control-label col-lg-3">
													{l s='Horaires de la journée :'}
												</label>
												<div class="col-lg-9">
													<input type="text" name="special_days_hours[]" value="" autocomplete="off"/>
													<p class="form-control-static">Exemple : 10:00-21:30</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<a href="#" class="btn btn-default rm-special-days"
										onclick="return removeSpecialDay($(this));">
										<span class="icon-eraser"></span>
										{l s="Supprimer"}
									</a>
								</div>
							</div>
						</div>
					{/if}
					</div>
					<div class="row">
						<div class="col-sm-12">
							<br/>
							<a href="#" class="btn btn-default add-special-days"
								onclick="return addSpecialDay();">
								<span class="icon-plus"></span>
								{l s="Ajouter une date"}
							</a>
							<a href="#" class="btn btn-default remove-all-special-days"
								onclick="return clearSpecialDays();">
								<span class="icon-eraser"></span>
								{l s="Tout supprimer"}
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	{/if}
{/block}