/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 */

var BV = BV || {};
BV.map = null;
BV.firstAutocompleteValue = false;
BV.enterPressed = false;
BV.pending = false;
BV.clickncollect = false;
BV.info_window = null;
BV.markers = {};
BV.latLng = [];

//fonction qui retourne la premiere occurence d'un tableau
function first(p){for(var i in p)return p[i];}

BV.initMap = function() {
    BV.info_window = new google.maps.InfoWindow();
    // BV.info_window.attr('class', 'shop__item');
    //si pas de magasin la map est centrée sur paris
    var selected_store = {
        latitude: 48.8587741,
        longitude: 2.2069771
    };

    if(BV.stores) {
        var selected_store = BV.stores[BV.selected_id_store];
        //si le store par defaut n'est pas trouvé prend le premier de la liste
        if(typeof(selected_store) == 'undefined') {
            selected_store = first(BV.stores);
        }
    }


    var map_zoom = BV.map_zoom; 

    var nbStores = Object.keys(BV.stores).length;
    if(nbStores == 1) {
        BV.map_zoom = 16;
        map_zoom = 16;
    }

    var map_options = {
        scrollwheel: false,
        zoom: parseInt(map_zoom),
        center: new google.maps.LatLng(parseFloat(selected_store.latitude), parseFloat(selected_store.longitude))
    };

    if (BV.map_styles != 'none') {
        map_options.styles = BV.map_styles;
    }

    BV.map = new google.maps.Map(document.getElementById('bevisible-map'), map_options);
    BV.addMarkers();

    if (BV.map_zoom == 'auto') {
        var latLngBounds = new google.maps.LatLngBounds();
        for (var i = 0; i < BV.latLng.length; i++) {
            latLngBounds.extend(BV.latLng[i]);
        }
        BV.map.fitBounds(latLngBounds);
    }


    if (BV.geocode_customer) {
        BV.geocodeCustomer();
    }

    if (BV.open_info_window) {
        setTimeout(function(){
            BV.openInfoWindow(BV.selected_id_store);
        }, 500);
    }
};
 

 

BV.addMarkers = function() {
    for (i in BV.stores) {
        var marker = null;
        if (BV.marker_type == 'image') {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(BV.stores[i].latitude, BV.stores[i].longitude),
                info: BV.stores[i].id_store,
                map: BV.map,
                icon: BV.marker_url
            });
        } else {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(BV.stores[i].latitude, BV.stores[i].longitude),
                info: BV.stores[i].id_store,
                map: BV.map
            });
        }

        BV.latLng.push(new google.maps.LatLng(BV.stores[i].latitude, BV.stores[i].longitude));

        BV.markers[BV.stores[i].id_store] = marker;

        google.maps.event.addListener(marker, 'click', function () {
            BV.openInfoWindow(this.info);
        });
    }
};

BV.openInfoWindow = function(id_store) {
    BV.info_window.setContent(BV.stores[id_store].info_box_html)
    BV.info_window.open(BV.map, BV.markers[id_store]);
};

BV.geocodeCustomer = function() {
    $.ajax({
        url: BV.callback_url,
        type: 'GET',
        async: true,
        data: {mode: 'context'},
        dataType: 'json',
        cache: false,
        success: function(json) {
            if (!json.hasOwnProperty('error')) {
                if (json.hasOwnProperty('id_store')) {
                    BV.selectStore(json.id_store, false);
                }

                if (json.hasOwnProperty('address')) {
                    $('#bevisible-locator-value').val(json.address);
                }
            }/* else {
                console.log(json.error);
            }*/
        }
    });
};

BV.geocodeAddress = function(value = false, centerOnStore = 1) {
    //reinit des message alert
    $('.alert.alert-danger').remove();
    // ADD BY ESTEBANW INSITACTION 12/12/2017
    if(!value)
        value = $('#bevisible-locator-value').val();
    if($('#notifications .container').length) {
        $('#notifications .container').html('');
    }
    if(!BV.pending) {
        // recuperation des filtres services
        var services = {};
         $('.filtres__container input.input-checkbox').each(function(event) {
            var inputName = $(this).attr('name');
            if(inputName == 'filtre-cafe') {
                services.cafe = $(this).val();
            } else if(inputName == 'filtre-resto') {
                services.resto = $(this).val();
            } else if(inputName == 'filtre-sansgluten') {
                services.sansgluten = $(this).val();
            }
        });                 
        services = JSON.stringify(services); 
        var linkButton = $('a.bevisible-search-button:first-child');
        var back = false;
        if(linkButton.length > 0) {
            var back = linkButton.data('back');
        }
        BV.showSearchLoader();
        $.ajax({
            url: BV.callback_url,
            type: 'GET',
            data: {address: value, mode: 'address', centerOnStore: centerOnStore, services: services, back: back},
            async: true,
            dataType: 'json',
            cache: false,
            success: function(json) { 
                if (!json.hasOwnProperty('error')) {
                    var centerOnStore = 1;
                    if(typeof(json.centerOnStore) != 'undefined') {
                        centerOnStore = json.centerOnStore; 
                    }  
                    var refresh = false;
                    if (json.hasOwnProperty('refresh_content'))
                        refresh = true;
                    if(!centerOnStore) {
                        BV.showZone(json.coords);
                    } else if (json.hasOwnProperty('id_store')) {
                        if(!json.id_store) {
                            //si l'id store n'est pas transmis c'est que le magasin n'a pas été trouvé
                            BV.showZone(json.coords);
                        } else {
                            if (json.hasOwnProperty('stores') && refresh) {
                                BV.stores = json.stores; 
                            }
                            if(json.hasOwnProperty('reload') && json.reload) {
                                BV.selectStore(json.id_store, true);
                            } else {
                                BV.selectStore(json.id_store, false);                                
                            }
                        }
                    }
                    if(refresh)
                        BV.updateContent(json.refresh_content);

                }/* else {
                    console.log(json.error);
                }*/
                BV.hideSearchLoader();
            },
            error: function() {
                BV.hideSearchLoader();
            }
        });
    }
};

BV.updateContent = function(content) {
    $('#containerScrollbar').html(content);
}

BV.selectStore = function(id_store, reload) { 
    //si select store undefined on switch sur l'affichage de tout les magasins dispos
    if(reload) {
        // reinitialisation de la map pour refaire un zoom auto 
        // sur la selection des magasins
        BV.markers = {};
        BV.latLng = [];
        // force les champs souhaités
        BV.selected_id_store = id_store;  
        BV.map_zoom = 'auto';
        BV.initMap(); 
    } else if(typeof(BV.stores[id_store]) == 'undefined') {  
        BV.stores = BV.all_stores; 
        BV.selected_id_store = id_store;        
        BV.map_zoom = 12 ;
        BV.initMap(); 
    } else {
        var selected_store = BV.stores[id_store];
        BV.selected_id_store = id_store;
        BV.map.setCenter(new google.maps.LatLng(parseFloat(selected_store.latitude), parseFloat(selected_store.longitude)));

        if (BV.open_info_window) {
            setTimeout(function(){
                BV.openInfoWindow(BV.selected_id_store);
            }, 200);
        }
    }
};

BV.showZone = function(coords) {  
    BV.map.setCenter(new google.maps.LatLng(parseFloat(coords.lat), parseFloat(coords.lng))); 
};

BV.showSearchLoader = function() {
    BV.pending = true;
    $('.bevisible-search-button').addClass('bevisible-loading');
};

BV.hideSearchLoader = function() {
    BV.pending = false;
    $('.bevisible-search-button').removeClass('bevisible-loading');
};

BV.extendDirectionLink = function(dom) {
    var href = $(dom).data('href');
    href = href.replace('saddr=', 'saddr='+$('#bevisible-locator-value').val());
    $(dom).attr('href', href);
};

$(document).ready(function(){
    BV.initMap(); 
    $(document).on('click', '[data-button-action="choose-store"]', function (event) {
        // choisir le magasin si pas de panier 
        var token = $('input[name$="token"]').val();
        var back = $('input[name$="back"]').val();
        var actionUrl = $(this).data('actionUrl');
        var method = $(this).attr('name');
        var idStore = $(this).data('idStore');       

        // add overlay to prevent user other click
        var div = $('<div/>');
        div.attr({
           id: "cartChangeLoading" 
        });
        div.css({
           position: 'absolute',
           top: 0,
           left: 0,
           zIndex: 99,
           background: '#fff',
           opacity: 0.7,     
           width: '100%',
           height: $('main').height()+'px'       
        });
        var loader = $('<div/>');
        loader.attr({
           class: "swiper-lazy-preloader" 
        });
        div.append(loader);

        $('body').append(div);

        $.post(actionUrl,
        {
            token: token,
            id_store: idStore,
            action_url: actionUrl,
            ajax: 1,
            method: method,
            back: back

        }, function(json) { 
            if(typeof(json.success) != 'undefined' && json.success) {
                if(typeof(json.modal) != 'undefined'&& json.modal) {
                    showModal(json.modal);
                }
                if(typeof(json.redirect) != 'undefined'&& json.redirect) {
                    window.location.href = json.redirect;
                }
            }            
        });
        // sinon demander confirmation de migration panier vers la boutique  
        return false;
    });

    // gestion des filtres services
    $('.filtres__container input.input-checkbox').click(function(event) { 
        toggleCheckBox($(this)); 
    });

    $('#bevisible-locator-value').on('keydown', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13) { 
            e.preventDefault();
            e.stopPropagation();  
            replaceFormValue();      
        }
    });
    $('a.bevisible-search-button').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();  
        replaceFormValue();
    });

    var autocomplete;
    $('#bevisible-locator-value').on('focus', function (e) {
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */(document.getElementById('bevisible-locator-value')),
            { types: ['geocode'] });
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            if(!BV.enterPressed)
                BV.geocodeAddress(false, 1);
        });
    });

   

});

function replaceFormValue() {
    value = $.trim($('#bevisible-locator-value').val());
    if(value != '') {
        BV.enterPressed = true;
        var service = new google.maps.places.AutocompleteService();
        service.getPlacePredictions({
               input: value
            },
            getFirstPlace
        );
        return false;
    } else {
        BV.geocodeAddress(false, 1);
    }
}

var getFirstPlace = function(places, status) {
    if (status != google.maps.places.PlacesServiceStatus.OK) {
        BV.enterPressed = false;
        alert(status);
        return;
    }
    if($(places).length) {
        BV.firstAutocompleteValue = $(places).first();
        $('#bevisible-locator-value').val(BV.firstAutocompleteValue.attr('description')); 
        BV.geocodeAddress(BV.firstAutocompleteValue.attr('description'), 1);                   
    }                

    BV.enterPressed = false;
};


function toggleCheckBox(checkbox) {
    var value = checkbox.val(); 
    if(value == 1) {
        checkbox.val(0); 
    } else {
        checkbox.val(1);
    }
}

function showModal(modal) { 
    $('body').append(modal);
    $('#choose-store-modal').modal('show').on('hide.bs.modal', function (event) {
        $('.modal-backdrop').remove();
    });
    $('body').one('click', '#choose-store-modal', function (event) {
      if (event.target.id === 'choose-store-modal') {
        $(event.target).remove();
      }
    }); 
};
 