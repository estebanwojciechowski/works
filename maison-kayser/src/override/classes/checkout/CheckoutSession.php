<?php
class CheckoutSession extends CheckoutSessionCore
{

	public function getDeliveryDate()
    {
    	$context = Context::getContext();  
        return $context->cart->delivery_option_date;
    }

	public function setDeliveryDate($timestamp)
    {
    	$context = Context::getContext(); 
        $context->cart->setDeliveryDate($timestamp);
        return $context->cart->update();
    }

    public function getCarrierMessage()
    {
        $context = Context::getContext();
        return  $context->cookie->__get('carrier_message');
    }

    public function setMessage($message)
    { 
        $this->_updateMessage(Tools::safeOutput($message));

        return $this;
    }

    /**
     * [_updateMessage Enregistrement du message dans les variables de session du panier]
     * @param  [type] $messageContent [description]
     * @return [type]                 [description]
     */
    private function _updateMessage($messageContent)
    { 
        $context = Context::getContext();
        if ($messageContent) {
                $context->cookie->__set('carrier_message', $messageContent);
        } else {
            $context->cookie->__unset('carrier_message');
        }

        return true;
    }
}