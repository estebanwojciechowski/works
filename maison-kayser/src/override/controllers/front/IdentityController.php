<?php

class IdentityController extends IdentityControllerCore
{

	public function initContent()
    {
    	$my_shop = $this->context->customer->getMyShop();
    	if($my_shop) {
    		$shop = new Shop($my_shop);
    		if(Validate::isLoadedObject($shop) && $shop->active)
    			$my_shop = $shop;
    	}

    	$this->context->smarty->assign([
            'my_shop' => $my_shop
        ]);

        parent::initContent();
    }

	/**
	 * [postProcess description]
	 * @return [type] [description]
	 */
	public function postProcess()
    {
    	parent::postProcess();
    	// suppression de la boulangerie préférée
    	if(Tools::getValue('deleteMyshop') == 1) {
    		if($this->context->customer->isLogged()) {
                DB::getInstance()->execute('
                    UPDATE mk_customer SET my_shop = NULL 
                    WHERE id_customer = '.$this->context->customer->id
                );

                // ADD BY ESTEBANW INSITACTION 31052018
                // SUPPRESSION DU COOKIE BOULANGERIE PREFEREE
                if(isset($_COOKIE['wp_shop'])) {
                    setcookie("wp_shop", 0, time()+3600, '/');
                }
                // log vers la boutique main
                $this->context->logToOtherShop($this->context->customer, 1);
                // END
                Tools::redirect($this->context->link->getPageLink('identity', true, null, null ,false, 1));
	    	}
    	}
    }

}