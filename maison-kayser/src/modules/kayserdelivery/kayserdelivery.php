<?php
/**
 *  2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction
 */

/**
 * Dépendances
 * Override Classe Carrier.php
 * Override Classe Cart.php
 * Override Classe Store.php
 */
if (!defined('_PS_VERSION_')) {
    include_once(dirname(__FILE__).'/../../config/config.inc.php');
    if (!defined('_PS_VERSION_'))
        exit;
}


class KayserDelivery extends CarrierModule
{
    private $sql_tables = array();
    private $fields_value = array();
    private $errors = array();
    private $success = array();
    protected $initial_cost;
    public $url = '';

    /*
    TODO CLEAN
    private $config = array(
        'name' => 'Kayser Delivery',
        'id_tax_rules_group' => 0,
        'url' => 'http://www.colissimo.fr/portail_colissimo/suivreResultat.do?parcelnumber=@',
        'active' => true,
        'deleted' => 0,
        'shipping_handling' => false,
        'range_behavior' => 0,
        'is_module' => true,
        'delay' => array(
            'fr' => 'Avec La Poste, Faites-vous livrer là ou vous le souhaitez en France Métropolitaine.',
            'en' => 'Do you deliver wherever you want in France.'),
        'id_zone' => 1,
        'shipping_external' => true,
        'external_module_name' => 'kayserdelivery',
        'need_range' => true
    );*/

    public function __construct()
    {
        $this->name = 'kayserdelivery';
        $this->tab = 'shipping_logistics';
        $this->trusted = false;
        $this->version = '1.0.0';
        $this->author = 'Insitaction - EstebanW';
        $this->need_instance = 1;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Kayser Delivery');
        $this->description = $this->l('Vous permet d\'ajouter des transporteurs personnalisés à votre boutique.');
        $this->confirmUninstall = $this->l('Removing the module will also delete the associated carrier');
        $this->ps_versions_compliancy = array(
            'min' => '1.7',
            'max' => _PS_VERSION_);

        $this->url = Tools::getHttpHost().Tools::getShopDomainSsl().__PS_BASE_URI__.'modules/'.$this->name.'/validation.php';
        $this->sql_tables = array(
                array(
                    'name' => 'carrier_attribute',
                    'columns' => array(
                        array(
                            'name' => 'id_carrier_attribute',
                            'params' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
                            'is_primary_key' => true,
                        ),
                        array(
                            'name' => 'name',
                            'params' => 'varchar(256) COLLATE utf8_unicode_ci',
                        ),
                        array(
                            'name' => 'field',
                            'params' => 'varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL',
                        ),
                        array(
                            'name' => 'object',
                            'params' => 'varchar(64) DEFAULT "product"',
                        ),
                        array(
                            'name' => 'datepicker',
                            'params' => 'TINYINT(1) unsigned DEFAULT 0',
                        ),
                    ),
                ),
                array(
                    'name' => 'carrier_attribute_carrier',
                    'columns' => array(
                        array(
                            'name' => 'id_carrier',
                            'params' => 'int(10) unsigned NOT NULL',
                            'is_primary_key' => true,
                        ),
                        array(
                            'name' => 'id_carrier_attribute',
                            'params' => 'int(10) unsigned NOT NULL',
                            'is_primary_key' => true,
                        ),
                    ),
                ),
        );
    }

    public function install()
    {
        return parent::install() &&
            $this->installDB() &&
            $this->registerHook('header') &&
           // $this->registerHook('backOfficeHeader') &&
            $this->registerHook('updateCarrier') &&
            $this->registerHook('actionValidateOrder') &&
            $this->registerHook('moduleRoutes') &&
            $this->registerHook('actionCarrierUpdate') &&
            //$this->registerHook('displayAdminOrder') &&
            $this->registerHook('displayCarrierExtraContent');
    }

    private function installDB()
    {
        $return = true;
        if (count($this->sql_tables) > 0) {
            foreach ($this->sql_tables as $table) {
                $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.pSQL($table['name']).'` (';
                $columns = array();
                $primary_keys = array();
                $keys = array();

                foreach ($table['columns'] as $column) {
                    $columns[] = '`'.$column['name'].'` '.$column['params'];

                    if (isset($column['is_primary_key']) && $column['is_primary_key']) {
                        $primary_keys[] = $column['name'];
                    }

                    if (isset($column['is_key']) && $column['is_key']) {
                        $keys[] = 'KEY `'.$column['name'].'` (`'.$column['name'].'`)';
                    }
                }

                $keys = array_merge(array('PRIMARY KEY (`'.implode('`, `', $primary_keys).'`)'), $keys);

                $sql .= implode(', ', array_merge($columns, $keys)). ') ENGINE='.
                    _MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';

                $return = $return && Db::getInstance()->execute($sql);
            }
        }

        return $return;
    }

    public function uninstall()
    {
        //TODO EFFACER CONTENU
        return parent::uninstall();
    }

    public function hookModuleRoutes() {
        //add classes to autoload
        // include_once(dirname(__FILE__).'/classes/Carrier.php');
        include_once(dirname(__FILE__).'/classes/CarrierAttribute.php');
        // include_once(dirname(__FILE__).'/classes/Cart.php');
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        //redirection vers adminCarriers
        $post_datas = Tools::getAllValues();
        if(isset($post_datas['updatecarrier']) && isset($post_datas['id_carrier']) && $post_datas['id_carrier']) {
             Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminCarrierWizard').'&id_carrier='.$post_datas['id_carrier']);
        }
        if (((bool)Tools::isSubmit('submit_kayser_delivery_module')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign(array(
            'module_dir' => $this->_path,
            'kayser_version' => $this->version
        ));

        return $this->renderForm();
    }

    protected function renderForm()
    {

        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_kayser_delivery_module';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        return $helper->generateForm(array(
                array(
                    'form' => $this->getConfigForm()
                )
        ));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $form = array(
            'legend' => array(
                'title' => $this->l('Kayser Delivery').' V'.$this->version,
                'icon' => 'icon-cogs',
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            )
        );

        //======================================================================
        // CONFIGS TAB
        $form['tabs']['config'] = $this->l('Configurations');
        $form['input'][] = array(
            'tab' => 'config',
            'type' => 'html',
            'name' => 'alert',
            'html_content' => '</div><p class="alert alert-info">Ces configurations seront utilisées pour toutes les boutiques.</p><div>',
        );
        $form['input'][] = array(
            'tab' => 'config',
            'type' => 'checkbox',
            'label' => $this->l('Jours de livraison en voiture').':',
            'name' => 'KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS',
            'required' => true,
            'values' => array(
                'query' => array(
                    array(
                        'id' => '1',
                        'name' => 'Lundi',
                        'value' => 1
                    ),
                    array(
                        'id' => '2',
                        'name' => 'Mardi',
                        'value' => 2
                    ),
                    array(
                        'id' => '3',
                        'name' => 'Mercredi',
                        'value' => 3
                    ),
                    array(
                        'id' => '4',
                        'name' => 'Jeudi',
                        'value' => 4
                    ),
                    array(
                        'id' => '5',
                        'name' => 'Vendredi',
                        'value' => 5
                    ),
                    array(
                        'id' => '6',
                        'name' => 'Samedi',
                        'value' => 6
                    ),
                    array(
                        'id' => '7',
                        'name' => 'Dimanche',
                        'value' => 7
                    ),

                ),
                'id' => 'id',
                'name' => 'name',
            ),
            'desc' => $this->l('Jours de livraisons en voiture'),
        );

        $form['input'][] = array(
            'tab' => 'config',
            'type' => 'text',
            'label' => $this->l('Poids maximum en vélo').':',
            'name' => 'KAYSER_DELIVERY_LIVRAISON_VELO_POIDS',
            'required' => true,
            'values' =>  '',
            'desc' => $this->l('Poids total maximum en grammes pouvant être porté par un livreur à vélo'),
        );

        $form['input'][] = array(
            'tab' => 'config',
            'type' => 'text',
            'label' => $this->l('Horaires livraison vélo').':',
            'name' => 'KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES',
            'required' => true,
            'values' =>  '',
            'desc' => $this->l('Tranches horaires de livraison vélo ex: 9:30-21:00'),
        );

        //======================================================================
        // CARRIER TAB
        $form['tabs']['carrier'] = $this->l('Création d\'un transporteur');

        $form['input'][] = array(
            'tab' => 'carrier',
            'type' => 'text',
            'label' => $this->l('Nom du transporteur').':',
            'name' => 'name',
            'required' => true,
            'values' => $this->fields_value['name'],
            'desc' => $this->l('Caractères autorisés : lettres, espaces et "().-". Nom du transporteur tel qu\'il apparaîtra durant la commande. Pour le retrait en boutique, mettez le chiffre 0 afin de remplacer le nom du transporteur par le nom de votre boutique.'),
        );
         $form['input'][] = array(
            'tab' => 'carrier',
            'type' => 'text',
            'label' => $this->l('Délai de livraison').':',
            'name' => 'delay',
            'required' => true,
            'lang' => true,
            'values' => $this->fields_value['delay'],
            'desc' => $this->l('Le délai de livraison sera affiché durant la commande'),
        );
        $form['input'][] = array(
            'tab' => 'carrier',
            'type' => 'checkbox',
            'label' => $this->l('Attributs').':',
            'name' => 'attributs',
            'required' => true,
            'values' => array(
                'query' => CarrierAttribute::getAttributes(),
                'id' => 'id_carrier_attribute',
                'name' => 'name',
            ),
            'desc' => $this->l('Attributs utilisés par le transporteur'),
        );

        //======================================================================
        // ATTRIBUTS TAB
        $form['tabs']['attributs'] = $this->l('Ajouter un attribut');
        $form['input'][] = array(
            'tab' => 'attributs',
            'type' => 'text',
            'label' => $this->l('Nom du Champ dans la BDD').':',
            'name' => 'attribute_field',
            'required' => true,
            'values' => $this->fields_value['attribute_name'],
        );
        $form['input'][] = array(
            'tab' => 'attributs',
            'type' => 'text',
            'label' => $this->l('Nom d\'affichage de l\'attribut').':',
            'name' => 'attribute_name',
            'required' => true,
            'values' => $this->fields_value['attribute_name'],
        );
        $form['input'][] = array(
            'tab' => 'attributs',
            'type' => 'radio',
            'label' => $this->l('Besoin d\'une date de rendez-vous ?').':',
            'name' => 'attribute_datepicker',
            'required' => true,
            'values' => array(
                    array(
                        'id' => 1,
                        'label' => 'Oui',
                        'value' => 1
                    ),
                    array(
                        'id' => 0,
                        'label' => 'Non',
                        'value' => 0
                    ),
            ),
        );
        //======================================================================
        // LISTE ATTRIBUTES TAB
        $form['tabs']['liste_attributs'] = $this->l('Atributs existants');

        $helper = new HelperList;
        $helper->title = 'Liste des attributs de transporteurs';
        $helper->actions = array();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_carrier_attribute';
        $helper->orderBy = 'name';
        $helper->orderWay = 'ASC';
        $helper->table = 'carrier_attribute';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $fields_list = array(
                    'id_carrier_attribute' => array('title' => $this->l('ID'), 'align' => 'center', 'class' => 'fixed-width-xs'),
                    'name' => array('title' => $this->l('Nom')),
                    'object' => array('title' => $this->l('Type')),
                    'field' => array('title' => $this->l('Champ')),
                );
        $attributes = CarrierAttribute::getAttributes();
        $list_content = $helper->generateList($attributes, $fields_list);

        $form['input'][] = array(
            'tab' => 'liste_attributs',
            'type' => 'html',
            'name' => 'liste_attributs',
            'html_content' => '</div>'.$list_content.'<div>',
        );

        //======================================================================
        // INFO TAB
        $form['tabs']['carriers'] = $this->l('Transporteurs existants');

        $helper = new HelperList;
        $helper->title = 'Liste des transporteurs gérés par ce module';
        $helper->actions = array('edit');
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_carrier';
        $helper->orderBy = 'position';
        $helper->orderWay = 'ASC';
        $helper->table = 'carrier';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $fields_list = array(
                    'id_carrier' => array('title' => $this->l('ID'), 'align' => 'center', 'class' => 'fixed-width-xs'),
                    'name' => array('title' => $this->l('Nom')),
                );
        $carriers = Carrier::getCarriersByModuleName(1, false, false, false, null, 'kayserdelivery');
        $list_content = $helper->generateList($carriers, $fields_list);

        // Check current language
        $language = new Language($this->context->language->id);
        $defaut_tpl = $this->local_path.'views/templates/admin/about_fr.tpl';
        if ($language->iso_code != 'fr') {
            $defaut_tpl = $this->local_path.'views/templates/admin/about_en.tpl';
        }
        $form['input'][] = array(
            'tab' => 'carriers',
            'type' => 'html',
            'name' => 'carriers',
            'html_content' => '</div>'.$list_content.'<div>',
        );

        return $form;
    }

    public function getCheckboxValues($input_name, $values = array()) {
        if(is_array($values) && count($values) && is_string($input_name)) {
            $return = array();
            foreach ($values as $key => $value) {
                 if($value)
                    $return[$input_name.'_'.$key] = 1;
            }
            return $return;
        }
        return false;
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $delay = array();
        $langues = Language::getLanguages();
        foreach ($langues as $key => $langue) {
           $delay[$langue['id_lang']] = Tools::getValue('delay_'.$langue['id_lang'],'');
        }

        $car_delivery = json_decode(Configuration::get('KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS'), true);
        $car_delivery = $this->getCheckboxValues('KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS', $car_delivery);

        $checkbox_post = array();
        for ($i=1; $i < 8; $i++) {
           if(Tools::getValue('KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS_'.$i)) {
                $checkbox_post['KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS_'.$i] = 1;
           }
        }
        if(empty($checkbox_post)) {
            $checkbox_post = $car_delivery;
        }

        $return = array(
            'name' => Tools::getValue('name',''),
            'delay' => $delay,
            'attributs' => Tools::getValue('attributs',''),
            'attribute_name' => Tools::getValue('attribute_name',''),
            'attribute_field' => Tools::getValue('attribute_field',''),
            'attribute_datepicker' => Tools::getValue('attribute_datepicker', 0),
            'KAYSER_DELIVERY_LIVRAISON_VELO_POIDS' => Tools::getValue('KAYSER_DELIVERY_LIVRAISON_VELO_POIDS', Configuration::get('KAYSER_DELIVERY_LIVRAISON_VELO_POIDS')),
            'KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES' => Tools::getValue('KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES', Configuration::get('KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES')),

        );

        if($checkbox_post)
             $return = array_merge_recursive($return, $checkbox_post);

        $this->fields_value = $return;

        return $return;
    }


    /**
     * Save form data.
     */
    protected function postProcess()
    {
       $langues = Language::getLanguages();
       $post_datas = Tools::getAllValues();
       $configs = Configuration::getMultiple(
                array(
                    'KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS',
                    'KAYSER_DELIVERY_LIVRAISON_VELO_POIDS',
                    'KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES'
                )
       );

        $car_delivery = array();
        for ($i=1; $i < 8; $i++) {
            if(isset($post_datas['KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS_'.$i])
                    && trim($post_datas['KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS_'.$i]) == 'on') {
                $car_delivery[$i] = 1;
            } else {
                $car_delivery[$i] = 0;
            }
        }
        $config_value = json_encode($car_delivery);

        if($config_value != $configs['KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS']) {
            Configuration::updateGlobalValue('KAYSER_DELIVERY_LIVRAISON_VOITURE_DAYS', $config_value);
        }

        if(isset($post_datas['KAYSER_DELIVERY_LIVRAISON_VELO_POIDS'])
            && strlen(trim($post_datas['KAYSER_DELIVERY_LIVRAISON_VELO_POIDS']))) {
            $config_value = floatval(trim($post_datas['KAYSER_DELIVERY_LIVRAISON_VELO_POIDS']));
            if($config_value != $configs['KAYSER_DELIVERY_LIVRAISON_VELO_POIDS']) {
                Configuration::updateGlobalValue('KAYSER_DELIVERY_LIVRAISON_VELO_POIDS', $config_value);
            }
        }
        if(isset($post_datas['KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES'])
            && strlen(trim($post_datas['KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES']))) {
            $config_value = trim($post_datas['KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES']);
            if($config_value != $configs['KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES']) {
                Configuration::updateGlobalValue('KAYSER_DELIVERY_LIVRAISON_VELO_HORAIRES', $config_value);
            }
        }
        // Ajout d'un attribut aux transporteurs
        if(isset($post_datas['attribute_field']) && strlen(trim($post_datas['attribute_field']))
            && isset($post_datas['attribute_name']) && strlen(trim($post_datas['attribute_name']))) {
            if(CarrierAttribute::getIDByField($post_datas['attribute_field'])) {
                // le champ existe déja
            } else {
                $attribute = new CarrierAttribute();
                $attribute->field = trim($post_datas['attribute_field']);
                $attribute->name = trim($post_datas['attribute_name']);
                $attribute->object = 'product';
                $attribute->datepicker = $post_datas['attribute_datepicker'];
                $message = $attribute->validateFields(false, true);
                if($message === true) {
                    if($attribute->save()) {
                        // succes
                    } else {
                        // erreur
                        $this->context->controller->errors[] = $this->l('Une erreur a eu lieu lors de la sauvegarde de l\'attribut.');
                    }
                } else {
                    $this->context->controller->errors[] = $message;
                }
            }

        }
        //ajout d'un transporteur
        if(isset($post_datas['name']) && strlen(trim($post_datas['name']))) {
            $post_datas['attributes'] = array();
            $attributes = CarrierAttribute::getAttributes();
            $clickandcollect = false;
            if(is_array($attributes) && count($attributes)) {
                foreach ($attributes as $key => $value) {
                    if(isset($post_datas['attributs_'.$value['id_carrier_attribute']])){
                        //transporteur gratuit si clickncollect
                        if($value['field'] == 'delai_clickandcollect')
                            $clickandcollect = true;
                        $post_datas['attributes'][] = $value['id_carrier_attribute'];
                    }
                }
            }
            $attributes = $post_datas['attributes'];

            $name = isset($post_datas['name']) ? trim($post_datas['name']) : '';
            $delay = array();
            $default_delay = trim($post_datas['delay_'.Configuration::get('PS_LANG_DEFAULT')]);
            foreach ($langues as $key => $langue) {
                if(strlen(trim($post_datas['delay_'.$langue['id_lang']]))) {
                    $delay[$langue['id_lang']] = trim($post_datas['delay_'.$langue['id_lang']]);
                } else {
                    $delay[$langue['id_lang']] =  $default_delay;
                }
            }

            $carrier = new Carrier();
            $carrier->name = $name;
            $carrier->id_tax_rules_group = 0;
            $carrier->id_zone = 1;
            $carrier->url = '';
            $carrier->active = 1;
            $carrier->deleted = 0;
            $carrier->shipping_handling = false;
            $carrier->range_behavior = 0;
            $carrier->is_module =  1;
            $carrier->is_free = ($clickandcollect ? 1 : 0);
            $carrier->shipping_external = 1;
            $carrier->external_module_name = 'kayserdelivery';
            $carrier->need_range = 1;
            $carrier->delay = $delay;
            $carrier->attributes = $attributes;
            if (Shop::isFeatureActive()) {
                Shop::setContext(Shop::CONTEXT_ALL);
            }

            $message = $carrier->validateFields(false, true);
            if($message === true) {
                if ($carrier->add()) {
                    //succes -> redirection vers l'edition du transporteur
                    Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminCarrierWizard').'&id_carrier='.$carrier->id);
                } else  {
                    //erreur
                    $this->context->controller->errors[] = $this->l('Une erreur a eu lieu lors de l\'enregistrement du transporteur.');
                }
            } else {
                $this->context->controller->errors[] = $message;
            }
       }
    }

    /**
     * [getOrderShippingCost description]
     * @param  [type] $params        [description]
     * @param  [type] $shipping_cost [description]
     * @return [type]                [description]
     *
     * Conservé si traitements spéciaux sur le prix d'envoi
     *
     */
    public function getOrderShippingCost($params, $shipping_cost)
    {
        // for order in BO
        if (!$this->context->cart instanceof Cart || !$this->context->cart->id) {
            $this->context->cart = new Cart($params->id);
        }

        if (!$this->initial_cost) {
            $this->initial_cost = $shipping_cost;
        }

        // check api already return a shipping cost ?
        /*$api_price = $this->getApiPrice((int)$this->context->cart->id);

        if ($api_price) {
            $carrier_colissimo = new Carrier((int)Configuration::get('COLISSIMO_CARRIER_ID'));
            $address = new Address((int)$this->context->cart->id_address_delivery);
            $tax = $carrier_colissimo->getTaxesRate($address);

            // must retrieve the price without tax if needed
            if ($tax) {
                (float)$tax_rate = ((float)$tax / 100) + 1;
                $api_price = (float)$api_price / (float)$tax_rate;
            }
            return (float)$api_price;
        }*/
        return $shipping_cost;
    }

    public function getOrderShippingCostExternal($params)
    {
        return true;
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        /*if (isset($this->context->controller->page_name) && $this->context->controller->page_name == "checkout") {
            $this->context->controller->addJS($this->_path.'/views/js/front.js');
            $this->context->controller->addJqueryPlugin(array(
                'fancybox'));
        } else {
            $this->context->controller->addJS($this->_path.'/views/js/redirect.js');
        }
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');*/
    }

    public function hookActionValidateOrder($params)
    {
        //recupération de la date de livraison définie dans le panier si datepicker
        // défini pour le transporteur
        if(isset($params['cart']) && Validate::isLoadedObject($params['cart'])) {
            if(isset($params['order']) && Validate::isLoadedObject($params['order'])) {
                $cart = $params['cart'];
                $order = $params['order'];

                $carrier = new Carrier($order->id_carrier);
                if(Validate::isLoadedObject($carrier)) {
                    //si le transporteur dispose d'une date de livraison
                    $id_order_carrier = $order->getIdOrderCarrier();
                    if($id_order_carrier) {
                        $order_carrier = new OrderCarrier($id_order_carrier);
                        if($carrier->datepicker) {
                            $order_carrier->date_livraison = $cart->delivery_option_date;
                        }
                        $order_carrier->message = $this->context->cookie->__get('carrier_message');
                        $this->context->cookie->__set('carrier_message', '');
                        $this->context->cookie->__unset('carrier_message');
                        $this->context->cookie->write();
                        $order_carrier->save();

                    }
                }
            }
        }
    }

    public function hookAdminOrder($params)
    {
        /*require_once _PS_MODULE_DIR_.'colissimo_simplicite/classes/SCFields.php';

        $delivery_mode = array(
            'DOM' => 'Livraison à domicile',
            'BPR' => 'Livraison en Bureau de Poste',
            'A2P' => 'Livraison Commerce de proximité',
            'MRL' => 'Livraison Commerce de proximité',
            'CMT' => 'Livraison Commerce',
            'CIT' => 'Livraison en Cityssimo',
            'ACP' => 'Agence ColiPoste',
            'CDI' => 'Centre de distribution',
            'BDP' => 'Bureau de poste Belge',
            'RDV' => 'Livraison sur Rendez-vous');

        $order = new Order($params['id_order']);
        $address_delivery = new Address((int)$order->id_address_delivery, (int)$params['cookie']->id_lang);

        $so_carrier = new Carrier((int)Configuration::get('COLISSIMO_CARRIER_ID'));
        $order_carrier = new Carrier((int)$order->id_carrier);
        $id_colissimo_delivery_info = ColissimoDeliveryInfo::getDeliveryInfoExist((int)$order->id_cart, (int)$order->id_customer);

        if ((int)$id_colissimo_delivery_info) {
            $delivery_infos = new ColissimoDeliveryInfo((int)$id_colissimo_delivery_info);

            $sql = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'country c
										  LEFT JOIN '._DB_PREFIX_.'country_lang cl ON cl.id_lang = '.(int)$params['cookie']->id_lang.'
										  AND cl.id_country = c.id_country WHERE iso_code = "'.pSQL($delivery_infos->cecountry).'"');
            $name_country = $sql['name'];
            if (((int)$order_carrier->id_reference == (int)$so_carrier->id_reference) && $delivery_infos->id) {
                $sc_fields = new SCFields($delivery_infos->delivery_mode);

                switch ($sc_fields->delivery_mode) {
                    case SCFields::HOME_DELIVERY:
                        $is_home = true;
                        break;
                    case SCFields::RELAY_POINT:
                        $is_home = false;
                        break;
                }

                $this->context->smarty->assign(array(
                    'path_img' => $this->_path.'logo.gif',
                    'delivery_infos' => $delivery_infos,
                    'address_delivery' => $address_delivery,
                    'is_home' => $is_home,
                    'name_country' => $name_country,
                    'delivery_mode' => $delivery_mode
                ));
                return $this->display(__FILE__, 'views/templates/hook/admin_order.tpl');
            }
        }*/
    }

    public function hookActionCarrierUpdate($params)
    {
        /*if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if ((int)$params['id_carrier'] == (int)Configuration::get('COLISSIMO_CARRIER_ID')) {
            Configuration::updateValue('COLISSIMO_CARRIER_ID', (int)$params['carrier']->id);
        }*/
    }


    public function hookDisplayCarrierExtraContent($params)
    {
       /* $carrier_so = new Carrier((int)Configuration::get('COLISSIMO_CARRIER_ID'));

        if (!isset($carrier_so) || !$carrier_so->active) {
            return '';
        }
        $id_carrier = (int)$carrier_so->id;
        $address_delivery = new Address((int)$params['cart']->id_address_delivery);

        $country = new Country((int)$address_delivery->id_country);
        $carriers = Carrier::getCarriers(
            $this->context->language->id,
            true,
            false,
            false,
            null,
            (defined('ALL_CARRIERS') ? ALL_CARRIERS : Carrier::ALL_CARRIERS)
        );

        // bug fix for cart rule with restriction
        CartRule::autoAddToCart($this->context);

        // For now works only with single shipping !
        if (method_exists($params['cart'], 'carrierIsSelected')) {
            if ($params['cart']->carrierIsSelected((int)$carrier_so->id, $address_delivery->id)) {
                $id_carrier = (int)$carrier_so->id;
            }
        }

        $customer = new Customer($address_delivery->id_customer);

        $gender = array(
            '1' => 'MR',
            '2' => 'MME',
            '3' => 'MLE');

        if (in_array((int)$customer->id_gender, array(
                1,
                2))) {
            $cecivility = $gender[(int)$customer->id_gender];
        } else {
            $cecivility = 'MR';
        }

        $tax_rate = Tax::getCarrierTaxRate($id_carrier, isset($params['cart']->id_address_delivery) ? $params['cart']->id_address_delivery : null);
        $std_cost_with_taxes = number_format((float)$this->initial_cost * (1 + ($tax_rate / 100)), 2, ',', ' ');

        $seller_cost_with_taxes = 0;
        if (Configuration::get('COLISSIMO_COST_SELLER')) {
            if (Configuration::get('COLISSIMO_COST_IMPACT')) {
                $seller_cost_with_taxes = $std_cost_with_taxes + number_format((float)Configuration::get('COLISSIMO_SELLER_AMOUNT'), 2, ',', ' ');
            } else {
                $seller_cost_with_taxes = $std_cost_with_taxes - number_format((float)Configuration::get('COLISSIMO_SELLER_AMOUNT'), 2, ',', ' ');
                if ((float)$seller_cost_with_taxes < 0) {
                    $seller_cost_with_taxes = 0;
                }
            }
        }

        $free_shipping = false;

        $rules = $params['cart']->getCartRules();
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                if ($rule['free_shipping'] && !$rule['carrier_restriction']) {
                    $free_shipping = true;
                    break;
                }
            }
            if (!$free_shipping) {
                $key_search = $id_carrier.',';
                $deliveries_list = $params['cart']->getDeliveryOptionList();
                foreach ($deliveries_list as $deliveries) {
                    foreach ($deliveries as $key => $elt) {
                        if ($key == $key_search) {
                            $free_shipping = $elt['is_free'];
                        }
                    }
                }
            }
        } else {
            // for cart rule with restriction
            $key_search = $id_carrier.',';
            $deliveries_list = $params['cart']->getDeliveryOptionList();
            foreach ($deliveries_list as $deliveries) {
                foreach ($deliveries as $key => $elt) {
                    if ($key == $key_search) {
                        $free_shipping = $elt['is_free'];
                    }
                }
            }
        }

        // town fix
        $town = str_replace('\'', ' ', Tools::substr($address_delivery->city, 0, 32));
        // Keep this fields order (see doc.)
        $inputs = array(
            'pudoFOId' => Configuration::get('COLISSIMO_ID'),
            'ceName' => $this->replaceAccentedChars(Tools::substr($address_delivery->lastname, 0, 34)),
            'dyPreparationTime' => (int)Configuration::Get('COLISSIMO_PREPARATION_TIME'),
            'dyForwardingCharges' => $std_cost_with_taxes,
            'dyForwardingChargesCMT' => $seller_cost_with_taxes,
            'trClientNumber' => (int)$address_delivery->id_customer,
            'orderId' => $this->formatOrderId((int)$address_delivery->id),
            'numVersion' => $this->api_num_version,
            'ceCivility' => $cecivility,
            'ceFirstName' => $this->replaceAccentedChars(Tools::substr($address_delivery->firstname, 0, 29)),
            'ceCompanyName' => $this->replaceAccentedChars(Tools::substr($address_delivery->company, 0, 38)),
            'ceAdress3' => $this->replaceAccentedChars(Tools::substr($address_delivery->address1, 0, 38)),
            'ceAdress4' => $this->replaceAccentedChars(Tools::substr($address_delivery->address2, 0, 38)),
            'ceZipCode' => $this->replaceAccentedChars($address_delivery->postcode),
            'ceTown' => $this->replaceAccentedChars($town),
            'ceEmail' => $this->replaceAccentedChars($params['cookie']->email),
            'cePhoneNumber' => $this->replaceAccentedChars(
                str_replace(
                    array(
                        ' ',
                        '.',
                        '-',
                        ',',
                        ';',
                        '/',
                        '\\',
                        '(',
                        ')'
                    ),
                    '',
                    $address_delivery->phone_mobile
                )
            ),
            'dyWeight' => (float)$params['cart']->getTotalWeight() * 1000,
            'trParamPlus' => $carrier_so->id,
            'trReturnUrlKo' => htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
            'trReturnUrlOk' => htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
            'CHARSET' => 'UTF-8',
            'cePays' => $country->iso_code,
            'trInter' => 1,
            'ceLang' => 'FR'
        );
        if (!$inputs['dyForwardingChargesCMT'] && !Configuration::get('COLISSIMO_COST_SELLER')) {
            unset($inputs['dyForwardingChargesCMT']);
        }

        // set params for Api 3.0 if needed
        $inputs = $this->setInputParams($inputs);

        // generate key for API
        $inputs['signature'] = $this->generateKey($inputs);

        // calculate lowest cost
        $from_cost = $std_cost_with_taxes;
        if (($seller_cost_with_taxes < $std_cost_with_taxes ) && Configuration::get('COLISSIMO_COST_SELLER')) {
            $from_cost = $seller_cost_with_taxes;
        }
        $rewrite_active = true;
        if (!Configuration::get('PS_REWRITING_SETTINGS')) {
            $rewrite_active = false;
        }

        $link = new Link();
        $module_link = $link->getModuleLink('colissimo_simplicite', 'redirect', array(), true);
        $module_link_mobile = $link->getModuleLink('colissimo_simplicite', 'redirectmobile', array(), true);

        // automatic settings api protocol for ssl
        $protocol = 'http://';
        if (Configuration::get('PS_SSL_ENABLED')) {
            $protocol = 'https://';
        }

        $from_mention = $this->l('From Cost');
        $initial_cost = $from_cost.$this->l(' €');
        $tax_mention = $this->l(' TTC');
        if ($free_shipping) {
            $from_mention = '';
            $initial_cost = $this->l('Free (Will be apply after address selection)');
            $tax_mention = '';
        }

        $on_mobile_device = false;

        if ($this->isMobileDevice()) {
            $on_mobile_device = true;
        }
        $this->context->smarty->assign(array(
            'select_label' => $this->l('Select delivery mode'),
            'edit_label' => $this->l('Edit delivery mode'),
            'token' => sha1('colissimo'._COOKIE_KEY_.Context::getContext()->cookie->id_cart),
            'urlSo' => $protocol.Configuration::get('COLISSIMO_URL').'?trReturnUrlKo='.htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
            'urlSoMobile' => $protocol.Configuration::get('COLISSIMO_URL_MOBILE').'?trReturnUrlKo='.htmlentities($this->url, ENT_NOQUOTES, 'UTF-8'),
            'id_carrier' => $id_carrier,
            'inputs' => $inputs,
            'initialCost_label' => $from_mention,
            'initialCost' => $initial_cost, // to change label for price in tpl
            'taxMention' => $tax_mention, // to change label for price in tpl
            'finishProcess' => $this->l('To choose SoColissimo, click on a delivery method'),
            'rewrite_active' => $rewrite_active,
            'link_socolissimo' => $module_link,
            'link_socolissimo_mobile' => $module_link_mobile,
            'on_mobile_device' => $on_mobile_device
        ));
        return $this->display(__FILE__, 'extra_content.tpl');*/
    }


   /*
    FONCTIONS COLISSIMOS -- TODO CLEAN
   protected function addCarrier()
    {
        $carrier = new Carrier();
        $carrier->name = $this->config['name'];
        $carrier->id_tax_rules_group = $this->config['id_tax_rules_group'];
        $carrier->id_zone = $this->config['id_zone'];
        $carrier->url = $this->config['url'];
        $carrier->active = $this->config['active'];
        $carrier->deleted = $this->config['deleted'];
        $carrier->shipping_handling = $this->config['shipping_handling'];
        $carrier->range_behavior = $this->config['range_behavior'];
        $carrier->is_module = $this->config['is_module'];
        $carrier->shipping_external = $this->config['shipping_external'];
        $carrier->external_module_name = $this->config['external_module_name'];
        $carrier->need_range = $this->config['need_range'];

        foreach (Language::getLanguages() as $lang) {
            if ($lang['iso_code'] == 'fr') {
                $carrier->delay[$lang['id_lang']] = $this->config['delay'][$lang['iso_code']];
            }
            if ($lang['iso_code'] == 'en') {
                $carrier->delay[$lang['id_lang']] = $this->config['delay'][$lang['iso_code']];
            }
        }
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if ($carrier->add() == true) {
            @copy(dirname(__FILE__).'/views/img/colissimo.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
            Configuration::updateValue('COLISSIMO_CARRIER_ID', (int)$carrier->id);
            return $carrier;
        }

        return false;
    }

    protected function addGroups($carrier)
    {
        $groups_ids = array();
        $groups = Group::getGroups(Context::getContext()->language->id);
        foreach ($groups as $group) {
            $groups_ids[] = $group['id_group'];
        }

        $carrier->setGroups($groups_ids);
    }

    protected function addRanges($carrier)
    {
        $range_price = new RangePrice();
        $range_price->id_carrier = $carrier->id;
        $range_price->delimiter1 = '0';
        $range_price->delimiter2 = '10000';
        $range_price->add();

        $range_weight = new RangeWeight();
        $range_weight->id_carrier = $carrier->id;
        $range_weight->delimiter1 = '0';
        $range_weight->delimiter2 = '10000';
        $range_weight->add();
    }

    protected function addZones($carrier)
    {
        $zones = Zone::getZones();

        foreach ($zones as $zone) {
            $carrier->addZone($zone['id_zone']);
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
   /* public function hookBackOfficeHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/back.css');
    }

    /**
     * Validate SIRET Code Taken from prestashop core for compatibility 1.4 reason
     * @static
     * @param $siret SIRET Code
     * @return boolean Return true if is valid
     */
 /*   public function isSiret($siret)
    {
        if (Tools::strlen($siret) != 14) {
            return false;
        }
        $sum = 0;
        for ($i = 0; $i != 14; $i++) {
            $tmp = ((($i + 1) % 2) + 1) * (int)$siret[$i];
            if ($tmp >= 10) {
                $tmp -= 9;
            }
            $sum += $tmp;
        }
        return ($sum % 10 === 0);
    }

    public function getApiPrice($id_cart)
    {
        if ((int)$id_cart) {
            return Db::getInstance()->getValue('SELECT dyforwardingcharges
            FROM '._DB_PREFIX_.'colissimo_delivery_info
            WHERE id_cart = '.(int)$id_cart);
        }
        return false;
    }

    public function checkZone($id_carrier)
    {
        return (bool)Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'carrier_zone WHERE id_carrier = '.(int)$id_carrier);
    }

    public function checkGroup($id_carrier)
    {
        return (bool)Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'carrier_group WHERE id_carrier = '.(int)$id_carrier);
    }

    public function checkRange($id_carrier)
    {
        $sql = '';
        $carrier = new Carrier($id_carrier);
        if ($carrier->shipping_method) {
            switch ($carrier->shipping_method) {
                case '2':
                    $sql = 'SELECT * FROM '._DB_PREFIX_.'range_price WHERE id_carrier = '.(int)$id_carrier;
                    break;
                case '1':
                    $sql = 'SELECT * FROM '._DB_PREFIX_.'range_weight WHERE id_carrier = '.(int)$id_carrier;
                    break;
            }
        }
        if (!$sql) {
            switch (Configuration::get('PS_SHIPPING_METHOD')) {
                case '0':
                    $sql = 'SELECT * FROM '._DB_PREFIX_.'range_price WHERE id_carrier = '.(int)$id_carrier;
                    break;
                case '1':
                    $sql = 'SELECT * FROM '._DB_PREFIX_.'range_weight WHERE id_carrier = '.(int)$id_carrier;
                    break;
            }
        }
        return (bool)Db::getInstance()->getRow($sql);
    }

    public function checkDelivery($id_carrier)
    {
        return (bool)Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'delivery WHERE id_carrier = '.(int)$id_carrier);
    }

    public function reallocationCarrier($id_socolissimo)
    {
        // carrier must be module carrier
        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'carrier SET
            shipping_handling = 0,
            is_module = 1,
            shipping_external = 1,
            need_range = 1,
            external_module_name = "colissimo_simplicite"
            WHERE  id_carrier = '.(int)$id_socolissimo);

        // old carrier no longer linked with socolissimo
        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'carrier SET
            is_module = 0,
            external_module_name = ""
            WHERE  id_carrier NOT IN ( '.(int)Configuration::get('COLISSIMO_CARRIER_ID').')');
    }
*/
    /**
     * Generate good order id format.
     *
     * @param $id
     * @return string
     */
/*   public function formatOrderId($id)
    {
        $str_len = Tools::strlen($id);
        while ($str_len < 5) {
            $id = '0'.$id;
            $str_len = Tools::strlen($id);
        }
        return $id;
    }
*/
    /**
     * @param $str
     * @return mixed
     */
    public function replaceAccentedChars($str)
    {
        $str = preg_replace(
            array(
            /* Lowercase */
            '/[\x{0105}\x{00E0}\x{00E1}\x{00E2}\x{00E3}\x{00E4}\x{00E5}]/u',
            '/[\x{00E7}\x{010D}\x{0107}]/u',
            '/[\x{010F}]/u',
            '/[\x{00E8}\x{00E9}\x{00EA}\x{00EB}\x{011B}\x{0119}]/u',
            '/[\x{00EC}\x{00ED}\x{00EE}\x{00EF}]/u',
            '/[\x{0142}\x{013E}\x{013A}]/u',
            '/[\x{00F1}\x{0148}]/u',
            '/[\x{00F2}\x{00F3}\x{00F4}\x{00F5}\x{00F6}\x{00F8}]/u',
            '/[\x{0159}\x{0155}]/u',
            '/[\x{015B}\x{0161}]/u',
            '/[\x{00DF}]/u',
            '/[\x{0165}]/u',
            '/[\x{00F9}\x{00FA}\x{00FB}\x{00FC}\x{016F}]/u',
            '/[\x{00FD}\x{00FF}]/u',
            '/[\x{017C}\x{017A}\x{017E}]/u',
            '/[\x{00E6}]/u',
            '/[\x{0153}]/u',
            /* Uppercase */
            '/[\x{0104}\x{00C0}\x{00C1}\x{00C2}\x{00C3}\x{00C4}\x{00C5}]/u',
            '/[\x{00C7}\x{010C}\x{0106}]/u',
            '/[\x{010E}]/u',
            '/[\x{00C8}\x{00C9}\x{00CA}\x{00CB}\x{011A}\x{0118}]/u',
            '/[\x{0141}\x{013D}\x{0139}]/u',
            '/[\x{00D1}\x{0147}]/u',
            '/[\x{00D3}]/u',
            '/[\x{0158}\x{0154}]/u',
            '/[\x{015A}\x{0160}]/u',
            '/[\x{0164}]/u',
            '/[\x{00D9}\x{00DA}\x{00DB}\x{00DC}\x{016E}]/u',
            '/[\x{017B}\x{0179}\x{017D}]/u',
            '/[\x{00C6}]/u',
            '/[\x{0152}]/u',
            ),
            array(
            'a',
            'c',
            'd',
            'e',
            'i',
            'l',
            'n',
            'o',
            'r',
            's',
            'ss',
            't',
            'u',
            'y',
            'z',
            'ae',
            'oe',
            'A',
            'C',
            'D',
            'E',
            'L',
            'N',
            'O',
            'R',
            'S',
            'T',
            'U',
            'Z',
            'AE',
            'OE'
            ),
            $str
        );
        $array_unauthorised_api = array(
            ';',
            '€',
            '~',
            '#',
            '{',
            '(',
            '[',
            '|',
            '\\',
            '^',
            ')',
            ']',
            '=',
            '}',
            '$',
            '¤',
            '£',
            '%',
            'μ',
            '*',
            '§',
            '!',
            '°',
            '²',
            '"',
            );
        foreach ($array_unauthorised_api as $key => $value) {
            $str = str_replace($value, '', $str);
        }
        $str = str_replace('\'', ' ', $str);
        $str = preg_replace('/\s+/', ' ', $str);

        return $str;
    }

    /**
     * @param array
     * @return array
     */
    public function setInputParams($inputs)
    {
        $get_mobile_device = Context::getContext()->getMobileDevice();

        // set api params for 4.0 and mobile
        if ($get_mobile_device || $this->isIpad() || $this->isMobile()) {
            unset($inputs['CHARSET']);
            $inputs['numVersion'] = '4.0';
        }
        return $inputs;
    }

    public function isMobileDevice()
    {
        $get_mobile_device = Context::getContext()->getMobileDevice();

        // set api params for 4.0 and mobile
        if ($get_mobile_device || $this->isIpad() || $this->isMobile()) {
            return true;
        }
        return false;
    }

    public function upper($str_in)
    {
        return Tools::strtoupper(str_replace('-', ' ', Tools::link_rewrite($str_in)));
    }

    public function lower($str_in)
    {
        return Tools::strtolower(str_replace('-', ' ', Tools::link_rewrite($str_in)));
    }

    public function formatName($name)
    {
        return preg_replace('/[0-9!<>,;?=+()@#"°{}_$%:]/', '', Tools::stripslashes($name));
    }

    public function isSameAddress($id_address, $id_cart, $id_customer)
    {
        $id_colissimo_delivery_info = ColissimoDeliveryInfo::getDeliveryInfoExist((int)$id_cart, (int)$id_customer);
        if (!$id_colissimo_delivery_info) {
            return $id_address;
        }
        $colissimo_delivery_info = new ColissimoDeliveryInfo((int)$id_colissimo_delivery_info);
        $ps_address = new Address((int)$id_address);
        $new_address = new Address();
        $sql = Db::getInstance()->getRow('SELECT c.id_country, cl.name FROM '._DB_PREFIX_.'country c
										  LEFT JOIN '._DB_PREFIX_.'country_lang cl ON cl.id_lang = '.(int)$this->context->language->id.'
										  AND cl.id_country = c.id_country WHERE iso_code = "'.pSQL($colissimo_delivery_info->cecountry).'"');

        $iso_code = $sql['id_country'];

        if ($this->upper($ps_address->lastname) != $this->upper($colissimo_delivery_info->prname) || $ps_address->id_country != $iso_code || $this->upper($ps_address->firstname) != $this->upper($colissimo_delivery_info->prfirstname)
            || $this->upper($ps_address->address1) != $this->upper($colissimo_delivery_info->pradress3) || $this->upper($ps_address->address2) != $this->upper($colissimo_delivery_info->pradress2) || $this->upper($ps_address->postcode)
            != $this->upper($colissimo_delivery_info->przipcode) || $this->upper($ps_address->city) != $this->upper($colissimo_delivery_info->prtown) || str_replace(array(
                ' ',
                '.',
                '-',
                ',',
                ';',
                '+',
                '/',
                '\\',
                '+',
                '(',
                ')'), '', $ps_address->phone_mobile) != $colissimo_delivery_info->cephonenumber) {
            $new_address->id_customer = (int)$id_customer;
            $firstname_company = preg_replace('/\d/', '', Tools::substr($colissimo_delivery_info->prfirstname, 0, 31));
            $lastname_company = preg_replace('/\d/', '', Tools::substr($colissimo_delivery_info->prname, 0, 32));
            $firstname = preg_replace('/\d/', '', Tools::substr($colissimo_delivery_info->cefirstname, 0, 32));
            $lastname = preg_replace('/\d/', '', Tools::substr($colissimo_delivery_info->cename, 0, 32));
            $firstname_company_formatted = trim($this->formatName($firstname_company));
            $lastname_company_formatted = trim($this->formatName($lastname_company));
            $new_address->lastname = trim($this->formatName($lastname));
            $new_address->firstname = trim($this->formatName($firstname));
            $new_address->postcode = $colissimo_delivery_info->przipcode;
            $new_address->city = str_replace('\'', ' ', $colissimo_delivery_info->prtown);
            $new_address->id_country = $iso_code;
            $new_address->alias = 'Colissimo - '.date('d-m-Y');
            $new_address->phone_mobile = $colissimo_delivery_info->cephonenumber;

            if (!in_array($colissimo_delivery_info->delivery_mode, array(
                    'DOM',
                    'RDV'))) {
                $new_address->company = $firstname_company_formatted.' '.$lastname_company_formatted;
                $new_address->active = 0;
                $new_address->deleted = 1;
                $new_address->address1 = $colissimo_delivery_info->pradress1;
                $new_address->address2 = $colissimo_delivery_info->pradress2;
                $new_address->add();
                $new_address->deleted = 1;
                $new_address->save();
            } else {
                $new_address->address1 = $colissimo_delivery_info->pradress3;
                ((isset($colissimo_delivery_info->pradress2)) ? $new_address->address2 = $colissimo_delivery_info->pradress2 : $new_address->address2 = '');
                ((isset($colissimo_delivery_info->pradress1)) ? $new_address->other .= $colissimo_delivery_info->pradress1 : $new_address->other = '');
                ((isset($colissimo_delivery_info->pradress4)) ? $new_address->other .= ' | '.$colissimo_delivery_info->pradress4 : $new_address->other = '');
                $new_address->postcode = $colissimo_delivery_info->przipcode;
                $new_address->city = str_replace('\'', ' ', $colissimo_delivery_info->prtown);
                $new_address->id_country = $iso_code;
                $new_address->alias = 'Colissimo - '.date('d-m-Y');
                $new_address->add();
                $new_address->active = 0;
                $new_address->deleted = 1;
                $new_address->save();
            }
            return (int)$new_address->id;
        }
        return (int)$ps_address->id;
    }

    /**
     * Check if agent user is iPad(for so_mobile)
     * @return bool
     */
    public function isIpad()
    {
        return (bool)strpos($_SERVER['HTTP_USER_AGENT'], 'iPad');
    }

    public function isMobile()
    {
        if (method_exists(Context::getContext()->mobile_detect, 'isMobile')) {
            return (bool)Context::getContext()->mobile_detect->isMobile();
        } else {
            return false;
        }
    }

    /**
     * Generate the signed key
     *
     * @static
     * @param $params
     * @return string
     */
    public function generateKey($params)
    {
        $str = '';

        foreach ($params as $key => $value) {
            if (!in_array(Tools::strtoupper($key), array(
                    'SIGNATURE'))) {
                $str .= utf8_decode($value);
            }
        }

        return sha1($str.Tools::strtolower(Configuration::get('COLISSIMO_KEY')));
    }
}
