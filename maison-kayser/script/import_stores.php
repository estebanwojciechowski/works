<?php


/**
 * Pour le 1er import et la création des boutiques
 * cleanBDD utiliser les lignes pour effacer les boutiques existantes
 * ensuite les recommenter pour ajouter uniquement les boutiques non créées
 */

exec('chcp 65001'); // gestion accent sortie console
ini_set('memory_limit', '-1');
ini_set('max_ecution_time', '-1');
require 'vendor/autoload.php'; // logger
include_once(dirname(__FILE__) . '/../src/config/config.inc.php'); 

/**
 * Class permettant d'importer les données sur le site kayser
 */
class ImportStores {

    private static $fields = array(
        0   => 'id_store',                      // Identifiant boutique
        1   => 'ouverture_magasin',             // Date Ouverture magasin 
        2   => 'is_paris',                      // GMB Géré par Paris 
        3   => 'name',                          // Nom de l'établissement 
        4   => 'address1',                      // Adresse
        5   => 'postcode',                      // CP 
        6   => 'city',                          // Ville 
        7   => 'quartier',                      // Quartier  
        8   => 'arrondissement',                // Arrondissement
        9   => 'pays',                          // Pays
        10  => 'latitude',                      // Latitude
        11  => 'longitude',                     // Longitude 
        12  => 'phone',                         // Tel
        13  => 'email',                         // Mail
        14  => 'lundi',                         // Lundi
        15  => 'mardi',                         // Mardi
        16  => 'mercredi',                      // Mercredi 
        17  => 'jeudi',                         // Jeudi
        18  => 'vendredi',                      // Vendredi
        19  => 'samedi',                        // Samedi
        20  => 'dimanche',                      // Dimanche
        21  => 'lundi_cnc',                     // Lundi ClickNCollect
        22  => 'mardi_cnc',                     // Mardi ClickNCollect
        23  => 'mercredi_cnc',                  // Mercredi ClickNCollect
        24  => 'jeudi_cnc',                     // Jeudi ClickNCollect
        25  => 'vendredi_cnc',                  // Vendredi ClickNCollect
        26  => 'samedi_cnc',                    // Samedi ClickNCollect
        27  => 'dimanche_cnc',                  // Dimanche ClickNCollect
        28  => 'type_payment',                  // Type paiement
        29  => 'identifiant_caisse',            // identifiant caisse
        30  => 'sans_gluten',                   // Sans Gluten
        31  => 'restaurant',                    // Restaurant
        32  => 'cafe',                          // Café 
        33  => 'terrasse',                      // Terrasse  
        34  => 'colissimo',                     // Colissimo 
        35  => 'clickandcollect',               // ClickNCollect 
    );

    private $_logger;

    protected $errors = array();
    public $infos = array();
    protected $stores = array();
    protected $active_stores = array();
    protected $create_stores = array();
    protected $default_id_lang;
    protected static $default_id_country = 8;
    protected static $default_email = 'contact@maison-kayser.com';
    private $importInitial = false;
    public $dirImportFile = '';
    public $fileList = array(
        'magasins.csv',
    ); 

    public function __construct($dir = false, $fileList = array(), $importInitial = false) { 
        //enleve la boutique du context pour gérer nativement les produits sur toute les boutiques 
        $context = Context::getContext();
        $context->shop = new Shop();  
        Shop::setContext(Shop::CONTEXT_ALL);  

        $this->default_id_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        if(!$dir) {
            $this->dirImportFile = dirname(__FILE__) . '/../import/';
        } else {
            $this->dirImportFile = $dir;            
        }

        if(!empty($fileList)) {
            $this->fileList = $fileList;
        }
        $this->importInitial = $importInitial;
        $this->_logger = new Katzgrau\KLogger\Logger('./log/', Psr\Log\LogLevel::INFO); 
    }

    public function getErrors() {
        return $this->errors;
    }

    public function log($str) {
        //echo $str . "\r\n";
        $this->_logger->info($str);
    }
 

     /**
     * Function start
     * Permet de lancer les différentes actions nécessaires pour l'import des données
     */
    public function start() {
        $this->log('DEBUT IMPORT DES BOULANGERIES');
        if($this->readFiles()) {
            if(count($this->stores)) {   
                if($this->importInitial) {
                    $this->log("BDD nettoyée");
                    $this->cleanBDD();
                } 
                //la bdd a été nettoyé on crée les boutiques et on commence les imports 
                $this->addShops();
                $this->addStores(); 
            }
        }  
        $this->log('FIN IMPORT DES BOULANGERIES');       
    }

    protected function readFiles() {
        $this->log("Lecture des informations contenues dans le CSV");
        foreach ($this->fileList as $file) { 
            if(!$this->readFile($this->dirImportFile . $file, $file)) {
              return false;
            }
        }
        $this->log('Produits initialisés');
        return true;
    }

    /**
     * [readFile Lis la liste des fichiers]
     * @param  [type] $file     [chemin complet du fichier]
     * @param  [type] $fileName [nom du fichier]
     * @return [type]           [boolean]
     */
    protected function readFile($file, $fileName) {

        if (!file_exists($file)) {
            $this->log("ERREUR le fichier " . $fileName . " n\'a pas été trouvé");
            return false;
        }

        $this->log("le fichier " . $fileName . " a été trouvé");
        $fp = file($file);  

        if (($handle = fopen($file, "r")) !== false) { 
            $row = 0;
            $first = true;
            while (($data = fgetcsv($handle, 0, ";")) !== false) { 
                // if($first) {
                //     $first = false;
                //     continue;
                // }
                if(is_array($data) && count($data) == count(self::$fields)) {
                    $this->initStore($data, $row);
                    $row++; 
                } else {
                    $msg = 'ERREUR : le nombre de colonnes du csv ne correspond pas. CSV : '.count($data).' / Config : '.count(self::$fields);
                     $this->log($msg);
                     $this->errors[] = $msg;
                     return false;
                }
            }
            fclose($handle);
            unset($handle); 
        } else {
            $msg = "ERREUR Impossible de lire le fichier " . $fileName;
            $this->errors[] = $msg;
            $this->log($msg);
            unset($fp);
            return false;
        }
        unset($fp);
        return true;
    }

    // initialise la variable stores et teste le contenu des lignes du csv avant traitement
    protected function initStore($csv_row, $nb = 0) {
        $this->log('Initialisation du magasin');
        $errors = array(); 
        //objet product utilisé pour la validation des champs
        $store = new Store();
        $store_array = array();
        $definition = Store::$definition['fields'];
        $id_store = null;
        $main_category = ''; 
        if(!is_numeric($csv_row[0])) {
            return;
        }

        foreach ($csv_row as $key => $value) {     
            $value = trim($value);

            if(in_array($value, array('null', 'à venir'))) {
                $value = null;
            }   

            if(self::$fields[$key] == 'id_store') {
                $id_store = (int) $value; 
                if(!$id_store) {
                  // si l'id id_store est invalide on stoppe le traitement
                  // on empeche pas l'import on assume, c'est des lignes vides..
                  // bon on log quand même on sait jamais
                  $msg =  'LIGNE '.$nb.' '.self::$fields[$key].' invalide : "'.addslashes($value).'"';
                  $this->errors[self::$fields[$key]] = $msg; 
                  break;
                }
                $store_array = array('id_store' => $id_store);
                continue;
            } elseif(in_array(self::$fields[$key], array('longitude', 'latitude'))) {
                $value = floatval($value);
                $value = number_format($value, 8);
            }
            // si la propriété existe il n'y a pas d traitement spécifique
            if(property_exists($store, self::$fields[$key])) { 
                if(isset($definition[self::$fields[$key]]['validate'])) {
                    if($definition[self::$fields[$key]]['validate'] == 'isBool') {
                        if(strtolower($value) == 'oui') {
                            $value = 1;
                        } else 
                            $value = 0;
                    } 
                    // si pas d'email de contact, valeur par défaut
                    if(self::$fields[$key] == 'email' && is_null($value))
                        $value = self::$default_email;

                    if(!call_user_func('Validate::'.$definition[self::$fields[$key]]['validate'], $value)) { 
                        $this->log('ERREUR '.self::$fields[$key].' '.$definition[self::$fields[$key]]['validate'].' a échoué pour la valeur : '.$value);
                        if(isset($definition[self::$fields[$key]]['required']) && $definition[self::$fields[$key]]['required']) {
                            // si le champs est obligatoire on skip le store
                            return false;
                        } 
                    }
                } 
                $store_array[self::$fields[$key]] = $value; 
            } 
        }
        //active la boutique ou non
        // $ouverture_magasin = $csv_row[array_search('ouverture_magasin', self::$fields)];
        // par défaut magasin actif
        $store_array['active'] = 1;
        // if($date = self::isDate($ouverture_magasin)) {
        //     if(time() < $date->getTimestamp())
        //         $store_array['active'] = 0;
        // } 
        // TODO initialement on active uniquement les boutiques parisiennes
        // $is_paris = $csv_row[array_search('is_paris', self::$fields)];
        // if(strtolower($is_paris)  == 'non') {
        //     $store_array['active'] = 0; 
        // }
        $pays = $csv_row[array_search('pays', self::$fields)];

        if($id_country = Country::getIdByName($this->default_id_lang, $pays)) {
            $store_array['id_country'] = $id_country;
        } else {
            $store_array['id_country'] = self::$default_id_country;
        }
        // traitement spécifique
        if(stripos($pays, 'amérique') !== false)
            $store_array['id_country'] = 21;
        elseif(stripos($pays, 'congo') !== false)
            $store_array['id_country'] = 71;
        elseif(stripos($pays, 'hong-kong') !== false)
            $store_array['id_country'] = 22;
        elseif(stripos($pays, 'ivoire') !== false)
            $store_array['id_country'] = 32;

        //horaires
        $hours = array(
            $csv_row[array_search('lundi', self::$fields)],
            $csv_row[array_search('mardi', self::$fields)],
            $csv_row[array_search('mercredi', self::$fields)],
            $csv_row[array_search('jeudi', self::$fields)],
            $csv_row[array_search('vendredi', self::$fields)],
            $csv_row[array_search('samedi', self::$fields)],
            $csv_row[array_search('dimanche', self::$fields)]
        );
        $hours_cnc = array(
            $csv_row[array_search('lundi_cnc', self::$fields)],
            $csv_row[array_search('mardi_cnc', self::$fields)],
            $csv_row[array_search('mercredi_cnc', self::$fields)],
            $csv_row[array_search('jeudi_cnc', self::$fields)],
            $csv_row[array_search('vendredi_cnc', self::$fields)],
            $csv_row[array_search('samedi_cnc', self::$fields)],
            $csv_row[array_search('dimanche_cnc', self::$fields)]
        ); 

        $store_array['hours'] = json_encode($hours);
        $store_array['hours_clickandcollect'] = json_encode($hours_cnc);
 
        $store_array['date_add'] = date('Y-m-d H:i:s');
        $store_array['date_upd'] = date('Y-m-d H:i:s'); 

        $this->stores[$store_array['id_store']] = $store_array;
        if($store_array['active'] == 1) {
            $this->active_stores[$store_array['id_store']] = $store_array['id_store'];
        }
 
        // Creation des boutiques auto si active et dispose d'un moyen de livraison
        if($store_array['active'] && ($store_array['colissimo'] || $store_array['clickandcollect'])) {
            $this->create_stores[$store_array['id_store']] = $store_array;
        }

        if(empty($errors)) {
          $this->log('Ligne '.$nb.' magasin '.$id_store.' OK');
          $this->stores[$id_store] = $store_array;
        } else {
          $this->log('ERREUR Ligne '.$nb.' magasin '.$id_store." KO \nERREURS :\n".print_r($errors, true)."\nLIGNE:\n".print_r($csv_row, true));
          $this->errors[] = $errors;
        }  
    }

    public static function isDate($value) 
    {
        if (!$value) {
            return false;
        }

        try {
            $date = new \DateTime($value);
            return $date;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function cleanBDD() { 

        $clean = DB::getInstance()->execute(
                /*' TRUNCATE `'._DB_PREFIX_.'store`;
                  TRUNCATE `'._DB_PREFIX_.'store_shop`;  
                  DELETE FROM `'._DB_PREFIX_.'shop` WHERE `id_shop` > 1;  
                  DELETE FROM `'._DB_PREFIX_.'shop_url` WHERE `id_shop` > 1;
                  ALTER TABLE `'._DB_PREFIX_.'shop_url` AUTO_INCREMENT=2;
                  '*/
                   ' TRUNCATE `'._DB_PREFIX_.'store`;
                  TRUNCATE `'._DB_PREFIX_.'store_shop`;  
                  '
            );
        if(!$clean) {
          $this->log("ERREUR Clean BDD KO");
          return false;
        }
        $this->log("Clean BDD OK");  
        return true;
    }

    
    /**
     * [addShops creation des boutiques (ps_shop)]
     */
    public function addShops() { 
        if(count($this->create_stores)) {    
            $has_new_shop = false;
            $datas = array('shop' => array(), 'shop_url' => array());
            $main_shop_domain = ShopUrl::getMainShopDomain(1); 
            $shop_group = new ShopGroup(1);
            $result = array('shop' => false, 'shop_url' => false);
            //les id_store et id_shop sont identiques
            //initialisation des données à importer
            foreach ($this->create_stores as $id_store => $store_array) {
                //recuperation du shop par le cache
                $shop = Shop::getShop($id_store);
                if($shop) {
                    $this->log('La boutique '.$id_store.' est déjà existante.');                    
                    continue;
                }
                $name = $store_array['name'];
                if ($shop_group->shopNameExists($store_array['name'])) {
                    // Il ne peut y avoir 2 shops du meme nom dans le même groupe
                    $name = $store_array['name'].'-'.$id_store;
                } 

                $this->log('La boutique '.$id_store.' va être insérée.');
                //add shop
                $data = array(
                    'id_shop' => $id_store,
                    'id_shop_group' => 1,
                    'id_category' => 2,
                    'theme_name' => 'insitaction',
                    'name' => $name,
                    'active' => $store_array['active'],
                );
                $datas['shop'][$id_store] = $data;

                //add shop_url
                $data = array(
                    'id_shop' => $id_store,
                    'domain' => $main_shop_domain,
                    'domain_ssl' => $main_shop_domain,
                    'physical_uri' => '/',
                    'virtual_uri' => Tools::link_rewrite($name).'/',
                    'main' => 1,
                    'active' => $store_array['active'],
                );
                $datas['shop_url'][$id_store] = $data; 
                $has_new_shop = true;
            }
            // insertion des données
            if($has_new_shop) {
                // initialisation des variables nécessaire à la duplication de la boutique principal
                $import_data = array(
                    'carrier' => 1,
                    'cms' => 1,
                    'category' => 1,
                    'contact' => 1,
                    'country' => 1,
                    'currency' => 1,
                    'discount' => 1,
                    'employee' => 1,
                    'image' => 1,
                    'lang' => 1,
                    'manufacturer' => 1,
                    'module' => 1,
                    'hook_module' => 1,
                    'meta_lang' => 1,
                    'product' => 1,
                    'product_attribute' => 1,
                    'stock_available' => 1,
                    'store' => 1,
                    'warehouse' => 1,
                    'webservice_account' => 1,
                    'attribute_group' => 1,
                    'feature' => 1,
                    'group' => 1,
                    'tax_rules_group' => 1,
                    'supplier' => 1,
                    'referrer' => 1,
                    'zone' => 1,
                    'cart_rule' => 1,
                );

                // Hook for duplication of shop data
                $modules_list = Hook::getHookModuleExecList('actionShopDataDuplication');
                if (is_array($modules_list) && count($modules_list) > 0) {
                    foreach ($modules_list as $m) {
                        $import_data['Module'.ucfirst($m['module'])] = Module::getModuleName($m['module']);
                    }
                }

                asort($import_data);

                $this->log('Debut insertion des boutiques.');
                foreach ($datas as $key => $data) {
                    $config = array(
                        'table' => _DB_PREFIX_.$key,
                        'delimiter' => 10000,
                        'datas' => $data
                    ); 

                    $result[$key] = $this->insert($config);
                    if($result['shop'] && $result['shop_url']) {
                        //copie du contenu de la boutique principale pour les nouvelles boutiques
                        foreach ($data as $id_store => $value) { 
                            $shop = new Shop($id_store);  
                            if($shop && !is_null($shop->name)) {
                                $this->log('Debut Création du contenu de la boutique #'.$id_store);
                                $shop->copyShopData(1, $import_data);
                                $this->log('Fin Création du contenu de la boutique #'.$id_store);
                            } else {
                                $this->log('ERREUR Création du contenu de la boutique #'.$id_store.' impossible, la boutique est introuvable');
                            }
                        }                        
                    }
                } 
                if(!$result['shop'] || !$result['shop_url']) {
                    $this->log('ERREUR Création des boutiques');
                    if(!$result['shop']) {
                        $this->log('ERREUR Création de boutique pour la table '._DB_PREFIX_.'shop');
                    }
                    if(!$result['shop_url']) {
                        $this->log('ERREUR Création de boutique pour la table '._DB_PREFIX_.'shop_url');

                    }

                }
                // regeneration de l'htaccess pour inclure les nouvelles boutiques 
                $this->log('Regeneration du htaccess');
                Tools::generateHtaccess();

                $this->log('Fin insertion des boutiques.');
            } else {
                $this->log('Aucun  site e-commerce n\'a été créée.');
                $this->infos[] = 'Aucun site e-commerce n\'a été créé, ils sont déja existants.';
            }
        }
    }   

     /**
     * Import des stores (ps_stores)
     * @return boolean
     */
    public function addStores() {
        $this->log('Ajout des boulangeries');
        if(!$this->importInitial) {
            $this->log('Import en mode ajout');
            if(count($this->stores)) {
                // verification de l'existance des magasins
                foreach ($this->stores as $id_store => $store) {
                    if(Store::storeExists($id_store)) {
                        $this->log('La boulangerie #'.$id_store.' existe déjà');
                        unset($this->stores[$id_store]);
                        if(isset($this->active_stores[$id_store])) {
                            unset($this->active_stores[$id_store]);
                        }
                    }
                }
            }
        } else {
            $this->log('Import initial');
        }

        $this->log('Début insertion des magasins.');
        if(count($this->stores)) {  
            $config = array(
                'table' => _DB_PREFIX_.'store',
                'delimiter' => 10000,
                'datas' => $this->stores
            );

            $this->insert($config);
 
            //association shop
            if ($this->active_stores) {
                $shops = array_values($this->active_stores); 
                $shops[] = 1;
                foreach ($shops as $key => $id_shop) {
                    $datas = array();
                    foreach ($this->active_stores as $id_store) {
                        $datas[] = array(
                            'id_store' => $id_store,
                            'id_shop' => $id_shop,
                        );
                    }

                    $table = array(
                        'table' => _DB_PREFIX_.'store_shop',
                        'delimiter' => 10000,
                        'datas' => $datas
                    );

                    $this->insert($table);
                }
            } 
            if($this->importInitial) {
                // maj des stores pour le module bevisible (on affiche que les magasins actifs)
                if(Configuration::updateGlobalValue('BEVISIBLE_STORES_ENABLE', json_encode($this->active_stores))) {
                    //met a jour le magasin par defaut, le premier magasin actif en l'occurence
                    Configuration::updateGlobalValue('BEVISIBLE_STORE_DEFAULT', current($this->active_stores));
                    $this->log('BEVISIBLE_STORES_ENABLE : Config mis a jour avec succès.');
                } else {
                    $this->log('BEVISIBLE_STORES_ENABLE : Config fail.');
                }
            }            
        } else {            
            $this->infos[] = 'Aucune boulangerie n\'a été créée, elles sont déja toutes présentes.';
        }
    }

    
    public function insert($args) {
        $error = false;
        if (isset($args['datas']) && count($args['datas'])) {

            //génération des clés
            $data = current($args['datas']);
            foreach ($data as $key => $val) {

                $keys[] = '`' . $key . '`';
            }
 
            while (count($args['datas']) > 0) {
                echo 'CREATION EN COURS';
                echo "\r\n";

                $o = 0;
                $tab_value_stringified = $values = array();

                foreach ($args['datas'] as $key => $data) { // BOUCLE SUR LES CLES
                    if ($o == $args['delimiter'])
                        break;

                    foreach ($data as $val) {
                        $val = pSQL($val);
                        $values[$key][] = ($val === '') ? "''" : ((is_null($val)) ? 'NULL' : "'{$val}'");                        
                    }

                    $o++;
                    unset($args['datas'][$key]);
                }

                foreach ($values as $value)
                    $tab_value_stringified[] = '(' . implode(', ', $value) . ')';


                $values_stringified = implode(', ', $tab_value_stringified) . ';';
                $keys_stringified = implode(',', $keys);

                $sql = 'INSERT INTO `' . $args['table'] . '` (' . $keys_stringified . ') VALUES ';
                $sql .= $values_stringified;
                //print_r($sql);exit;
                if(!DB::getInstance()->execute($sql)) {
                    $error = true;
                }
            }
            if($error) {
                $this->log('ERREUR La requete SQL d\'insertion des magasins a échoué');
            }
            echo 'DONNEES INSEREE';
            echo "\r\n";

            return !$error;
        } else {
            $this->log('ERREUR Aucun data a insérer');
        }
        return false;
    }
 
}
/*
$timestamp_debut = microtime(true);

$import = new ImportStores();
$import->start();

$timestamp_fin = microtime(true);
$difference_ms = $timestamp_fin - $timestamp_debut;
echo 'Exécution du script : ' . $difference_ms . ' secondes.';

*/