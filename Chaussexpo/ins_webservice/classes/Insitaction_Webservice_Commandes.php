<?php
 
include_once(dirname(__FILE__).'/Insitaction_Webservice.php');  
include_once(dirname(__FILE__).'/Insitaction_Webservice_Clients.php');

class Insitaction_Webservice_Commandes extends Insitaction_Webservice {

    protected $curl_header = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">   <soap:Body>';

    protected $curl_footer = '</soap:Body></soap:Envelope>';
    protected $service_url = 'http://tempuri.org/WebServiceCommandes/Service1';
    protected $header_url = '/WSCommande_TEST/ServiceCommande.asmx';
    protected $host = 'ws.chaussexpo.fr';
    protected static $id_attribute_pointure = 1;

	protected static $data = array(
		'ajout_commande' => array(
			'ajout_commande' => array(
				'code_avantage' => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
	            "code_concept" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
	            "code_client" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
	            "REMISE_AVANTAGE" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isName', 'size' => 255),
	            "type_reglement" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isAddress', 'size' => 128),
	            "type_livraison" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isAddress', 'size' => 128),
	            "frais_reglement" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPostCode', 'size' => 12),
	            "frais_livraison" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isCityName', 'size' => 64),
	            "numero_carte" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPhoneNumber', 'size' => 32),
                "date_valid_carte" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPhoneNumber', 'size' => 32),                
	            "NUM_CVV_CARTE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPhoneNumber', 'size' => 32),
	            "civilite_livraison" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isEmail', 'size' => 128),
	            "NOM_LIVRAISON" => array('type' => self::TYPE_DATE, 'typeXML' => 'string', 'validate' => 'isBirthDate'),
	            "PRENOM_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPostCode', 'size' => 12),
	            "ADRESSE1_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isLanguageIsoCode', 'size' => 2),
	            "ADRESSE2_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'lang' => true, 'validate' => 'isGenericName', 'size' => 64),
	            "CP_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isLanguageIsoCode', 'size' => 2),
	            "VILLE_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	            "CODE_PAYS" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	            "PAYS" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	            "CODE_RELAIS_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	            "MONTANT" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
	            "QUANTITE" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isPasswd', 'size' => 60),
	            "NOUVEAU_CLIENT" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	            "NUM_PANIER_ALTIMA" => array('type' => self::TYPE_STRING, 'typeXML' => 'int', 'validate' => 'isPasswd', 'size' => 60),
	            "CODE_LANGUE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	            "CODE_DEVISE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	        ),
	        'ajout_article' => array(
				"ARTICLE_CODE_ARTICLE" => array('type' => self::TYPE_STRING, 'typeXML' => 'long', 'validate' => 'isPasswd', 'size' => 60),
				"POINTURE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
				"QUANTITE" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isPasswd', 'size' => 60),
               	"PRIX_NORMAL" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
               	"PRIX_BARRE" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
               	"POS_AVANTAGE" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isPasswd', 'size' => 60), 
               	"REMISE_AVANTAGE" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
               	"CODE_CATALOGUE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60), 
               	"CADEAU" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
               	"CODE_DEVISE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
	        ),
        ),
        'ajout_commande_asynchro' => array(
            'ajout_commande' => array(
                'code_avantage' => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
                "code_concept" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
                "code_client" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isName', 'size' => 255),
                "REMISE_AVANTAGE" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isName', 'size' => 255),
                "type_reglement" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isAddress', 'size' => 128),
                "type_livraison" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isAddress', 'size' => 128),
                "frais_reglement" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPostCode', 'size' => 12),
                "frais_livraison" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isCityName', 'size' => 64),
                "numero_carte" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPhoneNumber', 'size' => 32),
                "date_valid_carte" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPhoneNumber', 'size' => 32),                
                "NUM_CVV_CARTE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPhoneNumber', 'size' => 32),
                "civilite_livraison" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isEmail', 'size' => 128),
                "NOM_LIVRAISON" => array('type' => self::TYPE_DATE, 'typeXML' => 'string', 'validate' => 'isBirthDate'),
                "PRENOM_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPostCode', 'size' => 12),
                "ADRESSE1_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isLanguageIsoCode', 'size' => 2),
                "ADRESSE2_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'lang' => true, 'validate' => 'isGenericName', 'size' => 64),
                "CP_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isLanguageIsoCode', 'size' => 2),
                "VILLE_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "CODE_PAYS" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "PAYS" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "CODE_RELAIS_LIVRAISON" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "MONTANT" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
                "QUANTITE" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isPasswd', 'size' => 60),
                "NOUVEAU_CLIENT" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "NUM_PANIER_ALTIMA" => array('type' => self::TYPE_STRING, 'typeXML' => 'int', 'validate' => 'isPasswd', 'size' => 60),
                "CODE_LANGUE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "CODE_DEVISE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
            ),
            'ajout_article' => array(
                "ARTICLE_CODE_ARTICLE" => array('type' => self::TYPE_STRING, 'typeXML' => 'long', 'validate' => 'isPasswd', 'size' => 60),
                "POINTURE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "QUANTITE" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isPasswd', 'size' => 60),
                "PRIX_NORMAL" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
                "PRIX_BARRE" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
                "POS_AVANTAGE" => array('type' => self::TYPE_STRING, 'typeXML' => 'short', 'validate' => 'isPasswd', 'size' => 60), 
                "REMISE_AVANTAGE" => array('type' => self::TYPE_STRING, 'typeXML' => 'double', 'validate' => 'isPasswd', 'size' => 60),
                "CODE_CATALOGUE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60), 
                "CADEAU" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
                "CODE_DEVISE" => array('type' => self::TYPE_STRING, 'typeXML' => 'string', 'validate' => 'isPasswd', 'size' => 60),
            ),
        ),
	);

	protected static $response_data = array( 
		'ajout_commande' => array(
			'code_client' => '',
            "civilite" => '',
            "nom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "prenom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "adresse1" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "adresse2" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "code_postal" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "ville" => array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'size' => 64),
            "tel_fixe" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "tel_portable" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "email" => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            "date_naissance" => array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
            "nb_enfants" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "code_pays" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "pays" => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 64),
            "code_langue" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "mot_de_passe" => array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'size' => 60)
        ), 
        'ajout_commande_asynchro' => array(
            'code_client' => '',
            "civilite" => '',
            "nom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "prenom" => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 255),
            "adresse1" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "adresse2" => array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            "code_postal" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "ville" => array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'size' => 64),
            "tel_fixe" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "tel_portable" => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            "email" => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            "date_naissance" => array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
            "nb_enfants" => array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            "code_pays" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "pays" => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 64),
            "code_langue" => array('type' => self::TYPE_STRING, 'validate' => 'isLanguageIsoCode', 'size' => 2),
            "mot_de_passe" => array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'size' => 60)
        ), 
	);

	protected $schema = '';
	protected $content;

	public function __construct($verbose = false)
    {
    	$this->setUrl(Configuration::get('INS_WEBSERVICE_URL_COMMANDE'));   
        // construction d'url du header depuis la config
        $url = Configuration::get('INS_WEBSERVICE_URL_COMMANDE');
        $url = str_replace('http://', '', $url);
        $url = str_replace('?wsdl', '', $url);
        $url = explode('/', $url);
        $header = '';
        if(count($url)) {
            $first = true;
            foreach ($url as $key => $value) {
                if($first) {
                    $this->host = $value;
                    $first = false;
                    continue;
                }
                $header .= '/'.$value;
            }
        }
        $this->header_url = $header;
    	parent::__construct($verbose);
    }

    /**
     * [parseOrder crée le contenu du XML depuis un objet Order]
     * @param  [type]  $order [description]
     * @param  integer $index [description]
     * @return [type]         [description]
     */
    public function parseOrder($order, $index = 1) {  
    	$customer = $order->getCustomer();
        $id_group = Customer::getDefaultGroupId((int)$customer->id);
    	//appel webservice pour récupérer le code_client
    	$webservice = new Insitaction_Webservice_Clients();
		$code_client = $webservice->getCodeClientByEmail($customer->email);
		//recupération des infos de la commande
    	$cartRules = $order->getCartRules();  
    	$transports = $order->getShipping();
    	$products = $order->getProducts();  
    	$payments = $order->getOrderPayments();
    	$delivery = new Address($order->id_address_delivery);
    	$country = new Country($delivery->id_country, Configuration::get('PS_LANG_DEFAULT')); 
    	$currency = new Currency($order->id_currency, Configuration::get('PS_LANG_DEFAULT'));
    	$langue = new Language($customer->id_lang, Configuration::get('PS_LANG_DEFAULT'));

        // Type_livraison : 1 pour domicile (colissimo), 3 pour relais
        // Par défaut Colissimo
    	$type_livraison = 1; 
        $cout_transports = 0;
        $point_relais = '';
        //TODO Multi shipping ??
        if($transports && is_array($transports) && !empty($transports)) {
            //pas possible de traiter du multishipping avec le webservice chaussexpo on prend le premier transporteur
            $carrier = current($transports);
            if(isset($carrier['id_carrier'])) {
                $carrierObj = new Carrier($carrier['id_carrier']);
                if($carrierObj && $carrierObj instanceof Carrier && !is_null($carrierObj->id)) {
                    // Mondial Relay
                    if(in_array($carrierObj->id_reference, array(4))) {
                        $type_livraison = 3;
                        //Recupération des infos point relais
                        $result = DB::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'mr_selected` WHERE id_order = '.$order->id);

                        if(isset($result['MR_Selected_Num']))
                            $point_relais = $result['MR_Selected_Num'];
                    }
                }
            }
            foreach ($transports as $key => $transport) {
                $cout_transports += $transport['shipping_cost_tax_incl'];
            }
        }
    	

    	$code_avantage = '';
    	$REMISE_AVANTAGE = 0;  
        $total_products = $order->getTotalProductsWithTaxes();
    	$gifts = array(); 

    	if(is_array($cartRules) && !empty($cartRules)) {
    		foreach ($cartRules as $key => $cartRule) {
    			$gift = array();
    			$code_avantage .= $cartRule['name'].',';
    			$REMISE_AVANTAGE += $cartRule['value'];
    			$cart_rule = new CartRule($cartRule['id_cart_rule']);
    			if(property_exists($cart_rule, 'gift_product'))
    				$gift['id_product'] = $cart_rule->gift_product;
    			if(property_exists($cart_rule, 'gift_product_attribute'))
    				$gift['id_product_attribute'] = $cart_rule->gift_product_attribute;
    			if(isset($gift['id_product'])) {  
    				$gifts[] = $gift;
    			}
    		}
    		$code_avantage = substr($code_avantage, 0, -1);
    	} 

        // Type_reglement : valeurs possibles : PP pour paypal, ou SK pour sherlock (carte bleue). 
        $type_reglement = 'SK';  
        if($payments && is_array($payments) && !empty($payments)) {
            $payment = array_shift($payments);
            if(stripos($payment->payment_method, 'paypal')  !== false) {
                $type_reglement = 'PP'; 
            } 
        }


    	$commande = array (
            'AJOUT_COMMANDE'.$index => array(
            'code_avantage' => $code_avantage,
            "code_concept" => 'ECO',
            "code_client" => $code_client,
            "REMISE_AVANTAGE" => $REMISE_AVANTAGE,
            "type_reglement" => $type_reglement, 
            "type_livraison" => $type_livraison, 
            "frais_reglement" => 0,
            "frais_livraison" => $cout_transports,
            "numero_carte" => '',
            "NUM_CVV_CARTE" => '',
            "civilite_livraison" => $customer->id_gender ? $customer->id_gender : 1,
            "NOM_LIVRAISON" => strtoupper($delivery->lastname),
            "PRENOM_LIVRAISON" => strtoupper($delivery->firstname),
            "ADRESSE1_LIVRAISON" => strtoupper($delivery->address1),
            "ADRESSE2_LIVRAISON" => strtoupper($delivery->address2),
            "CP_LIVRAISON" => $delivery->postcode,
            "VILLE_LIVRAISON" => strtoupper($delivery->city),
            "CODE_PAYS" => strtoupper($country->iso_code),
            "PAYS" => strtoupper($country->name),
            "CODE_RELAIS_LIVRAISON" => $point_relais,
            "MONTANT" => number_format($total_products, 2),
            "QUANTITE" => count($products) + count($gifts),
            "NOUVEAU_CLIENT" => ($code_client ? 'N' : 'O'),
            "NUM_PANIER_ALTIMA" => (int)$order->id_cart,
            "CODE_LANGUE" => strtoupper($langue->iso_code),
            "CODE_DEVISE" => strtoupper($currency->iso_code),
            )
        ); 

    	foreach ($products as $key => $product) { 
            $obj = new Product($product['product_id']); 
    		if($obj && $obj instanceof Product && !is_null($obj->id)) { 
	    		//test si article cadeau, 
	    		//parcours de tout les cadeaux potentiels
	    		$is_gift = 'N';
	    		foreach ($gifts as $key => $gift) {
	    			if(isset($gift['id_product']) && $gift['id_product_attribute']) {
			    		if($product['product_id'] == $gift['id_product'] 
			    			&& $product['product_attribute_id'] == $gift['id_product_attribute']) {
			    			$is_gift = 'Y'; 
			    			unset($gifts[$key]);
			    			break;
			    		}
			    	}
	    		}
                //recuperation du prix barré si produit non cadeau
                $PRIX_BARRE = $product['unit_price_tax_incl'];
                if($is_gift == 'N') {
                    $specific_price = SpecificPrice::getSpecificPrice(
                                        $product['product_id'], 
                                        $order->id_shop, 
                                        $currency->id, 
                                        $country->id, 
                                        $id_group, 
                                        $product['product_quantity'], 
                                        $product['product_attribute_id'], 
                                        $customer->id, 
                                        $order->id_cart  
                                ); 
                    if($specific_price && isset($specific_price['id_specific_price']) && !is_null($specific_price['id_specific_price'])) {
                        if(isset($specific_price['reduction']) && $specific_price['reduction'] > 0) {
                            if($specific_price['reduction_type'] == 'percentage')
                                $PRIX_BARRE = ($PRIX_BARRE / (1-$specific_price['reduction']));
                            elseif($specific_price['reduction_type'] == 'amount')
                                $PRIX_BARRE = $PRIX_BARRE + $specific_price['reduction'];

                        }
                    }  
                }

	    		//recuperation de la pointure
	    		$pointure = '';              
                $combinations = $obj->getAttributeCombinationsById($product['product_attribute_id'], Configuration::get('PS_LANG_DEFAULT'));
	    		foreach ($combinations as $key => $combination) {
	    			if($combination['id_attribute_group'] ==  self::$id_attribute_pointure) {
	    				$pointure = $combination['attribute_name'];
	    				break;
	    			}
	    		} 
                $pointure = self::getPointure4WS($pointure);
                
	    		$commande['AJOUT_ARTICLE'.$index] = array(
	    												"ARTICLE_CODE_ARTICLE" => $product['product_reference'],
														"POINTURE" => $pointure,
														"QUANTITE" => $product['product_quantity'],
											           	"PRIX_NORMAL" => number_format($product['unit_price_tax_incl'],2),
											           	"PRIX_BARRE" => number_format($PRIX_BARRE,2),
											           	"POS_AVANTAGE" => '0',
											           	"REMISE_AVANTAGE" => '0', //TODO
											           	"CODE_CATALOGUE" => '',
											           	"CADEAU" => $is_gift,
											           	"CODE_DEVISE" => strtoupper($currency->iso_code),
	    											);
	    		 
	    		$index++;
    		} else {
    			$this->log('Insitaction_Webservice_Commandes : Impossible de retrouver le produit#'.$product['product_id']);
    		}
    	}
    	return $commande;

    }

    protected function parseSchema($data) {    	
    	if(is_array($data)) {
	    	foreach ($data as $key => $value) {
				if(is_array($value) && !isset($value['typeXML'])) {
	    			$this->schema .= '<xs:element name="'.strtoupper($key).'">'."\n"
	    								.'<xs:complexType>'."\n"
											.'<xs:sequence>'."\n";
					$this->schema .= $this->parseSchema($value);
					$this->schema .= '</xs:sequence>'."\n"
								.'</xs:complexType>'."\n"
							.'</xs:element>'."\n";
				} elseif(is_array($value) && isset($value['typeXML'])) {
					$this->schema .= '<xs:element name="'.$key.'" type="xs:'.$value['typeXML'].'" minOccurs="0" />'."\n";
				}
			}
		} 

    }

    protected function parseXMLContent($parsedOrder, $index = 0) {
    	$actualKey = '';
    	if(is_array($parsedOrder)) {
	    	foreach ($parsedOrder as $key => $value) {
				if(is_array($value)) {
					//si on change de clé alors on passe de commande a article sinon on incremente 
	    			if($actualKey == strtoupper(substr($key, 0, -1))) {
	    				$index++;
	    			} else {
	    				$index = 0;
	    			}
	    			//on actualise la clé pour la comparer avec la prochaine
	    			$actualKey = strtoupper(substr($key, 0, -1));
	    			$this->content .= '<'.strtoupper(substr($key, 0, -1)).' diffgr:id="'.strtoupper($key).'" msdata:rowOrder="'.$index.'" diffgr:hasChanges="inserted">'."\n";
	    			
					$this->content .= $this->parseXMLContent($value, $index);
					$this->content .= '</'.strtoupper(substr($key, 0, -1)).'>'."\n";
				} else {
					if(strlen(trim($value)) == 0) {
						$this->content .= '<'.$key.' />'."\n";
					} else {
						$this->content .= '<'.$key.'>'.trim($value).'</'.$key.'>'."\n";
					}
				}
			}
		} 
    }

    /**
     * [getMSXML Construction de l'objet dataset .NET en xml]
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
    private function getMSXML($order) {
        //construction du fichier xml
        $this->content = $this->curl_header
                        .'<'.$this->method.' xmlns="'.$this->service_url.'">'
                            .'<dataSetCommandeEntree>';
        
        //construction du header du schema
        $this->schema = '<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">'."\n"
                            .'<xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">'."\n"
                                .'<xs:complexType>'."\n"
                                    .'<xs:choice minOccurs="0" maxOccurs="unbounded">'."\n";
                                        $this->parseSchema(self::$data[$this->method]);
                        $this->schema .= '</xs:choice>'."\n"
                                    .'</xs:complexType>'."\n"
                                .'</xs:element>'."\n"
                            .'</xs:schema>';    

        $this->content .= $this->schema."\n";
        $this->content .= '<diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">'."\n"
                                .'<NewDataSet xmlns="">'."\n";
                                    $this->parseXMLContent($this->parseOrder($order));
                        $this->content .= '</NewDataSet>'."\n"
                                        .'</diffgr:diffgram>'."\n"
                                    .'</dataSetCommandeEntree>'."\n"
                                .'</'.$this->method.'>'."\n"
                            .$this->curl_footer; 
    }

    public function ajoutCommande($order, $mode = 'sync') { 
    	// ATENTION ne pas envoyer des order->module = clickncollect
		if($mode == 'async')
			$this->method = 'ajout_commande_asynchro';
		elseif($mode == 'sync')
			$this->method = 'ajout_commande'; 

		if($this->method) {  
	    	//construction du fichier xml
	    	$this->getMSXML($order); 

            $headers = array(
                                'POST '.$this->header_url.' HTTP/1.1',
                                'Host: '.$this->host,
                                "Content-Type: text/xml; charset=utf-8",
                                "SOAPAction: \"".$this->service_url."/".$this->method."\"",
                                "Content-length: ".strlen($this->content),
                            );  

            try {
                
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $this->url.'?WSDL');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_COOKIESESSION, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
                curl_setopt($curl, CURLOPT_TIMEOUT, 10);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $this->content); // the SOAP request
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

                $this->response = curl_exec($curl);       

                $this->log('CONTENT WEBSERVICE COMMANDES : '.'WEBSERVICE COMMANDES : '.$this->content);
                $this->log('REPONSE WEBSERVICE COMMANDES : '.'WEBSERVICE COMMANDES : '.json_encode($this->response).' DATA : '.$this->content);

                if($this->response) { 
                    // enleve les éléments qui ne permettent pas de créer un objet dom
                    $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:', 'diffgr:', 'msdata:'], '', $this->response);
                    $xml = simplexml_load_string($clean_xml);  
                    $doc = new DOMDocument();  
                    $str = $xml->asXML(); 
                    $doc->loadXML($str); 
                    /*
                     WSVC_STATE                  
                        - code_retour = 0 si test connexion a dit que la base n'était pas disponible
                        - code_retour = -1 si bug quelconque lors de l'appel
                        - code_retour = 1 si tout ok
                     WSVC_COMMANDES
                     - code_retour = -1, -2, -3 si l'ajout n'a pu se faire correctement :
                       -1 : référence introuvable dans le stock
                       -2 : référence épuisée
                       -3 : pointure incorrecte ou référence inexistante
                     */
                    //parcours du dom a la recherche de code_retour
                    $bar_count = $doc->getElementsByTagName("code_retour");
                    $code_retour = array();
                    foreach ($bar_count as $node) 
                    {   
                        $code_retour[] = $node->nodeValue; 
                    }

                    //d'apres le schema de retour de la methode WSVC_STATE vient avant WSVC_COMMANDES
                    $WSVC_STATE = 0;
                    $WSVC_COMMANDES = 0;
                    if(isset($code_retour[0])) {
                        $WSVC_STATE = $code_retour[0];
                    }
                    if(isset($code_retour[1])) {
                        $WSVC_COMMANDES = $code_retour[1];
                    }

                    if($WSVC_STATE == 1) {
                        //l'appel s'est bien déroulé  
                        if($WSVC_COMMANDES != 1) {
                            if(in_array($WSVC_COMMANDES, array(-1, -2, -3)) ) {
                                //erreur relative à un article
                                $this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' WSVC_COMMANDES : code retour '.$WSVC_COMMANDES."\n"; 
                                $bar_count = $doc->getElementsByTagName("code_article");
                                $code_article = array();
                                foreach ($bar_count as $node) 
                                {   
                                    $code_article[] = $node->nodeValue; 
                                }

                                $this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' ERREUR ARTICLE WSVC_COMMANDES : code retour '.$WSVC_COMMANDES." CODE ARTICLE : ".implode(",", $code_article)."\n"; 

                            } else {
                                //erreur validation commande
                                $this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' ERREUR VALIDATION PANIER WSVC_COMMANDES : code retour '.$WSVC_COMMANDES."\n"; 
                            }

                            $this->logErrors(); 

                            // ERREUR : On ajoute la commande a liste des commandes a appeler en mode_degrade
                            // Désactivé car les commandes en erreur sont traités manuellement par chaussexpo -> obsolete
                            // $this->addCron($order);           

                            return array(
                                'WSVC_STATE' => $WSVC_STATE,
                                'WSVC_COMMANDES' => $WSVC_COMMANDES,
                                'code_article' => $code_article,
                                'log' => array(
                                    'fullWsCall' => $this->content,
                                    'fullWsResponse' => json_encode($this->response)
                                )
                            );
                        }
                        $this->log('WEBSERVICE COMMANDES : '.'WEBSERVICE COMMANDES : '.__FUNCTION__.' Commande#'.$order->id." envoyée avec succés\n");
                        return array(
                                'WSVC_STATE' => $WSVC_STATE,
                                'WSVC_COMMANDES' => $WSVC_COMMANDES,
                            );
                    } else {
                        $this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' WSVC_STATE : code retour '.$WSVC_STATE."\n"; 
                    } 
                }
            } catch (Exception $e) {
                 $this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' Exception reçue : '.$e->getMessage()."\n";     
            }           
	        
	    } else {
	    	$this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' La methode \'est pas definie.';		    	
	    } 
        $this->logErrors();        
	    return false;
    }
    
	/**
	 * [verifieCommande description]
	 * @param  [type]  $code_client     [description]
	 * @param  array   $params          [description]
	 * @param  boolean $get_params_keys [description]
	 * @return [type]                   [description]
	 * TODO
	 */
   /* public function verifieCommande($code_client, $params = array(), $get_params_keys = false) {
			$this->method = 'verifie_commande';
    		if($this->method) {    	
				if($get_params_keys) { 
	    			if(isset(self::$data[$this->method])) {			
	    				$keys = array();
	    				foreach (self::$data[$this->method] as $key => $value) {
	    					$keys[$key] = array_keys($value);
	    				}
	    				return $keys;    				
	    			}
	    			return $this->errors[] = 'self::$data['.$this->method.'] n\'est pas defini!';
		    	}
	       		try {
		            $this->response = $this->client->__soapCall(
		            	$this->method, 
		                array(
		                		'dataSetCommandeEntree' => 'test'
       						) 
					); 
			        print_r($this->response); 
					print_r($this->client->__getLastRequest());
					print_r($this->client->__getLastRequestHeaders());

					return $this->response;
		        } catch (Exception $e) {
		            $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
		        }
		    } else {
		    	$this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' La methode \'est pas definie.';		    	
		    } 
        $this->logErrors();      
	    return false;
    }*/

   /* public function listeCommandes($code_client, $params = array(), $get_params_keys = false) { 
		if ($this->client) {
    		$this->method = 'liste_commandes'; 
    		if($this->method) {    	
				if($get_params_keys) { 
	    			if(isset(self::$data[$this->method])) {			
	    				$keys = array();
	    				foreach (self::$data[$this->method] as $key => $value) {
	    					$keys[$key] = array_keys($value);
	    				}
	    				return $keys;    				
	    			}
	    			return $this->errors[] = 'self::$data['.$this->method.'] n\'est pas defini!';
		    	}
	       		try {
		            $this->response = $this->client->__soapCall(
		            	$this->method, 
		                array(
		                		'code_client' => $code_client
       						) 
					); 
			        print_r($this->response); 
					print_r($this->client->__getLastRequest());
					print_r($this->client->__getLastRequestHeaders());
					
					return $this->response;
		        } catch (Exception $e) {
		            $this->errors[] = 'Exception reçue : '.$e->getMessage()."\n";
		        }
		    } else {
		    	$this->errors[] = 'WEBSERVICE COMMANDES : '.__FUNCTION__.' La methode \'est pas definie.';		    	
		    }
	    }
        $this->logErrors();      
        return false;
    }*/

	
}