<?php

/*
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2015 PrestaShop SA

 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_Clients.php');
include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_Commandes.php');
include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_ClickNCollect.php');

class Ins_Webservice extends Module {

    private $templateFile;

    public function __construct() {
        $this->name = 'ins_webservice';
        $this->author = 'Insitaction';
        $this->version = '1.0.0';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Webservice', array(), 'Modules.Webservice.Admin');
        $this->description = $this->trans('Configuration du webservice', array(), 'Modules.Webservice.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.2.2', 'max' => '1.7.2.2');

        $this->templateFile = 'module:ins_webservice/ins_webservice.tpl';
    }

    public function install() {
        if (parent::install() 
                && $this->registerHook('actionPasswordRenew')
                && $this->registerHook('actionInsitactionBeforeAuthentication')
                && $this->registerHook('actionAuthentication')
                && $this->registerHook('actionCustomerAccountAdd') 
                && $this->registerHook('actionCustomerAccountUpdate') 
                && $this->registerHook('actionCustomerAddressSaveAfter') 
                && $this->registerHook('actionCustomerAddressDeleteAfter')
                && $this->registerHook('actionValidateOrder')
                && $this->registerHook('actionValidateClickNCollect')
                && $this->registerHook('moduleRoutes')
        ) {
            return true;
        }

        return false;
    }

    public function uninstall() {
        return parent::uninstall();
    }

    public function hookModuleRoutes() {
        //add class Insitaction_Webservice_Clients to autoload
        include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_Clients.php'); 
        //add class Insitaction_Webservice_Commandes to autoload
        include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_Commandes.php'); 
        include_once(dirname(__FILE__).'/classes/Insitaction_Webservice_ClickNCollect.php'); 
    }
    
    public function getContent() {
        $output = '';

        if (Tools::isSubmit('submitAccountWS')) {

            Configuration::updateValue('INS_WEBSERVICE_URL_CLIENT', Tools::getValue('INS_WEBSERVICE_URL_CLIENT', false));
            Configuration::updateValue('INS_WEBSERVICE_URL_COMMANDE', Tools::getValue('INS_WEBSERVICE_URL_COMMANDE', false));
            Configuration::updateValue('INS_WEBSERVICE_URL_RELAIS', Tools::getValue('INS_WEBSERVICE_URL_RELAIS', false));
            Configuration::updateValue('INS_WEBSERVICE_URL_STOCK', Tools::getValue('INS_WEBSERVICE_URL_STOCK', false));
            Configuration::updateValue('INS_WEBSERVICE_URL_RESERVATION_MAGASIN', Tools::getValue('INS_WEBSERVICE_URL_RESERVATION_MAGASIN', false));

            $output = '<div class="alert alert-success conf">'
                    . $this->trans('Paramètres mis à jour', array(), 'Admin.Notifications.Error')
                    . '</div>';

            $this->_clearCache($this->templateFile);
        }

        return $output . $this->renderForm();
    }

    protected function renderForm() {
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
        
        $fields_form = array(
            'legend' => array(
                'title' => $this->trans('Configuration link WS', array(), 'Modules.Webservice.Admin'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Client'),
                    'name' => 'INS_WEBSERVICE_URL_CLIENT'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Commande'),
                    'name' => 'INS_WEBSERVICE_URL_COMMANDE'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Relais'),
                    'name' => 'INS_WEBSERVICE_URL_RELAIS'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Stocks'),
                    'name' => 'INS_WEBSERVICE_URL_STOCK'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Réservations en magasin'),
                    'name' => 'INS_WEBSERVICE_URL_RESERVATION_MAGASIN'
                )
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            ),
            'buttons' => array(
                array(
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                    'title' => $this->trans('Back to list', array(), 'Admin.Actions'),
                    'icon' => 'process-icon-back'
                )
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'ins_webservice';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitAccountWS';

        $helper->fields_value = $this->getFormValues();

        return $helper->generateForm(array(array('form' => $fields_form)));
    }

    public function getFormValues() {
        
        $fields_value['INS_WEBSERVICE_URL_CLIENT'] = Configuration::get('INS_WEBSERVICE_URL_CLIENT');
        $fields_value['INS_WEBSERVICE_URL_COMMANDE'] = Configuration::get('INS_WEBSERVICE_URL_COMMANDE');
        $fields_value['INS_WEBSERVICE_URL_RELAIS'] = Configuration::get('INS_WEBSERVICE_URL_RELAIS');
        $fields_value['INS_WEBSERVICE_URL_STOCK'] = Configuration::get('INS_WEBSERVICE_URL_STOCK');
        $fields_value['INS_WEBSERVICE_URL_RESERVATION_MAGASIN'] = Configuration::get('INS_WEBSERVICE_URL_RESERVATION_MAGASIN');
        
        return $fields_value;
    }

    /**
     * Liste des hook où on appellera le WS
     */
    /**
     * [hookActionAuthenticationBefore Verification si le client existe en bdd
     * si il existe pas en local mais en distant le crée]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function hookActionInsitactionBeforeAuthentication($params) {

        $email = Tools::getValue('email');
        $password = Tools::getValue('password');

        $hashPassword = false;
        $same_password = true;
        $webservice = new Insitaction_Webservice_Clients(); 
        $response = $webservice->identification('email', array('email_client' => $email));
 
        if($response) { 
            if(isset($response['WSVC_CLIENTS']) 
                && $response['WSVC_CLIENTS'] 
                        && isset($response['WSVC_CLIENTS']['code_client']) 
                                && $response['WSVC_CLIENTS']['code_client']) {

                //client retrouvé en webservice
                //hash du password qui est fourni en clair
                try {
                    /** @var \PrestaShop\PrestaShop\Core\Crypto\Hashing $crypto */
                    $crypto = PrestaShop\PrestaShop\Adapter\ServiceLocator::get('\\PrestaShop\\PrestaShop\\Core\\Crypto\\Hashing');
                } catch (CoreException $e) {
                    return false;
                }
                    
                $hashPassword = $crypto->hash($password);     


                $customer = new Customer();
                $customer->getByEmail($email);
                if(Validate::isLoadedObject($customer)) {
                    // A chaque connexion on met a jour les infos clients
                    // Avec les infos du webservice
                    Insitaction_Webservice_Clients::updateCustomerFields($customer, $response);
                    // Le client existe en BDD on le laisse se logguer de manière classique
                    // mais on vérifie quand même que le mot de passe correspond à 
                    // celui du webservice si le client existe
                    if($hashPassword) {
                        if($hashPassword != $response['WSVC_CLIENTS']['mot_de_passe']) {
                            $webservice->log("Les mots de passes sont différents :\nSAISIE CLIENT PRESTASHOP : ".$hashPassword."\nMOT DE PASSE DISTANT : ".$response['WSVC_CLIENTS']['mot_de_passe']);
                        }
                        if($customer->passwd != $response['WSVC_CLIENTS']['mot_de_passe']) { 
                            //maj du mot de passe client avec celui du webservice
                            //priorité au mot de passe webservice
                            $customer->passwd = $response['WSVC_CLIENTS']['mot_de_passe'];
                        } 
                    }

                    $result = $customer->update();
                    
                    $message = 'AUTHENTICATION - Mise à jour du client#'.$customer->id.' Email : '.$customer->email.' dans la bdd locale ';
                    if($result) {
                        $webservice->log($message.'OK');
                    } else {
                        $webservice->log($message.'KO');
                        $webservice->log('Objet client qui ne s\'est pas mis a jour : '.json_encode($customer));
                    }
                    return $result;
                } else {
                    if($response) { 
                        if(isset($response['WSVC_CLIENTS']) 
                            && isset($response['WSVC_CLIENTS']['code_client'])
                                && $response['WSVC_CLIENTS']['code_client']) {

                            $webservice->log('Insertion client non existant en bdd '.print_r($response, true));
                            // client retrouvé il faut donc le créer 
                            $new_customer = new Customer();
                            Insitaction_Webservice_Clients::updateCustomerFields($new_customer, $response, true);
                            $new_customer->passwd = $response['WSVC_CLIENTS']['mot_de_passe'];
                            try {                        
                                if($new_customer->save()) {
                                    $webservice->log('AUTHENTICATION Creation OK');
                                } else {                        
                                    $webservice->log('AUTHENTICATION Creation KO');
                                }
                            } catch (Exception $e) {
                                $webservice->log('AUTHENTICATION Creation KO : Exception '.$e->getMessage());
                            }

                            if(!$hashPassword 
                                || $hashPassword != $response['WSVC_CLIENTS']['mot_de_passe']) {
                                // mot de passe différent on ne crée pas, on ne log pas
                                $webservice->log("Les mots de passe ne correspondent pas\nSAISIE CLIENT PRESTASHOP : ".$hashPassword."\nMOT DE PASSE DISTANT : ".$response['WSVC_CLIENTS']['mot_de_passe']);
                                return false;
                            }
                            return true;          
                        }
                    }
                }
            } else{        
                
                $webservice->log('Identification échouée : '.$email);
            }
        } else {            
            $webservice->log('Pas de reponse du WS CLIENTS : '.$email);
        }

        
        return false;
    }

    public function hookActionPasswordRenew($params) {
        $webservice = new Insitaction_Webservice_Clients();  
        if(isset($params['customer']) && Validate::isLoadedObject($params['customer'])) {
            // reinitialisation du mot de passe par prestashop
            $webservice->log('Changement de mot de passe (Prestashop) : '.json_encode($params['customer']));
            $response = $webservice->crud('update', $params['customer']);
            $webservice->log('Changement de mot de passe, REPONSE :'.print_r($response, true));
        } elseif(isset($params['email']) && Validate::isEmail($params['email'])) {
            //client non existant en local, on le crée en local si il existe dans la bdd distante
            $response = $webservice->identification('email', array('email_client' => $params['email']));
            if($response) { 
                if(isset($response['WSVC_CLIENTS']) 
                    && isset($response['WSVC_CLIENTS']['code_client'])
                        && $response['WSVC_CLIENTS']['code_client']) {

                    $webservice->log('Insertion client non existant en bdd '.print_r($response, true));
                    $new_customer = new Customer();
                    Insitaction_Webservice_Clients::updateCustomerFields($new_customer, $response, true);
                    $new_customer->passwd = $response['WSVC_CLIENTS']['mot_de_passe'];
                    try {                        
                        if($new_customer->save()) {
                            $webservice->log('PasswordRenew Creation OK : '.$params['email'].' ID#'.$new_customer->id);
                        } else {                        
                            $webservice->log('PasswordRenew Creation KO : '.$params['email']);
                        }
                    } catch (Exception $e) {
                        $webservice->log('PasswordRenew Creation KO : Exception '.$e->getMessage());
                    }
                }
            }
            
        }
    }

    /**
     * [hookActionAuthentication Si un client n'existe pas en distant le crée après le login]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function hookActionAuthentication($params) {
        if(isset($params['customer'])
            && Validate::isLoadedObject($params['customer'])) {
            $email = $params['customer']->email;
            // si le client n'existe pas de maniere distante le crée
            $webservice = new Insitaction_Webservice_Clients(); 
            $response = $webservice->identification('email', array('email_client' => $email));
            if(isset($response['WSVC_CLIENTS_STATE']) && $response['WSVC_CLIENTS_STATE'] != 1) {
                // le client n'existe pas de maniere distante on fait la demande de création
                if(Validate::isLoadedObject($params['customer'])) {
                    $response = $webservice->crud('add', $params['customer']);
                }                
            } 
        }
    }

    //ajout d'un nouveau compte PS
    public function hookActionCustomerAccountAdd($params) {
        if(isset($params['newCustomer'])) {
            $customer = $params['newCustomer'];
        } else {
            $context = Context::getContext();
            $customer = $context->customer;
        }
        $webservice = new Insitaction_Webservice_Clients(); 
        $response = $webservice->crud('add', $customer);
        //$response['code_retour'] = 1 OK       
    }

    //Modification d'un compte PS existant
    public function hookActionCustomerAccountUpdate($params) {
        return $this->updateClient($params);
    }

    //ajout ou modification d'une adresse
    public function hookActionCustomerAddressSaveAfter($params) {
        return $this->updateClient($params);
    }

    //Suppression d'une adresse
    public function hookActionCustomerAddressDeleteAfter($params) {
        return $this->updateClient($params);
    }

    private function updateClient($params) {
        $context = Context::getContext();
        $webservice = new Insitaction_Webservice_Clients();  
        $response = $webservice->crud('update', $context->customer);
        //$response['code_retour'] = 1 OK        
    }

    public function hookActionValidateOrder($params) {
        if(Validate::isLoadedObject($params['order'])) {
            // Verification du statut de la commande 
            // On va tester l'order state si le statut est à payer on peut faire la résa          
            $webservice = new Insitaction_Webservice_Commandes();
            if(Validate::isLoadedObject($params['orderStatus'])) {
                if($params['orderStatus']->paid == 1) {
                    // SEULEMENT ICI ON PEUT FAIRE LA DEMANDE DE RESA      
                    $reponse = $webservice->ajoutCommande($params['order']); 
                    // si après validation de la commande 
                    // le contact webservice ne s'effectue pas correctement, 
                    // nous faisons marche arrière, annulation
                    $keep_order = false;
                    
                    // test du retour WS
                    if($reponse) {
                        if(
                            isset($reponse['WSVC_STATE']) && $reponse['WSVC_STATE'] == 1 
                            && isset($reponse['WSVC_COMMANDES']) && $reponse['WSVC_COMMANDES'] == 1 
                        ) {
                            // Appel Valide on conserve la commande
                            $keep_order = true;
                        }
                    }

                    // retour WS KO
                    if(!$keep_order) {
                        // on passe la commande en statut annulé
                        // on ne passe plus la commande en annulé car sinon le client obtient une commande annulée dans son historique de commande
                        //$params['order']->setCurrentState(Configuration::get('PS_OS_CANCELED'));
                        // envoi mail a l'admin prestashop 
                        // pour envoyer des infos sur la commande a traiter manuellement
                        $this->sendAlertMail($webservice, $params, $reponse['log']['fullWsCall'], $reponse['log']['fullWsResponse']);
                    }
                } else {
                    $webservice->log('Demande de réservation Non Effectuée, le paiment n\'a pas été validé ORDER - '.print_r($params['order'], true));
                }
            }
        }
                  
    }

    /**
     * @desc methode permettant d'envoyer par mail au client le détail de l'appel WS en cas d'erreur
     * @param Insitaction_Webservice_Commandes
     * @param array paramètres d'appel du hook
     * @param string xml d'appel du WS
     * @param string xml de réponse du WS
     */
    private function sendAlertMail($webservice, $params, $sWsCall = '', $sWsResponse = '') {

        // définition des valeurs pour envoi de mail
        $configuration = Configuration::getMultiple(
            array(
                'PS_SHOP_EMAIL',
                'PS_MAIL_METHOD',
                'PS_MAIL_SERVER',
                'PS_MAIL_USER',
                'PS_MAIL_PASSWD',
                'PS_SHOP_NAME',
                'PS_MAIL_SMTP_ENCRYPTION',
                'PS_MAIL_SMTP_PORT',
                'PS_MAIL_TYPE'
            ),
            null,
            null,
            null
        );

        // Returns immediately if emails are deactivated
        if ($configuration['PS_MAIL_METHOD'] == 3) {
            return true;
        }

        if (!isset($configuration['PS_MAIL_SMTP_ENCRYPTION']) || Tools::strtolower($configuration['PS_MAIL_SMTP_ENCRYPTION']) === 'off') {
            $configuration['PS_MAIL_SMTP_ENCRYPTION'] = false;
        }
        if (!isset($configuration['PS_MAIL_SMTP_PORT'])) {
            $configuration['PS_MAIL_SMTP_PORT'] = 'default';
        }

        $from = $to = $configuration['PS_SHOP_EMAIL'];

        // sujet du mail envoyé
        $subject =  Context::getContext()->shop->getBaseURL(true) . ' : Commande ID#' . $params['order']->id . ' réponse WEBSERVICE KO';
        // contenu du mail envoyé
        $content =
            "Message à transmettre à la DSI.\n\n
            Une erreur a été détectée concernant la commande ID#" . $params['order']->id . " .\n\n
            Appel WEBSERVICE : \n\n" .
            print_r($sWsCall, true) . "\n\n\n
            Réponse WEBSERVICE : \n\n" .
            print_r($sWsResponse, true) . "\n\n\n
            Paramètres :\n\n" .
            print_r($params, true);
        
        try {
            if ($configuration['PS_MAIL_METHOD'] == 2) {
                if (Tools::strtolower($smtpEncryption) === 'off') {
                    $smtpEncryption = false;
                }
                $smtp = \Swift_SmtpTransport::newInstance($configuration['PS_MAIL_SERVER'], $configuration['PS_MAIL_SMTP_PORT'], $configuration['PS_MAIL_SMTP_ENCRYPTION'])
                    ->setUsername($configuration['PS_MAIL_USER'])
                    ->setPassword($configuration['PS_MAIL_PASSWD']);
                $swift = \Swift_Mailer::newInstance($smtp);
            } else {
                $swift = \Swift_Mailer::newInstance(\Swift_MailTransport::newInstance());
            }

            // instanciation du mail
            $message = \Swift_Message::newInstance();
            $to = array(
                $to => $to,
                'informatique@desmazieres.tm.fr' => 'informatique@desmazieres.tm.fr',
                'ecatinat@insitaction.com' => 'ecatinat@insitaction.com',
                'thomas.marino@desmazieres.tm.fr' => 'thomas.marino@desmazieres.tm.fr'
            );

            // prepration du mail
            $message
                ->setFrom($from)
                ->setTo($to)
                ->setSubject($subject)
                ->setBody($content);
            // envoi du mail
            if ($swift->send($message)) {
                $result = true;
            }
        } catch (\Swift_SwiftException $e) {
            $webservice->log('ERREUR sendAlertMail :'.$e->getMessage());
        }
    }

    public function hookActionValidateClickNCollect($params) {
        $webservice = new Insitaction_Webservice_ClickNCollect();
        $reponse = $webservice->reservation($params); 
    }

}