/* global $, prestashop */

/**
 * This module exposes an extension point in the form of the `showModal` function.
 *
 * If you want to override the way the modal window is displayed, simply define:
 *
 * prestashop.blockcart = prestashop.blockcart || {};
 * prestashop.blockcart.showModal = function myOwnShowModal (modalHTML) {
 *   // your own code
 *   // please not that it is your responsibility to handle closing the modal too
 * };
 *
 * Attention: your "override" JS needs to be included **before** this file.
 * The safest way to do so is to place your "override" inside the theme's main JS file.
 *
 */

$(document).ready(function () {
  prestashop.blockcart = prestashop.blockcart || {};

  var showModal = prestashop.blockcart.showModal || function (modal) {
    var $body = $('body');
    $body.append(modal);
    $body.one('click', '#blockcart-modal', function (event) {
      if (event.target.id === 'blockcart-modal') {
        $(event.target).remove();
      }
    });
  };

  $(document).ready(function () {
    prestashop.on(
      'updateCart',
      function (event) {
        var refreshURL = $('.blockcart').data('refresh-url');
        var requestData = {};

        if (event && event.reason) {
          requestData = {
            id_product_attribute: event.reason.idProductAttribute,
            id_product: event.reason.idProduct, 
            action: event.reason.linkAction, 
          };  
          if(typeof event.resp != 'undefined') { 
            if(typeof event.resp.show_store_modal != 'undefined') { 
              requestData['show_store_modal'] = event.resp.show_store_modal;
              requestData['back'] = window.location.href;
            }
          } 
        }

        $.post(refreshURL, requestData).then(function (resp) { 
          $('.blockcart').replaceWith($(resp.preview).find('.blockcart'));
          $('.blockcart__quickcart').replaceWith($(resp.preview).find('.blockcart__quickcart'));
          $('.cart-overview-quickcart').replaceWith($(resp.preview).find('.cart-overview-quickcart'));
          $('#cart-subtotal-products').replaceWith($(resp.preview).find('#cart-subtotal-products'));
          $('.blockcart').css('z-index', 5);
          if(resp.store_modal) { 
            showModal(resp.store_modal);
          } else {
            prestashop.emit('updateShoppingCart');
            // if (resp.modal) {
            //   showModal(resp.modal); // Ne pas afficher la modal panier
            // }
          }
        }).fail(function (resp) {
          prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
        });
      }
    );
  });
});
