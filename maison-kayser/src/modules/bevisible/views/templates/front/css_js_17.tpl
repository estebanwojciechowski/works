{**
 * 2018 Insitaction
 *
 *  @author    Estéban Wojciechowski
 *  @copyright 2018 Insitaction 
 *
 *} 

{bevisibleMinifyCss}
<style type="text/css">
    #bevisible-map {
        height: {$bevisible_css.map_height}px;
    }
    {$bevisible_css.custom nofilter}
</style>
{/bevisibleMinifyCss}

<script type="text/javascript">
    var BV = BV || { };
    BV.callback_url = '{$bevisible_js.callback_url nofilter}';
    BV.stores = {$bevisible_js.stores nofilter};
    BV.all_stores = {$bevisible_js.all_stores nofilter};
    BV.selected_id_store = {$bevisible_js.selected_id_store};
    BV.marker_url = '{$bevisible_js.marker_url nofilter}';
    BV.marker_type = '{$bevisible_js.marker_type}';
    BV.map_zoom = '{$bevisible_js.map_zoom}';
    BV.map_styles = {$bevisible_js.map_styles nofilter};
    BV.open_info_window = {$bevisible_js.open_info_window};
    BV.geocode_customer = {$bevisible_js.geocode_customer};
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,places&language={$bevisible_js.language_code_for_map}{if isset($bevisible_js.map_api_key) && $bevisible_js.map_api_key}&amp;key={$bevisible_js.map_api_key}{/if}"></script>