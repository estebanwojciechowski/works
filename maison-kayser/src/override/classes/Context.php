<?php
class Context extends ContextCore
{
    /**
     * [updateCustomer Surcharge qui met a jour le cookie de toutes les boutique lors d'une connexion
     * pour permettre une seule connexion]
     * @param  Customer $customer [description]
     * @return [type]             [description]
     */
    public function updateCustomer(Customer $customer)
    {
        parent::updateCustomer($customer);

        if($this->shop->id != 1) {
            //ADD BY ESTEBANW INSITACTION 30/04/2018
            //log le client dans la boutique principale
            //pour permettre la redirection auto vers
            //sa boutique préférée
            $main_shop = new Shop(1);
            $this->logToOtherShop($customer, $main_shop);
        }
    }

    /**
     * [logToOtherShop Fonction qui permet de logger 
     * en meme temps le client dans sa boutique préférée]
     * @param  [type] $customer  [description]
     * @param  [type] $dest_shop [description]
     * @return [type]            [description]
     */
    public function logToOtherShop($customer, $dest_shop = null) {        
        $force_ssl = Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE');
        $cookie_lifetime = (int)Configuration::get('PS_COOKIE_LIFETIME_FO');
        if ($cookie_lifetime > 0) {
            $cookie_lifetime = time() + (max($cookie_lifetime, 1) * 3600);
        }

        if(is_numeric($dest_shop))
            $dest_shop = new Shop($dest_shop);
        
        if(Validate::isLoadedObject($dest_shop)) {
            $domains = null;
            // tant que les domaines ne sont pas différents 
            // prestashop le garde à null pour instancier les cookie
            if ($dest_shop->domain != $dest_shop->domain_ssl) {
                $domains = array($dest_shop->domain_ssl, $shop->domain);
            }
            $cookie = new Cookie('ps-s'.$dest_shop->id, '', $cookie_lifetime, $domains, false, $force_ssl);

            $cookie_customer_id = $cookie->__get('id_customer');
            $cookie_customer_logged = $cookie->__get('logged');
            if($cookie_customer_id != $customer->id || !$cookie_customer_logged) {
                $cookie->__set('id_customer', (int) $customer->id);
                $cookie->__set('customer_lastname', $customer->lastname);
                $cookie->__set('customer_firstname', $customer->firstname);
                $cookie->__set('passwd', $customer->passwd);
                $cookie->__set('logged', 1);
                $cookie->__set('email', $customer->email);
                $cookie->write();
            }

        }
    }

}