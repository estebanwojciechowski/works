{** * 2007-2017 PrestaShop * * NOTICE OF LICENSE * * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.txt. * It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0 * If you did not receive a copy of the license and are unable to * obtain it through
the world-wide-web, please send an email * to license@prestashop.com so we can send you a copy immediately. * * DISCLAIMER
* * Do not edit or add to this file if you wish to upgrade PrestaShop to newer * versions in the future. If you wish to customize
PrestaShop for your * needs please refer to http://www.prestashop.com for more information. * * @author PrestaShop SA
<contact@prestashop.com>
  * @copyright 2007-2017 PrestaShop SA * @license https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
  * International Registered Trademark & Property of PrestaShop SA *} {extends file='checkout/_partials/steps/checkout-step.tpl'}
  {block name='step_content'}
  <div id="hook-display-before-carrier">
    {$hookDisplayBeforeCarrier nofilter}
  </div>

  <div class="delivery-options-list">
    {if $delivery_options|count}
    <form class="clearfix" id="js-delivery" data-url-update="{url entity='order' params=['ajax' => 1, 'action' => 'selectDeliveryOption']}"
      method="post">
      {if $active_store}
        <input type="hidden" name="store_hours" value="{json_encode(Store::parseHours($active_store->hours, true))}" />
        <input type="hidden" name="store_hours_clickandcollect" value="{json_encode(Store::parseHours($active_store->hours_clickandcollect, true))}" />
        <input type="hidden" name="store_special_days" value="{json_encode(StoreSpecialDay::getDates($active_store->id))}" />
        <input type="hidden" name="store_max_date" value="{StoreSpecialDay::getMaxDate()}" />
       {/if}
      <input type="hidden" name="velo_hours" value="{$carrier_velo_hours}" />
      <input type="hidden" name="carrier_velo" value="{$id_carrier_velo}" />
      <div class="form-fields">
        {block name='delivery_options'}
        <div class="delivery-options">
          {foreach from=$delivery_options item=carrier key=carrier_id}
            {if $carrier.is_free}
              <div class="delivery-option">
                <span class="custom-radio float-xs-left">
                  <input type="radio" name="delivery_option[{$id_address}]" id="delivery_option_{$carrier.id}" value="{$carrier_id}" {if $delivery_option==$carrier_id} checked{/if}>
                  <span></span>
                </span>
                <label id="delivery_option_label_{$carrier.id_reference}" for="delivery_option_{$carrier.id}" class="delivery-option-2">
                  {if $carrier.logo}
                  <div class="carrier-logo">
                    <img src="{$carrier.logo}" alt="{$carrier.name}" />
                  </div>
                  {/if}
                  <span class="label-livraison">
                    <span class="mode-livraison">
                      <span class="h6 carrier-name">{$carrier.name}</span>
                      <span class="carrier-delay">
                        {if isset($carrier.cart_date_delivery_format) && $carrier.cart_date_delivery_format}
                          {if $carrier.cart_date_delivery_format.type == 'minutes'}
                            {l s='dans' d='Shop.Theme.Global'}&nbsp;{$carrier.cart_date_delivery_format.value} {l s='minutes'  d='Shop.Theme.Global'}
                          {elseif $carrier.cart_date_delivery_format.type == 'day'}
                            {l s='à partir de' d='Shop.Theme.Global'}&nbsp;{$carrier.cart_date_delivery_format.value}
                          {else}
                            {l s='à partir du' d='Shop.Theme.Global'}&nbsp;{$carrier.cart_date_delivery_format.value}
                          {/if}
                        {else}
                          {$carrier.delay}
                        {/if}
                      </span>
                    </span>
                    <span class="carrier-price">{$carrier.price}</span>
                  </span>

                </label>
              </div>

              <div class="carrier-extra-content" {if $delivery_option !=$carrier_id} style="display:none;" {/if}>
                  {$carrier.extraContent nofilter}
                  <div class="order-options">
                    <div class="info-livraison">
                      <p class="adresse-boulangerie"><span>{l s='Boulangerie' d='Shop.Theme.Global'} {Store::getName()}</span> {Store::getAddress()} </p>

                    {if isset($carrier.datepicker) && $carrier.datepicker}
                     <p class="choix-date-heure">{l s='Choisissez votre date et heure de retrait' d='Shop.Theme.Global'} :</p>
                     <div class="date-heure">
                        <input type="hidden" name="clickandcollect_{$carrier.id}" value="1" />
                        <input type="hidden" name="start_date_{$carrier.id}" value="{$carrier.cart_date_delivery}" />
                        <div class="row">
                          <div class="col-sm-8">
                            <div id="datepicker_{$carrier.id}" class="datepicker-here" data-timepicker="true"></div>
                          </div>
                          <div class="col-sm-4">
                            {*<p id="rdv_{$carrier.id}">
                              {l s='Votre rendez-vous :' d='Shop.Theme.Global'} <span class="btn btn-outline-primary"></span>
                            </p> *}
                            <select name="timepicker_{$carrier.id}" class="timepicker"></select>
                          </div>
                        </div>
                        <input type="hidden" name="datepicker_{$carrier.id}" value="{if $delivery_option_date && $delivery_option == $carrier_id}{$delivery_option_date}{else}{$carrier.cart_date_delivery}{/if}" />

                        <input type="hidden" name="carrier_ref_{$carrier.id}" value="{$carrier.id_reference}" />

                      </div>
                    {/if}


                  </div>
                  <div id="delivery">
                    <label for="delivery_message">{l s='If you would like to add a comment about your order, please write it in the field below.' d='Shop.Theme.Checkout'}</label>
                    <textarea rows="5" cols="79" id="delivery_message" name="delivery_message[{$carrier.id}]">{$delivery_message}</textarea>
                  </div>

                  {if $recyclablePackAllowed}
                  <span class="custom-checkbox">
                    <input type="checkbox" id="input_recyclable" name="recyclable" value="1" {if $recyclable} checked {/if}>
                    <span>
                      <i class="material-icons checkbox-checked">&#xE5CA;</i>
                    </span>
                    <label for="input_recyclable">{l s='I would like to receive my order in recycled packaging.' d='Shop.Theme.Checkout'}</label>
                  </span>
                  {/if} {if $gift.allowed}
                  <span class="custom-checkbox">
                    <input class="js-gift-checkbox" id="input_gift" name="gift" type="checkbox" value="1" {if $gift.isGift}checked="checked"
                      {/if}>
                    <span>
                      <i class="material-icons checkbox-checked">&#xE5CA;</i>
                    </span>
                    <label for="input_gift">{$gift.label}</label>
                  </span>

                  <div id="gift" class="collapse{if $gift.isGift} in{/if}">
                    <label for="gift_message">{l s='If you\'d like, you can add a note to the gift:' d='Shop.Theme.Checkout'}</label>
                    <textarea rows="2" cols="120" id="gift_message" name="gift_message">{$gift.message}</textarea>
                  </div>
                  {/if}

                </div>
              </div>

            {else}
                <div class="delivery-option">
                <span class="custom-radio float-xs-left">
                  <input type="radio" name="delivery_option[{$id_address}]" id="delivery_option_{$carrier.id}" value="{$carrier_id}" {if $delivery_option==$carrier_id} checked{/if}>
                  <span></span>
                </span>
                <label id="delivery_option_label_{$carrier.id_reference}" for="delivery_option_{$carrier.id}" class="delivery-option-2">
                  {if $carrier.logo}
                  <div class="carrier-logo">
                    <img src="{$carrier.logo}" alt="{$carrier.name}" />
                  </div>
                  {/if}
                  <span class="label-livraison">
                    <span class="mode-livraison">
                      <span class="h6 carrier-name">{$carrier.name}</span>
                      <span class="carrier-delay">
                        {if isset($carrier.cart_date_delivery_format) && $carrier.cart_date_delivery_format}
                          {if $carrier.cart_date_delivery_format.type == 'minutes'}
                            {l s='dans' d='Shop.Theme.Global'}&nbsp;{$carrier.cart_date_delivery_format.value} minutes
                          {elseif $carrier.cart_date_delivery_format.type == 'day'}
                            {l s='à partir de' d='Shop.Theme.Global'}&nbsp;{$carrier.cart_date_delivery_format.value}
                          {else}
                            {if $carrier.id_reference == Configuration::get('COLISSIMO_ID_REFERENCE')}
                              {l s='Expédition' d='Shop.Theme.Global'}
                            {/if}
                            {l s='à partir du' d='Shop.Theme.Global'}&nbsp;{$carrier.cart_date_delivery_format.value}
                          {/if}
                        {else}
                          {$carrier.delay}
                        {/if}
                      </span>
                    </span>
                    <span class="carrier-price">{$carrier.price}</span>
                  </span>

                </label>
              </div>

              <div class="carrier-extra-content" {if $delivery_option !=$carrier_id} style="display:none;" {/if}>
                  <div class="order-options">
                    <div class="info-livraison">

                      {$carrier.extraContent nofilter}

                      {if isset($carrier.datepicker) && $carrier.datepicker}
                       <p class="choix-date-heure">{l s='Choisissez votre date et heure de livraison' d='Shop.Theme.Global'} :</p>
                       {*<p>{l s='Livraison possible à partir du :' d='Shop.Theme.Global'} {date('d/m/Y H:i', $carrier.cart_date_delivery)}</p>*}

                       {* La livraison peut avoir lieu a partir de la date : $carrier.cart_date_delivery *}
                        <div class="date-heure">
                          <input type="hidden" name="start_date_{$carrier.id}" value="{$carrier.cart_date_delivery}" />
                           <div class="row">
                            <div class="col-sm-8">
                               <div id="datepicker_{$carrier.id}" class="datepicker-here" data-timepicker="true"></div>
                            </div>
                            <div class="col-sm-4">
                              {*<p id="rdv_{$carrier.id}">
                                {l s='Votre rendez-vous :' d='Shop.Theme.Global'} <span class="btn btn-outline-primary"></span>
                              </p>*}
                              <select name="timepicker_{$carrier.id}" class="timepicker"></select>

                            </div>
                          </div>
                             <input type="hidden" name="datepicker_{$carrier.id}" value="{if $delivery_option_date && $delivery_option == $carrier_id}{$delivery_option_date}{else}{$carrier.cart_date_delivery}{/if}" />
                             <input type="hidden" name="carrier_ref_{$carrier.id}" value="{$carrier.id_reference}" />
                        </div>
                      {/if}
                  </div>
                <div id="delivery">
                  {*if $carrier.id_reference == Configuration::get('COLISSIMO_ID_REFERENCE')}
                    <p id="info_collisimo_delay">
                    {l s='Pas d\'expedition le samedi et dimanche' d='Shop.Theme.Checkout'}
                    <br/>
                    {l s='Pas d\'expedition sans gluten le lundi, vendredi, samedi et dimanche' d='Shop.Theme.Checkout'}
                    </p>
                  {/if*}
                  <label for="delivery_message">{l s='If you would like to add a comment about your order, please write it in the field below.' d='Shop.Theme.Checkout'}</label>
                  <textarea rows="5" cols="79" id="delivery_message" name="delivery_message[{$carrier.id}]">{$delivery_message}</textarea>
                </div>

                {if $recyclablePackAllowed}
                <span class="custom-checkbox">
                  <input type="checkbox" id="input_recyclable" name="recyclable" value="1" {if $recyclable} checked {/if}>
                  <span>
                    <i class="material-icons checkbox-checked">&#xE5CA;</i>
                  </span>
                  <label for="input_recyclable">{l s='I would like to receive my order in recycled packaging.' d='Shop.Theme.Checkout'}</label>
                </span>
                {/if} {if $gift.allowed}
                <span class="custom-checkbox">
                  <input class="js-gift-checkbox" id="input_gift" name="gift" type="checkbox" value="1" {if $gift.isGift}checked="checked"
                    {/if}>
                  <span>
                    <i class="material-icons checkbox-checked">&#xE5CA;</i>
                  </span>
                  <label for="input_gift">{$gift.label}</label>
                </span>

                <div id="gift" class="collapse{if $gift.isGift} in{/if}">
                  <label for="gift_message">{l s='If you\'d like, you can add a note to the gift:' d='Shop.Theme.Checkout'}</label>
                  <textarea rows="2" cols="120" id="gift_message" name="gift_message">{$gift.message}</textarea>
                </div>
                {/if}

              </div>
              </div>
              <div class="clearfix"></div>

            {/if}
          {/foreach}
        </div>
        {/block}

      </div>
      <button type="submit" class="continue btn ins-button-bg btn-primary float-xs-right" name="confirmDeliveryOption" value="1">
        {l s='Continue' d='Shop.Theme.Actions'}
      </button>
    </form>
    {else}
    <p class="alert alert-danger">{l s='Unfortunately, there are no carriers available for your delivery address.' d='Shop.Theme.Checkout'}</p>
    {/if}
  </div>

  <div id="hook-display-after-carrier">
    {$hookDisplayAfterCarrier nofilter}
  </div>

  <div id="extra_carrier"></div>
  {/block}