{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_header_container'}
  <div class="header-connexion parallax-window" data-parallax="scroll" data-speed="0.7" data-image-src="{$urls.base_url}{$smarty.const._THEME_DIR_}img/pain-connexion.jpg">&nbsp;</div>
{/block}

{block name='page_content'}

  <div class="formulaires container">
    <div class="row">

      <div class="col-xs-12 col-md-4 col-lg-4 offset-lg-1">
        <section id="content" class="connexion page-content card card-block">
          <section class="login-form">
            {widget name="ps_contactinfo" hook='displayLeftColumn'}
          </section>
        </section>
      </div>

      <div id="left-column" class="col-xs-12 col-md-6 col-lg-6">
        <section id="content1" class="connexion page-content card card-block">
          {* <section class="login-form"> *}
            {widget name="contactform"}
          {* </section> *}
        </section>
      </div>

    </div>
  </div>
{/block}